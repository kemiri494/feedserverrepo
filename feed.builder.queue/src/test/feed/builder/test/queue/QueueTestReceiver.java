package test.feed.builder.test.queue;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.CommandInterface;
import com.eqtrade.Console;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

public class QueueTestReceiver implements CommandInterface, Receiver {
	private SocketInterface socket;
	private Logger log = LoggerFactory.getLogger(getClass());

	public QueueTestReceiver() throws Exception {
		socket = SocketFactory.createSocket("feed.builder.queue.test", this);
		socket.start();
	}

	@Override
	public boolean runCommand(String cmd) {

		if (cmd.startsWith("sub")) {
			String[] sp = cmd.split(" ");
			// type|stock|board|price
			String[] keys = sp[1].split("\\|");
			socket.sendMessage("queue-receiver|sub|" + sp[1]);
		} else if (cmd.startsWith("unsub")) {
			String[] sp = cmd.split(" ");
			// type|stock|board|price
			String[] keys = sp[1].split("\\|");
			socket.sendMessage("queue-receiver|unsub|" + sp[1]);

		}

		return true;
	}

	@Override
	public void receive(ClientSocket socket, byte[] msg) {
		String smsg = new String(msg);
		log.info("receive " + smsg);

	}

	@Override
	public void connected(ClientSocket sock) {
		// TODO Auto-generated method stub

	}

	@Override
	public void disconnect(ClientSocket sock) {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		PropertyConfigurator.configure("log.properties");
		try {
			Console c = new Console(new QueueTestReceiver());
			c.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
