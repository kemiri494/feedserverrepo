package socket.feed.builder.builder;

import java.util.Hashtable;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import socket.feed.builder.socket.core.SubscriberSocketManager;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

import feed.builder.builder.FeedBuilder;
import feed.builder.consumer.FeedConsumer;
import feed.provider.core.MsgListener;

public class FeedBuilderSocket extends FeedBuilder implements Receiver {
	private Logger log = LoggerFactory.getLogger(getClass());
	private SocketInterface socketServer;
	// private Hashtable<ClientSocket, MsgListener> hclient = new
	// Hashtable<ClientSocket, MsgListener>();
	private Hashtable<ClientSocket, Hashtable<String, MsgListener>> hclient = new Hashtable<ClientSocket, Hashtable<String, MsgListener>>();

	public FeedBuilderSocket(FeedConsumer consumer) throws Exception {
		super(consumer, false);
		socketServer = SocketFactory.createSocket(
				(String) config.get("socket.server.config"), this);
	}

	@Override
	public void start() throws Exception {
		log.info("start");
		socketServer.start();

		if (tradeBuilder != null)
			tradeBuilder.connect();

		if (quoteBuilder != null)
			quoteBuilder.connect();

		if (otherBuilder != null)
			otherBuilder.connect();

		if (queueBuilder != null)
			queueBuilder.connect();

		log.info("started");
	}

	@Override
	public void receive(ClientSocket socket, byte[] msg) {
		try {
			String smsg = new String(msg);

			if (smsg.startsWith("subscribe")) {
				String[] sp = smsg.split(" ");
				String type = sp[1];
				int seqno = Integer.parseInt(sp[2]);
				SubscriberSocketManager listener = new SubscriberSocketManager(
						type, socket);
				log.info(smsg);
				if (type.equals("0")) {
					otherBuilder.subscribe(listener, "", "", seqno);
				} else if (type.equals("1")) {
					quoteBuilder.subscribe(listener, "", "", seqno);
				} else if (type.equals("2")) {
					tradeBuilder.subscribe(listener, "", "", seqno);
				}
				// hclient.put(socket, listener);
				Hashtable<String, MsgListener> hlist = hclient.get(socket);
				if (hlist == null) {
					hlist = new Hashtable<String, MsgListener>();
				}

				hlist.put(type, listener);
				hclient.put(socket, hlist);

			} else if (smsg.startsWith("unsubscribe")) {
				String[] sp = smsg.split(" ");
				String type = sp[1];
				int seqno = Integer.parseInt(sp[2]);
				MsgListener listener = hclient.get(socket).get(type);
				if (type.equals("0")) {
					otherBuilder.unsubscribe(listener);
				} else if (type.equals("1")) {
					quoteBuilder.unsubscribe(listener);
				} else if (type.equals("2")) {
					tradeBuilder.unsubscribe(listener);
				}
			} else if (smsg.startsWith("queue-receiver")) {
				// queue-receiver|sub/unsub|type|stock|board|price

				String[] sdata = smsg.split("\\|");
				SubscriberSocketManager listener = new SubscriberSocketManager(
						"queue-receiver", socket);

				log.info(smsg);
				if (sdata[1].equals("sub")) {
					queueBuilder.subscribe(listener, smsg);
				} else if (sdata[1].equals("unsub")) {
					queueBuilder.unsubscribe(listener, smsg);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connected(ClientSocket sock) {
		sock.validated(true);
	}

	@Override
	public void disconnect(ClientSocket sock) {
		Hashtable<String, MsgListener> listeners = hclient.get(sock);
		try {

			log.info("disconnect from client with listener " + listeners.size());
			Iterator<String> iters = listeners.keySet().iterator();
			while (iters.hasNext()) {
				String skey = iters.next();
				MsgListener listener = listeners.get(skey);
				if (skey.equals("0"))
					otherBuilder.unsubscribe(listener);
				else if (skey.equals("1"))
					quoteBuilder.unsubscribe(listener);
				else if (skey.equals("2"))
					tradeBuilder.unsubscribe(listener);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
