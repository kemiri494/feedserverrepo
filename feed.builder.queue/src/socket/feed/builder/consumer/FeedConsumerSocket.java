package socket.feed.builder.consumer;

import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import socket.feed.builder.socket.core.MsgManagerProcessor;

import com.eqtrade.ClientSocket;
import com.eqtrade.CommandInterface;
import com.eqtrade.Console;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;
import com.eqtrade.TimeCollector;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.MsgConsumer;
import feed.builder.core.MsgManager;
import feed.provider.core.FileConfig;

public class FeedConsumerSocket extends FeedConsumer implements Receiver,CommandInterface {
	private MsgManagerProcessor orderConsumer;
	private MsgManagerProcessor tradeConsumer;
	private MsgManagerProcessor otherConsumer;
	private SocketInterface socketConsumer;
	private Logger log = LoggerFactory.getLogger(getClass());
	private TimeCollector timerBench = new TimeCollector("receiver.consumer");
	private boolean isstart = false,stoppedbench = false;
	private long i = 0;

	public FeedConsumerSocket(String sfile) throws Exception {
		config = new FileConfig(sfile);
		socketConsumer = SocketFactory.createSocket(
				config.getProperty("socket.client.config"), this);
		boolean isconnected = socketConsumer.start();

		log.info("trying to connect to server consumer "
				+ socketConsumer.getIPAddress() + ":"
				+ socketConsumer.getPort() + " " + isconnected);

		orderConsumer = new MsgManagerProcessor(socketConsumer, "1");
		tradeConsumer = new MsgManagerProcessor(socketConsumer, "2");
		otherConsumer = new MsgManagerProcessor(socketConsumer, "0");
		Runtime.getRuntime().addShutdownHook(new Thread(){

			@Override
			public void run() {
				log.info("apps stop ");
				FeedConsumerSocket.this.stop();
				log.info("apps stop finished");

			}
			
		});
	}

	@Override
	public MsgManager getOrderConsumer() {
		// TODO Auto-generated method stub
		return orderConsumer;
	}

	@Override
	public MsgManager getOtherConsumer() {
		// TODO Auto-generated method stub
		return otherConsumer;
	}

	@Override
	public MsgManager getTradeConsumer() {
		// TODO Auto-generated method stub
		return tradeConsumer;
	}

	@Override
	public void start() throws Exception {
		orderConsumer.connect();
		otherConsumer.connect();
		tradeConsumer.connect();
	}

	@Override
	public void stop() {
		try {
			orderConsumer.exit();
			otherConsumer.exit();
			tradeConsumer.exit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	@Override
	public void connected(ClientSocket sock) {
		log.info("connected socket " + sock);
	}

	@Override
	public void disconnect(ClientSocket sock) {
		log.info("disconnect socket");
		this.stop();
	}

	@Override
	public void receive(ClientSocket sock, byte[] bt) {
		
		/*if(!isstart){
			timerBench.setStartTime(System.nanoTime(),0);
			isstart = true;
		}
		
		
		if(i <= 100000){
		timerBench.savedLatency(0, System.nanoTime(), msg);
		i++;
		}else if(!stoppedbench) {
			timerBench.setEndTime(System.nanoTime());
			stoppedbench = true;
		}*/
		String msg = new String(bt);
		
		
		String[] sp = msg.split("#");
		//log.info("receive "+msg);
		if (sp[0].equals("0")) {
			otherConsumer.addMsg(sp[1]);			
		} else if (sp[0].equals("1")) {
			orderConsumer.addMsg(sp[1]);
		} else if (sp[0].equals("2")) {
			tradeConsumer.addMsg(sp[1]);
		}
	}

	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure("log.properties");
			FeedConsumerSocket consumer = new FeedConsumerSocket("feed.builder.config");
			consumer.start();
			Console con = new Console(consumer);
			con.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean runCommand(String cmd) {
		if (cmd.toLowerCase().startsWith("data")) {
			String[] sp = cmd.split(" ");
			List o = null;
			long seqno = 0;

			if (sp[1].equals("1")){
				o = orderConsumer.getSnapShot(0);
				seqno = orderConsumer.getLastSeqno();
			}else if (sp[1].equals("2")){
				o = tradeConsumer.getSnapShot(0);
				seqno = tradeConsumer.getLastSeqno();
			}
			else if (sp[1].equals("0")){
				o = otherConsumer
						.getSnapShot( 0);
				seqno = otherConsumer.getLastSeqno();
			}

			if (o != null) {
				System.out.println("size of message "+o.size()+" last seqno "+seqno);
			}else {
				System.out.println("data is null "+o);
			}
			
			return true;
		}else if (cmd.toLowerCase().startsWith("exit")) {
			try {
				System.exit(0);
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.println("error : stopping services");
				log.error("error stopping service", ex);
			}
			return true;
		}
		return false;
	}

	public TimeCollector getTimerBench() {
		return timerBench;
	}
	
	

}
