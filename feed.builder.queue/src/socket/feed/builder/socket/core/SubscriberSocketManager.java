package socket.feed.builder.socket.core;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;

import feed.provider.core.MsgListener;

public class SubscriberSocketManager implements MsgListener, Comparable {
	private ClientSocket socket;
	private Logger log = LoggerFactory.getLogger(getClass());
	private String type;

	public SubscriberSocketManager(String type, ClientSocket socket) {
		super();
		this.type = type;
		this.socket = socket;
	}

	@Override
	public void close() throws RemoteException {
		// socket.close();
	}

	@Override
	public void newMessage(String msg) throws RemoteException {
		if (socket.isConnected()) {
			msg = type + "!" + msg;
			socket.sendMessage(msg.getBytes());
		} else
			throw new RemoteException("Connection closed");
	}

	public ClientSocket getSocket() {
		return socket;
	}

	public void setSocket(ClientSocket socket) {
		this.socket = socket;
	}

	@Override
	public int compareTo(Object o) {
		SubscriberSocketManager so = (SubscriberSocketManager) o;
		return so.getSocket().equals(getSocket()) ? 0 : -1;
	}

}
