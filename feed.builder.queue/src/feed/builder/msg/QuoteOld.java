package feed.builder.msg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

public class QuoteOld extends Message{
	private String ordertime;
	private String stock;
	private String board;
	private HashMap bid; // price --> Price
	private HashMap offer; // price --> Price
	private double last;
	private double lastlot;
	private double previous;
	private String stream;
	
	//new for frequency
	private HashMap order; //id --> Queue
	private String streamorder;
	
	public QuoteOld(){		
		bid = new HashMap(20,5);
		offer = new HashMap(20,5);
		
		order = new HashMap(10000,50);
	}
	
	private String orderToString(){
		StringBuffer sb = new StringBuffer(100);
		sb.append("ORDER[");
		Iterator it = order.keySet().iterator();
		while (it.hasNext()){
			Queue q = (Queue)order.get(it.next());
			sb.append(q.toString());
		}
		sb.append("]");
		return sb.toString();
	}
	
	private void setOrder(String s){
		order.clear();
		
        int nstart = s.indexOf("[") + 1;
        int nend = s.indexOf("]", nstart);
        String sbid = s.substring(nstart, nend);

        String[] orderdetail = sbid.split("\\>");
        for (int i=0; i<orderdetail.length;i++){
        	String[] brow = orderdetail[i].split("\\|");
        	if (brow.length>1){
		        	Queue q = new Queue(brow[0], Double.parseDouble(brow[1]), Double.parseDouble(brow[2]), Double.parseDouble(brow[3]),"");
		        	order.put(q.getId()	, q);
        	}
        }
	}
	
	public void pack(){
		stream = this.toString();
		streamorder = orderToString();
	}
	
	public void unpack(){
		setContent(stream);
		setOrder(streamorder);
	}
	
	public String getOrdertime(){
		return ordertime;
	}
	
	public void setOrdertime(String time){
		this.ordertime= time;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public HashMap getBid() {
		return bid;
	}
	
	public HashMap getOrder(){
		return order;
	}

	public void setBid(HashMap buy) {
		this.bid = buy;
	}

	public HashMap getOffer() {
		return offer;
	}
	
	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getLastlot() {
		return lastlot;
	}

	public void setLastlot(double lastlot) {
		this.lastlot = lastlot;
	}
	
	public double getPrevious() {
		return previous;
	}

	public void setPrevious(double prev) {
		this.previous = prev;
	}
	
	
	public void setContent(String data){
        int nstart = 0;
        int nend = 0;
        String sdata = "";
		String[] temp = data.split("\\<");
		String[] header = temp[0].split("\\|");
		super.setContent(header);
		ordertime = header[3].trim();
		stock = header[4].trim();
		board = header[5].trim();
		previous = Double.parseDouble(header[6]);
		bid.clear();
		offer.clear();
		try {
			sdata = temp[1];
	        nstart = sdata.indexOf("[") + 1;
	        nend = sdata.indexOf("]", nstart);
	        String sbid = sdata.substring(nstart, nend);
	        String[] sbiddetail = sbid.split("\\>");
	        for (int i=0; i<sbiddetail.length;i++){
	        	String[] bidrow = sbiddetail[i].split("\\|");
	        	Price p = new Price(Double.parseDouble(bidrow[0]), Double.parseDouble(bidrow[1]), Double.parseDouble(bidrow[2]));
	        	bid.put(new Double(p.getPrice()), p);
	        }
		} catch (Exception ex){}
        

		try {
	        nstart = sdata.indexOf("[", nend) + 1;
	        nend = sdata.indexOf("]", nstart);
	        String soffer = sdata.substring(nstart, nend);
	        String[] soffdetail = soffer.split("\\>");
	        for (int i=0; i<soffdetail.length;i++){
	        	String[] offrow = soffdetail[i].split("\\|");
	        	Price p = new Price(Double.parseDouble(offrow[0]), Double.parseDouble(offrow[1]), Double.parseDouble(offrow[2]));
	        	offer.put(new Double(p.getPrice()), p);
	        }
		} catch (Exception ex){}
        
		try {
	        nstart = sdata.indexOf("[", nend) + 1;
	        nend = sdata.indexOf("]", nstart);
	        String slast = sdata.substring(nstart, nend);
	    	String[] lastrow = slast.split("\\|");        
	    	last = Double.parseDouble(lastrow[0]);
	    	lastlot = Double.parseDouble(lastrow[1]);
		} catch (Exception ex){}
	}
	
	
    //OQ-ASII|ASII<B[13150|80>13100|10>13050|27>13000|308>12950|653>12900|426>12850|406>12800|90>12750|38>12700|95>12650|111>12600|273>12550|101>12500|70>12450|1>12400|31>12350|40>12300|118>12200|25>12100|50>12050|50>12000|20>11600|60>11250|100>]O[13200|452>13250|244>13300|383>13350|130>13400|447>13450|205>13500|483>13550|100>13600|93>13700|25>13750|12>13800|38>14000|9>]L[13150|2]	
	public String toString(){
		StringBuffer	result = new StringBuffer(100);
		result.append(super.toString()).append(delimiter);
		result.append(ordertime).append(delimiter);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);		
		result.append(format.format(previous)).append("<");		
		result.append("B[");

		ArrayList keys = new ArrayList();
		keys.addAll(getBid().keySet());
		Collections.sort(keys, Collections.reverseOrder());
		Iterator it = keys.iterator();
		while (it.hasNext()){
				Price p = (Price)getBid().get(it.next());
				if (p.getLot()>0) {
					result.append(format.format(p.getPrice())).append(delimiter);
					result.append(format.format(p.getLot())).append(delimiter);
					result.append(format.format(p.getFreq())).append(">");
				}
		} 
		
		result.append("]");
		result.append("O[");

		keys = new ArrayList();
		keys.addAll(getOffer().keySet());
		Collections.sort(keys);
		it = keys.iterator();
		while (it.hasNext()){
				Price p = (Price)getOffer().get(it.next());
				if (p.getLot()>0) {
					result.append(format.format(p.getPrice())).append(delimiter);
					result.append(format.format(p.getLot())).append(delimiter);
					result.append(format.format(p.getFreq())).append(">");
				}
		} 
		
		result.append("]");
		result.append("L[");
		result.append(format.format(last));
		result.append("|");
		result.append(format.format(lastlot));
		result.append("]");
		return result.toString();
	}

}
