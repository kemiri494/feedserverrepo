package feed.builder.msg;

public class Stock extends Message {
	private String code;
	private String name;
	private String status;
	private String stockType;
	private String sector;
	private double ipoprice;
	private double baseprice;
	private double listedshares;
	private double tradableshares;
	private double lotsize;
	private String remark;

	public Stock() {
	}

	public String toString() {
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(code);
		result.append(delimiter).append(name);
		result.append(delimiter).append(status);
		result.append(delimiter).append(stockType);
		result.append(delimiter).append(sector);
		result.append(delimiter).append(format.format(ipoprice));
		result.append(delimiter).append(format.format(baseprice));
		result.append(delimiter).append(format.format(listedshares));
		result.append(delimiter).append(format.format(tradableshares));
		result.append(delimiter).append(format.format(lotsize));
		result.append(delimiter).append(remark);
		return result.toString();
	}

	public void setContent(String[] data) {
		super.setContent(data);
		code = data[3].trim();
		name = data[4].trim();
		status = data[5].trim();
		stockType = data[6].trim();
		sector = data[7].trim();
		ipoprice = Double.parseDouble(data[8]);
		baseprice = Double.parseDouble(data[9]);
		listedshares = Double.parseDouble(data[10]);
		tradableshares = Double.parseDouble(data[11]);
		lotsize = Double.parseDouble(data[12]);
		remark = data[13].trim();

	}

	public void fromProtocol(String[] data) {
		super.fromProtocol(data);
		code = data[5].trim();
		name = data[6].trim();
		status = data[7].trim();
		stockType = data[8].trim();
		sector = data[9].trim();
		ipoprice = Double.parseDouble(data[10]);
		baseprice = Double.parseDouble(data[11]);
		listedshares = Double.parseDouble(data[12]);
		tradableshares = Double.parseDouble(data[13]);
		lotsize = Double.parseDouble(data[14]);
		remark = data[15].trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStockType() {
		return stockType;
	}

	public void setStockType(String stockType) {
		this.stockType = stockType;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public double getIpoprice() {
		return ipoprice;
	}

	public void setIpoprice(double ipoprice) {
		this.ipoprice = ipoprice;
	}

	public double getBaseprice() {
		return baseprice;
	}

	public void setBaseprice(double baseprice) {
		this.baseprice = baseprice;
	}

	public double getListedshares() {
		return listedshares;
	}

	public void setListedshares(double listedshares) {
		this.listedshares = listedshares;
	}

	public double getTradableshares() {
		return tradableshares;
	}

	public void setTradableshares(double tradableshares) {
		this.tradableshares = tradableshares;
	}

	public double getLotsize() {
		return lotsize;
	}

	public void setLotsize(double lotsize) {
		this.lotsize = lotsize;
	}

}
