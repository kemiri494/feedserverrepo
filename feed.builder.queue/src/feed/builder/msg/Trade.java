package feed.builder.msg;

public class Trade extends Message {
	private String tradetime;
	private String stock;
	private String board;
	private String tradeno;
	private double price;
	private double lot;
	private String buyer;
	private String buyertype;
	private String seller;
	private String sellertype;
	private double bestbid;
	private double bestbidlot;
	private double bestoffer;
	private double bestofferlot;
	private double previous;
	
	public String getTradetime() {
		return tradetime;
	}
	public void setTradetime(String time) {
		this.tradetime = time;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getTradeno() {
		return tradeno;
	}
	public void setTradeno(String tradeno) {
		this.tradeno = tradeno;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getLot() {
		return lot;
	}
	public void setLot(double lot) {
		this.lot = lot;
	}
	public String getBuyer() {
		return buyer;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public String getBuyertype() {
		return buyertype;
	}
	public void setBuyertype(String buyertype) {
		this.buyertype = buyertype;
	}
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public String getSellertype() {
		return sellertype;
	}
	public void setSellertype(String sellertype) {
		this.sellertype = sellertype;
	}
	public double getBestbid() {
		return bestbid;
	}
	public void setBestbid(double bestbid) {
		this.bestbid = bestbid;
	}
	public double getBestbidlot() {
		return bestbidlot;
	}
	public void setBestbidlot(double bestbidlot) {
		this.bestbidlot = bestbidlot;
	}
	public double getBestoffer() {
		return bestoffer;
	}
	public void setBestoffer(double bestoffer) {
		this.bestoffer = bestoffer;
	}
	public double getBestofferlot() {
		return bestofferlot;
	}
	public void setBestofferlot(double bestofferlot) {
		this.bestofferlot = bestofferlot;
	}
	public double getPrevious() {
		return previous;
	}
	public void setPrevious(double prev) {
		this.previous = prev;
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		tradetime = data[5].trim();
		stock = data[7].trim();
		board = data[8].trim();
		tradeno = data[9].trim();
		price = Double.parseDouble(data[10]);
		lot = Double.parseDouble(data[11]);
		buyer = data[12].trim();
		buyertype = data[13].trim();
		seller = data[14].trim();
		sellertype = data[15].trim();
		bestbid = Double.parseDouble(data[16]);
		bestbidlot = Double.parseDouble(data[17]);
		bestoffer = Double.parseDouble(data[18]);
		bestofferlot = Double.parseDouble(data[19]);
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		tradetime = data[3].trim();
		stock = data[4].trim();
		board = data[5].trim();
		price = Double.parseDouble(data[6]);
		lot = Double.parseDouble(data[7]);
		buyer = data[8].trim();
		buyertype = data[9].trim();
		seller = data[10].trim();
		sellertype = data[11].trim();
		bestbid = Double.parseDouble(data[12]);
		bestbidlot = Double.parseDouble(data[13]);
		bestoffer = Double.parseDouble(data[14]);
		bestofferlot = Double.parseDouble(data[15]);
		tradeno = data[16].trim();
		previous = Double.parseDouble(data[17]);
	}
	
	public String toString(){
		return super.toString()+delimiter+tradetime+delimiter+stock+delimiter+board+delimiter+format.format(price)+delimiter+format.format(lot)+delimiter+buyer+delimiter+buyertype
		+delimiter+seller+delimiter+sellertype+delimiter+format.format(bestbid)+delimiter+format.format(bestbidlot)+delimiter+format.format(bestoffer)+delimiter+format.format(bestofferlot)
		+ delimiter+tradeno+delimiter+format.format(previous);
	}

}
