package feed.builder.builder;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.LoggerFactory;

import com.db4o.query.Query;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.FileProcessListener;
import feed.builder.core.MsgBuilder;
import feed.builder.msg.Price;
import feed.builder.msg.Queue;
import feed.builder.msg.Quote;
import feed.builder.msg.StockSummary;
import feed.provider.data.FeedMsg;

public class QuoteBuilder extends MsgBuilder {
	protected boolean preopening = true;
	private HashMap quoteMap;
	protected FeedMsg msg2;

	// private FeedMsg msg3;
	// private HashMap orderMap;

	public QuoteBuilder(FeedConsumer consumer, FeedBuilder builder, int port)
			throws Exception {
		super("", "1", consumer, builder, port);
		initFileProcess();
	}

	public QuoteBuilder(FeedConsumer consumer, String type,
			FeedBuilder builder, int port) throws Exception {
		super("", type, consumer, builder, port);
		initFileProcess();
	}
	
	public QuoteBuilder(FeedConsumer consumer, FeedBuilder feedBuilder)
			throws Exception {
		super("", "1", consumer, feedBuilder);
		initFileProcess();
	}

	protected void initFileProcess() {
		try {
			FileProcessListener list = new FileProcessListener("data/quote-"
					+ getClass());
			subscribe(list, "", "", (int) 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void exit() throws Exception {
		// consumer.getTradeConsumer().unsubscribe(this);
		consumer.getOrderConsumer().unsubscribe(this);
		this.close();
	}

	public boolean connect() throws Exception {
		// if (preopening) consumer.getTradeConsumer().subscribe(this, "",
		// "",(int) msg.getSeqno());
		log.info("connecting....");
		consumer.getOrderConsumer().subscribe(this, "", "",
				(int) msg2.getSeqno());
		log.info("connected");

		return true;
	}

	public List getSnapShot(int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno))
				.greater();
		List o = query.execute();
		log.info("request snapshot for type Quote with seqno: " + seqno
				+ " and result: " + (o != null ? o.size() + "" : "0")
				+ " record(s)");
		return o;
	}

	public String getSnapShot(String stock, String board) {

		String q = ((Quote) quoteMap.get(stock + "#" + board)).toString();
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("stock").constrain(stock)
				.and(query.descend("board").constrain(board));
		List l = query.execute();
		return q + "\n\n\n" + query.execute().get(0).toString() + "\n\n\n"
				+ l.size();
	}

	protected void loadConfig() {
		log = LoggerFactory.getLogger(getClass());
		super.loadConfig();
		preopening = true;
		quoteMap = new HashMap(500, 10F);
		// orderMap = new HashMap(1000, 50);
		msg2 = new FeedMsg();
		msg2.setType("SETTING2");
		List o = db.getDb().getInstance().queryByExample(msg2);
		if (o.size() > 0) {
			msg2 = (FeedMsg) o.get(0);
			msg2.setSeqno(msg2.getMsg().equals(formatDate.format(new Date())) ? msg2
					.getSeqno() : 0L);
			msg2.setMsg(formatDate.format(new Date()));
		} else {
			msg2.setMsg(formatDate.format(new Date()));
			msg2.setSeqno(0L);
		}
		// msg3 = new FeedMsg();
		// msg3.setType("PREOPENING");
		// List p = db.getDb().getInstance().queryByExample(msg3);
		// if (p.size() > 0) {
		// msg3 = (FeedMsg) p.get(0);
		// msg3
		// .setSeqno(msg3.getMsg().equals(
		// formatDate.format(new Date())) ? msg3.getSeqno()
		// : 1L);
		// msg3.setMsg(formatDate.format(new Date()));
		// } else {
		// msg3.setMsg(formatDate.format(new Date()));
		// msg3.setSeqno(1L);
		// }
		// preopening = msg3.getSeqno() == 1L;
		Query query = db.getDb().getInstance().query();
		query.constrain(feed.builder.msg.Quote.class);
		query.descend("stock").orderAscending();
		List t = query.execute();
		for (int i = 0; i < t.size(); i++) {
			Quote q = (Quote) t.get(i);
			q.unpack();
			quoteMap.put(q.getStock() + "#" + q.getBoard(), q);
		}
		// query = db.getDb().getInstance().query();
		// query.constrain(feed.builder.msg.Queue.class);
		// t = query.execute();
		// for (int i = 0; i < t.size(); i++) {
		// Queue q = (Queue) t.get(i);
		// orderMap.put(q.getId(), q);
		// }
	}

	public void processOrder(int idx, String[] data) {
		String key = data[7].trim() + "#" + data[8].trim();
		String command = data[6].trim();
		String orderid = data[14].trim();
		String time = data[5].trim();// +"#"+new Date().getTime();
		double price = Double.parseDouble(data[10]);
		double bal = Double.parseDouble(data[12]) / consumer.getLot();
		double match = Double.parseDouble(data[11]) / consumer.getLot() - bal;
		String oldorderid = data[19].trim();
		Quote quote = (Quote) quoteMap.get(key);

		double timeorder = Double.parseDouble(data[5]);

		/*
		 * datafeed old 12 november 2012 if (timeorder < 90000) match = 0;
		 */
		// new datafeed with new time feed
		if (timeorder < 90000 || (timeorder >= 155000 && timeorder < 160500))
			match = 0;
		// end of change

		if (quote == null) {
			quote = new Quote();
			quote.setStock(data[7].trim());
			quote.setBoard(data[8].trim());
			StockSummary ss = builder.getSS(quote.getStock() + "#"
					+ quote.getBoard());
			quote.setPrevious(ss != null ? ss.getPrev() : 0.0D);
		}
		quote.setOrdertime(data[5].trim() + "#" + new Date().getTime());
		// quote.setOrdertime(data[5].trim());
		quote.fromProtocol(data);

		// log.info("data quote "
		// +price+" "+bal+" "+match+" "+orderid+" "+quote.getSeqno()+" "+command+" "+timeorder+" "+quote.getType());

		Queue test = (Queue) quote.getWithdraw().get(oldorderid);
		if (Double.parseDouble(oldorderid) > 0 && test == null) {
			quote.getTemp().put(oldorderid, data);
			// log.info("old order not null "+oldorderid+" "+orderid);

			return;
		} else {
			quote.getTemp().remove(oldorderid);
		}

		if (command.equals("0")) {
			// log.info("command 0");
			if (bal != 0.0D) {
				Queue q = new Queue(orderid, price, bal, time);
				Price p = (Price) quote.getBid().get(new Double(price));

				if (p == null) {
					p = new Price(price, bal, 1.0D);
					Vector v = new Vector(100, 1);
					v.addElement(q);
					quote.getBidOrder().put(new Double(price), v);
				} else {

					p.setLot(p.getLot() + bal);
					Vector v = (Vector) quote.getBidOrder().get(
							new Double(price));
					if (v == null) {
						v = new Vector(100, 5);
						quote.getBidOrder().put(new Double(price), v);
					}
					// v.addElement(q);
					if (idx >= 0)
						v.add(idx, q);
					else
						v.addElement(q);

					p.setFreq(v.size());
				}
				if (p.getLot() == 0.0D) {
					quote.getBid().remove(new Double(price));
					quote.getBidOrder().remove(new Double(price));
				} else {
					quote.getBid().put(new Double(price), p);
				}
			}
			if (match != 0.0D) {

				Queue q = (Queue) quote.getWithdraw().get(oldorderid);
				match = match - (q != null ? q.getMatch() : 0);
				// log.info("processOrder:" + key + ":" + ":" + oldorderid
				// + ":match:" + match + ":bal:" + bal + ":" + data[11]
				// + ":" + data[12]);
				// log.info("match----------:" + oldorderid + ":" + match + ":"
				// + data[14] + ":" + (q != null ? q.getMatch() : 0));
				// log.info("match:" + key + ":" + oldorderid + ":" + match);
				if (match <= 0)
					return;
				ArrayList keys = new ArrayList();
				keys.addAll(quote.getOffer().keySet());
				Collections.sort(keys);
				Object k;
				for (Iterator it = keys.iterator(); it.hasNext(); quote
						.getOffOrder().remove(k)) {
					k = it.next();
					Price p = (Price) quote.getOffer().get(k);
					// log.info("price offer:" + k + ":" + p.getLot());
					if (p.getLot() > match) {
						p.setLot(p.getLot() - match);
						quote.setLast(p.getPrice());
						quote.setLastlot(match);
						Vector v = (Vector) quote.getOffOrder().get(k);
						int c = 0;
						int rowcount = v.size();
						double matchorder = new Double(match).doubleValue();
						for (int i = 0; i < rowcount; i++) {
							Queue u = (Queue) v.elementAt(0);
							// log.info("queue offer:" + u.getPrice() + ":"
							// + u.getLot() + ":" + matchorder);

							if (quote.getSeqno() == 66443) {
								log.info("quote builder lot " + u.getLot()
										+ " " + matchorder + " " + v.size()
										+ " priceof " + k + " " + u.getId()
										+ ":" + quote.getSeqno());
							}

							if (u.getLot() > matchorder) {

								u.setLot(u.getLot() - matchorder);
								break;
							}
							if (u.getLot() == matchorder) {
								v.remove(c);
								break;
							}

							matchorder -= u.getLot();
							v.remove(c);
						}

						p.setFreq(v.size());
						quote.getOffer().put(k, p);
						break;
					}
					if (p.getLot() == match) {
						quote.setLast(p.getPrice());
						quote.setLastlot(match);
						quote.getOffer().remove(k);
						quote.getOffOrder().remove(k);
						break;
					}
					quote.setLast(p.getPrice());
					quote.setLastlot(p.getLot());
					match -= p.getLot();
					quote.getOffer().remove(k);
				}
				// String offOrder = quote.toStringOfferOrder();
				// broadcast(offOrder);

			}
			quote.pack();
			quoteMap.put(key, quote);
			addSnapShot(quote);
			String quoteString = quote.toString();
			broadcast(quoteString);
			// String bidOrder = quote.toStringBidOrder();
			/*
			 * if (key.contains("RAJA")) { log.info("quote:" + quoteString);
			 * log.info("bidorder:" + bidOrder); }
			 */
			// broadcast(bidOrder);
			// log.info("bidorder finished " + bidOrder);
			// String[] sd = quoteString.split("\\|");
			// log.info("quote bid finished " + data[0] + "|" + data[1] + "|"
			// + data[2] + "|" + data[3] + "|" + data[4] + "|" + data[5]
			// + "|" + data[6] + " size of quote " + " size of queue bid ");
		} else if (command.equals("1")) {
			if (bal != 0.0D) {
				Price p = (Price) quote.getOffer().get(new Double(price));
				Queue q = new Queue(orderid, price, bal, time);
				if (p == null) {
					p = new Price(price, bal, 1.0D);
					Vector v = new Vector(100, 1);
					v.addElement(q);

					quote.getOffOrder().put(new Double(price), v);
				} else {
					p.setLot(p.getLot() + bal);
					Vector v = (Vector) quote.getOffOrder().get(
							new Double(price));
					if (v == null) {
						v = new Vector(100, 5);
						quote.getOffOrder().put(new Double(price), v);
					}
					// v.addElement(q);
					if (idx >= 0)
						v.add(idx, q);
					else
						v.addElement(q);

					p.setFreq(v.size());
				}
				if (p.getLot() == 0.0D) {
					quote.getOffer().remove(new Double(price));
					quote.getOffOrder().remove(new Double(price));
				} else {
					quote.getOffer().put(new Double(price), p);
				}
			}
			if (match != 0.0D) {
				Queue q = (Queue) quote.getWithdraw().get(oldorderid);
				match = match - (q != null ? q.getMatch() : 0);
				if (match <= 0) {
					// log.info("old order in match not null "+oldorderid+" "+orderid+" "+match);

					return;
				}
				ArrayList keys = new ArrayList();
				keys.addAll(quote.getBid().keySet());
				Collections.sort(keys, Collections.reverseOrder());
				Object k;
				for (Iterator it = keys.iterator(); it.hasNext(); quote
						.getBidOrder().remove(k)) {
					k = it.next();
					Price p = (Price) quote.getBid().get(k);
					if (p.getLot() > match) {
						p.setLot(p.getLot() - match);
						quote.setLast(p.getPrice());
						quote.setLastlot(match);
						Vector v = (Vector) quote.getBidOrder().get(k);
						int c = 0;
						int rowcount = v.size();
						double matchorder = new Double(match).doubleValue();
						for (int i = 0; i < rowcount; i++) {
							Queue u = (Queue) v.elementAt(0);
							if (u.getLot() > matchorder) {
								u.setLot(u.getLot() - matchorder);
								break;
							}
							if (u.getLot() == matchorder) {
								v.remove(c);
								break;
							}
							matchorder -= u.getLot();
							v.remove(c);
						}

						p.setFreq(v.size());
						quote.getBid().put(k, p);
						break;
					}
					if (p.getLot() == match) {
						quote.setLast(p.getPrice());
						quote.setLastlot(match);
						quote.getBid().remove(k);
						quote.getBidOrder().remove(k);
						break;
					}
					quote.setLast(p.getPrice());
					quote.setLastlot(p.getLot());
					match -= p.getLot();
					quote.getBid().remove(k);
				}

				// String bidOrder = quote.toStringBidOrder();

				// broadcast(bidOrder);

			}
			quote.pack();
			quoteMap.put(key, quote);
			addSnapShot(quote);

			String quoteOffer = quote.toString();
			broadcast(quoteOffer);
			// String offOrder = quote.toStringOfferOrder();

			// broadcast(offOrder);
			// String[] sd = quoteOffer.split("\\|");

			// log.info("quote offer finished " + data[0] + "|" + data[1] + "|"
			// + data[2] + "|" + data[3] + "|" + data[4] + "|" + data[5]
			// + "|" + data[6] + " size of quote " + " size of queue bid ");

		} else if (command.equals("2")) {

			Queue q = new Queue(orderid, price, bal, match, time);
			Queue q2 = new Queue(orderid, price, bal, match, time);
			quote.getWithdraw().put(q2.getId(), q2);
			Double price2 = new Double(price);
			Vector v = (Vector) quote.getBidOrder().get(price2);
			idx = -1;
			if (v != null) {
				idx = v.indexOf(q);
				boolean isremove = v.remove(q);
			} else {
				v = new Vector(1);

			}
			Price p = (Price) quote.getBid().get(new Double(price));
			if (p == null) {
				p = new Price(price, bal * -1D, -1D);
			} else {
				p.setLot(p.getLot() - bal);
				p.setFreq(v.size());
			}
			if (p.getLot() == 0.0D) {
				quote.getBid().remove(new Double(price));
				quote.getBidOrder().remove(new Double(price));
				// broadcast(quote.removeOrder("12", price));
			} else {
				quote.getBid().put(new Double(price), p);
			}
			quote.pack();
			quoteMap.put(key, quote);
			addSnapShot(quote);
			String quoteWd = quote.toString();
			broadcast(quoteWd);
			// String bidOrder = quote.toStringBidOrder();

			// broadcast(bidOrder);
			String[] str = (String[]) quote.getTemp().get(orderid);
			if (str != null) {

				price = Double.parseDouble(str[10]);

				// log.info("process order with "+orderid+" "+str[14].trim());
				processOrder(price == q.getPrice() ? idx : -1, str);
			}

			// String[] sd = quoteWd.split("\\|");

			// log.info("quote bwd finished " + data[0] + "|" + data[1] + "|"
			// + data[2] + "|" + data[3] + "|" + data[4] + "|" + data[5]
			// + "|" + data[6] + " size of quote " + " size of queue bid ");

		} else if (command.equals("3") && bal != 0.0D) {
			Queue q = new Queue(orderid, price, bal, match, time);
			Queue q2 = new Queue(orderid, price, bal, match, time);
			quote.getWithdraw().put(q2.getId(), q2);
			Vector v = (Vector) quote.getOffOrder().get(new Double(price));
			idx = -1;
			if (v != null) {
				idx = v.indexOf(q);
				v.remove(q);
			} else
				v = new Vector(1);
			Price p = (Price) quote.getOffer().get(new Double(price));
			if (p == null) {
				p = new Price(price, bal * -1D, -1D);
			} else {
				p.setLot(p.getLot() - bal);
				p.setFreq(v.size());
			}
			if (p.getLot() == 0.0D) {
				quote.getOffer().remove(new Double(price));
				quote.getOffOrder().remove(new Double(price));
				// broadcast(quote.removeOrder("13", price));
			} else {
				quote.getOffer().put(new Double(price), p);
			}
			quote.pack();
			quoteMap.put(key, quote);
			addSnapShot(quote);
			String quoteOwd = quote.toString();
			broadcast(quoteOwd);
			// String offOrder = quote.toStringOfferOrder();

			// broadcast(offOrder);

			String[] str = (String[]) quote.getTemp().get(orderid);
			if (str != null) {
				price = Double.parseDouble(str[10]);

				processOrder(q.getPrice() == price ? idx : -1, str);
			}

			// String[] sd = quoteOwd.split("\\|");
			// log.info("quote owd finished " + data[0] + "|" + data[1] + "|"
			// + data[2] + "|" + data[3] + "|" + data[4] + "|" + data[5]
			// + "|" + data[6] + " size of quote " + " size of queue bid ");
		}
	}

	/*
	 * public static String arrayToString2(String[] a, String separator) {
	 * StringBuffer result = new StringBuffer(); if (a.length > 0) {
	 * result.append(a[0]); for (int i = 1; i < a.length; i++) {
	 * result.append(separator); result.append(a[i]); } } return
	 * result.toString(); }
	 */

	protected void processPreopening(String[] data) {
		// double time = Double.parseDouble(data[5]);
		// if (time >= 93000D) {
		// preopening = false;
		// msg3.setSeqno(0L);
		// db.putMsg(msg3);
		// try {
		// consumer.getTradeConsumer().unsubscribe(this);
		// } catch (Exception exception) {
		// }
		// return;
		// }
		String key = data[7].trim() + "#" + data[8].trim(); // keys :
															// stock+board
		String command = data[6].trim();
		double price = Double.parseDouble(data[10]);
		double lotbid = Double.parseDouble(data[11]) / consumer.getLot();
		double lotoff = Double.parseDouble(data[11]) / consumer.getLot();
		if (command.equals("0")) {
			Quote quote = (Quote) quoteMap.get(key);
			if (quote == null) {
				quote = new Quote();
				quote.setStock(data[7].trim());
				quote.setBoard(data[8].trim());
				StockSummary ss = builder.getSS(quote.getStock() + "#"
						+ quote.getBoard());
				quote.setPrevious(ss == null ? 0 : ss.getPrev());
			}
			quote.setOrdertime(data[5].trim());
			quote.fromProtocol(data);

			ArrayList keys = new ArrayList();
			keys.addAll(quote.getBid().keySet());
			Collections.sort(keys, Collections.reverseOrder());
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				Object k = it.next();
				Price p = (Price) quote.getBid().get(k);
				if (p.getLot() > lotbid) {
					p.setLot(p.getLot() - lotbid);
					quote.setLast(price);
					quote.setLastlot(lotbid);
					Vector v = (Vector) quote.getBidOrder().get(k);
					int c = 0, rowcount = v.size();
					double matchorder = new Double(lotbid).doubleValue();
					for (int i = 0; i < rowcount; i++) {
						Queue u = (Queue) v.elementAt(0);
						if (u.getLot() > matchorder) {
							u.setLot(u.getLot() - matchorder);
							break;
						} else if (u.getLot() == matchorder) {
							v.remove(c);
							break;
						} else {
							matchorder = matchorder - u.getLot();
							v.remove(c);
						}
					}
					p.setFreq(v.size());
					quote.getBid().put(k, p);
					break;
				} else if (p.getLot() == lotbid) {
					quote.setLast(price);
					quote.setLastlot(lotbid);
					quote.getBid().remove(k);
					quote.getBidOrder().remove(k);
					break;
				} else {
					quote.setLast(price);
					quote.setLastlot(p.getLot());
					lotbid = lotbid - p.getLot();
					quote.getBid().remove(k);
					quote.getBidOrder().remove(k);
				}
			}

			keys = new ArrayList();
			keys.addAll(quote.getOffer().keySet());
			Collections.sort(keys);
			it = keys.iterator();
			while (it.hasNext()) {
				Object k = it.next();
				Price p = (Price) quote.getOffer().get(k);
				if (p.getLot() > lotoff) {
					p.setLot(p.getLot() - lotoff);
					quote.setLast(price);
					quote.setLastlot(lotoff);
					Vector v = (Vector) quote.getOffOrder().get(k);
					int c = 0, rowcount = v.size();
					double matchorder = new Double(lotoff).doubleValue();
					for (int i = 0; i < rowcount; i++) {
						Queue u = (Queue) v.elementAt(0);
						if (u.getLot() > matchorder) {
							u.setLot(u.getLot() - matchorder);
							break;
						} else if (u.getLot() == matchorder) {
							v.remove(c);
							break;
						} else {
							matchorder = matchorder - u.getLot();
							v.remove(c);
						}
					}
					p.setFreq(v.size());
					quote.getOffer().put(k, p);
					break;
				} else if (p.getLot() == lotoff) {
					quote.setLast(price);
					quote.setLastlot(lotoff);
					quote.getOffer().remove(k);
					quote.getOffOrder().remove(k);
					break;
				} else {
					quote.setLast(price);
					quote.setLastlot(p.getLot());
					lotoff = lotoff - p.getLot();
					quote.getOffer().remove(k);
					quote.getOffOrder().remove(k);
				}
			}
			quote.pack();
			quoteMap.put(key, quote);
			addSnapShot(quote);
			broadcast(quote.toString());
			// System.out.println("quote trade--> "+quote.toString());
		}
	}

	public void newMessage(String message) throws RemoteException {
		try {

			String[] data = message.split("\\|");
			// if (Long.parseLong(data[3]) > msg.getSeqno()){
			FeedMsg feedMsg = new FeedMsg(data[4].trim(), message,
					Long.parseLong(data[3]));

			/*
			 * old if (data[4].trim().equals("1")) { processOrder(data);
			 * msg2.setSeqno(feedMsg.getSeqno()); db.putMsg(msg2); }
			 */

			// datafeed changes time
			if (data[4].trim().equals("1")) {

				processOrder(-1, data);

				msg2.setSeqno(feedMsg.getSeqno());

				db.putMsg(msg2);

			} else if (data[4].trim().equals("2") && preopening) {

				processPreopening(data);

				msg.setSeqno(feedMsg.getSeqno());

				db.putMsg(msg);

			}
			// end of change

			// }
		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/*
		 * Queue q = new Queue("0230662945", new Double(100), new Double(10));
		 * Queue q2 = new Queue("0230662946", new Double(100), new Double(10));
		 * Queue q3 = new Queue("0230662947", new Double(100), new Double(10));
		 * Queue q4 = new Queue("0230662948", new Double(100), new Double(10));
		 * Vector v = new Vector(); v.add(q); v.add(q2); v.add(q3); v.add(q4);
		 * Queue qdel = new Queue("0230662948", new Double(100), new
		 * Double(10)); boolean isremove = v.remove(qdel);
		 * System.out.println(isremove+":"+v.toString());
		 */

	}

}
