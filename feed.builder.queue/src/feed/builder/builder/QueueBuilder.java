package feed.builder.builder;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.LoggerFactory;

import com.db4o.query.Query;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.FileProcessListener;
import feed.builder.core.MsgBuilder;
import feed.builder.core.QueueProcessClientHandler;
import feed.builder.core.Subscriber;
import feed.builder.msg.Price;
import feed.builder.msg.Queue;
import feed.builder.msg.Quote;
import feed.builder.msg.StockSummary;
import feed.provider.core.MsgListener;
import feed.provider.data.FeedMsg;

public class QueueBuilder extends MsgBuilder {

	private FeedMsg msg2;
	private boolean preopening = true;
	private HashMap quoteMap;
	private QueueProcessClientHandler queueClientHandler;

	public QueueBuilder(FeedConsumer consumer, FeedBuilder builder, int port)
			throws Exception {
		super("", "10", consumer, builder, port);
		// initFileProcess();
		// queueClientHandler = new QueueProcessClientHandler();
		// subscribe(queueClientHandler, "", "", 0);
	}

	public QueueBuilder(FeedConsumer consumer, FeedBuilder feedBuilder)
			throws Exception {
		super("", "10", consumer, feedBuilder);
		// initFileProcess();
		queueClientHandler = new QueueProcessClientHandler();
		subscribe(queueClientHandler, "", "", 0);
	}

	public void subscribe(MsgListener listener, String msg) {
		queueClientHandler.subscribe(listener, msg);
	}

	@Override
	public boolean connect() throws Exception {
		// log.info("connecting QueueBuilder ....");
		consumer.getOrderConsumer().subscribe(this, "", "",
				(int) msg2.getSeqno());
		// log.info("connected QueueBuilder ");

		return true;
	}

	protected void initFileProcess() {
		try {
			FileProcessListener list = new FileProcessListener("data/queue-"
					+ getClass());
			subscribe(list, "", "", (int) 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void exit() throws Exception {
		consumer.getOrderConsumer().unsubscribe(this);
		this.close();
	}

	public List getSnapShot(int seqno) {
		/*
		 * Query query = db.getDb().getInstance().query();
		 * query.constrain(Quote.class);
		 * query.descend("seqno").orderAscending().constrain(new Long(seqno))
		 * .greater(); List o = query.execute();
		 */
		/*
		 * log.info("request snapshot for type Queue with seqno: " + seqno +
		 * " and result: " + (o != null ? o.size() + "" : "0") + " record(s)");
		 */
		return null;
	}

	public String getSnapShot(String stock, String board) {

		String q = ((Quote) quoteMap.get(stock + "#" + board)).toString();
		Query query = db.getDb().getInstance().query();
		query.constrain(Quote.class);
		query.descend("stock").constrain(stock)
				.and(query.descend("board").constrain(board));
		List l = query.execute();
		return q + "\n\n\n" + query.execute().get(0).toString() + "\n\n\n"
				+ l.size();
	}

	protected void loadConfig() {
		log = LoggerFactory.getLogger(getClass());
		super.loadConfig();
		preopening = true;
		quoteMap = new HashMap(500, 10F);
		// orderMap = new HashMap(1000, 50);
		msg2 = new FeedMsg();
		msg2.setType("SETTING2");
		List o = db.getDb().getInstance().queryByExample(msg2);
		if (o.size() > 0) {
			msg2 = (FeedMsg) o.get(0);
			msg2.setSeqno(msg2.getMsg().equals(formatDate.format(new Date())) ? msg2
					.getSeqno() : 0L);
			msg2.setMsg(formatDate.format(new Date()));
		} else {
			msg2.setMsg(formatDate.format(new Date()));
			msg2.setSeqno(0L);
		}
		// msg3 = new FeedMsg();
		// msg3.setType("PREOPENING");
		// List p = db.getDb().getInstance().queryByExample(msg3);
		// if (p.size() > 0) {
		// msg3 = (FeedMsg) p.get(0);
		// msg3
		// .setSeqno(msg3.getMsg().equals(
		// formatDate.format(new Date())) ? msg3.getSeqno()
		// : 1L);
		// msg3.setMsg(formatDate.format(new Date()));
		// } else {
		// msg3.setMsg(formatDate.format(new Date()));
		// msg3.setSeqno(1L);
		// }
		// preopening = msg3.getSeqno() == 1L;
		Query query = db.getDb().getInstance().query();
		query.constrain(feed.builder.msg.Quote.class);
		query.descend("stock").orderAscending();
		List t = query.execute();
		for (int i = 0; i < t.size(); i++) {
			Quote q = (Quote) t.get(i);
			q.unpack();
			quoteMap.put(q.getStock() + "#" + q.getBoard(), q);
		}
		// query = db.getDb().getInstance().query();
		// query.constrain(feed.builder.msg.Queue.class);
		// t = query.execute();
		// for (int i = 0; i < t.size(); i++) {
		// Queue q = (Queue) t.get(i);
		// orderMap.put(q.getId(), q);
		// }
	}

	public void newMessage(String message) throws RemoteException {
		try {
			String[] data = message.split("\\|");
			// if (Long.parseLong(data[3]) > msg.getSeqno()){
			FeedMsg feedMsg = new FeedMsg(data[4].trim(), message,
					Long.parseLong(data[3]));
			if (data[4].trim().equals("1")) {
				processOrder(-1, data);
				msg2.setSeqno(feedMsg.getSeqno());
				db.putMsg(msg2);
			} else if (data[4].trim().equals("2") && preopening) {
				processPreopening(data);
				msg.setSeqno(feedMsg.getSeqno());
				db.putMsg(msg);
			}
			// }
		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
		}
	}

	private void processPreopening(String[] data) {
		String key = data[7].trim() + "#" + data[8].trim(); // keys :
		// stock+board
		String command = data[6].trim();
		double price = Double.parseDouble(data[10]);
		double lotbid = Double.parseDouble(data[11]) / consumer.getLot();
		double lotoff = Double.parseDouble(data[11]) / consumer.getLot();
		if (command.equals("0")) {
			Quote quote = (Quote) quoteMap.get(key);
			if (quote == null) {
				quote = new Quote();
				quote.setStock(data[7].trim());
				quote.setBoard(data[8].trim());
				// StockSummary ss = builder.getSS(quote.getStock() + "#"
				// + quote.getBoard());
				// quote.setPrevious(ss == null ? 0 : ss.getPrev());
			}
			quote.setOrdertime(data[5].trim());
			quote.fromProtocol(data);

			ArrayList keys = new ArrayList();
			keys.addAll(quote.getBidOrder().keySet());
			Collections.sort(keys, Collections.reverseOrder());
			Iterator it = keys.iterator();

			StringBuffer sbfb = new StringBuffer();
			StringBuffer sbfo = new StringBuffer();

			/*
			 * if(quote.getSeqno() == 679942 || quote.getSeqno()== 677568){
			 * log.info
			 * ("bidorder "+quote.getBidOrder().size()+" "+quote.getOffOrder
			 * ().size()+" "+quote.getSeqno()); }
			 */

			while (it.hasNext()) {
				Object k = it.next();
				Vector v = (Vector) quote.getBidOrder().get(k);
				int c = 0, rowcount = v.size();
				double matchorder = new Double(lotbid).doubleValue();
				for (int i = 0; i < rowcount; i++) {
					Queue u = (Queue) v.elementAt(0);
					if (u.getLot() > matchorder) {
						u.setLot(u.getLot() - matchorder);
						matchorder = 0;
						sbfb.append(u.changeOrder(-1));

						break;
					} else if (u.getLot() == matchorder) {
						v.remove(c);

						if (v.size() == 0) {
							quote.getBidOrder().remove(k);
						}
						matchorder = 0;
						sbfb.append(u.deleteOrder(-1));

						break;
					} else {
						matchorder = matchorder - u.getLot();
						v.remove(c);
						sbfb.append(u.deleteOrder(-1));

						if (v.size() == 0 || matchorder <= 0) {
							quote.getBidOrder().remove(k);
							break;
						}

					}

					if (v.size() == 0 || matchorder <= 0) {
						quote.getBidOrder().remove(k);
						break;
					}
				}
				if (matchorder <= 0)
					break;
			}

			/*
			 * while (it.hasNext()) { Object k = it.next(); Price p = (Price)
			 * quote.getBid().get(k); if (p.getLot() > lotbid) {
			 * p.setLot(p.getLot() - lotbid); quote.setLast(price);
			 * quote.setLastlot(lotbid); Vector v = (Vector)
			 * quote.getBidOrder().get(k); int c = 0, rowcount = v.size();
			 * double matchorder = new Double(lotbid).doubleValue(); for (int i
			 * = 0; i < rowcount; i++) { Queue u = (Queue) v.elementAt(0); if
			 * (u.getLot() > matchorder) { u.setLot(u.getLot() - matchorder);
			 * break; } else if (u.getLot() == matchorder) { v.remove(c); break;
			 * } else { matchorder = matchorder - u.getLot(); v.remove(c); } }
			 * p.setFreq(v.size()); quote.getBid().put(k, p); break; } else if
			 * (p.getLot() == lotbid) { quote.setLast(price);
			 * quote.setLastlot(lotbid); quote.getBid().remove(k);
			 * quote.getBidOrder().remove(k); break; } else {
			 * quote.setLast(price); quote.setLastlot(p.getLot()); lotbid =
			 * lotbid - p.getLot(); quote.getBid().remove(k);
			 * quote.getBidOrder().remove(k); } }
			 */

			keys = new ArrayList();
			keys.addAll(quote.getOffOrder().keySet());
			Collections.sort(keys);
			it = keys.iterator();
			while (it.hasNext()) {
				Object k = it.next();
				Vector v = (Vector) quote.getOffOrder().get(k);
				int c = 0, rowcount = v.size();
				double matchorder = new Double(lotoff).doubleValue();
				for (int i = 0; i < rowcount; i++) {
					Queue u = (Queue) v.elementAt(0);
					if (u.getLot() > matchorder) {
						u.setLot(u.getLot() - matchorder);
						matchorder = 0;
						sbfo.append(u.changeOrder(-1));

						break;
					} else if (u.getLot() == matchorder) {
						v.remove(c);
						if (v.size() == 0) {
							quote.getOffOrder().remove(k);
						}
						matchorder = 0;
						sbfo.append(u.deleteOrder(-1));

						break;
					} else {
						matchorder = matchorder - u.getLot();
						v.remove(c);
						sbfo.append(u.deleteOrder(-1));

					}

					if (v.size() == 0 || matchorder <= 0) {
						quote.getOffOrder().remove(k);
						break;
					}
				}

				if (matchorder <= 0)
					break;

			}

			/*
			 * while (it.hasNext()) { Object k = it.next(); Price p = (Price)
			 * quote.getOffer().get(k); if (p.getLot() > lotoff) {
			 * p.setLot(p.getLot() - lotoff); quote.setLast(price);
			 * quote.setLastlot(lotoff); Vector v = (Vector)
			 * quote.getOffOrder().get(k); int c = 0, rowcount = v.size();
			 * double matchorder = new Double(lotoff).doubleValue(); for (int i
			 * = 0; i < rowcount; i++) { Queue u = (Queue) v.elementAt(0); if
			 * (u.getLot() > matchorder) { u.setLot(u.getLot() - matchorder);
			 * break; } else if (u.getLot() == matchorder) { v.remove(c); break;
			 * } else { matchorder = matchorder - u.getLot(); v.remove(c); } }
			 * p.setFreq(v.size()); quote.getOffer().put(k, p); break; } else if
			 * (p.getLot() == lotoff) { quote.setLast(price);
			 * quote.setLastlot(lotoff); quote.getOffer().remove(k);
			 * quote.getOffOrder().remove(k); break; } else {
			 * quote.setLast(price); quote.setLastlot(p.getLot()); lotoff =
			 * lotoff - p.getLot(); quote.getOffer().remove(k);
			 * quote.getOffOrder().remove(k); } }
			 */
			quote.pack();
			quoteMap.put(key, quote);
			/*
			 * addSnapShot(quote); broadcast(quote.toString());
			 */
			// String quoteString = quote.toStringSummaryByQueue();
			// broadcast(quoteString);
			broadcast(generateToString(quote, sbfb, sbfo));

			// System.out.println("quote trade--> "+quote.toString());
		}
	}

	private void processOrder(int idx, String[] data) {
		String key = data[7].trim() + "#" + data[8].trim();
		String command = data[6].trim();
		String orderid = data[14].trim();
		String time = data[5].trim();
		double price = Double.parseDouble(data[10]);
		double bal = Double.parseDouble(data[12]) / consumer.getLot();
		double match = Double.parseDouble(data[11]) / consumer.getLot() - bal;
		String oldorderid = data[19].trim();
		Quote quote = (Quote) quoteMap.get(key);

		double timeorder = Double.parseDouble(data[5]);

		if (timeorder < 90000 || (timeorder >= 155000 && timeorder < 160500))
			match = 0;

		if (quote == null) {
			quote = new Quote();
			quote.setStock(data[7].trim());
			quote.setBoard(data[8].trim());
			// StockSummary ss = builder.getSS(quote.getStock() + "#"
			// + quote.getBoard());
			// quote.setPrevious(ss != null ? ss.getPrev() : 0.0D);
		}
		quote.setOrdertime(data[5].trim());
		quote.fromProtocol(data);

		// if (orderid.equals("000427436052")) {
		// log.info("data "
		// +price+" "+bal+" "+match+" "+orderid+" "+quote.getSeqno()+" "+command+" "+timeorder);

		// }

		// log.info("seqno "+quote.getSeqno());

		Queue test = (Queue) quote.getWithdraw().get(oldorderid);
		if (Double.parseDouble(oldorderid) > 0 && test == null) {
			quote.getTemp().put(oldorderid, data);

			return;
		} else {
			quote.getTemp().remove(oldorderid);
		}

		if (command.equals("0")) {
			// log.info("command 0");

			StringBuffer sbfb = new StringBuffer();
			StringBuffer sbfo = new StringBuffer();

			if (bal != 0.0D) {
				Queue q = new Queue(quote.getStock(), quote.getBoard(), "0",
						orderid, price, bal, time);
				// Price p = (Price) quote.getBid().get(new Double(price));
				Double priced = new Double(price);
				Vector v = (Vector) quote.getBidOrder().get(priced);
				if (v == null) {
					v = new Vector();
					quote.getBidOrder().put(priced, v);
				}

				if (idx >= 0)
					v.add(idx, q);
				else
					v.addElement(q);

				sbfb.append(q.newOrder(idx)).append(">");
			}

			if (match != 0.0D) {

				Queue q = (Queue) quote.getWithdraw().get(oldorderid);
				match = match - (q != null ? q.getMatch() : 0);
				// log.info("processOrder:" + key + ":" + ":" + oldorderid
				// + ":match:" + match + ":bal:" + bal + ":" + data[11]
				// + ":" + data[12]);
				// log.info("match----------:" + oldorderid + ":" + match + ":"
				// + data[14] + ":" + (q != null ? q.getMatch() : 0));
				// log.info("match:" + key + ":" + oldorderid + ":" + match);
				if (match <= 0)
					return;
				ArrayList keys = new ArrayList();
				keys.addAll(quote.getOffOrder().keySet());
				Collections.sort(keys);
				Object k;

				for (Iterator it = keys.iterator(); it.hasNext();) {
					k = it.next();
					// Price p = (Price) quote.getOffer().get(k);
					// log.info("price offer:" + k + ":" + p.getLot());

					Vector v = (Vector) quote.getOffOrder().get(k);

					if (v != null) {
						int c = 0;
						int rowcount = v.size();
						// double matchorder = new Double(match).doubleValue();
						for (int i = 0; i < rowcount; i++) {
							Queue u = (Queue) v.elementAt(0);
							// log.info("queue offer:" + u.getPrice() + ":"
							// + u.getLot() + ":" + matchorder);

							/*
							 * if (quote.getSeqno() == 68041) {
							 * log.info("queue builder lot " + u.getLot() + " "
							 * + match + " " + v.size() + " priceof "
							 * +k+" "+u.getId()); }
							 */
							if (u.getLot() > match) {
								u.setLot(u.getLot() - match);
								match = 0;
								sbfo.append(u.changeOrder(-1)).append(">");

								break;
							}

							if (u.getLot() == match) {
								v.remove(c);
								if (v.size() == 0) {
									quote.getOffOrder().remove(k);
								}
								match = 0;
								sbfo.append(u.deleteOrder(-1)).append(">");
								break;
							}

							match -= u.getLot();
							v.remove(c);
							sbfo.append(u.deleteOrder(-1)).append(">");

							if (v.size() == 0 || match <= 0) {
								quote.getOffOrder().remove(k);
								break;
							}
						}

						if (match <= 0) {
							break;
						}

					}
				}

				// String offOrder = quote.toStringOfferOrder();
				// broadcast(offOrder);

			}
			// quote.pack();
			quoteMap.put(key, quote);

			// addSnapShot(quote);
			// String quoteString = quote.toStringSummaryByQueue();
			broadcast(generateToString(quote, sbfb, sbfo));

			// log.info(command + ":" + "quote order " + quoteString);

			// String bidOrder = quote.toStringBidOrder();
			// broadcast(bidOrder);
			// log.info(command + ": queue order " +
			// bidOrder.getBytes().length);

		} else if (command.equals("1")) {

			StringBuffer sbfb = new StringBuffer();
			StringBuffer sbfo = new StringBuffer();

			if (bal != 0.0D) {
				// Price p = (Price) quote.getOffer().get(new Double(price));

				Queue q = new Queue(quote.getStock(), quote.getBoard(), "1",
						orderid, price, bal, time);
				Double priced = new Double(price);

				Vector v = (Vector) quote.getOffOrder().get(priced);
				if (v == null) {
					v = new Vector();
					quote.getOffOrder().put(priced, v);
				}

				if (idx >= 0)
					v.add(idx, q);
				else
					v.addElement(q);

				// v.addElement(q);
				sbfo.append(q.newOrder(idx)).append(">");
			}

			if (match != 0.0D) {
				Queue q = (Queue) quote.getWithdraw().get(oldorderid);
				match = match - (q != null ? q.getMatch() : 0);
				if (match <= 0)
					return;
				ArrayList keys = new ArrayList();
				keys.addAll(quote.getBidOrder().keySet());
				Collections.sort(keys, Collections.reverseOrder());
				Object k;
				for (Iterator it = keys.iterator(); it.hasNext();) {
					k = it.next();
					double kd = (Double) k;
					// Price p = (Price) quote.getBid().get(k);

					Vector v = (Vector) quote.getBidOrder().get(k);
					int c = 0;
					int rowcount = v.size();
					// double matchorder = new Double(match).doubleValue();
					for (int i = 0; i < rowcount; i++) {
						Queue u = (Queue) v.elementAt(0);

						/*
						 * if (kd == 9250d) { log.info("quote " +
						 * quote.getSeqno() + " " + u.getLot() + " " + u.getId()
						 * + " " + k + " " + match); }
						 * 
						 * if (u.getId().equals("000427436052")) {
						 * log.info("quote-12 " + quote.getSeqno() + " " +
						 * u.getLot() + " " + u.getId() + " " + k + " " +
						 * match);
						 * 
						 * }
						 */

						if (u.getLot() > match) {
							u.setLot(u.getLot() - match);
							match = 0;
							sbfb.append(u.changeOrder(-1)).append(">");

							break;
						}
						if (u.getLot() == match) {
							v.remove(c);
							if (v.size() == 0) {
								quote.getBidOrder().remove(k);
							}
							sbfb.append(u.deleteOrder(0)).append(">");
							match = 0;
							break;
						}
						match -= u.getLot();
						v.remove(c);

						sbfb.append(u.deleteOrder(-1)).append(">");

						if (v.size() == 0 || match <= 0) {
							quote.getBidOrder().remove(k);
						}

					}

					if (match <= 0)
						break;

				}

				// String bidOrder = quote.toStringBidOrder();

				// broadcast(bidOrder);

			}
			// quote.pack();
			quoteMap.put(key, quote);
			broadcast(generateToString(quote, sbfb, sbfo));

			/*
			 * addSnapShot(quote); String quoteString = quote.toString();
			 * broadcast(quoteString);
			 */
			// log.info(command + ":" + "quote order " + quoteString);

			// String offOrder = quote.toStringOfferOrder();

			// broadcast(offOrder);
			// log.info(command + ": queue order  " + offOrder);
			// quoteString = quote.toStringSummaryByQueue();
			// broadcast(quoteString);

		} else if (command.equals("2")) {

			Queue q = new Queue(quote.getStock(), quote.getBoard(), "0",
					orderid, price, bal, time);
			// q.setType("0");
			Queue q2 = new Queue(quote.getStock(), quote.getBoard(), "0",
					orderid, price, bal, "", match);
			quote.getWithdraw().put(q2.getId(), q2);
			Double price2 = new Double(price);
			Vector v = (Vector) quote.getBidOrder().get(price2);
			idx = -1;

			StringBuffer sbfb = new StringBuffer();
			StringBuffer sbfo = new StringBuffer();

			if (v != null) {

				/*
				 * if (quote.getSeqno() == 354391) { for (int i = 0; i <
				 * v.size(); i++) { Queue qb = (Queue) v.elementAt(i);
				 * log.info("queue before "
				 * +qb.getId()+" "+qb.getLot()+" "+qb.getPrice()); } }
				 */

				idx = v.indexOf(q);

				boolean isremove = v.remove(q);
				if (v.size() == 0)
					quote.getBidOrder().remove(price2);

				sbfb.append(q.deleteOrder(-1)).append(">");

				// log.info("delete order "+q.deleteOrder(-1));

				/*
				 * if (quote.getSeqno() == 354391) { for (int i = 0; i <
				 * v.size(); i++) { Queue qb = (Queue) v.elementAt(i);
				 * log.info("queue after "
				 * +qb.getId()+" "+qb.getLot()+" "+qb.getPrice()); } }
				 */

			} else {
				v = new Vector(1);
			}

			// quote.pack();
			quoteMap.put(key, quote);
			// String quoteString = quote.toStringSummaryByQueue();
			// broadcast(quoteString);

			broadcast(generateToString(quote, sbfb, sbfo));

			/*
			 * addSnapShot(quote); String quoteString = quote.toString();
			 * broadcast(quoteString);
			 */
			// log.info(command + ":" + "quote order " + quoteString);

			// -- Buat Queue

			// String bidOrder = quote.toStringBidOrder();
			// broadcast(bidOrder);

			String[] str = (String[]) quote.getTemp().get(orderid);

			if (str != null) {
				price = Double.parseDouble(str[10]);

				processOrder(price == q.getPrice() ? idx : -1, str);
			}

		} else if (command.equals("3") && bal != 0.0D) {
			Queue q = new Queue(quote.getStock(), quote.getBoard(), "1",
					orderid, price, bal, time);
			// q.setType("1");

			Queue q2 = new Queue(quote.getStock(), quote.getBoard(), "0",
					orderid, price, bal, "", match);
			quote.getWithdraw().put(q2.getId(), q2);
			Vector v = (Vector) quote.getOffOrder().get(new Double(price));

			StringBuffer sbfb = new StringBuffer();
			StringBuffer sbfo = new StringBuffer();

			if (v != null) {

				idx = v.indexOf(q);

				v.remove(q);
				if (v.size() == 0)
					quote.getOffOrder().remove(price);

				sbfo.append(q.deleteOrder(-1)).append(">");

			} else
				v = new Vector(1);

			// quote.pack();
			quoteMap.put(key, quote);
			// String quoteString = quote.toStringSummaryByQueue();
			// broadcast(quoteString);
			broadcast(generateToString(quote, sbfb, sbfo));

			/*
			 * addSnapShot(quote); String quoteString = quote.toString();
			 * broadcast(quote.toString());
			 */
			// log.info(command + ":" + "quote order " + quoteString);

			// -- Buat Queue

			// String offOrder = quote.toStringOfferOrder();
			// broadcast(offOrder);

			String[] str = (String[]) quote.getTemp().get(orderid);
			if (str != null) {
				price = Double.parseDouble(str[10]);

				processOrder(q.getPrice() == price ? idx : -1, str);
			}
			// log.info("queue order " + quote.getStock() + ":" +
			// quote.getBoard());
		}

	}

	private String generateToString(Quote quote, StringBuffer sbfb,
			StringBuffer sbfo) {
		StringBuffer sb = new StringBuffer();
		sb.append(quote.getStock()).append("|").append(quote.getBoard())
				.append("<").append("B[").append(sbfb.toString()).append("]O[")
				.append(sbfo.toString()).append("]");
		return sb.toString();
	}

	public void unsubscribe(MsgListener listener, String msg)
			throws RemoteException {
		queueClientHandler.unsubscribe(listener, msg);
	}

	public String getQuote(String stock, String board) {
		String key = stock + "#" + board;

		Quote q = (Quote) quoteMap.get(key);

		return q.toStringSummaryByQueue();

	}

	public Vector getQueue(String stock, String board, Double price) {
		return queueClientHandler.getQueue(stock, board, price);

		// return q.toStringSummaryByQueue();

	}

	public static void main(String[] args) {
		/*
		 * Queue q = new Queue("0230662945", new Double(100), new Double(10));
		 * Queue q2 = new Queue("0230662946", new Double(100), new Double(10));
		 * Queue q3 = new Queue("0230662947", new Double(100), new Double(10));
		 * Queue q4 = new Queue("0230662948", new Double(100), new Double(10));
		 * Vector v = new Vector(); v.add(q); v.add(q2); v.add(q3); v.add(q4);
		 * Queue qdel = new Queue("0230662948", new Double(100), new
		 * Double(10)); boolean isremove = v.remove(qdel);
		 * System.out.println(isremove+":"+v.toString());
		 */

	}
}
