package feed.builder.core;

import java.util.Hashtable;
import java.util.List;

import com.db4o.query.Query;

import feed.builder.builder.FeedBuilder;
import feed.builder.consumer.FeedConsumer;
import feed.builder.msg.Message;
import feed.provider.data.FeedDb;

public abstract class MsgBuilder extends MsgManager{
	protected FeedConsumer consumer;
	protected MsgService service;
	protected FeedBuilder builder;
	
	public MsgBuilder(){
		
	}
	
	public MsgBuilder(String url, String type, FeedConsumer consumer, FeedBuilder builder) throws Exception{
		super();
		log.info("builder starting...");
		this.builder = builder;
		this.url = url;
		this.types = type;
		clientList = new Hashtable();
		
		db = new FeedDb("builder-"+type);
		db.start();
		loadConfig();
		this.consumer = consumer;
		log.info("builder started");

	}

	
	public MsgBuilder(String url, String type, FeedConsumer consumer, FeedBuilder builder, int port) throws Exception{
		super();
		log.info("builder starting...");
		this.builder = builder;
		this.url = url;
		this.types = type;
		clientList = new Hashtable();
		
		db = new FeedDb("builder-"+type);
		db.start();
		loadConfig();
		this.consumer = consumer;
		
		service = new MsgService(this, port);
		log.info("builder started");


	}
	
	public List getSnapShot(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(Message.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		log.info("request snapshot for type FeedMsg with seqno: "+seqno+" and result: "+(o!=null?o.size()+"" :"0")+ " record(s)");
		return o;
	}
	
	
	public MsgService getService(){
		return service;
	}
}
