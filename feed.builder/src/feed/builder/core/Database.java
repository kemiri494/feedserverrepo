package feed.builder.core;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;

import snaq.db.ConnectionPoolManager;

public class Database {
//    private final Log log = LogFactory.getLog(getClass());
 	ConnectionPoolManager cpm = null;
	private HashMap mapStmt = new HashMap();

	public void registerStatement(String name, String stmt) throws Exception{
			Connection con = cpm.getConnection("local");
			PreparedStatement query = con.prepareCall(stmt);
			mapStmt.put(name, query);
			con.close();
	}
	
	public PreparedStatement getStatement(String name){
		return (PreparedStatement)mapStmt.get(name);
	}
	
	public int executeStatement(String name, Vector param){
		int result = 0;
		PreparedStatement ps = ((PreparedStatement)getStatement(name));
		try {
			for (int i=0; i<param.size();i++){
				ps.setObject(i+1, param.elementAt(0));
			}
			result =  ps.executeUpdate();
			ps.close();
		} catch (Exception ex){
			result = -1;
		}
		return result;
	}
	
	public void start(String config) throws Exception {
			cpm = ConnectionPoolManager.getInstance(new File(config));
			Connection con = cpm.getConnection("local");
			con.close();
	}

	public void setStop(){
		cpm.release();
	}
	
//	public Statement createStatement() throws Exception {
//		return cpm.getConnection("local").createStatement();
//	}
	
	public Connection getConnection() throws Exception{
		return cpm.getConnection("local");
	}
	
	public boolean  update(String sql){
		Connection con = null;
		Statement st = null;
		int rows = 0;
		try {
			con = cpm.getConnection("local");		  
			st = con.createStatement();
	        rows = st.executeUpdate(sql ) ;
	        st.close();		
	    } catch (SQLException sqle){
	    	sqle.printStackTrace();
	    	rows = 0;
		}
		finally{
		  try { con.close(); }
		  catch (SQLException e) { }
		}
		return rows > 0;
	}	
}
