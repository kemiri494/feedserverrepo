package feed.builder.core;

import java.util.Vector;

import feed.builder.socket.core.SubscriberSocketManager;
import feed.provider.core.MsgListener;

public class SusbcriberQueue implements Comparable<SusbcriberQueue> {

	private MsgListener listener;
	private boolean isstart = false;
	private Vector<String> vtemp = new Vector<String>();

	public SusbcriberQueue(MsgListener listener) {
		this.listener = listener;
	}

	public boolean newMessage(String msg) {
		try {
			if (isstart) {
				if (vtemp.isEmpty()) {
					while (vtemp.size() > 0)
						listener.newMessage(vtemp.remove(0));

				}

				listener.newMessage(msg);

			} else {
				vtemp.add(msg);
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
			return false;
		}
		return true;
	}

	public MsgListener getListener() {
		return listener;
	}

	public void setListener(MsgListener listener) {
		this.listener = listener;
	}

	public boolean isIsstart() {
		return isstart;
	}

	public void setIsstart(boolean isstart) {
		this.isstart = isstart;
	}

	@Override
	public int compareTo(SusbcriberQueue o) {
		// TODO Auto-generated method stub

		SubscriberSocketManager ss = (SubscriberSocketManager) o.getListener();
		SubscriberSocketManager ss2 = (SubscriberSocketManager) getListener();

		return ss.getSocket().equals(ss2.getSocket()) ? 0 : -1;
	}

	@Override
	public boolean equals(Object obj) {
		SusbcriberQueue sq = (SusbcriberQueue) obj;
		SubscriberSocketManager ss =(SubscriberSocketManager) sq.getListener();
		SubscriberSocketManager th =(SubscriberSocketManager) getListener();
		
		
		return ss.getSocket().equals(th.getSocket());
	}
	
	

}
