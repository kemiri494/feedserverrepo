package feed.builder.core;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import feed.provider.core.MsgListener;
import feed.provider.core.MsgProducer;

public class MsgService extends UnicastRemoteObject implements MsgProducer{
	private static final long serialVersionUID = -4320908304882599066L;
	private MsgBuilder parent;
	
	public MsgService(MsgBuilder parent, int port) throws RemoteException{
		super(port);
		this.parent = parent;
	}

	public void heartbeat(int arg0) throws RemoteException {
	}

	public int subscribe(MsgListener arg0, String arg1, String arg2, int arg3) throws RemoteException {
		return parent.subscribe(arg0, arg1, arg2, arg3);
	}

	public void unsubscribe(MsgListener arg0) throws RemoteException {
		parent.unsubscribe(arg0);
	}
}
