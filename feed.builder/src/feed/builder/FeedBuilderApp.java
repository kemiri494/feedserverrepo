package feed.builder;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.builder.builder.FeedBuilder;
import feed.builder.builder.FeedBuilderSocket;
import feed.builder.consumer.FeedConsumer;
import feed.builder.consumer.FeedConsumerSocket;

public class FeedBuilderApp {
	private Console console;
	private FeedConsumerSocket consumer;
	private FeedBuilder builder;
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	public void start() throws Exception{
		//consumer = new FeedConsumer(true);
		consumer = new FeedConsumerSocket("feed.builder.config");
		//builder = new FeedBuilder(consumer);
		builder = new FeedBuilderSocket(consumer);
		consumer.start();
		builder.start();
		console = new Console(this);
		console.start();
	}
	
	public void stop(){
		builder.stop();
		consumer.stop();		
	}
	
	public FeedBuilder getBuilder(){
		return builder;
	}
	public FeedConsumerSocket getConsumer(){
		return consumer;
	}
	
	public String getQuote(String quote, String board){
		return builder.getQuote(quote, board);
	}
	
	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure("log.properties");
			new FeedBuilderApp().start();
		} catch (Exception ex){
			ex.printStackTrace();
			System.exit(-1);
		}
	}

}
