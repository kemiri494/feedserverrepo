package feed.builder.msg;

public class CurrencyMobile extends Currency{
	private String queue;
	
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(queue);
		return result.toString();
	}
	
	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}
}
