package feed.builder.msg;

public class BrokerMobile extends Message{
	private String code;
	
	public BrokerMobile(){}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String toString(){
		return new StringBuffer(super.toString()).append(delimiter).append(code).append(delimiter).toString();//.append(name).append(delimiter).append(status);
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
//		name = data[4].trim();
//		status = data[5].trim();
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
//		name = data[6].trim();
//		status = data[7].trim();
	}
	
}
