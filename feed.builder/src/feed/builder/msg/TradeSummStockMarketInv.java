package feed.builder.msg;

public class TradeSummStockMarketInv extends Message{
	
	protected String stock = "ALL";
	protected String board = "ALL";
	protected String broker = "ALL";
	protected String investor = "ALL";
	protected double buyavg;
	protected double sellavg;
	protected double buyvol;
	protected double buyval;
	protected double buyfreq;
	protected double sellvol;
	protected double sellval;
	protected double sellfreq;
	
	public TradeSummStockMarketInv() {
	}
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(stock);
		result.append(delimiter).append(board);
		result.append(delimiter).append(broker);
		result.append(delimiter).append(investor);		
		result.append(delimiter).append(format2.format(buyavg));
		result.append(delimiter).append(format2.format(sellavg));		
		result.append(delimiter).append(format.format(buyvol));
		result.append(delimiter).append(format.format(buyval));
		result.append(delimiter).append(format.format(buyfreq));
		result.append(delimiter).append(format.format(sellvol));
		result.append(delimiter).append(format.format(sellval));
		result.append(delimiter).append(format.format(sellfreq));
		return result.toString();
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		stock = data[3].trim();
		board = data[4].trim();
		broker = data[5].trim();
		investor = data[6].trim();
		buyavg = Double.parseDouble(data[7]);
		sellavg = Double.parseDouble(data[8]);
		buyvol = Double.parseDouble(data[9]);
		buyval = Double.parseDouble(data[10]);
		buyfreq = Double.parseDouble(data[11]);
		sellvol = Double.parseDouble(data[12]);
		sellval = Double.parseDouble(data[13]);
		sellfreq = Double.parseDouble(data[14]);
	}
	
	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getInvestor() {
		return investor;
	}

	public void setInvestor(String investor) {
		this.investor = investor;
	}
	
	public double getBuyavg() {
		return buyavg;
	}

	public void setBuyavg(double avgprice) {
		this.buyavg = avgprice;
	}

	public double getSellavg() {
		return sellavg;
	}

	public void setSellavg(double avgprice) {
		this.sellavg = avgprice;
	}

	public double getBuyvol() {
		return buyvol;
	}

	public void setBuyvol(double buyvol) {
		this.buyvol = buyvol;
	}

	public double getBuyval() {
		return buyval;
	}

	public void setBuyval(double buyval) {
		this.buyval = buyval;
	}

	public double getBuyfreq() {
		return buyfreq;
	}

	public void setBuyfreq(double buyfreq) {
		this.buyfreq = buyfreq;
	}

	public double getSellvol() {
		return sellvol;
	}

	public void setSellvol(double sellvol) {
		this.sellvol = sellvol;
	}

	public double getSellval() {
		return sellval;
	}

	public void setSellval(double sellval) {
		this.sellval = sellval;
	}

	public double getSellfreq() {
		return sellfreq;
	}

	public void setSellfreq(double sellfreq) {
		this.sellfreq = sellfreq;
	}

}
