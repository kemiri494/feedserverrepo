package feed.builder.msg;

public class Future extends Message{
	private String code;
	private String detail;
	private String market;
	private double last;
	private double close;
	private double change;
	private double percent;
	private double high;
	private double low;
	private double open;
	
	public Future(){}
	 
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(code);
		result.append(delimiter).append(detail);
		result.append(delimiter).append(market);
		result.append(delimiter).append(format3.format(last));
		result.append(delimiter).append(format3.format(close));
		result.append(delimiter).append(format3.format(high));
		result.append(delimiter).append(format3.format(low));
		result.append(delimiter).append(format3.format(open));
		result.append(delimiter).append(format3.format(change));
		result.append(delimiter).append(format3.format(percent));		
		return result.toString();
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		code = data[3].trim();
		detail = data[4].trim();
		market = data[5].trim();
		last = Double.parseDouble(data[6]);
		close = Double.parseDouble(data[7]);
		change = Double.parseDouble(data[8]);
		percent = Double.parseDouble(data[9]);		
		high = Double.parseDouble(data[10]);
		low = Double.parseDouble(data[11]);		
		open = Double.parseDouble(data[12]);
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code = data[5].trim();
		detail=data[6].trim();
		market=data[7].trim();
		last = Double.parseDouble(data[8]);
		close = Double.parseDouble(data[9]);
		change = Double.parseDouble(data[10]);
		percent = Double.parseDouble(data[11]);		
		high = Double.parseDouble(data[12]);
		low = Double.parseDouble(data[13]);		
		open = Double.parseDouble(data[14]);
	}
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}
	
	public double getHigh() {
		return high;
	}
 
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}
	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}
}
