package feed.builder.msg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.jfree.util.Log;

import feed.provider.core.Utils;

public class Quote extends Message {
	private String ordertime;
	private String stock;
	private String board;
	private double pricequeue;
	private double offerpricequeue;
	
	private HashMap bid; // price --> Price
	private HashMap offer; // price --> Price
	private double last;
	private double lastlot;
	private double previous;
	private String stream;

	// new for frequency
	private HashMap bidorder; // price --> vector of Queue
	private HashMap offorder; // price --> vector of Queue
	private String streamorder;

	private HashMap withdraw;
	private HashMap temp;
	private HashMap mqueue;
	
	

	public Quote() {
		bid = new HashMap(20, 5);
		offer = new HashMap(20, 5);

		bidorder = new HashMap(100, 5);
		offorder = new HashMap(100, 5);

		withdraw = new HashMap(100, 20);
		temp = new HashMap(100, 20);
	}

	public HashMap getWithdraw() {
		return withdraw;
	}

	public HashMap getTemp() {
		return temp;
	}

	private String orderToString() {
		StringBuffer sb = new StringBuffer(100);
		//System.out.println("queue ordertostring =");
		sb.append("B[");
		Iterator it = bidorder.keySet().iterator();
		while (it.hasNext()) {
			Vector v = (Vector) bidorder.get(it.next());
			for (int i = 0; i < v.size(); i++) {
				Queue q = (Queue) v.elementAt(i);
				if (q.getLot() > 0) {
					sb.append(q.toString());
				}
			}
		}
		sb.append("]O[");

		it = offorder.keySet().iterator();
		while (it.hasNext()) {
			Vector v = (Vector) offorder.get(it.next());
			for (int i = 0; i < v.size(); i++) {
				Queue q = (Queue) v.elementAt(i);
				if (q.getLot() > 0) {
					sb.append(q.toString());
				}
			}
		}
		sb.append("]");
		return sb.toString();
	}

	private void setOrder(String s) {
		bidorder.clear();
		offorder.clear();

		int nstart = s.indexOf("[") + 1;
		int nend = s.indexOf("]", nstart);
		String sbid = s.substring(nstart, nend);

		String[] biddetail = sbid.split("\\>");
		for (int i = 0; i < biddetail.length; i++) {
			String[] brow = biddetail[i].split("\\|");
			if (brow.length > 1) {
				Queue q = new Queue(brow[0], Double.parseDouble(brow[1]),
						Double.parseDouble(brow[2]),
						Double.parseDouble(brow[4]),
						brow[3]);
				Vector v = (Vector) bidorder.get(new Double(q.getPrice()));
				if (v == null)
					v = new Vector(100, 5);
				v.addElement(q);
				bidorder.put(new Double(q.getPrice()), v);
			}
		}

		nstart = s.indexOf("[", nend) + 1;
		nend = s.indexOf("]", nstart);
		String soffer = s.substring(nstart, nend);
		String[] offdetail = soffer.split("\\>");
		for (int i = 0; i < offdetail.length; i++) {
			String[] orow = offdetail[i].split("\\|");
			if (orow.length > 1) {
				Queue q = new Queue(orow[0], Double.parseDouble(orow[1]),
						Double.parseDouble(orow[2]),
						Double.parseDouble(orow[4]),orow[3]);
				Vector v = (Vector) bidorder.get(new Double(q.getPrice()));
				if (v == null)
					v = new Vector(100, 5);
				v.addElement(q);
				offorder.put(new Double(q.getPrice()), v);
			}
		}
	}

	public void pack() {
		stream = this.toString();
		streamorder = orderToString();
	}

	public void unpack() {
		setContent(stream);
		setOrder(streamorder);
	}

	public String getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(String time) {
		this.ordertime = time;
	}

	public String getStock() {
		return stock;
	}
	
		
	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public HashMap getBid() {
		return bid;
	}


	public void setBid(HashMap buy) {
		this.bid = buy;
	}

	public HashMap getOffer() {
		return offer;
	}

	public HashMap getOffOrder() {
		return offorder;
	}

	public void setOffer(HashMap sell) {
		this.offer = sell;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getLastlot() {
		return lastlot;
	}

	public void setLastlot(double lastlot) {
		this.lastlot = lastlot;
	}

	public double getPrevious() {
		return previous;
	}

	public void setPrevious(double prev) {
		this.previous = prev;
	}

	public HashMap getBidOrder() {
		return bidorder;
	}
	
	public void setBidOrder(HashMap bidOrder) {
		this.bidorder = bidOrder;
	}
	
	public double getBidPriceQueue() {
		return pricequeue;
	}
	
	public void setBidPriceQueue(double bidPriceQueue) {
		this.pricequeue = bidPriceQueue;
	}
	
	public double getOfferPriceQueue() {
		
		return offerpricequeue;
	}
	
	public void setOfferPriceQueue(double offerPriceQueue) {
		this.offerpricequeue = offerPriceQueue;
	}
	
	public void setContentQueueOffer(String dataOffer) {

		String[] header = dataOffer.split("\\|");
		setHeader(header[0]);
		setStock(header[2]);
		setBoard(header[3]);
		setType("11");
		//setBidPriceQueue(Utils.strToDouble(header[4], 0));
		setOfferPriceQueue(Utils.strToDouble(header[4], 0));
		
		int idx = dataOffer.indexOf("[");
		int idxEnd = dataOffer.indexOf("<]");
		String sQueue = dataOffer.substring(idx + 1, idxEnd);
		// System.out.println(sQueue + ":" + idx);
		// HashMap map = new HashMap();
		String[] array = sQueue.split("!");
		if (array.length >1) {
		for (int i = 0; i < array.length; i++) {
			// System.out.println(array[i]);
			String[] sp = array[i].split("\\[");
			Double price = new Double(sp[0]);
			String[] queuee = sp[1].split(">");
			Vector v = new Vector();
			for (int j = 0; j < queuee.length; j++) {
				String[] dQueue = queuee[j].split("\\|");
				//Queue q = new Queue(dQueue[0], price, new Double(dQueue[1]),dQueue[3]);
				Queue q = new Queue(dQueue[0], getOfferPriceQueue(), new Double(dQueue[1]),dQueue[3]);
				q.setMatch(new Double(dQueue[2].replace("]", "")));
				v.add(q);
				// System.out.println(dQueue[0]+":"+dQueue[1]+":"+dQueue[2]);
			}
			// map.put(price, v);
			//Log.info("Quote setContentQueueOffer on Builder="+ dataOffer);
			getOffOrder().put(price, v);
		}
		}
	}

	//IDX|10|CPRO|RG|B[<]
	//IDX|10|ELTY|RG|B[230.0[0242630744|500.0|0.0|091000|0]!<]
	public void setContentQueueBid(String dataBid) {

		 
		String[] header = dataBid.split("\\|");
		setHeader(header[0]);
		setStock(header[2]);
		//setStock(header[4]);
		setBoard(header[3]);
		//setStock(header[5]);
		setType("10");
		
		//ad price @yd
		
		setBidPriceQueue(Utils.strToDouble(header[4], 0));
//		System.out.println("setContentQueueBid= "+ header[4] + " on dataBid="+ dataBid);
		int idx = dataBid.indexOf("[");
		int idxEnd = dataBid.indexOf("<]");
		String sQueue = dataBid.substring(idx + 1, idxEnd);
		String[] array = sQueue.split("!");
		/*if (array.length>1) {
			for (int i = 0; i < array.length; i++) {
				// System.out.println(array[i]);
				String[] sp = array[i].split("\\[");
				Double price = new Double(sp[0]);
				String[] queuee = sp[1].split(">");
				Vector v = new Vector();
				
				for (int j = 0; j < queuee.length; j++) {
					String[] dQueue = queuee[j].split("\\|");
					//Queue q = new Queue(dQueue[0], price, new Double(dQueue[1]),dQueue[3]);
					//Queue q = new Queue(dQueue[0], getBidPriceQueue(), new Double(dQueue[1]),dQueue[3]);
					Queue q = new Queue(dQueue[0], price, new Double(dQueue[1]),dQueue[3]);
					q.setMatch(new Double(dQueue[2].replace("]", "")));
					v.add(q);
					//System.out.println(" q="+ q);
					
				}
				mqueue.put(price, v);
				Log.info("Quote setContentQueueBid ="+ dataBid + " --"+ price);
				//getBidOrder().put(price, v);
				
			}
		}*/
		try {
			for (int i = 0; i < array.length; i++) {
			
				String[] sp = array[i].split("\\|");
				
				int ibid = array[i].indexOf("[");
				int ebid = array[i].indexOf("]");
				
				String sQueBid = array[i].substring(ibid + 1, ebid);
				String[] queBid = array[i].split("\\|");
				
				String spricebid = array[i].substring(0,  ibid);
				
				String lot = queBid[1];
				String marketorderid = queBid[0];
				String time = queBid[3];
				System.out.println("array"+ array[i] + ": spricebid="+ spricebid);
				System.out.println("marketorderid"+ marketorderid + ": time="+ time);
				
				try {					
					//Log.info("setContentQueueuBid:price "+spricebid + " lot"+ lot);	
					Queue q = new Queue(queBid[0], new Double(spricebid), new Double(lot), "");
					q.setLot(new Double(lot));
					q.setPrice(new Double(spricebid));
					q.setMatch(new Double(queBid[1].replace("]", "")));
					//q.setTime(time);
				} catch (Exception exc){
					Log.info("err setContentQueueuBid:"+ exc);		
					exc.printStackTrace();
				}
				
				
				//String key2 = "10|"+ qb.getStock()+"|"+ q.getPrice() +"|"+qb.getBoard();
				//System.out.println("keynya "+ key2);
				//map.put(price, array[i]);
				mqueue.put(spricebid, array[i]);
				//Log.info("setContentQueueBid = "+ spricebid + " isi="+ array[1]);
				
			}
			
		} catch (Exception ex){
			Log.info("err setContentQueueuBid:"+ ex.toString());
			ex.printStackTrace();
		}
	}

	

	
	public void setContent(String data) {
		
		int nstart = 0;
		int nend = 0;
		String sdata = "";
		String[] temp = data.split("\\<");
		String[] header = temp[0].split("\\|");
		super.setContent(header);
		ordertime = header[3].trim();
		stock = header[4].trim();
		board = header[5].trim();
		previous = Double.parseDouble(header[6]);
		bid.clear();
		offer.clear();
		try {
			sdata = temp[1];
			nstart = sdata.indexOf("[") + 1;
			nend = sdata.indexOf("]", nstart);
			String sbid = sdata.substring(nstart, nend);
			String[] sbiddetail = sbid.split("\\>");
			for (int i = 0; i < sbiddetail.length; i++) {
				String[] bidrow = sbiddetail[i].split("\\|");
				Price p = new Price(Double.parseDouble(bidrow[0]),
						Double.parseDouble(bidrow[1]),
						Double.parseDouble(bidrow[2]));
				bid.put(new Double(p.getPrice()), p);
			}
		} catch (Exception ex) {
		}

		try {
			nstart = sdata.indexOf("[", nend) + 1;
			nend = sdata.indexOf("]", nstart);
			String soffer = sdata.substring(nstart, nend);
			String[] soffdetail = soffer.split("\\>");
			for (int i = 0; i < soffdetail.length; i++) {
				String[] offrow = soffdetail[i].split("\\|");
				Price p = new Price(Double.parseDouble(offrow[0]),
						Double.parseDouble(offrow[1]),
						Double.parseDouble(offrow[2]));
				offer.put(new Double(p.getPrice()), p);
			}
		} catch (Exception ex) {
		}

		try {
			nstart = sdata.indexOf("[", nend) + 1;
			nend = sdata.indexOf("]", nstart);
			String slast = sdata.substring(nstart, nend);
			String[] lastrow = slast.split("\\|");
			last = Double.parseDouble(lastrow[0]);
			lastlot = Double.parseDouble(lastrow[1]);
		} catch (Exception ex) {
		}
	}

	
	// IDX|10|CTRA|RG|B[820.0[0242706141|3.0|0.0|093946|0]!780.0[0242707071|3.0|0.0|094015|0]!830.0[0242666624|20.0|0.0|093006|0]!760.0[0242707365|3.0|0.0|094024|0]!720.0[0242707955|3.0|0.0|094043|0]!890.0[0242655826|40.0|0.0|093000|0>0242669646|20.0|0.0|093007|1>0242686322|10.0|0.0|093038|2>0242686682|200.0|0.0|093044|3]!800.0[0242706712|3.0|0.0|094004|0]!700.0[0242708240|3.0|0.0|094052|0]!850.0[0242709382|1.0|0.0|094133|0]!900.0[0242656392|200.0|0.0|093001|0>0242680882|5.0|0.0|093014|1>0242681179|188.0|0.0|093015|2>0242688074|8.0|0.0|093112|3>0242707067|400.0|0.0|094015|4>0242722101|4.0|0.0|094918|5>0242734561|20.0|0.0|095949|6>0242742847|20.0|0.0|100712|7]!880.0[0242655534|50.0|0.0|093000|0>0242671928|5.0|0.0|093008|1>0242671933|5.0|0.0|093008|2>0242683538|200.0|0.0|093019|3>0242692476|500.0|0.0|093300|4]!740.0[0242707647|3.0|0.0|094033|0]!910.0[0242745419|1.0|0.0|100956|0>0242746312|10.0|0.0|101107|1>0242746363|50.0|0.0|101111|2]!840.0[0242705642|2.0|0.0|093932|0]!<]
	
	public String toStringBidOrder() {
		
		StringBuffer result = new StringBuffer(100);
		result.append(header).append(delimiter).append("10").append(delimiter);
		result.append(stock).append(delimiter);
		//result.append(pricequeue).append(delimiter);
		result.append(board).append(delimiter);
		//result.append(pricequeue).append(delimiter);
		//result.append(pricequeue).append(delimiter);
		result.append("B[");
		//Log.info("Quote-toStringBidOrder PRICE= " + super.toString());
		//System.out.println("Quote-toStringBidOrder = " + bidorder.keySet());
		Iterator iter = bidorder.keySet().iterator();
		//System.out.println("toStringBidOrder= "+ result);
		
		while (iter.hasNext()) {
			Double price = (Double) iter.next();
			Vector v = (Vector) bidorder.get(price);
			//if (price.doubleValue() == pricequeue) {
				result.append(price);
				result.append("[");
				for (int i = 0; i < v.size(); i++) {
					Queue queu = (Queue) v.elementAt(i);
					result.append(queu.getId()).append(delimiter);
					result.append(queu.getLot()).append(delimiter);
					result.append(queu.getMatch()).append(delimiter);
					result.append(queu.getTime()).append(delimiter);
					//result.append(queu.getPrice()).append(delimiter);
					result.append(i);

					if (i < (v.size() - 1))
						result.append(">");
				}
				result.append("]!");
			//}
			
		}

		result.append("<]");

		return result.toString();
	}
	public String toStringOfferOrder() {
		StringBuffer result = new StringBuffer(100);
		result.append(header).append(delimiter).append("11").append(delimiter);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		//result.append(pricequeue).append(delimiter);
		result.append("O[");
		Iterator iter =offorder.keySet().iterator();

		while (iter.hasNext()) {
			Double price = (Double) iter.next();
			Vector v = (Vector) offorder.get(price);
			result.append(price);
			result.append("[");
			for (int i = 0; i < v.size(); i++) {
				Queue queu = (Queue) v.elementAt(i);
				result.append(queu.getId()).append(delimiter);
				result.append(queu.getLot()).append(delimiter);
				result.append(queu.getMatch()).append(delimiter);
				result.append(queu.getTime()).append(delimiter);
				result.append(i);

				if (i < (v.size() - 1))
					result.append(">");
			}
			result.append("]!");
		}

		result.append("<]");

		return result.toString();
	}

	// OQ-ASII|ASII<B[13150|80>13100|10>13050|27>13000|308>12950|653>12900|426>12850|406>12800|90>12750|38>12700|95>12650|111>12600|273>12550|101>12500|70>12450|1>12400|31>12350|40>12300|118>12200|25>12100|50>12050|50>12000|20>11600|60>11250|100>]O[13200|452>13250|244>13300|383>13350|130>13400|447>13450|205>13500|483>13550|100>13600|93>13700|25>13750|12>13800|38>14000|9>]L[13150|2]
	public String toString() {
		StringBuffer result = new StringBuffer(100);
		result.append(super.toString()).append(delimiter);
		result.append(ordertime).append(delimiter);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		result.append(format.format(previous)).append("<");
		result.append("B[");

		ArrayList keys = new ArrayList();
		keys.addAll(getBid().keySet());
		Collections.sort(keys, Collections.reverseOrder());
		Iterator it = keys.iterator();
		while (it.hasNext()) {
			Price p = (Price) getBid().get(it.next());
			if (p.getLot() > 0) {
				result.append(format.format(p.getPrice())).append(delimiter);
				result.append(format.format(p.getLot())).append(delimiter);
				result.append(format.format(p.getFreq())).append(">");
			}
		}

		result.append("]");
		result.append("O[");

		keys = new ArrayList();
		keys.addAll(getOffer().keySet());
		Collections.sort(keys);
		it = keys.iterator();
		while (it.hasNext()) {
			Price p = (Price) getOffer().get(it.next());
			if (p.getLot() > 0) {
				result.append(format.format(p.getPrice())).append(delimiter);
				result.append(format.format(p.getLot())).append(delimiter);
				result.append(format.format(p.getFreq())).append(">");
			}
		}

		result.append("]");
		result.append("L[");
		result.append(format.format(last));
		result.append("|");
		result.append(format.format(lastlot));
		result.append("]");
		return result.toString();
	}
	
	public String toStringSummaryByQueue() {
		StringBuffer result = new StringBuffer(100);
		result.append(super.toString()).append(delimiter);
		result.append(ordertime).append(delimiter);
		result.append(stock).append(delimiter);
		result.append(board).append(delimiter);
		result.append(format.format(previous)).append("<");
		result.append("B[");

		ArrayList keys = new ArrayList();
		keys.addAll(getBidOrder().keySet());
		Collections.sort(keys, Collections.reverseOrder());
		Iterator it = keys.iterator();
		while (it.hasNext()) {
			Double price = (Double) it.next();
			Vector v = (Vector) getBidOrder().get(price);
			if (v.size() > 0) {
				double lot = 0;
				for (int i = 0; i < v.size(); i++) {
					Queue q = (Queue) v.elementAt(i);
					lot += q.getLot();
				}
				result.append(format.format(price.doubleValue())).append(
						delimiter);
				result.append(format.format(lot)).append(delimiter);
				result.append(format.format(v.size())).append(">");

			}

		}

		result.append("]");
		result.append("O[");

		keys = new ArrayList();
		keys.addAll(getOffOrder().keySet());
		Collections.sort(keys);
		it = keys.iterator();
		while (it.hasNext()) {
			Double price = (Double) it.next();
			Vector v = (Vector) getOffOrder().get(price);
			if (v != null) {
				double lot = 0;
				for (int i = 0; i < v.size(); i++) {
					Queue q = (Queue) v.elementAt(i);
					lot += q.getLot();
				}
				result.append(format.format(price)).append(delimiter);
				result.append(format.format(lot)).append(delimiter);
				result.append(format.format(v.size())).append(">");

			}

		}

		result.append("]");
		result.append("L[");
		result.append(format.format(last));
		result.append("|");
		result.append(format.format(lastlot));
		result.append("]");

		return result.toString();

	}
	
	/*@Override	
	public void fromProtocol(String[] data){
		header = data[0].trim();
		date = data[1].trim();
		time = (data[2]).trim();
		seqno = Double.parseDouble(data[3]);
		//price = Double.parseDouble(data[4]);
		type = "1";//data[4].trim();				           		
		//type = data[5].trim();
	}
*/
}
