package feed.builder.msg;

public class ChartIntraday extends Message {
	private String transtime;
	private double last;
	private String code;
	
	public ChartIntraday(){
		
	}
	
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(transtime);
		result.append(delimiter).append(code);
		result.append(delimiter).append(format3.format(last));
		return result.toString();		
	}
	
	public void setContent(String[] data){
		super.setContent(data);
		transtime = data[3].trim();
		code = data[4].trim();
		last = Double.parseDouble(data[5]);
	}

	public String getTranstime() {
		return transtime;
	}

	public double getLast() {
		return last;
	}

	public String getCode() {
		return code;
	}

	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
