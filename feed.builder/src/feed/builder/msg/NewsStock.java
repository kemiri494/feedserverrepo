package feed.builder.msg;

public class NewsStock extends Message{
	private String id;
	private String date;
	private String stock;
	private String title;
	
	public NewsStock(){
		
	}
	
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(id);
		result.append(delimiter).append(date);
		result.append(delimiter).append(stock);
		result.append(delimiter).append(title);
		return result.toString();
	}
	public void setContent(String[] data){
		super.setContent(data);
		id= data[3].trim();
		date= data[4].trim();
		stock= data[5].trim();
		title= data[6].trim();
	}
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		id = data[5].trim();
		date = data[6].trim();
		stock = data[7].trim();
		title = data[8].trim();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
}
