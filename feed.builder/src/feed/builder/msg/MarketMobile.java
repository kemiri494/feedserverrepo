package feed.builder.msg;

import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MarketMobile extends Message
{
  private String stock;
  private String board;
  private String remark = "--U3---2";
  private double prev;
  private double high;
  private double low;
  private double close;
  private double change;
  private double tradevol;
  private double tradeval;
  private double tradefreq;
  private double index;
  private double foreign;
  private double open;
  private double bestbid;
  private double bestbidvol;
  private double bestoffer;
  private double bestoffervol;
  private double percent;
  private double value;
  private double Volume;
  private double Freq;

  public String toString()
  {
    StringBuffer result = new StringBuffer(super.toString());
    result.append("|").append(this.stock);
    result.append("|").append(this.board);
    result.append("|").append(this.remark);
    result.append("|").append(format.format(this.prev));
    result.append("|").append(format.format(this.high));
    result.append("|").append(format.format(this.low));
    result.append("|").append(format.format(this.close));
    result.append("|").append(format.format(this.change));
    result.append("|").append(format.format(this.tradevol));
    result.append("|").append(format.format(this.tradeval));
    result.append("|").append(format.format(this.tradefreq));
    result.append("|").append(format.format(this.index));
    result.append("|").append(format.format(this.foreign));
    result.append("|").append(format.format(this.open));
    result.append("|").append(format.format(this.bestbid));
    result.append("|").append(format.format(this.bestbidvol));
    result.append("|").append(format.format(this.bestoffer));
    result.append("|").append(format.format(this.bestoffervol));
    result.append("|").append(format.format(this.percent));
    result.append("|").append(format.format(this.value));
    result.append("|").append(format.format(this.Volume));
    result.append("|").append(format.format(this.Freq));
    return result.toString();
  }

  public void setContent(String[] data)
  {
    super.setContent(data);
    this.stock = data[3].trim();
    this.board = data[4].trim();
    this.remark = data[5].trim();
    this.prev = Double.parseDouble(data[6]);
    this.high = Double.parseDouble(data[7]);
    this.low = Double.parseDouble(data[8]);
    this.close = Double.parseDouble(data[9]);
    this.change = Double.parseDouble(data[10]);
    this.tradevol = Double.parseDouble(data[11]);
    this.tradeval = Double.parseDouble(data[12]);
    this.tradefreq = Double.parseDouble(data[13]);
    this.index = Double.parseDouble(data[14]);
    this.foreign = Double.parseDouble(data[15]);
    this.open = Double.parseDouble(data[16]);
    this.bestbid = Double.parseDouble(data[17]);
    this.bestbidvol = Double.parseDouble(data[18]);
    this.bestoffer = Double.parseDouble(data[19]);
    this.bestoffervol = Double.parseDouble(data[20]);
  }

  public void fromProtocol(String[] data) {
    super.fromProtocol(data);
    this.stock = data[5].trim();
    this.board = data[6].trim();
    this.remark = "--U3---2";
    this.prev = Double.parseDouble(data[7]);
    this.high = Double.parseDouble(data[8]);
    this.low = Double.parseDouble(data[9]);
    this.close = Double.parseDouble(data[10]);
    this.change = Double.parseDouble(data[11]);
    this.tradevol = Double.parseDouble(data[12]);
    this.tradeval = Double.parseDouble(data[13]);
    this.tradefreq = Double.parseDouble(data[14]);
    this.index = Double.parseDouble(data[15]);
    this.foreign = Double.parseDouble(data[16]);
    this.open = Double.parseDouble(data[17]);
    this.bestbid = Double.parseDouble(data[18]);
    this.bestbidvol = Double.parseDouble(data[19]);
    this.bestoffer = Double.parseDouble(data[20]);
    this.bestoffervol = Double.parseDouble(data[21]);
  }

  public String getStock() {
    return this.stock;
  }

  public void setStock(String stock) {
    this.stock = stock;
  }

  public String getBoard() {
    return this.board;
  }

  public void setBoard(String board) {
    this.board = board;
  }

  public String getRemark() {
    return this.remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public double getPrev() {
    return this.prev;
  }

  public void setPrev(double prev) {
    this.prev = prev;
  }

  public double getHigh() {
    return this.high;
  }

  public void setHigh(double high) {
    this.high = high;
  }

  public double getLow() {
    return this.low;
  }

  public void setLow(double low) {
    this.low = low;
  }

  public double getClose() {
    return this.close;
  }

  public void setClose(double close) {
    this.close = close;
  }

  public double getChange() {
    return this.change;
  }

  public void setChange(double change) {
    this.change = change;
  }

  public double getTradevol() {
    return this.tradevol;
  }

  public void setTradevol(double tradevol) {
    this.tradevol = tradevol;
  }

  public double getTradeval() {
    return this.tradeval;
  }

  public void setTradeval(double tradeval) {
    this.tradeval = tradeval;
  }

  public double getTradefreq() {
    return this.tradefreq;
  }

  public void setTradefreq(double tradefreq) {
    this.tradefreq = tradefreq;
  }

  public double getIndex() {
    return this.index;
  }

  public void setIndex(double index) {
    this.index = index;
  }

  public double getForeign() {
    return this.foreign;
  }

  public void setForeign(double foreign) {
    this.foreign = foreign;
  }

  public double getOpen() {
    return this.open;
  }

  public void setOpen(double open) {
    this.open = open;
  }

  public double getBestbid() {
    return this.bestbid;
  }

  public void setBestbid(double bestbid) {
    this.bestbid = bestbid;
  }

  public double getBestbidvol() {
    return this.bestbidvol;
  }

  public void setBestbidvol(double bestbidvol) {
    this.bestbidvol = bestbidvol;
  }

  public double getBestoffer() {
    return this.bestoffer;
  }

  public void setBestoffer(double bestoffer) {
    this.bestoffer = bestoffer;
  }

  public double getBestoffervol() {
    return this.bestoffervol;
  }

  public void setBestoffervol(double bestoffervol) {
    this.bestoffervol = bestoffervol;
  }

  public double getPercent() {
    return this.percent;
  }

  public void setPercent(double percent) {
    this.percent = percent;
  }

  public double getValue() {
    return this.value;
  }

  public void setValue(double value) {
    this.value = value;
  }

  public double getVolume() {
    return this.Volume;
  }

  public void setVolume(double volume) {
    this.Volume = volume;
  }

  public double getFreq() {
    return this.Freq;
  }

  public void setFreq(double freq) {
    this.Freq = freq;
  }

  public void calculate()
  {
    double nlast = getClose();
    double nprev = getPrev();
    double nchange = nlast - nprev;
    if (nlast == 0.0D) {
      setChange(new Double(0.0D).doubleValue());
      setPercent(new Double(0.0D).doubleValue());
    } else {
      setChange(new Double(nchange).doubleValue());
      if (nprev != 0.0D) {
        double persen = nchange * 100.0D / nprev;
        setPercent(new Double(persen).doubleValue());
      } else {
        setPercent(new Double(0.0D).doubleValue());
      }
    }
  }

  public static void main(String[] args) {
    NumberFormat format2 = new DecimalFormat("####0");
    String sdouble = "000000426.7953";

    double dd = Double.parseDouble(sdouble);
    String sdd = format2.format(dd);
    System.out.println(Double.parseDouble(sdd) + ":" + dd + ":" + sdd);
  }
}