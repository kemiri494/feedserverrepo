package feed.builder.msg;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TopGainer extends Message{


	private String stock;
	private String board;
	private String remark="--U3---2";
	private double prev;
	private double high;
	private double low;
	private double close;
	private double change;
	private double tradevol;
	private double tradeval;
	private double tradefreq;
	private double index;
	private double foreign;
	private double open;
	private double bestbid;
	private double bestbidvol;
	private double bestoffer;
	private double bestoffervol;
	private double percent;
	private String name = "";
	private String sector = "";
	
	public TopGainer() {
		// TODO Auto-generated constructor stub
	}
	
	public String  toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(stock);
		result.append(delimiter).append(board);
		result.append(delimiter).append(remark);
		result.append(delimiter).append(format.format(prev));
		result.append(delimiter).append(format.format(high));
		result.append(delimiter).append(format.format(low));
		result.append(delimiter).append(format.format(close));
		result.append(delimiter).append(format.format(change));
		result.append(delimiter).append(format.format(tradevol));
		result.append(delimiter).append(format.format(tradeval));
		result.append(delimiter).append(format.format(tradefreq));
		result.append(delimiter).append(format.format(index));
		result.append(delimiter).append(format.format(foreign));
		result.append(delimiter).append(format.format(open));
		result.append(delimiter).append(format.format(bestbid));
		result.append(delimiter).append(format.format(bestbidvol));
		result.append(delimiter).append(format.format(bestoffer));
		result.append(delimiter).append(format.format(bestoffervol));
		result.append(delimiter).append(format.format(percent));
		result.append(delimiter).append(name);
		result.append(delimiter).append(sector);
		return result.toString();
	}

	
	public void setContent(String[] data){
		super.setContent(data);
		stock = data[3].trim();
		board = data[4].trim();
		remark = data[5].trim();
		prev = Double.parseDouble(data[6]);
		high = Double.parseDouble(data[7]);
		low = Double.parseDouble(data[8]);
		close = Double.parseDouble(data[9]);
		change = Double.parseDouble(data[10]);
		tradevol = Double.parseDouble(data[11]);
		tradeval = Double.parseDouble(data[12]);
		tradefreq = Double.parseDouble(data[13]);
		index = Double.parseDouble(data[14]);
		foreign = Double.parseDouble(data[15]);
		open = Double.parseDouble(data[16]);
		bestbid = Double.parseDouble(data[17]);
		bestbidvol = Double.parseDouble(data[18]);
		bestoffer = Double.parseDouble(data[19]);
		bestoffervol = Double.parseDouble(data[20]);
	}
	
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		stock = data[5].trim();
		board = data[6].trim();
		//remark = data[7].trim();
		remark = "--U3---2";
/*		prev = Double.parseDouble(data[8]);
		high = Double.parseDouble(data[9]);
		low = Double.parseDouble(data[10]);
		close = Double.parseDouble(data[11]);
		change = Double.parseDouble(data[12]);
		tradevol = Double.parseDouble(data[13]);
		tradeval = Double.parseDouble(data[14]);
		tradefreq = Double.parseDouble(data[15]);
		index = Double.parseDouble(data[16]);
		foreign = Double.parseDouble(data[17]);
		open = Double.parseDouble(data[18]);
		bestbid = Double.parseDouble(data[19]);
		bestbidvol = Double.parseDouble(data[20]);
		bestoffer = Double.parseDouble(data[21]);
		bestoffervol = Double.parseDouble(data[22]);		
*/
		prev = Double.parseDouble(data[7]);
		high = Double.parseDouble(data[8]);
		low = Double.parseDouble(data[9]);
		close = Double.parseDouble(data[10]);
		change = Double.parseDouble(data[11]);
		tradevol = Double.parseDouble(data[12]);
		tradeval = Double.parseDouble(data[13]);
		tradefreq = Double.parseDouble(data[14]);
		index = Double.parseDouble(data[15]);
		foreign = Double.parseDouble(data[16]);
		open = Double.parseDouble(data[17]);
		bestbid = Double.parseDouble(data[18]);
		bestbidvol = Double.parseDouble(data[19]);
		bestoffer = Double.parseDouble(data[20]);
		bestoffervol = Double.parseDouble(data[21]);	
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getPrev() {
		return prev;
	}

	public void setPrev(double prev) {
		this.prev = prev;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}

	public double getTradevol() {
		return tradevol;
	}

	public void setTradevol(double tradevol) {
		this.tradevol = tradevol;
	}

	public double getTradeval() {
		return tradeval;
	}

	public void setTradeval(double tradeval) {
		this.tradeval = tradeval;
	}

	public double getTradefreq() {
		return tradefreq;
	}

	public void setTradefreq(double tradefreq) {
		this.tradefreq = tradefreq;
	}

	public double getIndex() {
		return index;
	}

	public void setIndex(double index) {
		this.index = index;
	}

	public double getForeign() {
		return foreign;
	}

	public void setForeign(double foreign) {
		this.foreign = foreign;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getBestbid() {
		return bestbid;
	}

	public void setBestbid(double bestbid) {
		this.bestbid = bestbid;
	}

	public double getBestbidvol() {
		return bestbidvol;
	}

	public void setBestbidvol(double bestbidvol) {
		this.bestbidvol = bestbidvol;
	}

	public double getBestoffer() {
		return bestoffer;
	}

	public void setBestoffer(double bestoffer) {
		this.bestoffer = bestoffer;
	}

	public double getBestoffervol() {
		return bestoffervol;
	}

	public void setBestoffervol(double bestoffervol) {
		this.bestoffervol = bestoffervol;
	}
	
	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}
	
	public void calculate(){
		
		 double nlast = getClose();
	        double    nprev = getPrev();
	        double nchange = nlast - nprev;
	        if (nlast==0) {
	            setChange(new Double(0));
	            setPercent(new Double(0));
	        } else {
	            setChange(new Double(nchange));
	            if  (nprev != 0){
	                double  persen = (nchange * 100) / nprev;
	                setPercent(new Double(persen));
	            } else {
	                setPercent(new Double(0));
	            }
	        }
	}

	public String getName() {
		return name;
	}

	public String getSector() {
		return sector;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public static void main(String[] args){
		NumberFormat format2= new DecimalFormat("####0");
		String sdouble = "000000426.7953";
		
		double dd = Double.parseDouble(sdouble);
		String sdd = format2.format(dd);
		System.out.println(Double.parseDouble(sdd)+":"+dd+":"+sdd);
	}
	
}
