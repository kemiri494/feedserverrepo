package feed.builder.msg;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CorpAction extends Message {
	private String actiontype;
	private String stock;
	private double amount;
	//private String amount;
	private String ratio1;
	private String ratio2;
	private String cumdate;
	private String exdate;
	private String recordingdate;
	private String distdate;
	private String transactiondate;
	private String lasttransactiondate;
	private BigDecimal temp;
	
	public CorpAction() {
		
	}
	
	public void setContent(String[] data) {
		super.setContent(data);
		actiontype = data[3].trim();
		stock = data[4].trim();
		temp =  BigDecimal.valueOf(Double.parseDouble(data[5]));
		amount = temp.doubleValue();
		//amount = data[5].trim();
		ratio1 = data[6].trim();
		ratio2 = data[7].trim();
		cumdate = data[8].trim();
		exdate = data[9].trim();
		recordingdate = data[10].trim();
		distdate = data[11].trim();
		transactiondate=data[12].trim();
		lasttransactiondate=data[13].trim();         
	}
	
public void fromProtocol(String[] data) {
	//System.out.println("from protocol");
	super.fromProtocol(data);
	actiontype = data[5].trim();
	stock = data[6].trim();
	temp =  BigDecimal.valueOf(Double.parseDouble(data[7]));
	amount = temp.doubleValue();
	//amount = Double.parseDouble(data[7]);
	//amount = data[7].trim();
	ratio1 = data[8].trim();  
	ratio2 = data[9].trim(); 
	cumdate = data[10].trim();
	exdate = data[11].trim();
	recordingdate = data[12].trim();
	distdate = data[13].trim();	
	transactiondate=data[14].trim();
	lasttransactiondate=data[15].trim();
}
	
	public String toString() {
		//System.out.println("toString");
		StringBuffer result = new StringBuffer(super.toString()).append(delimiter).append(actiontype).append(delimiter).append(stock);
		//result.append(delimiter).append(format.format(amount));
		result.append(delimiter).append(temp.setScale(4, RoundingMode.FLOOR));
		result.append(delimiter).append(ratio1);
		result.append(delimiter).append(ratio2);
		result.append(delimiter).append(cumdate);
		result.append(delimiter).append(exdate);
		result.append(delimiter).append(recordingdate);
		result.append(delimiter).append(distdate);
		result.append(delimiter).append(transactiondate);
		result.append(delimiter).append(lasttransactiondate);
		//System.out.println(result.toString());
		return result.toString();
	}

	public String getActiontype() {
		return actiontype;
	}

	public void setActiontype(String actiontype) {
		this.actiontype = actiontype;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public double getAmount() {
		amount = temp.doubleValue();		
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
		this.temp = BigDecimal.valueOf(amount).setScale(4,RoundingMode.FLOOR);		
	}

	public String getRatio1() {
		return ratio1;
	}

	public void setRatio1(String ratio1) {
		this.ratio1 = ratio1;
	}

	public String getRatio2() {
		return ratio2;
	}

	public void setRatio2(String ratio2) {
		this.ratio2 = ratio2;
	}

	public String getCumdate() {
		return cumdate;
	}

	public void setCumdate(String cumdate) {
		this.cumdate = cumdate;
	}

	public String getExdate() {
		return exdate;
	}

	public void setExdate(String exdate) {
		this.exdate = exdate;
	}

	public String getRecordingdate() {
		return recordingdate;
	}

	public void setRecordingdate(String recordingdate) {
		this.recordingdate = recordingdate;
	}

	public String getDistdate() {
		return distdate;
	}

	public void setDistdate(String distdate) {
		this.distdate = distdate;
	}
	public String getTransactiondate() {
		return transactiondate;
	}

	public void setTransactiondate(String transactiondate) {
		this.transactiondate = transactiondate;
	}
	public String getLasttransactiondate() {
		return lasttransactiondate;
	}

	public void setLasttransactiondate(String lasttransactiondate) {
		this.lasttransactiondate = lasttransactiondate;
	}

	
}
