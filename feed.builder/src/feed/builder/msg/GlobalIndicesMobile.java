package feed.builder.msg;

public class GlobalIndicesMobile extends GlobalIndices{
	private int	urut; 
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());	
		result.append(delimiter).append(getUrut());
		return result.toString();
	}
	public int getUrut() {
		return urut;
	}
	public void setUrut(int urut) {
		this.urut = urut;
	}
}
