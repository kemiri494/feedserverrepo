package feed.builder.msg;

public class Sector extends Message{
	private double code;
	private String name;
	
	public Sector(){
		
	}
	public String toString(){
		StringBuffer result = new StringBuffer(super.toString());
		result.append(delimiter).append(format.format(code));
		result.append(delimiter).append(name);
		return result.toString();
		
	}
	public void setContent(String[] data){
		super.setContent(data);
		code=Double.parseDouble(data[3]);
		name=data[4].trim();
	}
	public void fromProtocol(String[] data){
		super.fromProtocol(data);
		code=Double.parseDouble(data[5]);
		name=data[6].trim();
	}
	public Double getCode() {
		return code;
	}
	public void setCode(Double code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) { 
		this.name = name;
	}
	
	
}
