package feed.builder.builder;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.query.Query;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.MsgBuilder;
import feed.builder.msg.Broker;
import feed.builder.msg.BrokerSummarry;
import feed.builder.msg.CorpAction;
import feed.builder.msg.Indices;
import feed.builder.msg.Message;
import feed.builder.msg.News;
import feed.builder.msg.Stock;
import feed.builder.msg.StockSummary;
import feed.builder.msg.Trade;
import feed.builder.msg.TradePrice;
import feed.builder.msg.TradeSummBroker;
import feed.builder.msg.TradeSummBrokerStock;
import feed.builder.msg.TradeSummInv;
import feed.builder.msg.TradeSummStockBroker;
import feed.builder.msg.TradeSummStockInv;
import feed.builder.msg.TradeSummary;
import feed.provider.data.FeedMsg;

public class OtherBuilder extends MsgBuilder {
	protected HashMap indicesMap;
	protected HashMap brokerMap;
	protected HashMap actionMap;
	protected HashMap newsMap;
	protected HashMap stockMap;
	protected HashMap ssMap;
	protected HashMap tradepriceMap;
	protected HashMap tsMap;
	
	//summ broker, brokerstock, stockbroker, inv, stockinv
	protected HashMap tsBroker;
	protected HashMap tsBrokerStock;
	protected HashMap tsStockBroker;
	protected HashMap tsBrokerSumm;
	protected HashMap tsInv;
	protected HashMap tsStockInv;	
	protected FeedMsg msg2;
	private Logger logIndices = LoggerFactory.getLogger("log.feed.builder.builder.logIndices");
	
	public OtherBuilder(FeedConsumer consumer, FeedBuilder builder, int port) throws Exception{
		super("", "0", consumer, builder, port);
	}
	
	public OtherBuilder(FeedConsumer consumer, FeedBuilder builder) throws Exception{
		super("", "0", consumer, builder);
	}
	
	public List getSnapShot(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(Message.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		/*for (int i = 0; i < o.size(); i++) {
			if (o.get(i) instanceof StockSummary) {
				System.out.println(o.get(i) );	
			}
		}*/
		log.info("request snapshot for type Other with seqno: "+seqno+" and result: "+(o!=null?o.size()+"" :"0")+ " record(s)");
		return o;
	}
	
	protected void loadConfig(){
		super.loadConfig();
		indicesMap = new HashMap(500,1);	//key index
		brokerMap = new HashMap(500,1);		//key brokercode
		actionMap = new HashMap(500,1);		//key actiontype+stock
		newsMap = new HashMap(500,1);		//key no
		stockMap = new HashMap(500,1);		//key stockcode
		ssMap = new HashMap(500,1);				//key stock+board
		tradepriceMap = new HashMap(500,1);				//key stock+board
		tsMap = new HashMap(500,20);				//key stock+board+broker
		//summ broker, brokerstock, stockbroker, inv, stockinv
		tsBroker = new HashMap(500,1);	//key broker
		tsBrokerStock = new HashMap(500,10); //key broker
		tsStockBroker = new HashMap(500,10); //key stock
		tsInv = new HashMap(2, 1); //key inv
		tsStockInv = new HashMap(500,1);  //key stock+inv
		
		msg2 = new FeedMsg();
		msg2.setType("SETTING2");
		List o =  db.getDb().getInstance().queryByExample(msg2);
		if (o.size()>0){
			msg2 = (FeedMsg)o.get(0);
			msg2.setSeqno(msg2.getMsg().equals(formatDate.format(new Date()))? msg2.getSeqno() : 0);
			msg2.setMsg(formatDate.format(new Date()));
		} else {
			msg2.setMsg(formatDate.format(new Date()));
			msg2.setSeqno(0);
		}
		List listIndices = db.query(Indices.class);
		for (int i=0; i<listIndices.size();i++){
			Indices q = (Indices)listIndices.get(i);
			indicesMap.put(q.getCode(), q);
		}		
		List listBroker = db.query(Broker.class);
		for (int i=0; i<listBroker.size();i++){
			Broker q = (Broker)listBroker.get(i);
			brokerMap.put(q.getCode(), q);
		}		
		List listAction = db.query(CorpAction.class);
		for (int i=0; i<listAction.size();i++){
			CorpAction q = (CorpAction)listAction.get(i);
			actionMap.put(q.getActiontype()+q.getStock(), q);
		}		
		List listNews = db.query(News.class);
		for (int i=0; i<listNews.size();i++){
			News q = (News)listNews.get(i);
			newsMap.put(q.getNo(), q);
		}		
		List listStock = db.query(Stock.class);
		for (int i=0; i<listStock.size();i++){
			Stock q = (Stock)listStock.get(i);
			stockMap.put(q.getCode(), q);
		}		
		List listSS = db.query(StockSummary.class);
		for (int i=0; i<listSS.size();i++){ 
			StockSummary q = (StockSummary)listSS.get(i);
			ssMap.put(q.getStock()+q.getBoard(), q);
			builder.updateOpen(q);
		}		
		List listTradeprice = db.query(TradePrice.class);
		for (int i=0; i<listTradeprice.size();i++){
			TradePrice q = (TradePrice)listTradeprice.get(i);
			tradepriceMap.put(q.getStock()+q.getBoard(), q);
		}		
		List listTS = db.query(TradeSummary.class);
		for (int i=0; i<listTS.size();i++){
			TradeSummary q = (TradeSummary)listTS.get(i);
			tsMap.put(q.getStock()+q.getBoard()+q.getBroker()+q.getInvestor(), q);
		}	
		List list = db.query(TradeSummBroker.class);
		for (int i=0; i<list.size();i++){
			TradeSummBroker q = (TradeSummBroker)list.get(i);
			tsBroker.put(q.getBroker(), q);
		}		
		List listbs = db.query(TradeSummBrokerStock.class);
		for (int i=0; i<listbs.size();i++){
			TradeSummBrokerStock q = (TradeSummBrokerStock)listbs.get(i);
			tsBrokerStock.put(q.getBroker()+q.getStock(), q);
		}		
		List listinv = db.query(TradeSummInv.class);
		for (int i=0; i<listinv.size();i++){
			TradeSummInv q = (TradeSummInv)listinv.get(i);
			tsInv.put(q.getInvestor(), q);
		}		
		List listsb = db.query(TradeSummStockBroker.class);
		for (int i=0; i<listsb.size();i++){
			TradeSummStockBroker q = (TradeSummStockBroker)listsb.get(i);
			tsStockBroker.put(q.getStock()+q.getBroker(), q);
		}	
		List listinv1 = db.query(TradeSummStockInv.class);
		for (int i=0; i<listinv1.size();i++){
			TradeSummStockInv q = (TradeSummStockInv)listinv1.get(i);
			tsStockInv.put(q.getStock()+q.getInvestor(), q);
		}		
	}
	
	public boolean connect() throws Exception {
		consumer.getTradeConsumer().subscribe(this,"","",(int)msg.getSeqno());
		//consumer.getOrderConsumer().subscribe(this,"","",0);
		consumer.getOtherConsumer().subscribe(this,"","",(int)msg2.getSeqno());
		return true;
	}
	
	public void exit() throws Exception {
		consumer.getTradeConsumer().unsubscribe(this);
		//consumer.getOrderConsumer().unsubscribe(this);
		consumer.getOtherConsumer().unsubscribe(this);
		this.close();
	}
	
	protected void processTradePrice(String[] data){		
		//proses trade summary
		//System.out.println("GET LOT + " +consumer.getLot());
		String key = data[7].trim()+data[8].trim()+format.format(Double.parseDouble(data[10]));  //keys : stock+board
		if (data[6].trim().equals("0")){
			TradePrice tp  = (TradePrice) tradepriceMap.get(key);  
	    	if (tp==null) {
	    		tp = new TradePrice();
	    		tp.setStock(data[7].trim());
	        	tp.setBoard(data[8].trim());
	        	tp.setPrice(Double.parseDouble(data[10]));
	        	tp.setLot(Double.parseDouble(data[11])/consumer.getLot());
	        	tp.setFreq(1);
	    	} else {
	        	tp.setLot(tp.getLot()+Double.parseDouble(data[11])/consumer.getLot());
	        	tp.setFreq(tp.getFreq()+1);
	    	}
	    	tp.fromProtocol(data);
	    	tp.setType("TP");
	    	tradepriceMap.put(key, tp);
	    	addSnapShot(tp);
	    	broadcast(tp.toString());
	        //System.out.println("trade price--> "+key+"  "+tp.getLot()+ " "+tp.getFreq()+" -->"+ tp.toString());
		}
	}
	
	protected void processTrade(String[] data){
    	if (data[6].equals("0")) {
			    	Trade trade  = new Trade();						    	
			    	trade.fromProtocol(data);
			    	StockSummary ss = new StockSummary();
			    	ss.setStock(trade.getStock());
			    	ss.setBoard(trade.getBoard());
			    	ss.setOpen(trade.getPrice());
			    	ss = builder.getSS(trade.getStock()+"#"+trade.getBoard());
			    	trade.setPrevious(ss == null ? trade.getPrevious() : ss.getPrev());
			    	trade.setType("TH");
		        	addSnapShot(trade);
		        	broadcast(trade.toString());
    	}
	}

	protected void processTradeSummBroker(String[] data){		
		if (data[6].trim().equals("0")){
			//proses trade summary buyer
			String key = data[12].trim();  //keys : broker;
			boolean equal = data[12].trim().equals(data[14].trim());
			TradeSummBroker buyer  = (TradeSummBroker) tsBroker.get(key);  
			double vol = Double.parseDouble(data[11]);
			double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
	    	if (buyer==null) {
	    		buyer = new TradeSummBroker();
	        	buyer.setBroker(data[12].trim());
	        	buyer.setBuyvol(vol);
	        	buyer.setBuyval(val);
	        	buyer.setBuyfreq(1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getBuyvol());
	        		buyer.setSellval(buyer.getBuyval());
	        		buyer.setSellfreq(buyer.getBuyfreq());
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	} else {
		        	buyer.setSellvol(0);
		        	buyer.setSellval(0);
		        	buyer.setSellfreq(0);
		        	buyer.setSellavg(0);
	        	}
	    	} else {
	    		buyer.setBuyvol(buyer.getBuyvol() + vol);
	    		buyer.setBuyval(buyer.getBuyval() + val);
	    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getSellvol() + vol);
	        		buyer.setSellval(buyer.getSellval() + val);
	        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	}
	    	}
	    	buyer.fromProtocol(data);
	    	buyer.setType("TSBB");
	    	tsBroker.put(key, buyer);
	    	addSnapShot(buyer);
	    	broadcast(buyer.toString());

	        //proses trade summary seller
	        if (!equal) {
					key = data[14].trim();  //keys : broker
					TradeSummBroker seller  = (TradeSummBroker) tsBroker.get(key);  
			    	if (seller==null) {
			    		seller = new TradeSummBroker();
			        	seller.setBroker(data[14].trim());
			        	seller.setSellvol(vol);
			        	seller.setSellval(val);
			        	seller.setSellfreq(1);
			        	seller.setBuyvol(0);
			        	seller.setBuyval(0);
			        	seller.setBuyfreq(0);
			        	seller.setBuyavg(0);
			    	} else {
			    		seller.setSellvol(seller.getSellvol() + vol);
			    		seller.setSellval(seller.getSellval() + val);
			    		seller.setSellfreq(seller.getSellfreq() + 1);
			    	}
	        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
			    	seller.fromProtocol(data);
			    	seller.setType("TSBB");
			    	tsBroker.put(key, seller);
			    	addSnapShot(seller);
			    	broadcast(seller.toString());
	        }
		}
	}
	
	protected void processTradeSummBrokerStock(String[] data){		
			if (data[6].trim().equals("0")){
				//proses trade summary buyer
				String key = data[12].trim()+data[7].trim();//keys : broker+stock;
				boolean equal = data[12].trim().equals(data[14].trim());
				TradeSummBrokerStock buyer  = (TradeSummBrokerStock) tsBrokerStock.get(key);  
				double vol = Double.parseDouble(data[11]);
				double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
		    	if (buyer==null) {
		    		buyer = new TradeSummBrokerStock();
		    		buyer.setStock(data[7].trim());
		        	buyer.setBroker(data[12].trim());
		        	buyer.setBuyvol(vol);
		        	buyer.setBuyval(val);
		        	buyer.setBuyfreq(1);
		        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
		        	if (equal){
		        		buyer.setSellvol(buyer.getBuyvol());
		        		buyer.setSellval(buyer.getBuyval());
		        		buyer.setSellfreq(buyer.getBuyfreq());
		        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
		        	} else {
			        	buyer.setSellvol(0);
			        	buyer.setSellval(0);
			        	buyer.setSellfreq(0);
			        	buyer.setSellavg(0);
		        	}
		    	} else {
		    		buyer.setBuyvol(buyer.getBuyvol() + vol);
		    		buyer.setBuyval(buyer.getBuyval() + val);
		    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
		        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
		        	if (equal){
		        		buyer.setSellvol(buyer.getSellvol() + vol);
		        		buyer.setSellval(buyer.getSellval() + val);
		        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
		        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
		        	}
		    	}
		    	buyer.fromProtocol(data);
		    	buyer.setType("TSBS");
		    	tsBrokerStock.put(key, buyer);
		    	addSnapShot(buyer);
		    	broadcast(buyer.toString());
		
		        //proses trade summary seller
		        if (!equal) {
						key = data[14].trim()+data[7].trim();  //keys : broker+stock
						TradeSummBrokerStock seller  = (TradeSummBrokerStock) tsBrokerStock.get(key);  
				    	if (seller==null) {
				    		seller = new TradeSummBrokerStock();
				    		seller.setStock(data[7].trim());
				        	seller.setBroker(data[14].trim());
				        	seller.setSellvol(vol);
				        	seller.setSellval(val);
				        	seller.setSellfreq(1);
				        	seller.setBuyvol(0);
				        	seller.setBuyval(0);
				        	seller.setBuyfreq(0);
				        	seller.setBuyavg(0);
				    	} else {
				    		seller.setSellvol(seller.getSellvol() + vol);
				    		seller.setSellval(seller.getSellval() + val);
				    		seller.setSellfreq(seller.getSellfreq() + 1);
				    	}
		        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
				    	seller.fromProtocol(data);
				    	seller.setType("TSBS");
				    	tsBrokerStock.put(key, seller);
				    	addSnapShot(seller);
				    	broadcast(seller.toString());
		        }
			}
	}
	
	protected void processTradeSummStockBroker(String[] data){		
		if (data[6].trim().equals("0")){
			//proses trade summary buyer
			String key = data[7].trim()+data[12].trim();//keys : stock+broker;
			boolean equal = data[12].trim().equals(data[14].trim());
			TradeSummStockBroker buyer  = (TradeSummStockBroker) tsStockBroker.get(key);  
			double vol = Double.parseDouble(data[11]);
			double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
	    	if (buyer==null) {
	    		buyer = new TradeSummStockBroker();
	    		buyer.setStock(data[7].trim());
	        	buyer.setBroker(data[12].trim());
	        	buyer.setBuyvol(vol);
	        	buyer.setBuyval(val);
	        	buyer.setBuyfreq(1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getBuyvol());
	        		buyer.setSellval(buyer.getBuyval());
	        		buyer.setSellfreq(buyer.getBuyfreq());
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	} else {
		        	buyer.setSellvol(0);
		        	buyer.setSellval(0);
		        	buyer.setSellfreq(0);
		        	buyer.setSellavg(0);
	        	}
	    	} else {
	    		buyer.setBuyvol(buyer.getBuyvol() + vol);
	    		buyer.setBuyval(buyer.getBuyval() + val);
	    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getSellvol() + vol);
	        		buyer.setSellval(buyer.getSellval() + val);
	        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	}
	    	}
	    	buyer.fromProtocol(data);
	    	buyer.setType("TSSB");
	    	tsStockBroker.put(key, buyer);
	    	addSnapShot(buyer);
	    	broadcast(buyer.toString());
	
	        //proses trade summary seller
	        if (!equal) {
					key = data[7].trim()+data[14].trim();  //keys : stock+broker
					TradeSummStockBroker seller  = (TradeSummStockBroker) tsStockBroker.get(key);  
			    	if (seller==null) {
			    		seller = new TradeSummStockBroker();
			    		seller.setStock(data[7].trim());
			        	seller.setBroker(data[14].trim());
			        	seller.setSellvol(vol);
			        	seller.setSellval(val);
			        	seller.setSellfreq(1);
			        	seller.setBuyvol(0);
			        	seller.setBuyval(0);
			        	seller.setBuyfreq(0);
			        	seller.setBuyavg(0);
			    	} else {
			    		seller.setSellvol(seller.getSellvol() + vol);
			    		seller.setSellval(seller.getSellval() + val);
			    		seller.setSellfreq(seller.getSellfreq() + 1);
			    	}
	        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
			    	seller.fromProtocol(data);
			    	seller.setType("TSSB");
			    	tsStockBroker.put(key, seller);
			    	addSnapShot(seller);
			    	broadcast(seller.toString());
	        }
		}
	}
	
	
	protected void processTradeSummInv(String[] data){		
			if (data[6].trim().equals("0")){
				//proses trade summary buyer
				String key = data[13].trim();  //keys : inv;
				boolean equal = data[13].trim().equals(data[15].trim());
				TradeSummInv buyer  = (TradeSummInv) tsInv.get(key);  
				double vol = Double.parseDouble(data[11]);
				double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
		    	if (buyer==null) {
		    		buyer = new TradeSummInv();
		        	buyer.setInvestor(data[13].trim());
		        	buyer.setBuyvol(vol);
		        	buyer.setBuyval(val);
		        	buyer.setBuyfreq(1);
		        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
		        	if (equal){
		        		buyer.setSellvol(buyer.getBuyvol());
		        		buyer.setSellval(buyer.getBuyval());
		        		buyer.setSellfreq(buyer.getBuyfreq());
		        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
		        	} else {
			        	buyer.setSellvol(0);
			        	buyer.setSellval(0);
			        	buyer.setSellfreq(0);
			        	buyer.setSellavg(0);
		        	}
		    	} else {
		    		buyer.setBuyvol(buyer.getBuyvol() + vol);
		    		buyer.setBuyval(buyer.getBuyval() + val);
		    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
		        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
		        	if (equal){
		        		buyer.setSellvol(buyer.getSellvol() + vol);
		        		buyer.setSellval(buyer.getSellval() + val);
		        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
		        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
		        	}
		    	}
		    	buyer.fromProtocol(data);
		    	buyer.setType("TSI");
		    	tsInv.put(key, buyer);
		    	addSnapShot(buyer);
		    	broadcast(buyer.toString());
		
		        //proses trade summary seller
		        if (!equal) {
						key = data[15].trim();  //keys : investortype
						TradeSummInv seller  = (TradeSummInv) tsInv.get(key);  
				    	if (seller==null) {
				    		seller = new TradeSummInv();
				        	seller.setInvestor(data[15].trim());
				        	seller.setSellvol(vol);
				        	seller.setSellval(val);
				        	seller.setSellfreq(1);
				        	seller.setBuyvol(0);
				        	seller.setBuyval(0);
				        	seller.setBuyfreq(0);
				        	seller.setBuyavg(0);
				    	} else {
				    		seller.setSellvol(seller.getSellvol() + vol);
				    		seller.setSellval(seller.getSellval() + val);
				    		seller.setSellfreq(seller.getSellfreq() + 1);
				    	}
		        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
				    	seller.fromProtocol(data);
				    	seller.setType("TSI");
				    	tsInv.put(key, seller);
				    	addSnapShot(seller);
				    	broadcast(seller.toString());
		        }
			}
		}

	protected void processTradeSummStockInv(String[] data){		
		if (data[6].trim().equals("0")){
			//proses trade summary buyer
			String key = data[7].trim()+data[13].trim();  //keys : inv;
			boolean equal = data[13].trim().equals(data[15].trim());
			TradeSummStockInv buyer  = (TradeSummStockInv) tsStockInv.get(key);  
			double vol = Double.parseDouble(data[11]);
			double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
	    	if (buyer==null) {
	    		buyer = new TradeSummStockInv();
	    		buyer.setStock(data[7].trim());
	        	buyer.setInvestor(data[13].trim());
	        	buyer.setBuyvol(vol);
	        	buyer.setBuyval(val);
	        	buyer.setBuyfreq(1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getBuyvol());
	        		buyer.setSellval(buyer.getBuyval());
	        		buyer.setSellfreq(buyer.getBuyfreq());
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	} else {
		        	buyer.setSellvol(0);
		        	buyer.setSellval(0);
		        	buyer.setSellfreq(0);
		        	buyer.setSellavg(0);
	        	}
	    	} else {
	    		buyer.setBuyvol(buyer.getBuyvol() + vol);
	    		buyer.setBuyval(buyer.getBuyval() + val);
	    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getSellvol() + vol);
	        		buyer.setSellval(buyer.getSellval() + val);
	        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	}
	    	}
	    	buyer.fromProtocol(data);
	    	buyer.setType("TSSI");
	    	tsStockInv.put(key, buyer);
	    	addSnapShot(buyer);
	    	broadcast(buyer.toString());
	
	        //proses trade summary seller
	        if (!equal) {
					key = data[7].trim()+data[15].trim();  //keys : stock+investortype
					TradeSummStockInv seller  = (TradeSummStockInv) tsStockInv.get(key);  
			    	if (seller==null) {
			    		seller = new TradeSummStockInv();
			    		seller.setStock(data[7].trim());
			        	seller.setInvestor(data[15].trim());
			        	seller.setSellvol(vol);
			        	seller.setSellval(val);
			        	seller.setSellfreq(1);
			        	seller.setBuyvol(0);
			        	seller.setBuyval(0);
			        	seller.setBuyfreq(0);
			        	seller.setBuyavg(0);
			    	} else {
			    		seller.setSellvol(seller.getSellvol() + vol);
			    		seller.setSellval(seller.getSellval() + val);
			    		seller.setSellfreq(seller.getSellfreq() + 1);
			    	}
	        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
			    	seller.fromProtocol(data);
			    	seller.setType("TSSI");
			    	tsStockInv.put(key, seller);
			    	addSnapShot(seller);
			    	broadcast(seller.toString());
	        }
		}
	}
	
	
	protected void processTradeSumm(String[] data){		
		if (data[6].trim().equals("0")){
			//proses trade summary buyer
			String key = data[7].trim()+data[8].trim()+data[12].trim()+data[13].trim();  //keys : stock+board+broker+invtype;
			boolean equal = data[12].trim().equals(data[14].trim()) && data[13].trim().equals(data[15].trim());
			TradeSummary buyer  = (TradeSummary) tsMap.get(key);  
			double vol = Double.parseDouble(data[11]);
			double val = Double.parseDouble(data[10]) * Double.parseDouble(data[11]);
	    	if (buyer==null) {
	    		buyer = new TradeSummary();
	    		buyer.setStock(data[7].trim());
	        	buyer.setBoard(data[8].trim());
	        	buyer.setBroker(data[12].trim());
	        	buyer.setInvestor(data[13].trim());
	        	buyer.setBuyvol(vol);
	        	buyer.setBuyval(val);
	        	buyer.setBuyfreq(1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getBuyvol());
	        		buyer.setSellval(buyer.getBuyval());
	        		buyer.setSellfreq(buyer.getBuyfreq());
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	} else {
		        	buyer.setSellvol(0);
		        	buyer.setSellval(0);
		        	buyer.setSellfreq(0);
		        	buyer.setSellavg(0);
	        	}
	    	} else {
	    		buyer.setBuyvol(buyer.getBuyvol() + vol);
	    		buyer.setBuyval(buyer.getBuyval() + val);
	    		buyer.setBuyfreq(buyer.getBuyfreq() + 1);
	        	buyer.setBuyavg(buyer.getBuyval() / buyer.getBuyvol());
	        	if (equal){
	        		buyer.setSellvol(buyer.getSellvol() + vol);
	        		buyer.setSellval(buyer.getSellval() + val);
	        		buyer.setSellfreq(buyer.getSellfreq() + 1);	        		
	        		buyer.setSellavg(buyer.getSellval() / buyer.getSellvol());
	        	}
	    	}
	    	buyer.fromProtocol(data);
	    	buyer.setType("TS");
	    	tsMap.put(key, buyer);
	    	addSnapShot(buyer);
	    	broadcast(buyer.toString());
	        //System.out.println("trade summary buyer--> "+buyer.toString());

	        //proses trade summary seller
	        if (!equal) {
					key = data[7].trim()+data[8].trim()+data[14].trim()+data[15].trim();  //keys : stock+board+broker+investortype
					TradeSummary seller  = (TradeSummary) tsMap.get(key);  
			    	if (seller==null) {
			    		seller = new TradeSummary();
			    		seller.setStock(data[7].trim());
			        	seller.setBoard(data[8].trim());
			        	seller.setBroker(data[14].trim());
			        	seller.setInvestor(data[15].trim());
			        	seller.setSellvol(vol);
			        	seller.setSellval(val);
			        	seller.setSellfreq(1);
			        	seller.setBuyvol(0);
			        	seller.setBuyval(0);
			        	seller.setBuyfreq(0);
			        	seller.setBuyavg(0);
			    	} else {
			    		seller.setSellvol(seller.getSellvol() + vol);
			    		seller.setSellval(seller.getSellval() + val);
			    		seller.setSellfreq(seller.getSellfreq() + 1);
			    	}
	        		seller.setSellavg(seller.getSellval() / seller.getSellvol());
			    	seller.fromProtocol(data);
			    	seller.setType("TS");
			    	tsMap.put(key, seller);
			    	addSnapShot(seller);
			    	broadcast(seller.toString());
			        //System.out.println("trade summary seller--> "+seller.toString());
	        }
		}
	}

	protected void processBroker(String[] data){
		String key = data[5].trim();  //keys : code
		
    	Broker broker  = (Broker) brokerMap.get(key);  
    	if (broker==null) {
    		broker = new Broker();
    	}
    	broker.fromProtocol(data);
    	brokerMap.put(key, broker);
    	addSnapShot(broker);
    	broadcast(broker.toString());
        //System.out.println("broker--> "+broker.toString());
	}

	protected void processBrokerStatus(String[] data){
		String key = data[5].trim();  //keys : code
		
    	Broker broker  = (Broker) brokerMap.get(key);  
    	if (broker!=null) {
        	broker.setStatus(data[7].trim().equals("1") ? "0" : "1");
        	brokerMap.put(key, broker);
        	addSnapShot(broker);
        	broadcast(broker.toString());
            //System.out.println("broker update status--> "+broker.toString());
    	}
	}

	protected void processAction(String[] data){
		String key = data[5].trim()+data[6].trim();  //keys : type+stock
		
    	CorpAction action  = (CorpAction) actionMap.get(key);  
    	if (action==null) {
    		action = new CorpAction();
    	}
    	action.fromProtocol(data);
    	actionMap.put(key, action);
    	addSnapShot(action);
    	broadcast(action.toString());
        //System.out.println("action--> "+action.toString());
	}

	protected void processIndices(String[] data){
		String key = data[5].trim();  //keys : no
		
    	Indices indices  = (Indices) indicesMap.get(key);  
    	if (indices==null) {
    		indices = new Indices();
    	}
    	indices.fromProtocol(data);
    	indicesMap.put(key, indices);
    	addSnapShot(indices);
    	String st = indices.toString();
    	broadcast(st);
    	logIndices.info(st);
        //System.out.println("indices--> "+indices.toString());
	}

	protected void processNews(String[] data){
		String key = data[5].trim();  //keys : no
		
    	News news  = (News) newsMap.get(key);  
    	if (news==null) {
    		news = new News();
    	}
    	news.fromProtocol(data);
    	newsMap.put(key, news);
    	addSnapShot(news);
    	broadcast(news.toString());
        //System.out.println("news--> "+news.toString());
	}

	protected void processStock(String[] data){
		String key = data[5].trim();  //keys : code
		
    	Stock stock  = (Stock) stockMap.get(key);  
    	if (stock==null) {
    		stock = new Stock();
    	}
    	stock.fromProtocol(data);
    	stockMap.put(key, stock);
    	addSnapShot(stock);
    	broadcast(stock.toString());
        //System.out.println("stock--> "+stock.toString());
	}

	protected void processStockStatus(String[] data){
		String key = data[5].trim();  //keys : code
		
    	Stock stock  = (Stock) stockMap.get(key);  
    	if (stock!=null) {
        	stock.setStatus(data[7].trim().equals("0") ? "1" : "0");
        	stockMap.put(key, stock);
        	addSnapShot(stock);
        	broadcast(stock.toString());
            //System.out.println("stock--> "+stock.toString());
    	}
	}

	protected void processSS(String[] data){
		String key = data[5].trim()+data[6].trim();  //keys : no
		
    	StockSummary ss  = (StockSummary) ssMap.get(key);  
    	if (ss==null) {
    		ss = new StockSummary();
    	}
    	ss.fromProtocol(data);
    	builder.updateOpen(ss);
    	if (ss.getOpen() == 0){
    		StockSummary s = builder.getSS(ss.getStock()+"#"+ss.getBoard());
    		ss.setOpen(s.getOpen());
    	}
//    	System.out.println(ss.toString());
    	ssMap.put(key, ss);
    	addSnapShot(ss);    	
    	broadcast(ss.toString());
	}
	public String getTHSnapShot(final String param, final int seqno){
		String result ;
		Query query = db.getDb().getInstance().query();
		query.constrain(Trade.class);
		//query.descend("stock").constrain(param).and(query.descend("board").constrain("RG")).and(
		query.descend("stock").constrain(param).and(query.descend("board").constrain("RG")).and(
		query.descend("seqno").orderDescending().constrain(new Long(seqno)).greater());
		List o = query.execute();
		result =  query.execute().get(0).toString();
		return result;		
	}
	public  void newMessage(String message) throws RemoteException {
		try {
		    	String[] data = message.split("\\|");
		    	FeedMsg feedMsg = new FeedMsg(data[4].trim(),message,Long.parseLong(data[3]));
	    		if (data[4].trim().equals("2") ){//&& Long.parseLong(data[3]) > msg.getSeqno()){
		    		processTradePrice(data);
		    		processTradeSumm(data);
		    	//	if (data[8].trim().equals("RG")){
			    		processTradeSummBroker(data);
			    		processTradeSummBrokerStock(data);
			    		processTradeSummStockBroker(data);
			    		processTradeSummStockInv(data);
			    		processTradeSummInv(data);
		    		//}
		    		processTrade(data);
			    	msg.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg);
		    	} else if (data[4].trim().equals("3") ){// && Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processStock(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("4") ){// && Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processBroker(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("5") ){// && Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processSS(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("6")  ){//&& Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processIndices(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("7")  ){//&& Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processBrokerStatus(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("8")  ){//&& Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processStockStatus(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("9")  ){//&& Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processNews(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	} else if (data[4].trim().equals("A")  ){//&& Long.parseLong(data[3]) > msg2.getSeqno()){
		    		processAction(data);
			    	msg2.setSeqno(feedMsg.getSeqno());
		    		db.putMsg(msg2);
		    	}
		} catch (Exception ex){
			System.out.println("error while processing: "+message);
			ex.printStackTrace();
		}
	}	
	
	void setYesterday(StockSummary ss){
    	addSnapShot(ss);    	
    	broadcast(ss.toString());
	}
	
	void setYesterday(Stock stock){
		addSnapShot(stock);
		broadcast(stock.toString());
	}
}
