package feed.builder.builder;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ThreadQuoteProcess extends Thread {

	private Vector<String> queueProcess = new Vector<String>();
	// private QuoteBuilder quote;
	// private MatchingProcess matching;
	private Log log = LogFactory.getLog(getClass());
	private QuoteMultiThreadBuilder pool;
	private String key;
	private int waitstate;
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSSSSS");

	// private boolean iswaiting = false;

	public ThreadQuoteProcess(String key, QuoteMultiThreadBuilder pool,
			int waitingstate) {

		this.key = key;
		this.pool = pool;
		this.waitstate = waitingstate;
	}

	@Override
	public void run() {
		while (!queueProcess.isEmpty()) {

			String sdata = queueProcess.get(0);
			// String[] data = sdata.split("\\|");
			process(sdata);
			//log.info("["+sdf.format(new Date())+"][process pooling:" + sdata+"]");

			// iswaiting = true;
			// queueProcess.remove(0);
			// String seqno = data[3].trim();
			// boolean ismatch = matching.isMatchingWithKey(seqno, result,
			// true);
			synchronized (queueProcess) {
				if (queueProcess.size() == 1) {

					try {
						queueProcess.wait(waitstate);
						// iswaiting = false;
					} catch (InterruptedException e) {
						e.printStackTrace();
						log.error(logger_info_exception(e));
					}
				}

				queueProcess.remove(0);
			}

		}
		pool.closeThread(this);
		if (!queueProcess.isEmpty()) {
			log.error("ada sisa di queueprocess " + queueProcess.size());
			System.out.println("ada sisa di queueprocess "
					+ queueProcess.size());
		}
	}

	public boolean isSuccess(String sdata) {
		synchronized (queueProcess) {
			if (!queueProcess.isEmpty()) {
				queueProcess.add(sdata);
				queueProcess.notifyAll();
				return true;
			} else
				return false;
		}
	}

	public static String logger_info_exception(Exception ex) {
		StringBuffer strresult = new StringBuffer(100);

		Object[] objarr = ex.getStackTrace();
		strresult.append("[Start:Error : ").append("\n");
		strresult.append(ex.toString()).append("\n");
		for (int i = 0; i < objarr.length; i++) {
			strresult.append("---->").append(objarr[i].toString()).append("\n");
		}
		strresult.append("End:Error]");

		return strresult.toString();
	}

	public String getKey() {
		return key;
	}

	public boolean isRunning() {
		return !queueProcess.isEmpty();
	}

	public void addProcess(String data) {
		synchronized (queueProcess) {

			queueProcess.add(data);
			queueProcess.notify();
		}
	}

	private void process(String message) {
		pool.processNewMessage(message);
	}

}
