package feed.builder.builder;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.FileProcessListener;
import feed.builder.core.MsgBuilder;
import feed.builder.thread.PoolingExecutor;
import feed.builder.thread.SimpleThreadPool;
import feed.builder.thread.ThreadPooling;
import feed.provider.core.FileConfig;
import feed.provider.data.FeedMsg;

public class QuoteMultiThreadBuilder extends QuoteBuilder {
	// private QuoteBuilder quote;
	// private ThreadPoolExecutor poolworker;
	private ThreadPooling poolworker;
	private Hashtable<String, ThreadQuoteProcess> hprocess;
	private int waitstate = 50;

	public QuoteMultiThreadBuilder(FeedConsumer consumer, FeedBuilder builder,
			int port, FileConfig config) throws Exception {
		super(consumer, builder, port);
		this.waitstate = Integer.parseInt((String) config.getProperty(
				"waitingstate", "50"));

		// poolworker = new ThreadPoolExecutor(
		// Integer.parseInt(config.get("corepoolsize").toString()),
		// Integer.parseInt(config.get("maxpoolsize").toString()),
		// Integer.parseInt(config.get("keepalivetime").toString()),
		// TimeUnit.SECONDS,
		// (BlockingQueue<Runnable>) new
		// java.util.concurrent.LinkedBlockingQueue<Runnable>());
		// poolworker = new PoolingExecutor(config);
		poolworker = new SimpleThreadPool(config);

		// boolean isprerunning = Boolean.parseBoolean((String) config
		// .get("isprerunning"));

		hprocess = new Hashtable<String, ThreadQuoteProcess>();

		// if (isprerunning)
		// poolworker.prestartAllCoreThreads();

		// quote = new QuoteBuilder(consumer, builder, port);

	}

	public QuoteMultiThreadBuilder(FeedConsumer consumer, String type,
			FeedBuilder builder, int port, FileConfig config) throws Exception {
		super(consumer, type, builder, port);
		this.waitstate = Integer.parseInt((String) config.getProperty(
				"waitingstate", "50"));

		// poolworker = new ThreadPoolExecutor(
		// Integer.parseInt(config.get("corepoolsize").toString()),
		// Integer.parseInt(config.get("maxpoolsize").toString()),
		// Integer.parseInt(config.get("keepalivetime").toString()),
		// TimeUnit.SECONDS,
		// (BlockingQueue<Runnable>) new
		// java.util.concurrent.LinkedBlockingQueue<Runnable>());
		// poolworker = new PoolingExecutor(config);
		poolworker = new SimpleThreadPool(config);

		// boolean isprerunning = Boolean.parseBoolean((String) config
		// .get("isprerunning"));

		hprocess = new Hashtable<String, ThreadQuoteProcess>();

		// if (isprerunning)
		// poolworker.prestartAllCoreThreads();

		// quote = new QuoteBuilder(consumer, builder, port);

	}

	@Override
	public void newMessage(String message) throws RemoteException {
		
		try {

		String[] data = message.split("\\|");
		String key = data[7].trim();// + "#" + data[8].trim();
		ThreadQuoteProcess processQuote = hprocess.get(key);

		if (processQuote != null && processQuote.isSuccess(message)) {
			// processQuote.addProcess(sdata);

			//log.info("process with current thread:" + data[3]);
		} else {
			ThreadQuoteProcess processQuote2 = new ThreadQuoteProcess(key,
					this, this.waitstate);
			processQuote2.addProcess(message);
			hprocess.put(key, processQuote2);
			poolworker.execute(processQuote2);
			//log.info("process with new thread:" + data[3]);
		}
		}catch (Exception e) {			
			e.printStackTrace();
			log.error("error while process message "+message);
		}
	}

	public void processNewMessage(String message) {
		try {

			// log.info("orders bulder "+message);
			String[] data = message.split("\\|");
			// if (Long.parseLong(data[3]) > msg.getSeqno()){
			FeedMsg feedMsg = new FeedMsg(data[4].trim(), message,
					Long.parseLong(data[3]));
			/*
			 * if (data[4].trim().equals("1")) { processOrder(data);
			 * msg2.setSeqno(feedMsg.getSeqno()); db.putMsg(msg2); } else if
			 * (data[4].trim().equals("2") && preopening) {
			 * processPreopening(data); msg.setSeqno(feedMsg.getSeqno());
			 * db.putMsg(msg); }
			 */
			if (data[4].trim().equals("1")) {

				processOrder(-1,data);

				synchronized (db) {
					msg2.setSeqno(feedMsg.getSeqno());

					db.putMsg(msg2);

				}

			} else if (data[4].trim().equals("2") && preopening) {

				processPreopening(data);

				synchronized (db) {
					msg.setSeqno(feedMsg.getSeqno());

					db.putMsg(msg);

				}

			}

			// }
		} catch (Exception ex) {
			System.out.println("error while processing: " + message);
			ex.printStackTrace();
			log.error("error while process message "+message);
		}
	}

	public void closeThread(ThreadQuoteProcess thread) {
		boolean isremove = hprocess.remove(thread.getKey()) != null;
		//log.info("thread-" + thread.getName() + " " + thread.getKey()
			//	+ " destroy " + isremove + ": size of process pool "
				//+ hprocess.size());
	}

}
