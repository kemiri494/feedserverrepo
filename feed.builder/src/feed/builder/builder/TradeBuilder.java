package feed.builder.builder;

import java.rmi.RemoteException;
import java.util.List;

import com.db4o.query.Query;

import feed.builder.consumer.FeedConsumer;
import feed.builder.core.MsgBuilder;
import feed.builder.msg.Message;
import feed.builder.msg.StockSummary;
import feed.builder.msg.Trade;
import feed.provider.data.FeedMsg;

public class TradeBuilder extends MsgBuilder {
	public TradeBuilder(FeedConsumer consumer, FeedBuilder builder, int port) throws Exception{
		super("", "2", consumer, builder, port);
	}
	
	public TradeBuilder(FeedConsumer consumer, FeedBuilder builder) throws Exception{
		super("", "2", consumer, builder);
	}
	
	public List getSnapShot(int seqno){
		//override running trade dikirim data terakhir
		return null;//20032014
/*
		Query query = db.getDb().getInstance().query();
		query.constrain(Message.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		log.info("request snapshot for type FeedMsg with seqno: "+seqno+" and result: "+(o!=null?o.size()+"" :"0")+ " record(s)");
		return o;*/
	}
	
	public List getForceSnapShot(int seqno){
		Query query = db.getDb().getInstance().query();
		query.constrain(Message.class);
		query.descend("seqno").orderAscending().constrain(new Long(seqno)).greater();
		List o = query.execute();
		log.info("request snapshot for type FeedMsg with seqno: "+seqno+" and result: "+(o!=null?o.size()+"" :"0")+ " record(s)");
		return o;
	}
	
	public boolean connect() throws Exception {
		consumer.getTradeConsumer().subscribe(this,"","", (int)msg.getSeqno());
		return true;
	}
	
	public void exit() throws Exception {
		consumer.getTradeConsumer().unsubscribe(this);
		this.close();
	}
	
	public  void  newMessage(String message) throws RemoteException {
		try {
				//System.out.println(message);
		    	String[] data = message.split("\\|");
		    	if (data[6].equals("0")) {
				    	//if (Long.parseLong(data[3]) > msg.getSeqno()){
				    			FeedMsg feedMsg = new FeedMsg(data[4].trim(),message,Long.parseLong(data[3]));
						    	Trade trade  = new Trade();						    	
						    	trade.fromProtocol(data);
						    	StockSummary ss = new StockSummary();
						    	ss.setStock(trade.getStock());
						    	ss.setBoard(trade.getBoard());
						    	ss.setOpen(trade.getPrice());
						    	builder.updateOpen(ss);
						    	StockSummary ss2 = builder.getSS(trade.getStock()+"#"+trade.getBoard());
						    	trade.setPrevious(ss2 == null ? trade.getPrevious() : ss2.getPrev());
					    		msg.setSeqno(feedMsg.getSeqno());
					    		db.putMsg(msg);
					        	addSnapShot(trade);
					        	broadcast(trade.toString());
					            //System.out.println("runningtrade--> "+trade.toString());
				    	//}
		    	}
		} catch (Exception ex){
			System.out.println("error while processing: "+message);
			ex.printStackTrace();
		}
	}		
}
