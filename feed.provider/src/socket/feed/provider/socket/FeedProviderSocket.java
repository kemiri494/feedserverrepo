package feed.provider.socket;

import org.jboss.netty.channel.ExceptionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

import feed.provider.core.FeedManager;
import feed.provider.core.MsgProvider;
import feed.provider.core.MsgProviderSocket;
import feed.provider.data.FeedMsg;
import feed.provider.provider.FeedProvider;
import feed.provider.provider.OrderProvider;
import feed.provider.source.FeedSource;

public class FeedProviderSocket extends FeedProvider implements Receiver {

	private SocketInterface socketServer;
	private MsgProviderSocket orderProvider;
	private MsgProviderSocket otherProvider;
	private MsgProviderSocket tradeProvider;
	private Logger log = LoggerFactory.getLogger(getClass());

	public FeedProviderSocket(FeedManager app) {
		super(app);

	}

	@Override
	public void start() throws Exception {
		socketServer = SocketFactory.createSocket(
				"feed.provider.socket.config", this);
		socketServer.start();
		orderProvider = new MsgProviderSocket("1", app);
		tradeProvider = new MsgProviderSocket("2", app);
		otherProvider = new MsgProviderSocket("0", app);
		log.info("started_app_feedsolace_"+orderProvider+" "+tradeProvider+" "+otherProvider);
	
	}
	
	@Override
	public void printClientlist() {
		orderProvider.printClientlist();
		tradeProvider.printClientlist();
		otherProvider.printClientlist();
	}

	@Override
	public void newMsg(FeedMsg msg) {
		char c = msg.getType().charAt(0);
		switch (c) {
		case '1':
			orderProvider.addMsg(msg);
			break;
		case '2':
			tradeProvider.addMsg(msg);
			// khusus untuk saham-saham preopening, quote awal dibentuk oleh
			// trade preop
			/*
			 * if (preopening) { if ((msg.getTime() < 93000) || ((msg.getTime()
			 * > 160000) && (msg.getTime() <161500)) ){
			 * orderProvider.addMsg(msg); }
			 * 
			 * // if (msg.getTime()<93000){ //orderProvider.addMsg(msg); //}
			 * else { //preopening = false; //} }
			 */

			// new datafeed with new time feed
			if (msg.getTime() >= 155000 && msg.getTime() < 160500) {
				orderProvider.addMsg(msg);
			}
			// end of change

			break;
		case '5':
			// log.info("stock summary "+msg.getMsg());
			otherProvider.addMsg(msg);
			break;
		case '3':
			// log.info("stock data "+msg.getMsg());
			otherProvider.addMsg(msg);

			break;
		default:
			otherProvider.addMsg(msg);
			break;
		}
	}

	@Override
	public void connected(ClientSocket sock) {
		sock.validated(true);
	}

	@Override
	public void disconnect(ClientSocket sock) {
		orderProvider.close(sock);
		otherProvider.close(sock);
		tradeProvider.close(sock);
	}

	@Override
	public void receive(ClientSocket sock, byte[] bt) {
		String msg = new String(bt);
		//System.out.println(msg.toString());

		if (msg.startsWith("subscribe")) {
			String[] sp = msg.split(" ");
			String type = sp[1];
			int seqno = Integer.parseInt(sp[2]);
			//log.info(msg);
			if (type.equals("1")) {
				orderProvider.subscribe(sock, "", "", seqno);
			} else if (type.equals("2")) {
				tradeProvider.subscribe(sock, "", "", seqno);
			} else if (type.equals("0")) {
				otherProvider.subscribe(sock, "", "", seqno);
			}

		} else if (msg.startsWith("unsubscribe")) {
			String[] sp = msg.split(" ");
			String type = sp[1];
			//log.info(msg);

			if (type.equals("1")) {
				orderProvider.unsubscribe(sock);
			} else if (type.equals("2")) {
				tradeProvider.unsubscribe(sock);
			} else if (type.equals("0")) {
				otherProvider.unsubscribe(sock);
			}
		}
	}

	@Override
	public MsgProvider getOrderProvider() {
		// TODO Auto-generated method stub
		return orderProvider;
	}

	@Override
	public MsgProvider getTradeProvider() {
		// TODO Auto-generated method stub
		return tradeProvider;
	}

	@Override
	public MsgProvider getOtherProvider() {
		// TODO Auto-generated method stub
		return otherProvider;
	}

	@Override
	public void close() {
		orderProvider.close();
		tradeProvider.close();
		otherProvider.close();
	}

	
	
	
	

}
