package feed.provider.provider;

import feed.provider.core.FeedManager;
import feed.provider.core.MsgProvider;

public class OtherProvider extends MsgProvider {
	private static final long serialVersionUID = 1L;

	public OtherProvider(String type, FeedManager provider, int port) throws Exception {
		super(type, provider, port);
	}
}
