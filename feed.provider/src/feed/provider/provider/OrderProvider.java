package feed.provider.provider;

import feed.provider.core.FeedManager;
import feed.provider.core.MsgProvider;

public class OrderProvider extends MsgProvider {
	private static final long serialVersionUID = 1L;

	public OrderProvider(String type, FeedManager provider, int port) throws Exception {
		super(type, provider, port);
	}
}
