package feed.provider.core;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MsgListener extends Remote{
	public void newMessage(String message) throws RemoteException;
	public void close() throws RemoteException;
}
