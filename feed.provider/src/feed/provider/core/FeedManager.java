package feed.provider.core;

import feed.provider.provider.FeedProvider;
import feed.provider.source.FeedSource;
import feed.provider.source.FeedSourceManager;


public interface FeedManager {
	public int  login(String userid, String password);
	public FeedProvider getProvider();
	public FeedSourceManager getSource();
}
