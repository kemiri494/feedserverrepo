package feed.provider.core;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.db4o.query.Query;

import feed.provider.data.FeedDb;
import feed.provider.data.FeedMsg;

public class MsgProvider extends UnicastRemoteObject implements MsgProducer {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private static final long serialVersionUID = 6263004426413596696L;
	protected Hashtable clientList;
	protected FeedManager manager;
	protected String type;
	protected FeedDb db;

	public MsgProvider(String type, FeedManager manager, int port)
			throws Exception {
		super(port);
		this.manager = manager;
		this.type = type;
		clientList = new Hashtable();
		db = new FeedDb(type);
		db.start();
		log.info("Msg Provider for " + type + " created");
	}
	
	public MsgProvider() throws RemoteException {

	}

	public void printClientlist() {
		System.out.println("total client for provider type " + type + " "
				+ clientList.size() + " record(s)");
	}

	public List getSnapShot(String type, int seqno) {
		Query query = db.getDb().getInstance().query();
		query.constrain(FeedMsg.class);
		// query.descend("type").constrain(type).and(query.descend("seqno").orderAscending().constrain(new
		// Long(seqno)).greater());
		query.descend("seqno").orderAscending().constrain(new Long(seqno))
				.greater();
		List o = query.execute();
		log.info("request snapshot for type: " + type + " with seqno: " + seqno
				+ " and result: " + (o != null ? o.size() + "" : "0")
				+ " record(s)");
		return o;
	}

	public void addSnapShot(FeedMsg message) {
		db.putMsg(message);
	}

	public int subscribe(MsgListener listener, String userid, String pass,
			int seqno) throws RemoteException {
		int sessionid = -1;
		if (!clientList.containsKey(listener)) {
			log.info("new subscriber : " + userid + " from seqno " + seqno+" "+listener.getClass());
			Subscriber s = new Subscriber(type, manager, this, listener,
					userid, pass, seqno);
			sessionid = s.login();
			if (sessionid != -1) {
				clientList.put(listener, s);
			}
		} else {
			sessionid = -2;
		}

		log.info("subscribe with " + sessionid + " " + clientList.size()+" "+getClass());

		return sessionid;
	}

	public void unsubscribe(MsgListener listener) throws RemoteException {
		Subscriber s = (Subscriber) clientList.get(listener);
		log.info("new unsubscribe ");
		if (s != null)
			s.setStop();
		clientList.remove(listener);
	}

	public void heartbeat(int sessionid) throws RemoteException {
		// if client has been failed call this method then client should execute
		// reconnect method
	}

	public void addMsg(FeedMsg message) {
		addSnapShot(message);
		for (Iterator clients = clientList.keySet().iterator(); clients
				.hasNext();) {
			MsgListener listener = (MsgListener) clients.next();
			Subscriber s = (Subscriber) clientList.get(listener);
			s.addQueue(message.getMsg());
		}
	}

	public void close() {
		for (Iterator clients = clientList.keySet().iterator(); clients
				.hasNext();) {
			MsgListener listener = (MsgListener) clients.next();
			try {
				listener.close();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		db.doStop();
	}
}
