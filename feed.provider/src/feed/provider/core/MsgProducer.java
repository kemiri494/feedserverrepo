package feed.provider.core;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MsgProducer extends Remote{
	public int subscribe(MsgListener listener, String userid, String pass, int seqno) throws RemoteException;
	public void unsubscribe(MsgListener listener) throws RemoteException;
	public void heartbeat(int sessionid) throws RemoteException;
}
