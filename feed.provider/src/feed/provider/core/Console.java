package feed.provider.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import feed.provider.FeedProviderApp;
import feed.provider.data.FeedMsg;

public final class Console extends Thread {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private FeedProviderApp apps;
	private static final String C_VERSION = "1.0.2.Release1";
	private boolean stop;

	public Console(FeedProviderApp apps) {
		this.apps = apps;
		stop = false;
	}

	private void prompt() {
		System.out.println("");
		System.out.print("provider> ");
	}

	public void showVersion() {
		System.out.println("Version : " + C_VERSION);
	}

	private void showMenu() {
		welcomScreen();
		System.out.println("List of All Commands : ");
		System.out
				.println("\tCommands must appear first on line and end with enter ");
		System.out.println("");
		System.out.println("");
		System.out.println("help\t\tdisplay this help (menu).");
		System.out.println("version\t\tdisplay server  version.");
		System.out.println("start\t\tconnect to IDX");
		System.out.println("client\t\tdisplay client list");
		System.out.println("exit\t\texit server application");
		System.out.println("");
	}

	public void setstop() {
		stop = true;
	}

	public void run() {
		welcomScreen();
		prompt();
		BufferedReader bufferedreader = new BufferedReader(
				new InputStreamReader(System.in));
		do {
			try {
				String cmd = bufferedreader.readLine();
				if (cmd.toLowerCase().equals("start")) {
					if (!apps.getSource().start()) {
						System.out.println("failed, please try again");
					}
				} else if (cmd.toLowerCase().equals("version")) {
					showVersion();
				} else if (cmd.toLowerCase().equals("help")) {
					showMenu();
				} else if (cmd.toLowerCase().equals("client")) {
					apps.getProvider().printClientlist();
				} else if (cmd.toLowerCase().startsWith("send.dummy")) {
					/*
					 * String[] data = cmd.split(" "); sendDummyData(data[1]);
					 */
					//send.dummy file stock 0

					final String[] data = cmd.split(" ");
					final String stock = data.length == 3 ? data[2] : "";
					final String scmd = data[3];
					Thread th = new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								sendDummyData(data[1], stock,
										scmd.equals("0") ? false : true);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});
					th.start();

				}else if (cmd.toLowerCase().startsWith("relogin")) {
					apps.getSource().relogin();
				} else if (cmd.toLowerCase().startsWith("restart")) {
					apps.getSource().restart();
				} else if (cmd.toLowerCase().equals("exit")) {
					try {
						stop = true;
						apps.getSource().quit();
				    	apps.getProvider().close();
						System.exit(0);
					} catch (Exception ex) {
						ex.printStackTrace();
						System.out.println("error : stopping services");
						log.error("error stopping service", ex);
					}
				}else if (cmd.toLowerCase().startsWith("changeconnection")) {

					String[] data = cmd.split("\\|");

					if(data.length == 2)
						apps.getSource().changeConnection(data[1]+".");
					else
						apps.getSource().changeConnection("");
				} 


			} catch (Exception exception) {
				exception.printStackTrace();
				System.out.println("bad command, please try again..");
			}
			prompt();
		} while (!stop);
		System.exit(0);
	}

	private final static SimpleDateFormat format = new SimpleDateFormat(
			"HHmmss");

	private void waitTillMatch(String data, boolean immediate) {
		try {
			if (!immediate) {
				String[] p = data.split("\\|");
				int dat = Integer.parseInt(p[2]);
				int curr;
				do {
					String ss = format.format(new Date());
					curr = Integer.parseInt(ss);
					//log.info("time " + curr + " " + dat);
				} while (dat >= curr);

			} else {
				try {
					Thread.sleep(3L);
				} catch (Exception localException) {
					localException.printStackTrace();
				}
			}
		} catch (Exception localException1) {
			localException1.printStackTrace();
		}
	}

	private void sendDummyData(String file, String stock, boolean immediate)
			throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String s = null;
		System.out.println("stock:" + stock.isEmpty() + ":" + immediate);
		boolean isforward = true;

		while ((s = reader.readLine()) != null) {
			String[] data = s.split("\\|");

			/*
			 * if (stock.isEmpty()) isforward = true; else if
			 * (s.contains(stock)) isforward = true;
			 */
			
			//log.info("data "+s);

			if (isforward) {

				FeedMsg feedMsg = new FeedMsg(data[4].trim(), s,
						Long.parseLong(data[3]));
				feedMsg.setTime(Integer.parseInt(data[2].trim()));

				waitTillMatch(s, immediate);

				apps.getProvider().newMsg(feedMsg);
			}
		}
	}

	private void sendDummyData(String file) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String s = null;
		while ((s = reader.readLine()) != null) {
			String[] data = s.split("\\|");
			FeedMsg feedMsg = new FeedMsg(data[4].trim(), s,
					Long.parseLong(data[3]));
			feedMsg.setTime(Integer.parseInt(data[2].trim()));
			apps.getProvider().newMsg(feedMsg);
		}
	}

	public void welcomScreen() {
		StringBuffer stringbuffer = new StringBuffer(100);
		stringbuffer
				.append("\nWelcome to the Feed Provider. Commands end with enter ");
		stringbuffer.append("\nYour  Module id is FeedProvider version : "
				+ C_VERSION);
		stringbuffer.append("\nCreated by vollux.team@05MAY10 ");
		stringbuffer.append("\n");
		stringbuffer.append("\n Type 'help' for help (menu). ");
		stringbuffer.append("\n");
		System.out.print(stringbuffer);
		System.out.println("");
	}
}