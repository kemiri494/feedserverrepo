package com.eqtrade.rm;

import static com.eqtrade.quickfix.resources.field.MsgType.hedLogonClient;
import static com.eqtrade.quickfix.resources.field.MsgType.hedLogoutClient;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Amend;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Delete;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Fully;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Open;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Partial;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Rejected;
import static com.eqtrade.quickfix.resources.field.OrdStatus.Withdraw;
import static com.eqtrade.utils.Function.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.omg.CORBA.OMGVMCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quickfix.FieldNotFound;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.database.OrderData.SQUERY;
import com.eqtrade.database.model.OrderList;
import com.eqtrade.database.model.OrderListUnion;
import com.eqtrade.database.model.OrderOut;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.jonec.InfoSymbol;
import com.eqtrade.jonec.InfoSymbolData;
import com.eqtrade.net.ThirdPartyInterface;
import com.eqtrade.net.data.HeaderAndData;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.*;
import com.eqtrade.rm.proxy.ProcessingProxyInterface;
import com.eqtrade.utils.FixMessage;
import com.eqtrade.utils.FixMessageUtils;
import com.eqtrade.net.CommClientHandling;
import com.eqtrade.utils.properties.FEProperties;

public class RiskManagementProcessing implements ThirdPartyInterface {
	
	private final Logger log = LoggerFactory.getLogger(super.getClass());
	protected ProcessingProxyInterface proxyRouting;
	private AccessQueryLocator queryData;
	private CommClientHandling comHandling;
	private int nmaxcounterlock;
	private MappingServerID muid;
	private String sfreqamend, sclordid_accid;
	private String sfreqwithdraw;
	private String sfreqsession;
	private String slastseq;
	private String sfonline;
	private String sjobid_rm, sentryby, sclordid_rm, sordid_clordid,data_negdeal, sclordid_clordcontra;
	protected InfoSymbol is;
	private Double lotsize ;

	private Hashtable<String, String> hreqsession = new Hashtable<String, String>(10);
	private Hashtable<String, String> hreqamend = new Hashtable<String, String>(100);
	private Hashtable<String, String> hreqwithdraw = new Hashtable<String, String>(10);
	protected Hashtable<String, RMType> mclord_rm = new Hashtable<String, RMType>(5000);
	
	protected Hashtable<String, String> mclord_message = new Hashtable<String, String>();
	protected Hashtable<String, String> hentryby = new Hashtable<String, String>(100);
	protected Hashtable<String, String> hcustidby = new Hashtable<String, String>(100);
	protected Hashtable<String, String> mapordid_clordid = new Hashtable<String, String>(100);
	protected Hashtable<String, String> mapclordid_ordid = new Hashtable<String, String>(100);
	
	private Hashtable<String, String> honline = new Hashtable<String, String>(500);
	private Hashtable<String, String> hnegdeal = new Hashtable<String, String>(500);
	
	private int nsession = 0;
	
	private FEProperties props;
	protected Vector<String> vordtaker = new Vector<String>(10, 5);
	public static HashMap<String, Integer> maptag = new HashMap<String, Integer>();
	public static HashMap<String, Integer> maptagamend = new HashMap<String, Integer>();
	
	public RiskManagementProcessing(CommClientHandling _comHandling,MappingServerID map, int nmaxcounterlock, AccessQueryLocator aq,ProcessingProxyInterface _proxy, InfoSymbol _is, Double lotsize) {
		this.comHandling = _comHandling;
		this.muid = map;
		this.nmaxcounterlock = nmaxcounterlock;
		this.queryData = aq;
		
		this.proxyRouting = _proxy;
		this.is = _is;
		this.lotsize = lotsize;
		
		//System.out.println(" == "+lotsize);
		
		loadData();
		load_datafromdb();
		loadSequence(new Date());
		initmaptag();
	}

	private void loadSequence(Date date) {
		
		slastseq = "data/" + getStringTimesdf(date) + "-lastseq.txt";

		Object obj = read_file_binary(slastseq);
		if (obj != null) {
			Integer ntemp = (Integer) obj;
			nsession = ntemp.intValue();
		}
	}

	public void saveData() {
		
		Date date = new Date();
		
		sentryby = "data/" + getStringTimesdf(date.getTime()) + "-entryby.txt";
		sclordid_rm = "data/" + getStringTimesdf(date.getTime())+ "-clord_rm.txt";
		sordid_clordid = "data/" + getStringTimesdf(date.getTime())+ "-ordid-clordid.txt";

		data_negdeal = "data/" + getStringTimesdf(date.getTime())+ "-message-negdeal.txt";
		sclordid_clordcontra = "data/" + getStringTimesdf(date.getTime())+ "-clord-negdeal.txt";

		Enumeration keys = hentryby.keys();		

		Enumeration keys2 = mclord_rm.keys();

		while (keys2.hasMoreElements()) {
			String key = (String) keys2.nextElement();
			StringBuffer sbf = new StringBuffer();
			sbf.append(key);
			sbf.append("###");
			sbf.append(mclord_rm.get(key).toString());
			save_datatofile(sbf.toString(), sclordid_rm);
		}

		keys = mclord_message.keys();
		
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			StringBuffer sbf = new StringBuffer();
			sbf.append(key);
			sbf.append("###");
			sbf.append(mclord_message.get(key).toString());
			save_datatofile(sbf.toString(), data_negdeal);
		}
	}

	public void loadData() {

		Date date = new Date();
		
		sfreqamend = "data/" + getStringTimesdf(date.getTime())+ "-reqamend.txt";
		sfreqwithdraw = "data/" + getStringTimesdf(date) + "-reqwithdraw.txt";
		sfreqsession = "data/" + getStringTimesdf(date) + "-reqsession.txt";
		sjobid_rm = "data/" + getStringTimesdf(date) + "-jobid_rm.txt";
		sclordid_accid = "data/" + getStringTimesdf(date)+ "-clordid_accid.txt";
		sfonline = "data/" + getStringTimesdf(date) + "-online.txt";
		sentryby = "data/" + getStringTimesdf(date.getTime()) + "-entryby.txt";
		sclordid_rm = "data/" + getStringTimesdf(date.getTime())+ "-clord_rm.txt";
		sordid_clordid = "data/" + getStringTimesdf(date.getTime())+ "-ordid-clordid.txt";
		data_negdeal = "data/" + getStringTimesdf(date.getTime())+ "-message-negdeal.txt";
		sclordid_clordcontra = "data/" + getStringTimesdf(date.getTime())+ "-clord-negdeal.txt";

		hreqamend.clear();
		hreqwithdraw.clear();
		hreqsession.clear();		
		honline.clear();		
		mclord_rm.clear();
		mclord_message.clear();

		load_datareqfromfile(hreqamend, sfreqamend);
		load_datareqfromfile(hreqwithdraw, sfreqwithdraw);
		load_datareqfromfile(hreqsession, sfreqsession);
		load_dataonlinefromfile(honline, sfonline);		
		load_datajobidrm_fromfile(mclord_rm, sclordid_rm);
	}

	private void load_datafromdb() {
		FOData fodata = new FOData();
		fodata.set_int(0);
		fodata.set_string("");
		try {
			Vector<Object> vobj = queryData	.getQueryData().set_insert(com.eqtrade.database.QueryData.SQUERY.orderlistParameter.val,fodata);

			for (Object obj : vobj) {
				OrderListUnion ord = (OrderListUnion) obj;
				String orderid = ord.getMarketorderid();
				String clordid = ord.getOrderid();
				String entryby = ord.getEntryrtby();
				String accid = ord.getAccid();
				String statusid = ord.getStatusid();

				if (!statusid.equals("M")) {
					if (!statusid.equals("o") && !statusid.equals("R")) {
						if (orderid != null && clordid != null)
							mapordid_clordid.put(orderid, clordid);

						if (clordid != null && orderid != null)
							mapclordid_ordid.put(clordid, orderid);
					}
					hentryby.put(clordid, entryby);
					hcustidby.put(clordid, accid);
					if (ord.getOrderidcontra() != null	&& !ord.getOrderidcontra().isEmpty()) {
						hentryby.put(ord.getOrderidcontra(), entryby);
						hcustidby.put(clordid, ord.getAccidcontra());

						// tambahan untuk benerin confirmation negdeal
						String temp = mapclordid_ordid.get(clordid);
						// if (temp == null)
						// temp = "";

						if (temp == null)
							hnegdeal.put(clordid, ord.getOrderidcontra());
					}
				}
			}
			/*log.info("load maporderid clordid:" + mapordid_clordid.size());
			log.info("load mapclordid orderid:" + mapclordid_ordid.size());
			log.info("hentryby:" + hentryby.size());
			log.info("hcustby:" + hcustidby.size());
			log.info("hnegdeal:" + hnegdeal.size());*/

		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void initmaptag() {		
		maptag.put("sukses", tudSukses.FIELD);
		maptag.put("statusorder", OrdStatus.FIELD);
		maptag.put("desc", tudDesc.FIELD);
		maptag.put("curtl", tudCurtl.FIELD);
		maptag.put("pavport", tudPavport.FIELD);
		maptag.put("ptlbefore", tudPtlbefore.FIELD);
		maptag.put("pavportbefore", tudPavportbefore.FIELD);
		maptag.put("plqvaluebefore", tudPlqvaluebefore.FIELD);
		maptag.put("ratiobefore", tudPratiobefore.FIELD);
		maptag.put("isoverlimit", tudIsoverlimit.FIELD);
		maptag.put("isshortsell", tudIsshortsell.FIELD);

		maptag.put("curratio", tudCurratio.FIELD);
		maptag.put("buy", tudBuy.FIELD);
		maptag.put("sell", tudSell.FIELD);
		maptag.put("netacc", tudNetac.FIELD);
		maptag.put("bidid", tudBid.FIELD);
		maptag.put("offer", tudOffer.FIELD);
		maptag.put("stockval", tudStockval.FIELD);
		maptag.put("marketval", tudMarketval.FIELD);
		maptag.put("plqvalue", tudPlqValue.FIELD);
		maptag.put("ptopup", tudTopup.FIELD);
		maptag.put("pforcesell", tudForcesell.FIELD);
		maptag.put("pcreditlimit", tudCreditlimit.FIELD);

		maptag.put("pdeposit", tudDeposit.FIELD);
		maptag.put("pcurlvol", tudCurlvol.FIELD);
		maptag.put("pavgprice", tudAvgprice.FIELD);

		// maptag.put("statusorder", tudStatus.FIELD);
		maptag.put("statusfrom", tudStatus.FIELD);
		maptag.put("descfrom", tudDesc.FIELD);
		maptag.put("curtlfrom", tudCurtl.FIELD);
		maptag.put("pavportfrom", tudPavport.FIELD);
		maptag.put("ptlbeforefrom", tudPtlbefore.FIELD);
		maptag.put("pavportbeforefrom", tudPavportbefore.FIELD);
		maptag.put("plqvaluebeforefrom", tudPlqvaluebefore.FIELD);
		maptag.put("ratiobeforefrom", tudPratiobefore.FIELD);
		maptag.put("isoverlimitfrom", tudIsoverlimit.FIELD);
		maptag.put("isshortsellfrom", tudIsshortsell.FIELD);
		maptag.put("curratiofrom", tudCurratio.FIELD);
		maptag.put("buyfrom", tudBuy.FIELD);
		maptag.put("sellfrom", tudSell.FIELD);
		maptag.put("netaccfrom", tudNetac.FIELD);
		maptag.put("bididfrom", tudBid.FIELD);
		maptag.put("offerfrom", tudOffer.FIELD);
		maptag.put("stockvalfrom", tudStockval.FIELD);
		maptag.put("marketvalfrom", tudMarketval.FIELD);
		maptag.put("plqvaluefrom", tudPlqValue.FIELD);
		maptag.put("ptopupfrom", tudTopup.FIELD);
		maptag.put("pforcesellfrom", tudForcesell.FIELD);
		maptag.put("pcreditlimitfrom", tudCreditlimit.FIELD);
		maptag.put("pdepositfrom", tudDeposit.FIELD);
		maptag.put("pcurlvolfrom", tudCurlvol.FIELD);
		maptag.put("pavgpricefrom", tudAvgprice.FIELD);
		maptag.put("statusto", tudStatusold.FIELD);
		maptag.put("descto", tudDescold.FIELD);
		maptag.put("curtlto", tudCurtlold.FIELD);
		maptag.put("pavportto", tudPavportold.FIELD);
		maptag.put("ptlbeforeto", tudPtlbeforeold.FIELD);
		maptag.put("pavportbeforeto", tudPavportbeforeold.FIELD);
		maptag.put("plqvaluebeforeto", tudPlqvaluebeforeold.FIELD);
		maptag.put("ratiobeforeto", tudPratiobeforeold.FIELD);
		maptag.put("isoverlimitto", tudIsoverlimitold.FIELD);
		maptag.put("isshortsellto", tudIsshortsellold.FIELD);
		maptag.put("curratioto", tudCurratioold.FIELD);
		maptag.put("buyto", tudBuyold.FIELD);
		maptag.put("sellto", tudSellold.FIELD);
		maptag.put("netaccto", tudNetacold.FIELD);
		maptag.put("bididto", tudBidold.FIELD);
		maptag.put("offerto", tudOfferold.FIELD);
		maptag.put("stockvalto", tudStockvalold.FIELD);
		maptag.put("marketvalto", tudMarketvalold.FIELD);
		maptag.put("plqvalueto", tudPlqValueold.FIELD);
		maptag.put("ptopupto", tudTopupold.FIELD);
		maptag.put("pforcesellto", tudForcesellold.FIELD);
		maptag.put("pcreditlimitto", tudCreditlimitold.FIELD);
		maptag.put("pdepositto", tudDepositold.FIELD);
		maptag.put("pcurlvolto", tudCurlvolold.FIELD);
		maptag.put("pavgpriceto", tudAvgpriceold.FIELD);
		maptag.put("pwithdraw", tudWithdraw.FIELD);
		maptag.put("pwithdrawfrom", tudWithdrawOld.FIELD);
		maptag.put("pwithdrawto", tudWithdrawNew.FIELD);

		maptagamend.put("sukses", tudSukses.FIELD);
		maptagamend.put("statusorder", tudStatusold.FIELD);
		maptagamend.put("statusordernew", OrdStatus.FIELD);
		maptagamend.put("desc", tudDesc.FIELD);
		maptagamend.put("curtl", tudCurtlold.FIELD);
		maptagamend.put("pavport", tudPavportold.FIELD);
		maptagamend.put("ptlbefore", tudPtlbefore.FIELD);
		maptagamend.put("pavportbefore", tudPavportbefore.FIELD);
		maptagamend.put("plqvaluebefore", tudPlqvaluebefore.FIELD);
		maptagamend.put("ratiobefore", tudPratiobefore.FIELD);
		maptagamend.put("isoverlimit", tudIsoverlimit.FIELD);
		maptagamend.put("isshortsell", tudIsshortsell.FIELD);

		maptagamend.put("curratio", tudCurratioold.FIELD);
		maptagamend.put("buy", tudBuyold.FIELD);
		maptagamend.put("sell", tudSellold.FIELD);
		maptagamend.put("netacc", tudNetacold.FIELD);
		maptagamend.put("bidid", tudBidold.FIELD);
		maptagamend.put("offer", tudOfferold.FIELD);
		maptagamend.put("stockval", tudStockvalold.FIELD);
		maptagamend.put("marketval", tudMarketvalold.FIELD);
		maptagamend.put("plqvalue", tudPlqValueold.FIELD);
		maptagamend.put("ptopup", tudTopupold.FIELD);
		maptagamend.put("pforcesell", tudForcesellold.FIELD);
		maptagamend.put("pcreditlimit", tudCreditlimitold.FIELD);

		maptagamend.put("pdeposit", tudDepositold.FIELD);
		maptagamend.put("pcurlvol", tudCurlvolold.FIELD);
		maptagamend.put("pavgprice", tudAvgpriceold.FIELD);

		maptagamend.put("curtlnew", tudCurtl.FIELD);
		maptagamend.put("pavportnew", tudPavport.FIELD);

		maptagamend.put("currationew", tudCurratio.FIELD);
		maptagamend.put("buynew", tudBuy.FIELD);
		maptagamend.put("sellnew", tudSell.FIELD);
		maptagamend.put("netaccnew", tudNetac.FIELD);
		maptagamend.put("bididnew", tudBid.FIELD);
		maptagamend.put("offernew", tudOffer.FIELD);
		maptagamend.put("stockvalnew", tudStockval.FIELD);
		maptagamend.put("marketvalnew", tudMarketval.FIELD);
		maptagamend.put("plqvaluenew", tudPlqValue.FIELD);
		maptagamend.put("ptopupnew", tudTopup.FIELD);
		maptagamend.put("pforcesellnew", tudForcesell.FIELD);
		maptagamend.put("pcreditlimitnew", tudCreditlimit.FIELD);

		maptagamend.put("pdepositnew", tudDeposit.FIELD);
		maptagamend.put("pcurlvolnew", tudCurlvol.FIELD);
		maptagamend.put("pavgpricenew", tudAvgprice.FIELD);
		maptagamend.put("pwithdrawnew", tudWithdrawNew.FIELD);
		maptagamend.put("pwithdrawold", tudWithdrawOld.FIELD);
	}

	public void init_filtermsg_orttaker() {
		if (props == null) {
			props = new FEProperties(getClass().getName());
		}
		String sheader = props.get("msgorttaker.header");// mrsc.getString("msgorttaker.header");
		if (sheader.trim().length() > 0) {
			String[] sparse = sheader.split("\\|", -2);
			vordtaker.clear();
			for (String s : sparse) {
				vordtaker.addElement(s);
			}
		}
	}

	@Override
	public void process_dat(String sessionid, Message message) {
		String sheader = message.getOverridedHeader().getMsgType().getValue();

		// print_pair("process Risk Management:" + sheader);
		 log.info("receive message:" + sheader);

		if (sheader.equals(hedLogonClient)) {
			process_logonclient(sessionid, message);
		} else if (sheader.equals(MsgType.hedReject)) {
		} else if (sheader.equals(hedLogoutClient)) {
			process_logoutclient(sessionid, message);
		} else if (sheader.equals(MsgType.hedCleanSession)) {
			process_cleansession(sessionid, message);
		} else if (sheader.equals(MsgType.hedNewOrderRequest) || sheader.equals(MsgType.hedRequestTemp)) {
			Message ortmsg = (Message) message.clone();
			ortmsg.setString(tudIsOrdTaker.FIELD, "1");
			proxyRouting.processTaker(sessionid, ortmsg);
			process_entryord(sessionid, message);
		} else if (sheader.equals(MsgType.hedCancelRequest) || sheader.equals(MsgType.hedDeleteTemp)) {
			String sclordid = message.getString(ClOrdID.FIELD);
			Message ortmsg = (Message) message.clone();
			ortmsg.setString(tudIsOrdTaker.FIELD, "1");
			String custid = hcustidby.get(sclordid);
			ortmsg.setString(tudAccountID.FIELD, custid);
			proxyRouting.processTaker(sessionid, ortmsg);
			process_reqwithdraw(sessionid, message);
		} else if (sheader.equals(MsgType.hedCancelReplaceRequest)) {
			String sclordid = message.getString(OrigClOrdID.FIELD);
			Message ortmsg = (Message) message.clone();
			ortmsg.setString(tudIsOrdTaker.FIELD, "1");
			proxyRouting.processTaker(sessionid, ortmsg);
			process_reqamend(sessionid, message);
		} else if (sheader.equals(MsgType.hedExecutionReport)) {
			process_executionreport(sessionid, message);
		} else if (sheader.equals(MsgType.hedCancelReject)) {
			process_cancelreject(sessionid, message);
		} else if (sheader.equals(MsgType.hedRequestSession)) {
			process_requestsession(sessionid, message);
		} else if (sheader.equals(MsgType.hedSchedullerProcessed)) {
			process_scheduller(sessionid, message);
		} else if (sheader.equals(MsgType.hedSplitOrder)) {
			message.setString(tudIsOrdTaker.FIELD, "1");
			proxyRouting.processTaker(sessionid, message);
		} else if (sheader.equals(MsgType.hedSplitEntryOrder)) {
			Message ortmsg = (Message) message.clone();
			ortmsg.setString(tudIsOrdTaker.FIELD, "1");
			process_splitentry(sessionid, message);
			proxyRouting.processTaker(sessionid, ortmsg);			
		} else if (sheader.equals(MsgType.hedSchedullerStop)) {
			try {
				int statusscheduller = message.getInt(tudBasketStopStatus.FIELD);
				Message executionreport = new Message();
				executionreport.getHeader().setString(MsgType.FIELD,MsgType.hedExecutionReport);

				String sclientid = message.getString(ClientID.FIELD);
				String ssrcuid = message.getString(tudSrcUID.FIELD);
				String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				String ssrcutype = message.getString(tudSrcUType.FIELD);

				executionreport.setString(tudDestUID.FIELD, ssrcuid); // "gtw");
				executionreport.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr); // "1234");
				executionreport.setString(tudDestUType.FIELD, ssrcutype); // "gtw");

				//log.info("message scheduller is stopped:" + statusscheduller);
				if (statusscheduller == tudBasketStopStatus.STOPPED) {
					String sclordid = message.getString(ClOrdID.FIELD);
					String sentrytime = message.getString(tudEntryTime.FIELD);
					String entryby = message.getString(tudEntryBy.FIELD);
					String entryterminal = message.getString(tudEntryTerminal.FIELD);
					String isfloor = message.getString(tudIsFloor.FIELD);
					
					Date entrytime = getDateTime1(sentrytime);

					FOData fodata = new FOData();
					fodata.set_string(sclordid, entryby, entryterminal);
					fodata.set_date(entrytime);

					Vector<Object> vresult = queryData.getOrderData().set_insert(SQUERY.withdrawninternal.val, fodata);

					OrderOut ord = (OrderOut) vresult.elementAt(0);
					ord.loadMessage(executionreport, maptag, queryData.getOrderData().mapoutparameter.get(SQUERY.withdrawninternal.val));

					RMType rmsource = get_basedonclordid(sclordid);

					String accid = hcustidby.get(sclordid);

					executionreport.setString(ClOrdID.FIELD, sclordid);
					executionreport.setString(ClientID.FIELD, sclientid);
					executionreport.setString(tudDestUID.FIELD,rmsource.getUID());
					executionreport.setString(tudDestIDAtsvr.FIELD,rmsource.getIDAtsvr());
					executionreport.setString(tudDestUType.FIELD,rmsource.getUType());
					executionreport.setString(tudAccountID.FIELD, accid);
					log.info("get status of withdraw internal:"+ ord.getSukses());
					if (ord.getSukses() == 0)
						executionreport.getHeader().setString(MsgType.FIELD,MsgType.hedCancelReject);

					Message ortmessage = (Message) executionreport.clone();

					comHandling.send_appmsg(sessionid, executionreport);
					ortmessage.setString(tudIsOrdTaker.FIELD, "1");
					proxyRouting.processTaker(sessionid, ortmessage);

				} else {
					String sclordid = message.getString(ClOrdID.FIELD);

					if (!message.isSetField(OrderID.FIELD)) {
						String sordid = mapclordid_ordid.get(sclordid);
						message.setString(OrderID.FIELD, sordid);
					} else {
						String orderid = message.getString(OrderID.FIELD);
						if (orderid != null && orderid.isEmpty()) {
							orderid = mapclordid_ordid.get(sclordid);
							if (orderid != null) {
								message.setString(OrderID.FIELD, orderid);
							} else if (orderid == null) {
								orderid = "";
								log.error("ERROR:ORDERID_NOTFOUND");
							}
						}

					}

					RMType rmsource = get_basedonclordid(sclordid); // mclord_rm.get(sclordid);//
					if (rmsource != null) {

						message.setString(tudSrcUID.FIELD, rmsource.getUID());
						message.setString(tudSrcIDAtsvr.FIELD,
								rmsource.getIDAtsvr());
						message.setString(tudSrcUType.FIELD,
								rmsource.getUType());
						message.setString(tudBasketOrder.FIELD, "0");
						message.getHeader().setString(MsgType.FIELD,
								MsgType.hedCancelRequest);
						process_reqwithdraw(sessionid, message);
					}
				}

			} catch (Exception e) {		
				log.error(logger_info_exception(e));
				
			}
		} else if (sheader.equals("q1")) {
			log.info("receive mass order");
			String ssrcuid = message.getString(tudSrcUID.FIELD);
			String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			String ssrcutype = message.getString(tudSrcUType.FIELD);
			RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);
			init_forscheduller(message, rmsource);

			boolean brouting = proxyRouting.processRouting(sessionid, message);

		} else if (sheader.equals("NOTIF")) {
			process_entryNotif(sessionid, message);			
		} else if(sheader.equals("OM")){
			process_entryOrderMatrix(sessionid, message);			
		} else if (sheader.equals("DOM")) {
			process_deleteordermatrix(sessionid, message);
		} else if (sheader.equals("DOMBL")) {
			process_deleteordermatrixbylabel(sessionid, message);
		} else if (sheader.equals(MsgType.hedGTC))	{
			try {
				Vector<Object> varg = new Vector<Object> (10,5);
				StringBuffer sbfile = new StringBuffer(100);
				
				FOData fodata = new FOData();
				
				String sentrytime = message.getString(tudEntryTime.FIELD);
				String OrderId = message.getString(ClOrdID.FIELD);
				String MarketOrderId = message.getString(OrderID.FIELD);
				String ClientId = message.getString(ClientID.FIELD);
				String BuyorSell = message.getString(Side.FIELD);
				String Stock = message.getString(Symbol.FIELD);
				Double price = get_doubletype(message.getString(Price.FIELD), 0);
				Double Amount = get_doubletype(message.getString(OrderQty.FIELD), 0);
				String ValidUntil = message.getString(999104);
				String entryby = message.getString(tudEntryBy.FIELD);
				String OrderStatus = message.getString(999105);
				String accId = message.getString(tudAccountID.FIELD);
				String symbol = message.getString(Symbol.FIELD);
				String BRDID = message.getString(SymbolSfx.FIELD);
				String entryterminal = message.getString(tudEntryTerminal.FIELD);
				InfoSymbolData isd = is.get_isd(BRDID, symbol);
				boolean isdnull = isd == null;
				if (isd == null)
					throw new Exception("ABCDEFGHIJK");
				Double qty = get_doubletype(message.getString(OrderQty.FIELD), 0);
				Double lotsize = qty / isd.get_lotsize();
				String ssales = message.getString(ClientID.FIELD);
				
				String sendingtime = message.getString(TransactTime.FIELD);
				Date entrytime = getDateTime1(sentrytime);
				Date validuntil = getDateTime1(ValidUntil);
				
				
				/*tyhthythy*hentryby.put(sclordid, ssales);
				hcustidby.put(sclordid, accid);

				String basketorder = message.getString(tudBasketOrder.FIELD);

				ssrcuid = message.getString(tudSrcUID.FIELD);
				ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				ssrcutype = message.getString(tudSrcUType.FIELD);

				// mapclordid_origclordid.put(sclordid, sorigclordid);

				//RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);*/

				Object[] objs = new Object[14];
				objs[0] = entrytime;
				objs[1] = OrderId;
				objs[2] = MarketOrderId;
				objs[3] = accId;
				objs[4] = BuyorSell;
				objs[5] = Stock;
				objs[6] = lotsize;
				objs[7] = price;
				objs[8] = Amount;
				objs[9] = validuntil;
				objs[10] = entryby;
				objs[11] = OrderStatus;
				objs[12] = BRDID;

				objs[13] = entryterminal;
				fodata.set_objects(objs);
				Vector<Object> vresult = null;

				// RMType rmsourcebefore = get_basedonclordid(sorigclordid);//
				// mclord_rm.get(sorigclordid);

				vresult = queryData.getOrderData().set_insert(
						SQUERY.creategtc.val, fodata);
			
			}catch(Exception e){e.printStackTrace();
				
			}
			
		} else if (sheader.equals(MsgType.hedWithdrawGtc))	{
			System.out.println("MSGTYPE");
			try{
				Vector<Object> varg = new Vector<Object> (10,5);
				StringBuffer sbfile = new StringBuffer(100);
				
				FOData fodata = new FOData();
				
				String OrderId = message.getString(ClOrdID.FIELD);
				String ClientId = message.getString(ClientID.FIELD);
				/*String MarketOrderId = message.getString(OrderID.FIELD);
				String BuyorSell = message.getString(Side.FIELD);
				String Stock = message.getString(Symbol.FIELD);
				Double price = get_doubletype(message.getString(Price.FIELD), 0);
				Double Amount = get_doubletype(message.getString(OrderQty.FIELD), 0);
				String ValidUntil = message.getString(999104);
				String entryby = message.getString(tudEntryBy.FIELD);*/
				String OrderStatus = message.getString(999105);
				String WithdrawTime = message.getString(666671);
				String WithdrawBy = message.getString(666677);
				String WithdrawTerminal = message.getString(666679);
				Date withdrawtime = getDateTime1 (WithdrawTime);
				/*String symbol = message.getString(Symbol.FIELD);
				String BRDID = message.getString(SymbolSfx.FIELD);
				InfoSymbolData isd = is.get_isd(BRDID, symbol);
				boolean isdnull = isd == null;
				if (isd == null)
					throw new Exception("ABCDEFGHIJK");
				Double qty = get_doubletype(message.getString(OrderQty.FIELD), 0);
				Double lotsize = qty / isd.get_lotsize();
				String ssales = message.getString(ClientID.FIELD);
				
				
				String sendingtime = message.getString(TransactTime.FIELD);
				Date entrytime = getDateTime1(sentrytime);
				Date validuntil = getDateTime1(ValidUntil);
				*/
				
				/*hentryby.put(sclordid, ssales);
				hcustidby.put(sclordid, accid);

				String basketorder = message.getString(tudBasketOrder.FIELD);

				ssrcuid = message.getString(tudSrcUID.FIELD);
				ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				ssrcutype = message.getString(tudSrcUType.FIELD);

				// mapclordid_origclordid.put(sclordid, sorigclordid);

				//RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);*/

				Object[] objs = new Object[5];
				objs[0] = OrderId;
				objs[1] = OrderStatus;
				objs[2] = withdrawtime;
				objs[3] = WithdrawBy;
				objs[4] = WithdrawTerminal;
				/*objs[2] = MarketOrderId;
				objs[3] = ClientId;
				objs[4] = BuyorSell;
				objs[5] = Stock;
				objs[6] = lotsize;
				objs[7] = price;
				objs[8] = Amount;
				objs[9] = validuntil;
				objs[10] = entryby;
				objs[11] = OrderStatus;
				objs[12] = BRDID;*/

				fodata.set_objects(objs);
				Vector<Object> vresult = null;

				// RMType rmsourcebefore = get_basedonclordid(sorigclordid);//
				// mclord_rm.get(sorigclordid);

				vresult = queryData.getOrderData().set_insert(
						SQUERY.withdrawgtc.val, fodata);
				//OrderId = (String)vresult.elementAt(0);
				OrderStatus = (String)vresult.elementAt(1);
				withdrawtime = (Date)vresult.elementAt(2);
				
				String ssrcuid = null, ssrcidatsvr = null, ssrcutype = null, status = null;
				
				ssrcuid = message.getString(tudSrcUID.FIELD);
				ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				ssrcutype = message.getString(tudSrcUType.FIELD);
				
				Message executionreport = new Message(); 
				executionreport.setString(tudDestUID.FIELD, ssrcuid); // "gtw");
				executionreport.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr); // "1234");
				executionreport.setString(tudDestUType.FIELD, ssrcutype); // "gtw");
				executionreport.setString(ClOrdID.FIELD, OrderId);
				executionreport.setString(gtcstatus.FIELD, OrderStatus);
				//executionreport.setString(gtcstatus.FIELD, "withdrawgtc");
				executionreport.setString(ClientID.FIELD,ClientId);
				executionreport.setString(OrderID.FIELD, OrderId);
				executionreport.setString(tudSendtime.FIELD, WithdrawTime);
				
				executionreport.getHeader().setString(MsgType.FIELD,
						MsgType.hedExecutionReport);
			
				boolean bresult = comHandling.send_appmsg(sessionid, executionreport);
				if(bresult){
					System.out.println("message send true "+message.toString());
				}else {
					System.out.println("message send false "+message.toString());
				}
			}catch(Exception e){e.printStackTrace();
				
			}
			
		} else if (sheader.equals(MsgType.hedAmmedGtc))	{
			try{
				Vector<Object> varg = new Vector<Object> (10,5);
				StringBuffer sbfile = new StringBuffer(100);
				
				FOData fodata = new FOData(); 

				//String accid = message.getString(tudAccountID.FIELD);
				//String clientid = message.getString(ClientID.FIELD);

				String OrderId = message.getString(ClOrdID.FIELD);
				String sorigclordid = message.getString(OrigClOrdID.FIELD);
				//String MarketOrderId = message.getString(OrderID.FIELD);
				String accId = message.getString(tudAccountID.FIELD);
				String ClientId = message.getString(ClientID.FIELD);
				String BuyorSell = message.getString(Side.FIELD);
				String Stock = message.getString(Symbol.FIELD);
				Double price = get_doubletype(message.getString(Price.FIELD), 0);
				Double Amount = get_doubletype(message.getString(OrderQty.FIELD), 0);
				String ValidUntil = message.getString(999104);
				String entryby = message.getString(tudEntryBy.FIELD);
				//String OrderStatus = message.getString(999105);
				String AmendTime = message.getString(666671);
				String AmendBy = message.getString(666677);
				String AmendTerminal = message.getString(666679);
				Date amendtime = getDateTime1 (AmendTime);
				String symbol = message.getString(Symbol.FIELD);
				String BRDID = message.getString(SymbolSfx.FIELD);
				InfoSymbolData isd = is.get_isd(BRDID, symbol);
				boolean isdnull = isd == null;
				if (isd == null)
					throw new Exception("ABCDEFGHIJK");
				Double qty = get_doubletype(message.getString(OrderQty.FIELD), 0);
				Double lotsize = qty / isd.get_lotsize();
				//String ssales = message.getString(ClientID.FIELD);
				Date validuntil = getDateTime1(ValidUntil);
				/*
				String sendingtime = message.getString(TransactTime.FIELD);
				Date entrytime = getDateTime1(sentrytime);
				Date validuntil = getDateTime1(ValidUntil);
				
				
				hentryby.put(sclordid, ssales);
				hcustidby.put(sclordid, accid);

				String basketorder = message.getString(tudBasketOrder.FIELD);

				ssrcuid = message.getString(tudSrcUID.FIELD);
				ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				ssrcutype = message.getString(tudSrcUType.FIELD);*/

				// mapclordid_origclordid.put(sclordid, sorigclordid);

				//RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);

				Object[] objs = new Object[14];
				objs[0] = OrderId;
				//objs[1] = OrderStatus;
				objs[1] = amendtime;
				objs[2] = AmendBy;
				objs[3] = AmendTerminal;
				objs[4] = sorigclordid;
				
				//objs[6] = MarketOrderId;
				objs[5] = accId;
				objs[6] = BuyorSell;
				objs[7] = Stock;
				objs[8] = lotsize;
				objs[9] = price;
				objs[10] = Amount;
				objs[11] = validuntil;
				objs[12] = entryby;
				//objs[14] = OrderStatus;
				objs[13] = BRDID;

				fodata.set_objects(objs);
				Vector<Object> vresult = null;

				// RMType rmsourcebefore = get_basedonclordid(sorigclordid);//
				// mclord_rm.get(sorigclordid);

				vresult = queryData.getOrderData().set_insert(
						SQUERY.ammendgtc.val, fodata);
				
				amendtime = (Date)vresult.elementAt(2);
				
				String ssrcuid = message.getString(tudSrcUID.FIELD);
				String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
				String ssrcutype = message.getString(tudSrcUType.FIELD);
				String sclordid = message.getString(ClOrdID.FIELD);

				Message executionreport = new Message(); 
				executionreport.setString(tudDestUID.FIELD, ssrcuid); // "gtw");
				executionreport.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr); // "1234");
				executionreport.setString(tudDestUType.FIELD, ssrcutype); // "gtw")
				executionreport.setString(ClOrdID.FIELD, sorigclordid);
				executionreport.setString(gtcstatus.FIELD, "A");
				
				//executionreport.setString(gtcstatus.FIELD, OrderStatus);
				//executionreport.setString(gtcstatus.FIELD, "withdrawgtc");
				executionreport.setString(ClientID.FIELD,ClientId);
				executionreport.setString(OrderID.FIELD, OrderId);
				executionreport.setString(tudSendtime.FIELD, AmendTime);
				//executionreport.setString(tudSendtime.FIELD, WithdrawTime);
				
				Message executionreportnew = new Message ();
				executionreportnew.setString(tudDestUID.FIELD, ssrcuid); // "gtw");
				executionreportnew.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr); // "1234");
				executionreportnew.setString(tudDestUType.FIELD, ssrcutype); // "gtw")
				executionreportnew.setString(ClOrdID.FIELD, OrderId);
				
				//executionreport.setString(gtcstatus.FIELD, OrderStatus);
				//executionreport.setString(gtcstatus.FIELD, "withdrawgtc");
				executionreportnew.setString(ClientID.FIELD,ClientId);
				executionreportnew.setString(OrderID.FIELD, OrderId);
				executionreportnew.setString(tudSendtime.FIELD, AmendTime);
				
				executionreport.getHeader().setString(MsgType.FIELD,
						MsgType.hedExecutionReport);
				
				executionreportnew.getHeader().setString(MsgType.FIELD,
						MsgType.hedExecutionReport);
			
				boolean bresult = comHandling.send_appmsg(sessionid, executionreport);
				if(bresult){
					System.out.println("message send true "+message.toString());
				}else {
					System.out.println("message send false "+message.toString());
				}
			}catch(Exception e){e.printStackTrace();
				
			}	 
		} else {
			String stext = message.getString(Text.FIELD);
			int num = 0;
			if (stext != null && !stext.isEmpty()) {
				num = stext.getBytes().length;
			}
			//log.info("rec_msg_with_header:" + sheader + ":lngth_of_msg:" + num);
		}
	} 
	

	private void process_splitentry(String sessionid, Message message) {
		//log.info("receive for entrysplitord:sessionid:" + sessionid + ":"	+ message.toString());

		String ssrcuid = message.getString(tudSrcUID.FIELD);
		String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
		String ssrcutype = message.getString(tudSrcUType.FIELD);
		// String ssales = message.getString(tudSales.FIELD);
		String accid = message.getString(tudAccountID.FIELD);
		String clientid = message.getString(ClientID.FIELD);
		RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);
		String sclordid = message.getString(ClOrdID.FIELD);

		try {

			String sendingtime = message.getString(TransactTime.FIELD);

			if (sendingtime == null || sendingtime.isEmpty()) {
				sendingtime = getStringTime1(new Date());
			}

			String sentrytime = message.getString(tudEntryTime.FIELD);

			Date entrytime = getDateTime1(sentrytime);

			Date orderdate = getDateTime1(sentrytime);
			String xchid = message.getString(tudExchange.FIELD);
			String board = message.getString(SymbolSfx.FIELD);

			String side = message.getString(Side.FIELD);

			side = side.equals("5") ? "2" : side.equals("M") ? "1" : side;

			String ordertype = message.getString(TimeInForce.FIELD);
			// secid
			String symbol = message.getString(Symbol.FIELD);

			String invtype = message.getString(Account.FIELD);
			Double price = message.getDouble(Price.FIELD);

			InfoSymbolData isd = is.get_isd(board, symbol);
			boolean isdnull = isd == null;

			// log.info("isd is null " + isdnull);

			if (isd == null)
				throw new Exception("Security ID not found on server");

			String basketorder = message.getString(tudBasketOrder.FIELD);
			Date basketsendtime = basketorder.equals("1") ? getDateTime1(message
					.getString(tudBasketSendTime.FIELD)) : null;

			String entryby = message.getString(tudEntryBy.FIELD);

			String entryterminal = message.getString(tudEntryTerminal.FIELD);

			Double splitto = message.getDouble(tudSplitTo.FIELD);
			Double counter = message.getDouble(tudCounter.FIELD);
			String marketref = message.getString(tudMarketRef.FIELD);

			int handlinst = message.getInt(HandlInst.FIELD);

			List<String> vfield = null;

			if (handlinst == HandlInst.NORMAL) {
				// insert inputan entry order
				// log.info("process entry order " + sclordid);

				String[] sclordids = sclordid.split("!");
				String[] oqtys = message.getString(OrderQty.FIELD).split("!");
				String[] marketrefs = marketref.split("!");

				Hashtable<Integer, String> hresult = new Hashtable<Integer, String>();

				String ordstatus = "";

				log.info("trying to execute bulk split order with size "
						+ sclordids.length);

				for (int i = 0; i < sclordids.length; i++) {

					

					sclordid = sclordids[i];

					if (!sclordid.isEmpty()) {
						
						FOData fodata = new FOData();

						Vector<Object> vresult = null;

						Double oqty = Double.parseDouble(oqtys[i]);
						marketref = marketrefs[i];

						Double lotsize = oqty / isd.get_lotsize();

						Object[] objs = new Object[21];
						objs[0] = sclordid;
						objs[1] = orderdate;
						objs[2] = xchid;
						objs[3] = board;
						objs[4] = side;
						objs[5] = ordertype;
						objs[6] = symbol;
						objs[7] = accid;
						objs[8] = invtype;
						objs[9] = price;
						objs[10] = lotsize;
						objs[11] = oqty;
						objs[12] = basketorder;
						objs[13] = basketsendtime;
						objs[14] = entrytime;
						objs[15] = entryby;
						objs[16] = entryterminal;
						objs[17] = splitto;
						objs[18] = counter;
						objs[19] = marketref;
						objs[20] = message.getString(tudIsFloor.FIELD);
						fodata.set_objects(objs);

						// print_pair("before inserted");

						vresult = queryData.getOrderData().set_insert(
								SQUERY.order.val, fodata);
						log.info("result inserted to database:" + sclordid);

						vfield = queryData.getOrderData().mapoutparameter
								.get(SQUERY.order.val);

						String sc = hresult.get(ClOrdID.FIELD);
						if (sc == null) {
							sc = "";
						}
						sc = sc + "!" + sclordid;
						hresult.put(ClOrdID.FIELD, sc);

						String mk = hresult.get(tudMarketRef.FIELD);
						if (mk == null)
							mk = "";

						mk = mk + "!" + marketref;

						hresult.put(tudMarketRef.FIELD, mk);
						
						//log.info("vresult "+vresult.size());

						if (vresult.size() > 0) {

							OrderOut orderout = (OrderOut) vresult.elementAt(0);

							ordstatus = orderout.getStatusorder();

							// ordstatus = ordstatus+"!"+sordstatus;
							//for(Object v : vresult)
								//log.info("result data order "+v.toString());

							orderout.loadMessage(hresult, maptag, vfield);
							
							//log.info("result tag from db "+hresult.toString());

							if (orderout.getSukses() == 1) {
								mclord_rm.put(sclordid, rmsource);
								// mclord_uid.put(sclordid, clientid);
								hentryby.put(sclordid, clientid);
								hcustidby.put(sclordid, accid);

								StringBuffer sbfile = new StringBuffer(100);
								sbfile.append(sclordid);
								sbfile.append("###");
								sbfile.append(accid);
								save_datatofile(sbfile.toString(),
										sclordid_accid);

							}

						} else{
							log.info("result of database is empty " + sclordid);
						}
						
						
						

					}
					
					// send to jonec or queue
					//ordstatus = hresult.get(OrdStatus.FIELD);
					
				}
				
				// send reply back to client (o or R)
				Message executionreport = new Message();
				executionreport.getHeader().setString(MsgType.FIELD,
						MsgType.hedSplitReplyOrder);
				executionreport.setString(MessageSchedullerType.FIELD,
						MsgType.hedExecutionReport);

				Iterator<Integer> iters = hresult.keySet().iterator();
				while (iters.hasNext()) {
					Integer tag = iters.next();
					String sdata = hresult.get(tag);
					executionreport.setString(tag, sdata);
				}

				executionreport.setString(tudDestUID.FIELD,
						rmsource.getUID());
				executionreport.setString(tudDestIDAtsvr.FIELD,
						rmsource.getIDAtsvr());
				executionreport.setString(tudDestUType.FIELD,
						rmsource.getUType());
				executionreport.setString(ClientID.FIELD, clientid);
				executionreport.setString(tudAccountID.FIELD, accid);

				
				log.info("send to client "+executionreport.toString());

				Message ortmessage = (Message) executionreport.clone();
				comHandling.send_appmsg(sessionid, executionreport);

				ortmessage.setString(tudIsOrdTaker.FIELD, "1");
				proxyRouting.processTaker(sessionid, ortmessage);
				
				ordstatus = hresult.get(OrdStatus.FIELD);
				log.info("order status reply "+ordstatus);
				if (ordstatus.contains("o")) {
					init_forscheduller(message, rmsource);

					boolean brouting = proxyRouting.processRouting(
							sessionid, message);
				}

				
				// log.info("result of database:" + vresult + " " + sclordid);
				// print_pair("after inserted:" + vresult.size());
			} else {
				log.info("cannot process this message " + handlinst);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(logger_info_exception(e));
		}

	}

	private void process_routingtoqueue(Message message, String sessionid) {
		String ssrcuid = message.getString(tudSrcUID.FIELD);
		String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
		String ssrcutype = message.getString(tudSrcUType.FIELD);
		// String ssales = message.getString(tudSales.FIELD);
		String accid = message.getString(tudAccountID.FIELD);
		String clientid = message.getString(ClientID.FIELD);
		RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);
		init_forscheduller(message, rmsource);
		// log.info("opend order:" + message);

		if (message != null) {

			log.info("routing to jones:" + message);
			boolean brouting = proxyRouting.processRouting(sessionid, message);
			// print_pair(" brouting to entryord : " +
			// brouting);

		}
	}

	private void process_scheduller(String sessionid, Message message) {
		// TODO Auto-generated method stub
		log.info("processing scheduller message :" + message.toString());
		// message = (Message) message.clone();
		String header = message.getString(MessageSchedullerType.FIELD);

		message.getHeader().setString(MsgType.FIELD, header);
		String stime = message.getString(tudBasketSendTime.FIELD);

		String uid = muid.getSjonecuid(), idatsvr = muid.getSjonecidatsvr(), utype = muid
				.getSjonecutype();
		String key = message.getString(tudBasketKey.FIELD);

		message.setString(tudDestUID.FIELD, uid);
		message.setString(tudDestIDAtsvr.FIELD, idatsvr);
		message.setString(tudDestUType.FIELD, utype);

		// print_pair("processing scheduller message :" + message.toString());

		proxyRouting.processRouting(sessionid, message);

	}

	private void process_cleansession(String sessionid, Message message) {
		// TODO Auto-generated method stub
		// print_pair("receive for clean session : " + sessionid);
		// print_pair("receive fro clean session data : " + message);
		// log.info("receive for clean session:"+message.toString());

		try {
			// FixMessage fxmsg = new FixMessage(data);

			String sclientid = message.getString(tudUserID.FIELD);
			// sclientid = sclientid.toLowerCase();

			// kill session info
			String sdata = honline.remove(sclientid);

			// System.out.println(sclientid + " has been removed ");
			log.info("clean session for client:" + sclientid + ":"
					+ (sdata != null && !sdata.isEmpty()));

			message.getHeader().setString(MsgType.FIELD,
					MsgType.hedLogoutClient);
			message.setString(ClientID.FIELD, sclientid);
			message.setString(tudIsOrdTaker.FIELD, "1");
			proxyRouting.processTaker(sessionid, message);

			if (sdata != null) {
				String[] split = sdata.split("\\|", -2);
				HashMap params = new HashMap();
				params.put(0, new Long(split[3]));

				comHandling.getEventDispatcher().pushEvent(
						EventDispatcher.EVENT_SESSION_LOGOUT, params);

				Object[] obj = new Object[5];
				obj[0] = "0";
				obj[1] = sclientid;
				obj[2] = new Date();
				obj[3] = split[3];
				obj[4] = "Logout Success";
				FOData fodata = new FOData();
				fodata.set_objects(obj);

				queryData.getOrderData().set_insert(SQUERY.entryloguser.val,
						fodata);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void process_logonclient(String sessionid, Message message) {
		// print_pair("receive for logon client : " + sessionid);
		// print_pair("receive for logon client data : " + message);

		log.info("receive for logon client:" + message.toString());

		try {
			String uid = message.getString(tudUserID.FIELD);
			String sessiongtw = message.getString(tudStartSession.FIELD);

			// uid = uid.toLowerCase();

			String sheader;

			boolean bkickuser = false;

			String slogout = null, solduid = null;
			if (honline.containsKey(uid)) {

				// send reply logon bad
				slogout = hedLogoutClient;
				solduid = new String(honline.get(uid));

				bkickuser = true;
			}

			log.info("kick user session:" + bkickuser + ":" + uid);
			if (bkickuser) {
				String slogsrc, slogidatsvr, slogutype, sessgtw;

				String[] split = solduid.split("\\|", -2);
				slogsrc = split[0];
				slogidatsvr = split[1];
				slogutype = split[2];
				sessgtw = split[3];

				message.setString(tudDestUID.FIELD, slogsrc);
				message.setString(tudDestIDAtsvr.FIELD, slogidatsvr);
				message.setString(tudDestUType.FIELD, slogutype);
				message.setString(Text.FIELD,
						"killed because you're login at another terminal");
				message.setString(tudUserID.FIELD, uid);
				message.setString(tudStartSession.FIELD, sessgtw);
				message.setString(ClientID.FIELD, uid);

				Message mkick = (Message) message.clone();
				mkick.getHeader().setString(MsgType.FIELD,
						MsgType.hedLogoutClient);
				if (mkick != null) {
					boolean bkickmsg = this.comHandling.send_appmsg(sessionid,
							mkick);
					// print_pair(" bkikc user session : " + bkickmsg);
					HashMap params = new HashMap();
					params.put(0, new Long(sessgtw));

					comHandling.getEventDispatcher().pushEvent(
							EventDispatcher.EVENT_SESSION_LOGOUT, params);

					Object[] obj = new Object[5];
					obj[0] = "0";
					obj[1] = uid;
					obj[2] = new Date();
					obj[3] = sessgtw;
					obj[4] = "killed because you're login at another terminal";
					FOData fodata = new FOData();
					fodata.set_objects(obj);

					queryData.getOrderData().set_insert(
							SQUERY.entryloguser.val, fodata);

				}
			}

			// send reply logon ok
			sheader = hedLogonClient;

			String ssrcuid = message.getString(tudSrcUID.FIELD);
			String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			String ssrcutype = message.getString(tudSrcUType.FIELD);
			String sencrypt = message.getString(EncryptMethod.FIELD);

			StringBuffer sb = new StringBuffer(100);
			sb.append(ssrcuid).append("|");
			sb.append(ssrcidatsvr).append("|");
			sb.append(ssrcutype).append("|");
			sb.append(sessiongtw);

			String test = honline.put(uid, sb.toString());

			message.setString(tudDestUID.FIELD, ssrcuid);
			message.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr);
			message.setString(tudDestUType.FIELD, ssrcutype);
			message.setString(Text.FIELD, "Logon Ok");
			message.setString(tudUserID.FIELD, uid);
			message.setString(tudStartSession.FIELD, sessiongtw);
			message.setString(EncryptMethod.FIELD, sencrypt);

			// HeaderAndData handd = new HeaderAndData(sheader, hdata);
			Message ortmessage = (Message) message.clone();
			if (message != null) {
				boolean brouting = this.comHandling.send_appmsg(sessionid,
						message);
				// hcounter.remove(uid);

			}

			ortmessage.setString(tudIsOrdTaker.FIELD, "1");
			ortmessage.setString(ClientID.FIELD, uid);
			proxyRouting.processTaker(sessionid, ortmessage);

			HashMap params = new HashMap();
			params.put(0, uid);
			params.put(1, ssrcuid + "|" + ssrcidatsvr + "|" + ssrcutype + "|"
					+ sessiongtw + "|" + "LOGIN");
			comHandling.getEventDispatcher().pushEvent(
					EventDispatcher.EVENT_SESSION_LOGGEDIN, params);

			Object[] obj = new Object[5];
			obj[0] = "1";
			obj[1] = uid;
			obj[2] = new Date();
			obj[3] = sessiongtw;
			obj[4] = "Login Success";
			FOData fodata = new FOData();
			fodata.set_objects(obj);

			queryData.getOrderData()
					.set_insert(SQUERY.entryloguser.val, fodata);

			StringBuffer sbfile = new StringBuffer(100);
			sbfile.append(uid);
			sbfile.append("###");
			sbfile.append(ssrcuid).append("|");
			sbfile.append(ssrcidatsvr).append("|");
			sbfile.append(ssrcutype).append("|");
			sbfile.append(sessiongtw).append("|");
			sbfile.append("1");

			save_datatofile(sbfile.toString(), sfonline);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void process_logoutclient(String sessionid, Message message) {
		log.info("receive for logout client:" + message.toString());
		try {

			String ssrcuid = message.getString(tudSrcUID.FIELD);
			String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			String ssrcutype = message.getString(tudSrcUType.FIELD);

			String uid = message.getString(tudUserID.FIELD);
			// uid = uid.toLowerCase();

			String sdata = honline.remove(uid);

			// log.info("logouting client:" + uid + ":"
			// + (sdata != null && !sdata.isEmpty()));

			message.setString(tudDestUID.FIELD, ssrcuid);
			message.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr);
			message.setString(tudDestUType.FIELD, ssrcutype);

			String sheader = hedLogoutClient;
			// HeaderAndData handd = new HeaderAndData(sheader, hdata);
			if (message != null) {
				Message ortmessage = (Message) message.clone();
				boolean brouting = this.comHandling.send_appmsg(sessionid,
						message);
				// print_pair(" brouting generate session : " + brouting);
				ortmessage.setString(tudIsOrdTaker.FIELD, "1");
				proxyRouting.processTaker(sessionid, ortmessage);

				String[] split = sdata.split("\\|", -2);
				HashMap params = new HashMap();
				params.put(0, new Long(split[3]));

				comHandling.getEventDispatcher().pushEvent(
						EventDispatcher.EVENT_SESSION_LOGOUT, params);

				Object[] obj = new Object[5];
				obj[0] = "0";
				obj[1] = uid;
				obj[2] = new Date();
				obj[3] = split[3];
				obj[4] = "Logout Success";
				FOData fodata = new FOData();
				fodata.set_objects(obj);

				queryData.getOrderData().set_insert(SQUERY.entryloguser.val,
						fodata);

				StringBuffer sbfile = new StringBuffer(100);
				sbfile.append(uid);
				sbfile.append("###");
				sbfile.append(ssrcuid).append("|");
				sbfile.append(ssrcidatsvr).append("|");
				sbfile.append(ssrcutype).append("|");
				sbfile.append(split[3]).append("|");
				sbfile.append("0");

				save_datatofile(sbfile.toString(), sfonline);
			}

			// send reply logout ok
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void process_requestsession(String sessionid, Message message) {
		try {

			String ssrcuid = message.getString(tudSrcUID.FIELD);
			String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			String ssrcutype = message.getString(tudSrcUType.FIELD);

			// ssrcuid, ssrcidatsvr, ssrcutype : gtw, 0123, gtw
			StringBuffer sb = new StringBuffer(100);
			sb.append(ssrcuid).append("|");
			sb.append(ssrcidatsvr).append("|");
			sb.append(ssrcutype);

			String skey = sb.toString();
			int nstart = nsession + 1;
			int nend = nsession + 100;
			nsession = nend;

			String sdata = nstart + "|" + nend;
			hreqsession.put(skey, sdata);
			String data = message.toString();

			message.setString(tudDestUID.FIELD, ssrcuid);
			message.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr);
			message.setString(tudDestUType.FIELD, ssrcutype);

			message.setString(tudStartSession.FIELD, nstart + "");
			message.setString(tudEndSession.FIELD, nend + "");

			// String sheader = hedRequestSession;
			// HeaderAndData handd = new HeaderAndData(sheader, hdata);

			boolean brouting = this.comHandling.send_appmsg(sessionid, message);
			log.info("request session:" + skey + ":" + sdata);

			StringBuffer sbfile = new StringBuffer(100);
			sbfile.append(skey);
			sbfile.append("###");
			sbfile.append(data);
			save_datatofile(sbfile.toString(), sfreqsession);

			write_file_binary(slastseq, new Integer(nsession));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	protected void process_entryNotif(String sessionid, Message message) {
		log.info("receive for Notification:sessionid:" + sessionid + ":"
				+ message.toString());
		try {
			FOData fodata = new FOData();

			Vector<Object> vresult = null;
			List<String> vfield = null;

			String userid = message.getString(notifUserId.FIELD);
			String notif = message.getString(notifID.FIELD);
			String selldone = message.getString(notifOnSell.FIELD);
			String buydone = message.getString(notifOnBuy.FIELD);
			String sendSms = message.getString(notifSendSms.FIELD);
			String mobile1 = message.getString(notifMobile1.FIELD);
			String mobile2 = message.getString(notifMobile2.FIELD);
			String mobile3 = message.getString(notifMobile3.FIELD);
			String sendemail = message.getString(notifSenEmail.FIELD);
			String email1 = message.getString(notifEmail1.FIELD);
			String email2 = message.getString(notifEmail2.FIELD);
			String email3 = message.getString(notifEmail3.FIELD);

			Object[] objs = new Object[12];
			objs[0] = userid;
			objs[1] = notif;
			objs[2] = selldone;
			objs[3] = buydone;
			objs[4] = sendSms;
			objs[5] = mobile1;
			objs[6] = mobile2;
			objs[7] = mobile3;
			objs[8] = sendemail;
			objs[9] = email1;
			objs[10] = email2;
			objs[11] = email3;
			fodata.set_objects(objs);
			vresult = queryData.getOrderData().set_insert(SQUERY.notofication.val, fodata);
			vfield = queryData.getOrderData().mapoutparameter.get(SQUERY.notofication.val);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	protected void process_entryOrderMatrix(String sessionid, Message message) {		
		
		log.info("receive for entryordermatrix : "+message.toString()+ " sessionid : " +sessionid);		
		
		try {
			FOData fodata = new FOData();

			Vector<Object> vresult = null;
			
			int nourut = message.getInt(OrderMatrixNoUrut.FIELD);
			String ordermatrixid = message.getString(OrderMatrixID.FIELD);
			String clientid = message.getString(ClientID.FIELD);
			String userid = message.getString(tudEntryBy.FIELD);
			String label = message.getString(atsLabel.FIELD);
			String buyorsell = message.getString(Side.FIELD);
			String secid = message.getString(Symbol.FIELD);
			Double lot = message.getDouble(Lot.FIELD);
			Double price = message.getDouble(Price.FIELD);
			Double amount = message.getDouble(Amount.FIELD);
			Double volume = message.getDouble(OrderQty.FIELD);			
			String isbasketorder = message.getString(tudBasketOrder.FIELD);
			String basketsendtime = isbasketorder.equals("1") ? message.getString(OrderMatrixBasketSendTime.FIELD) : null;		
			
			Object[] objs = new Object[13];
			 
			objs[0] = ordermatrixid;	
			objs[1] = clientid;	
			objs[2] = userid;
			objs[3] = label;
			objs[4]	= buyorsell;
			objs[5] = secid;
			objs[6] = lot;
			objs[7] = price;	
			objs[8] = amount;
			objs[9] = volume;	
			objs[10] = isbasketorder;
			objs[11] = basketsendtime;		
			objs[12] = nourut;
			
			fodata.set_objects(objs);
			
			vresult = queryData.getOrderData().set_insert(SQUERY.entryordermatrix.val, fodata);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}		
	}
	
	protected void process_deleteordermatrixdetil(String sessionid, Message message) {
		try {
			FOData fodata = new FOData();

			Vector<Object> vresult = null;
			
			String userid = message.getString(tudUserID.FIELD);
			String label = message.getString(atsLabel.FIELD);
			//Double autonum = message.getDouble(autoNumOrderMatrix.FIELD);
			
			Object[] obj = new Object[3];
			obj[0] = userid;
			obj[1] = label;
			//obj[2] = autonum;
			
			fodata.set_objects(obj);
			vresult = queryData.getOrderData().set_insert(SQUERY.deleteordermatrixdetil.val, fodata);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void process_deleteordermatrix(String sessionid, Message message){
		log.info("receive for deleteordermatrix : "+message.toString()+" sessionid : "+sessionid);		
		try {
			FOData fodata = new FOData();

			Vector<Object> vresult = null;

			String ordermatrixid = message.getString(OrderMatrixID.FIELD);
			
			System.out.println("delete omid : "+ordermatrixid);
			
			Object[] objs = new Object[1];
			objs[0] = ordermatrixid;
			fodata.set_objects(objs);			
			
			vresult = queryData.getOrderData().set_insert(SQUERY.deleteordermatrix.val, fodata);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void process_deleteordermatrixbylabel(String sessionid, Message message) {
		log.info("receive for deleteordermatrixbylabel : "+message.toString()+" sessionid : "+sessionid);	
		
		try {
			FOData fodata = new FOData();

			Vector<Object> vresult = null;

			String userid = message.getString(tudEntryBy.FIELD);
			String label = message.getString(atsLabel.FIELD);			
			
			Object[] objs = new Object[2];
			objs[0] = userid;
			objs[1] = label;
			fodata.set_objects(objs);			
			
			vresult = queryData.getOrderData().set_insert(SQUERY.deleteordermatrixbylabel.val, fodata);	
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected void process_entryord(String sessionid, Message message) {
		log.info("receive for entryord:sessionid:" + sessionid + ":"+ message.toString());

		String ssrcuid = message.getString(tudSrcUID.FIELD);
		String ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
		String ssrcutype = message.getString(tudSrcUType.FIELD);
		// String ssales = message.getString(tudSales.FIELD);
		String accid = message.getString(tudAccountID.FIELD);
		String clientid = message.getString(ClientID.FIELD);
		RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);

		String sclordid = message.getString(ClOrdID.FIELD);
		try {
			String sendingtime = message.getString(TransactTime.FIELD);

			if (sendingtime == null || sendingtime.isEmpty()) {
				sendingtime = getStringTime1(new Date());
			}

			String sentrytime = message.getString(tudEntryTime.FIELD);

			Date entrytime = getDateTime1(sentrytime);

			Date orderdate = getDateTime1(sentrytime);
			String xchid = message.getString(tudExchange.FIELD);
			String board = message.getString(SymbolSfx.FIELD);

			String side = message.getString(Side.FIELD);

			side = side.equals("5") ? "2" : side.equals("M") ? "1" : side;

			String ordertype = message.getString(TimeInForce.FIELD);
			// secid
			String symbol = message.getString(Symbol.FIELD);

			String invtype = message.getString(Account.FIELD);
			Double price = message.getDouble(Price.FIELD);
			Double oqty = message.getDouble(OrderQty.FIELD);

			InfoSymbolData isd = is.get_isd(board, symbol);
			boolean isdnull = isd == null;

			log.info("isd is null " + isdnull);

			if (isd == null)
				throw new Exception("Security ID not found on server");

			Double lotsize = oqty / isd.get_lotsize();
			
			String basketorder = message.getString(tudBasketOrder.FIELD);
			Date basketsendtime = basketorder.equals("1") ? getDateTime1(message.getString(tudBasketSendTime.FIELD)) : null;

			String entryby = message.getString(tudEntryBy.FIELD);

			String entryterminal = message.getString(tudEntryTerminal.FIELD);
			Double splitto = message.getDouble(tudSplitTo.FIELD);
			Double counter = message.getDouble(tudCounter.FIELD);
			String marketref = message.getString(tudMarketRef.FIELD);

			FOData fodata = new FOData();

			Vector<Object> vresult = null;

			int handlinst = message.getInt(HandlInst.FIELD);

			FOData fo = new FOData();
			fo.set_string(new String[] {entryby.toUpperCase(),accid.toUpperCase()});
			Vector ismargin = queryData.getQueryData().set_insert(com.eqtrade.database.QueryData.SQUERY.accountlist.val,	fo);
			if ((side.equalsIgnoreCase("1")) && ((com.eqtrade.database.model.Account)ismargin.get(0)).getAcctype().equalsIgnoreCase("MR")) {
				message.setString(54, "M");
			}

			List<String> vfield = null;
			boolean iscrossing = false;
			String orderidcontra = "", accidcontra = "";
			System.out.println("handlinst " + handlinst + " \n lot " + lotsize+ " \n isd lot " + isd.get_lotsize());
			// mapclordid_symbol.put(sclordid, board);
			if (handlinst == HandlInst.ADVERTISING) {
				// insert inputan negosiasi advertising
				// print_pair("process advertising");
				// log.info("process advertising " + sclordid);
				Object[] objs = new Object[20];
				objs[0] = sclordid;
				objs[1] = orderdate;
				objs[2] = board;
				objs[3] = side;
				objs[4] = symbol;
				objs[5] = accid;
				objs[6] = invtype;
				objs[7] = price;
				objs[8] = lotsize;
				objs[9] = oqty;
				objs[10] = entrytime;
				objs[11] = entryby;
				objs[12] = entryterminal;
				objs[13] = marketref;

				fodata.set_objects(objs);

				vresult = queryData.getOrderData().set_insert(SQUERY.entryadvertising.val, fodata);

				vfield = queryData.getOrderData().mapoutparameter.get(SQUERY.entryadvertising.val);

				// log.info("result of database:" + vresult + " " + sclordid);
			} else if (handlinst == HandlInst.NORMAL) {
				// insert inputan entry order
				 log.info("process entry order " + sclordid);

				Object[] objs = new Object[21];
				objs[0] = sclordid;
				objs[1] = orderdate;
				objs[2] = xchid;
				objs[3] = board;
				objs[4] = side;
				objs[5] = ordertype;
				objs[6] = symbol;
				objs[7] = accid;
				objs[8] = invtype;
				objs[9] = price;
				objs[10] = lotsize;
				objs[11] = oqty;
				objs[12] = basketorder;
				objs[13] = basketsendtime;
				objs[14] = entrytime;
				objs[15] = entryby;
				objs[16] = entryterminal;
				objs[17] = splitto;
				objs[18] = counter;
				objs[19] = marketref;
				objs[20] = message.getString(tudIsFloor.FIELD);
				fodata.set_objects(objs);

				 print_pair("before inserted");

				vresult = queryData.getOrderData().set_insert(SQUERY.order.val,fodata);
				log.info("result inserted to database:" + sclordid);

				vfield = queryData.getOrderData().mapoutparameter.get(SQUERY.order.val);

				 log.info("result of database:" + vresult + " " + sclordid);
				 print_pair("after inserted:" + vresult.size());
			} else if (handlinst == HandlInst.NEGDEAL) {

				// String counterpartbuy = message
				// .getString(tudCounterpartBuyTradingAccountNumber.FIELD);

				orderidcontra = message.getString(tudOrderIDContra.FIELD);
				// log.info("is crossing processing?"
				// + (orderidcontra != null && !orderidcontra.isEmpty())
				// + ": orderidcontra:" + orderidcontra);
				String smsg = message.toString();

				if (orderidcontra != null && !orderidcontra.isEmpty()) {
					// log.info("processing crossing:" + orderidcontra);

					String brokerid = message.getString(tudBrokerID.FIELD);
					String curid = message.getString(tudCurID.FIELD);
					accidcontra = message.getString(tudAccIDContra.FIELD);
					String invtypecontra = message
							.getString(tudInvTypeContra.FIELD);
					String ordtype = message.getString(OrdType.FIELD);

					// hnegdeal.put(sclordid, orderidcontra);
					hnegdeal.put(orderidcontra, sclordid);
					mclord_message.put(sclordid, smsg);
					mclord_message.put(orderidcontra, smsg);

					Object[] objs = new Object[22];
					objs[0] = sclordid;
					objs[1] = orderdate;
					objs[2] = xchid;
					objs[3] = board;
					objs[4] = side;
					objs[5] = ordertype;
					objs[6] = symbol;
					objs[7] = accid;
					objs[8] = invtype;
					objs[9] = price;
					objs[10] = lotsize;
					objs[11] = oqty;
					objs[12] = entrytime;
					objs[13] = entryby;
					objs[14] = entryterminal;
					objs[15] = marketref;
					objs[16] = brokerid;
					objs[17] = curid;
					objs[18] = ordtype;
					objs[19] = orderidcontra;
					objs[20] = accidcontra;
					objs[21] = invtypecontra;

					fodata.set_objects(objs);

					vresult = queryData.getOrderData().set_insert(
							SQUERY.entrynegdealcrossing.val, fodata);
					// log.info("result of database:" + vresult);

					vfield = queryData.getOrderData().mapoutparameter
							.get(SQUERY.entrynegdealcrossing.val);

					mclord_rm.put(orderidcontra, rmsource);
					// mclord_uid.put(orderidcontra, clientid);
					hentryby.put(orderidcontra, clientid);
					hcustidby.put(orderidcontra, accidcontra);

					iscrossing = true;

				} else {
					// log.info("processing two side:" + sclordid);
					String contrabroker = message
							.getString(tudContraBroker.FIELD);
					String contrauserid = message
							.getString(tudContraUserID.FIELD);
					String brokerid = message.getString(tudBrokerID.FIELD);
					String curid = message.getString(tudCurID.FIELD);
					String ordtype = message.getString(OrdType.FIELD);
					String marketorderidnegdealconfirm = message.getString(tudMarketOrdIDNegDealConfirm.FIELD);

					Object[] objs = new Object[22];
					objs[0] = sclordid;
					objs[1] = orderdate;
					objs[2] = xchid;
					objs[3] = board;
					objs[4] = side;
					objs[5] = ordertype;
					objs[6] = symbol;
					objs[7] = accid;
					objs[8] = invtype;
					objs[9] = price;
					objs[10] = lotsize;
					objs[11] = oqty;
					objs[12] = entrytime;
					objs[13] = entryby;
					objs[14] = entryterminal;

					objs[15] = contrabroker;
					objs[16] = contrauserid;
					objs[17] = marketref;
					objs[18] = brokerid;
					objs[19] = curid;
					objs[20] = ordtype;
					objs[21] = marketorderidnegdealconfirm;

					fodata.set_objects(objs);

					// print_pair("before inserted:"
					// + (String) fodata.getObject(0));

					vresult = queryData.getOrderData().set_insert(
							SQUERY.entrynegdeal.val, fodata);

					vfield = queryData.getOrderData().mapoutparameter
							.get(SQUERY.entrynegdeal.val);

				}

			}

			// print_pair("result of inserted to datavase:" + vresult.size());
			if (vresult.size() > 0) {

				// print_pair("vresult size:" + vresult.size());
				OrderOut orderout = (OrderOut) vresult.elementAt(0);

				String sordstatus = orderout.getStatusorder();

				// log.info("result inserted to database:" + psukses + ":"
				// + vresult.toString());

				Message executionreport = new Message();
				orderout.loadMessage(executionreport, maptag, vfield);

				if (executionreport != null) {

					executionreport.setString(ClOrdID.FIELD, sclordid);
					executionreport.setString(ClientID.FIELD, clientid);
					executionreport.setString(tudAccountID.FIELD, accid);

					executionreport.setString(tudDestUID.FIELD,
							rmsource.getUID());
					executionreport.setString(tudDestIDAtsvr.FIELD,
							rmsource.getIDAtsvr());
					executionreport.setString(tudDestUType.FIELD,
							rmsource.getUType());

					// print_pair("sending executio reportto client:"
					// + executionreport);
					Message ortmessage = (Message) executionreport.clone();
					comHandling.send_appmsg(sessionid, executionreport);

					// if (sordstatus.equals("o")) {

					// }
					if (iscrossing) {
						Message ortmessage2 = (Message) executionreport.clone();
						ortmessage2.setString(tudAccountID.FIELD, accidcontra);
						ortmessage2.setString(tudIsOrdTaker.FIELD, "1");
						proxyRouting.processTaker(sessionid, ortmessage2);
					}

					ortmessage.setString(tudIsOrdTaker.FIELD, "1");
					proxyRouting.processTaker(sessionid, ortmessage);

				}

				if (orderout.getSukses() == 1) {
					mclord_rm.put(sclordid, rmsource);
					// mclord_uid.put(sclordid, clientid);
					hentryby.put(sclordid, clientid);
					hcustidby.put(sclordid, accid);

					// print_pair(" vresult:insertneword/temp: " +
					// vresult.toString());

					String sreply, sheader;
					HeaderAndData handd = null;

					message.setString(OrdStatus.FIELD, sordstatus);

					// if (orderout.getDesc() != null
					// && !orderout.getDesc().isEmpty())
					// message.setString(Text.FIELD, orderout.getDesc());

					if (sordstatus.equals("o")) {
						// kirim ke jonec
						/*
						 * hdata.put(tudDestUID.getTag(), sjonecuid);
						 * hdata.put(tudDestIDAtsvr.getTag(), sjonecidatsvr);
						 * hdata.put(tudDestUType.getTag(), sjonectype);
						 * hdata.remove(tudIsOrdTaker.getTag());
						 */

						init_forscheduller(message, rmsource);
						// log.info("opend order:" + message);

						if (message != null) {

							log.info("routing to jones:" + message);
							boolean brouting = proxyRouting.processRouting(
									sessionid, message);
							// print_pair(" brouting to entryord : " +
							// brouting);

						}
					}

					if (iscrossing) {
						StringBuffer sbfile = new StringBuffer(100);
						sbfile.append(orderidcontra);
						sbfile.append("###");
						sbfile.append(accidcontra);
						save_datatofile(sbfile.toString(), sclordid_accid);
					}

					StringBuffer sbfile = new StringBuffer(100);
					sbfile.append(sclordid);
					sbfile.append("###");
					sbfile.append(accid);
					save_datatofile(sbfile.toString(), sclordid_accid);

				}

			} else {
				log.info("result of database is empty " + sclordid);
			}

		} catch (Exception ex) {
			// print_pair("error" + ex.getMessage());
			ex.printStackTrace();
			log.error(logger_info_exception(ex));

			String stext = ex.getMessage();

			String sheader = MsgType.hedExecutionReport;

			Hashtable<Integer, String> hreply = FixMessageUtils
					.buildcancelreject("000000000000", sclordid, "", "8", "2",
							"Internal reject req order: " + stext, ssrcuid,
							ssrcidatsvr, ssrcutype);

			hreply.put(ClientID.FIELD, clientid);
			hreply.put(tudAccountID.FIELD, accid);
			HeaderAndData handd = new HeaderAndData(sheader, hreply);

			if (handd != null) {
				Message ortmsg = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				Message mhand = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				log.error("routing to req entry order failed:"
						+ mhand.toString());

				boolean brouting = this.comHandling.send_appmsg(sessionid,
						mhand);
				// print_pair(" brouting to req entry order failed : " +
				// brouting);
				proxyRouting.processTaker(sessionid, ortmsg);

			}

		}
	}
	
	protected void process_reqamend(String sessionid, Message message) {

		String data = message.getStringDataOnly();

		// print_pair("receive for reqamend:sessionid:" + sessionid);
		// print_pair("receive for reqamend:data:" + data);
		log.info("receive reqamend:" + data);

		String accid = message.getString(tudAccountID.FIELD);
		String clientid = message.getString(ClientID.FIELD);

		// String sclordid = null, sorigclordid = null;
		String ssrcuid = null, ssrcidatsvr = null, ssrcutype = null;

		String sorigclordid = message.getString(OrigClOrdID.FIELD);
		String sclordid = message.getString(ClOrdID.FIELD);
		String accidold = hcustidby.get(sorigclordid);

		try {
			String orderid = message.getString(OrderID.FIELD);
			if (orderid.isEmpty()) {
				orderid = mapclordid_ordid.get(sorigclordid);
				message.setString(OrderID.FIELD, orderid);
			}

			Vector<Object> varg = new Vector<Object>(10, 5);
			StringBuffer sbfile = new StringBuffer(100);

			FOData fodata = new FOData();

			String isfloor = message.getString(tudIsFloor.FIELD);

			String sentrytime = message.getString(tudEntryTime.FIELD);
			String entryby = message.getString(tudEntryBy.FIELD);
			String entryterminal = message.getString(tudEntryTerminal.FIELD);
			String marketref = message.getString(tudMarketRef.FIELD);

			String symbolsfx = message.getString(SymbolSfx.FIELD);
			String symbol = message.getString(Symbol.FIELD);
			InfoSymbolData isd = is.get_isd(symbolsfx, symbol);

			Double price = get_doubletype(message.getString(Price.FIELD), 0);
			Double qty = get_doubletype(message.getString(OrderQty.FIELD), 0);
			Double lotsize = qty / isd.get_lotsize();
			String ssales = message.getString(ClientID.FIELD);

			String sendingtime = message.getString(TransactTime.FIELD);
			Date entrytime = getDateTime1(sentrytime);

			hentryby.put(sclordid, ssales);
			hcustidby.put(sclordid, accid);

			String basketorder = message.getString(tudBasketOrder.FIELD);

			ssrcuid = message.getString(tudSrcUID.FIELD);
			ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			ssrcutype = message.getString(tudSrcUType.FIELD);

			// mapclordid_origclordid.put(sclordid, sorigclordid);

			RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);

			Object[] objs = new Object[11];
			objs[0] = isfloor;
			objs[1] = sorigclordid;
			objs[2] = sclordid;
			objs[3] = accid;
			objs[4] = qty;
			objs[5] = lotsize;
			objs[6] = price;
			objs[7] = entrytime;
			objs[8] = entryby;
			objs[9] = entryterminal;
			objs[10] = marketref;

			fodata.set_objects(objs);
			Vector<Object> vresult = null;

			// RMType rmsourcebefore = get_basedonclordid(sorigclordid);//
			// mclord_rm.get(sorigclordid);

			vresult = queryData.getOrderData().set_insert(
					SQUERY.entryamend.val, fodata);
			/*
			 * if (rmsourcebefore != null) { synchronized (rmsourcebefore) {
			 * 
			 * vresult = queryData.getOrderData().set_insert(
			 * SQUERY.entryamend.val, fodata); } } else {
			 * log.info("rmsource before not found in entry amend");
			 * 
			 * }
			 */
			if (vresult == null)
				throw new Exception();

			OrderOut ord = null;
			if (vresult != null && vresult.size() > 0) {
				// print_pair(" bresult:reqamend: " + vresult.size());

				ord = (OrderOut) vresult.elementAt(0);

				log.info("entry amend:" + ord.getSukses() + ":ordestatus:"
						+ ord.getStatusorder() + ":new:"
						+ ord.getStatusordernew());

				Message executionreportclient = new Message();
				ord.loadMessage(executionreportclient, maptagamend, queryData
						.getOrderData().mapoutparameter
						.get(SQUERY.entryamend.val));

				if (ord.getSukses() == 1) {

					mclord_rm.put(sclordid, rmsource);
					// mclord_uid.put(sclordid, ssales);
					mclord_rm.put(sorigclordid, rmsource);
					// mclord_rm.put(sclordid, rmsource);
					// mclord_uid.put(sorigclordid, ssales);
					hentryby.put(sorigclordid, ssales);
					// String orderid = message.getString(OrderID.FIELD);

					hreqamend.put(message.getString(ClOrdID.FIELD), data);

					message.setString(tudDestUID.FIELD, muid.getSjonecuid());
					message.setString(tudDestIDAtsvr.FIELD,
							muid.getSjonecidatsvr());
					message.setString(tudDestUType.FIELD, muid.getSjonecutype());
					message.removeField(tudIsOrdTaker.FIELD);
					String sheader = MsgType.hedCancelReplaceRequest;

					// HeaderAndData handd = jp.process_reqamend(sheader,
					// hdata);
					// print_pair(" hdata:srepy : " + hdata);

					// print_pair_log(" brouting to proxy-routing : "
					// + message.toString() + ":" + brouting);

					init_forscheduller(message, rmsource);

					Message executionreportclientold = (Message) executionreportclient
							.clone();
					executionreportclientold.setString(ClOrdID.FIELD,
							sorigclordid);
					executionreportclientold.setString(OrdStatus.FIELD,
							ord.getStatusorder());
					executionreportclientold
							.setString(ClientID.FIELD, clientid);
					executionreportclientold.setString(tudAccountID.FIELD,
							accid);
					executionreportclientold.setString(tudAccountOld.FIELD,
							accidold);

					// log.info("send execution report OLD to client:"
					// + executionreportclientold.toString());

					Message ortold = (Message) executionreportclientold.clone();
					ortold.setString(tudIsOrdTaker.FIELD, "1");
					String shead = ortold.getOverridedHeader().getString(
							MsgType.FIELD);
					ortold.setString(tudBackupField.FIELD, shead);
					ortold.getHeader().setString(MsgType.FIELD,
							MsgType.hedCcMsg);

					// send ord status a
					comHandling
							.send_appmsg(sessionid, executionreportclientold);

					proxyRouting.processTaker(sessionid, ortold);

					executionreportclient.getHeader().setString(MsgType.FIELD,
							MsgType.hedExecutionReport);
					executionreportclient.setString(ClientID.FIELD, clientid);
					executionreportclient.setString(ClOrdID.FIELD, sclordid);
					executionreportclient.setString(OrdStatus.FIELD,
							ord.getStatusordernew());
					executionreportclient.setString(tudAccountID.FIELD, accid);
					executionreportclient.setString(tudAccountOld.FIELD,
							accidold);

				} else {
					executionreportclient.getHeader().setString(MsgType.FIELD,
							MsgType.hedCancelReject);
					executionreportclient.setString(ClientID.FIELD, clientid);
					executionreportclient.setString(ClOrdID.FIELD, sclordid);
					executionreportclient.setString(OrigClOrdID.FIELD,
							sorigclordid);
					executionreportclient.setString(OrdStatus.FIELD,
							ord.getStatusorder());
					executionreportclient.setString(tudAccountID.FIELD, accid);
					executionreportclient.setString(tudAccountOld.FIELD,
							accidold);

					message = null;
				}

				// send ord status o if success and a if failed
				Message ortmessage = (Message) executionreportclient.clone();
				// log.info("send execution report New to client:"
				// + executionreportclient.toString());

				comHandling.send_appmsg(sessionid, executionreportclient);

				String shead = ortmessage.getOverridedHeader().getString(
						MsgType.FIELD);
				// ortmessage.setString(tudBackupField.FIELD, shead);
				// ortmessage.getHeader().setString(MsgType.FIELD,
				// MsgType.hedCcMsg);
				// ortmessage.setString(tudIsOrdTaker.FIELD, "1");
				proxyRouting.processTaker(sessionid, ortmessage);
			}

			if (message != null) {
				log.info("routing message entry amend:" + message.toString());
				boolean brouting = proxyRouting.processRouting(sessionid,
						message);
			}

			if (ord != null && ord.getSukses() == 1) {
				sbfile = new StringBuffer(100);
				sbfile.append(message.getString(ClOrdID.FIELD));
				sbfile.append("###");
				sbfile.append(data);
				save_datatofile(sbfile.toString(), sfreqamend);
			}

			sbfile = new StringBuffer(100);
			sbfile.append(sclordid);
			sbfile.append("###");
			sbfile.append(accid);
			save_datatofile(sbfile.toString(), sclordid_accid);

		} catch (Exception ex) {
			ex.printStackTrace();

			String sheader = MsgType.hedCancelReject;
			Hashtable<Integer, String> hreply = FixMessageUtils
					.buildcancelreject("000000000000", sclordid, sorigclordid,
							"8", "2", "Internal reject req amend", ssrcuid,
							ssrcidatsvr, ssrcutype);
			hreply.put(tudDestUID.FIELD, ssrcuid);
			hreply.put(tudDestIDAtsvr.FIELD, ssrcidatsvr);
			hreply.put(tudDestUID.FIELD, ssrcutype);
			hreply.put(ClientID.FIELD, clientid);
			hreply.put(tudAccountID.FIELD, accid);
			hreply.put(tudAccountOld.FIELD, accidold);

			HeaderAndData handd = new HeaderAndData(sheader, hreply);
			if (handd != null) {
				Message ortmsg = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				Message mhand = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				log.info("routing to cancel req amend failed:" + mhand);
				boolean brouting = this.comHandling.send_appmsg(sessionid,
						mhand);
				// print_pair(" brouting to cancel req amend : " + brouting);

				String shead = ortmsg.getOverridedHeader().getString(
						MsgType.FIELD);
				ortmsg.setString(tudBackupField.FIELD, shead);
				ortmsg.getHeader().setString(MsgType.FIELD, MsgType.hedCcMsg);
				ortmsg.setString(tudIsOrdTaker.FIELD, "1");
				proxyRouting.processTaker(sessionid, ortmsg);

			}

			log.error(logger_info_exception(ex));
		}
	}

	protected void process_reqwithdraw(String sessionid, Message message) {
		// print_pair("receive for reqwithdraw/reqdeltemporder:sessionid:"
		// + sessionid);
		String sheader = message.getOverridedHeader().getMsgType().getValue();

		// Message message = FixMessageUtils.create_message("", sheader, hdata);
		String data = message.getStringDataOnly();

		log.info("receive for reqwithdraw:" + data);

		String sclordid = null, sorigclordid = "";
		String ssrcuid = null, ssrcidatsvr = null, ssrcutype = null;

		String sclient = message.getString(ClientID.FIELD);
		// String accid = message.getString(tudAccountID.FIELD);
		sorigclordid = message.getString(OrigClOrdID.FIELD);
		String accid = hcustidby.get(sorigclordid);

		try {

			FOData fodata = new FOData();

			sclordid = message.getString(ClOrdID.FIELD);
			String orderid = message.getString(OrderID.FIELD);

			String sentrytime = message.getString(tudEntryTime.FIELD);
			String entryby = message.getString(tudEntryBy.FIELD);
			String entryterminal = message.getString(tudEntryTerminal.FIELD);
			String isfloor = message.getString(tudIsFloor.FIELD);

			Date entrytime = getDateTime1(sentrytime);

			ssrcuid = message.getString(tudSrcUID.FIELD);
			ssrcidatsvr = message.getString(tudSrcIDAtsvr.FIELD);
			ssrcutype = message.getString(tudSrcUType.FIELD);

			RMType rmsource = new RMType(ssrcuid, ssrcidatsvr, ssrcutype);

			String basketorder = message.getString(tudBasketOrder.FIELD);
			int sukses = -1;
			hentryby.put(sclordid, sclient);

			if (basketorder.equals("1")) {
				mclord_rm.put(sclordid, rmsource);
				init_forscheduller(message, rmsource);
			} else {

				if (orderid.isEmpty()) {
					String sordid = mapclordid_ordid.get(sclordid);
					message.setString(OrderID.FIELD, sordid);
				}

				fodata.set_string(isfloor, sclordid, entryby, entryterminal);
				fodata.set_date(entrytime);

				// RMType rmbefore = mclord_rm.get(sclordid);

				Vector<Object> vresult = queryData.getOrderData().set_insert(
						SQUERY.entrywithdraw.val, fodata);
				;
				/*
				 * if (rmbefore != null) { synchronized (rmbefore) {
				 * 
				 * vresult = queryData.getOrderData().set_insert(
				 * SQUERY.entrywithdraw.val, fodata); } } else {
				 * log.info("can't find rmsource before in entry withdraw");
				 * 
				 * }
				 */

				if (vresult == null)
					throw new Exception();

				sukses = (Integer) vresult.elementAt(0);
				String status = (String) vresult.elementAt(1);
				String desc = (String) vresult.elementAt(2);

				// log.info("result of inserted to database:" + sukses + ":"
				// + status + ":" + desc);

				Message executionreport = new Message();
				if (sukses == 1) {

					// String sorigclordid =
					// message.getString(OrigClOrdID.FIELD);
					executionreport.getHeader().setString(MsgType.FIELD,
							MsgType.hedExecutionReport);

					mclord_rm.put(sclordid, rmsource);
					mclord_rm.put(orderid, rmsource);
					// mclord_uid.put(sclordid, sclient);
					// mapordid_clordid.put(orderid, sclordid);
					// hentryby.put(orderid, sclient);

					if (sheader.equals(MsgType.hedCancelRequest)) {
						/*
						 * hdata.put(tudDestUID.getTag(), sjonecuid);
						 * hdata.put(tudDestIDAtsvr.getTag(), sjonecidatsvr);
						 * hdata.put(tudDestUType.getTag(), sjonectype);
						 * hdata.remove(tudIsOrdTaker.getTag());
						 */

						// hdata = jp.process_reqwithdraw(sheader,
						// hdata).getData();
						// init_forscheduller(message, rmsource);
						message.setString(tudDestUID.FIELD, muid.getSjonecuid());
						message.setString(tudDestIDAtsvr.FIELD,
								muid.getSjonecidatsvr());
						message.setString(tudDestUType.FIELD,
								muid.getSjonecutype());
						message.setString(tudSrcUID.FIELD, muid.getSrmuid());
						message.setString(tudSrcIDAtsvr.FIELD,
								muid.getSrmidatsvr());
						message.setString(tudSrcUType.FIELD, muid.getSrmutype());

						message.removeField(tudIsOrdTaker.FIELD);

					} /*
					 * else { // RMType rmtype = mclord_rm.get(sclordid); RMType
					 * rmtype = get_basedonclordid(sclordid); if (rmtype !=
					 * null) { sheader = MsgType.hedExecutionReport; String
					 * sdata = htemp.get(sclordid); if (sdata != null) {
					 * FixMessage fxmsg = new FixMessage(sdata); message =
					 * fxmsg.getQuickfixMessage(); if (message != null) { //
					 * /message.setString(tudSales.FIELD, // ssales);
					 * message.setString(ClientID.FIELD, sclient);
					 * message.setString(OrdStatus.FIELD, Delete);
					 * message.setString(tudDestUID.FIELD, rmtype.getUID()); //
					 * "gtw"); message.setString(tudDestIDAtsvr.FIELD,
					 * rmtype.getIDAtsvr()); // "1234");
					 * message.setString(tudDestUType.FIELD, rmtype.getUType());
					 * // "gtw"); message.setString(tudIsOrdTaker.FIELD, "1"); }
					 * } } }
					 */

					hreqwithdraw.put(sclordid, data);
					hreqwithdraw.put(orderid, data);

				} else {
					message = null;
					executionreport.getHeader().setString(MsgType.FIELD,
							MsgType.hedCancelReject);
					executionreport.setString(OrigClOrdID.FIELD, sorigclordid);

				}

				if (desc == null)
					desc = "";

				executionreport.setString(tudDesc.FIELD, desc);

				if (status == null)
					status = "";

				executionreport.setInt(tudSukses.FIELD, sukses);

				executionreport.setString(tudDestUID.FIELD, ssrcuid); // "gtw");
				executionreport.setString(tudDestIDAtsvr.FIELD, ssrcidatsvr); // "1234");
				executionreport.setString(tudDestUType.FIELD, ssrcutype); // "gtw");
				executionreport.setString(ClOrdID.FIELD, sclordid);
				executionreport.setString(OrdStatus.FIELD, status);
				executionreport.setString(ClientID.FIELD, sclient);
				executionreport.setString(tudAccountID.FIELD, accid);

				// print_pair("send execution report withdraw to client:"
				// + executionreport.toString());
				// log.info("send execution report withdraw to client:"
				// + executionreport.toString());

				Message ortmessage = (Message) executionreport.clone();

				comHandling.send_appmsg(sessionid, executionreport);

				// if (status.equals("w")) {
				ortmessage.setString(tudIsOrdTaker.FIELD, "1");
				proxyRouting.processTaker(sessionid, ortmessage);
				// }

			}

			if (message != null) {
				// print_pair(" hdata:srepy : " + message.toString());

				// print_pair(" put reqwithdraw : " + sclordid + " : " +
				// data);

				log.info("routing-withdraw to jonec:" + message);
				boolean brouting = proxyRouting.processRouting(sessionid,
						message);
				// print_pair(" brouting to reqwithdraw/del4temp : " +
				// brouting);

			}

			if (sukses == 1) {
				StringBuffer sbfile = new StringBuffer(100);
				sbfile.append(sclordid);
				sbfile.append("###");
				sbfile.append(data);
				save_datatofile(sbfile.toString(), sfreqwithdraw);
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			log.error(logger_info_exception(ex));
			String sheaderreject = MsgType.hedCancelReject;
			Hashtable<Integer, String> hreply = FixMessageUtils
					.buildcancelreject("000000000000", sclordid, sorigclordid,
							"8", "1", "Internal reject req withdraw", ssrcuid,
							ssrcidatsvr, ssrcutype);

			hreply.put(ClientID.FIELD, sclient);
			hreply.put(tudAccountID.FIELD, accid);

			HeaderAndData handd = new HeaderAndData(sheaderreject, hreply);
			if (handd != null) {
				Message ortmsg = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				Message mhand = FixMessageUtils.create_headeranddata(
						handd.getMsgType(), handd.getData());

				log.info("routing to reqwithdraw failed:" + mhand.toString());

				boolean brouting = this.comHandling.send_appmsg(sessionid,
						mhand);
				// print_pair(" brouting to cancel req withdraw : " + brouting);
				proxyRouting.processTaker(sessionid, ortmsg);

			}
		}
	}

	protected void process_executionreport(String sessionid, Message message) {

		String data = message.toString();

		String sendercomp = message.getOverridedHeader().getString(
				SenderCompID.FIELD);

		log.info("process execution report:" + sendercomp + ":" + data);

		try {

			FOData fodata = new FOData();
			String ordstatus = message.getString(OrdStatus.FIELD);

			String sendingtime = message.getOverridedHeader().getString(
					TransactTime.FIELD);
			String transacttime = message.getOverridedHeader().getString(
					TransactTime.FIELD);

			if (sendingtime == null || sendingtime.isEmpty())
				sendingtime = getStringTime1(new Date());

			if (transacttime == null || transacttime.isEmpty())
				transacttime = getStringTime1(new Date());

			String sendercompid = message.getString(SenderCompID.FIELD);

			if (sendercomp.equals("MARTINS")) {

				String exectranstype = message.getString(ExecTransType.FIELD);

				if (exectranstype.equals("0")) {
					// log.info("process matchorder:");

					if (ordstatus.equals(Partial) || ordstatus.equals(Fully)) {
						process_fullorpartial(message, fodata, sessionid);
					}

				} else if (exectranstype.equals("3")) {

					String secondaryorderid = message
							.getString(SecondaryOrderID.FIELD);
					String handlinst = message.getString(HandlInst.FIELD);

					if (handlinst != null && handlinst.equals("1")
							&& ordstatus != null && !ordstatus.isEmpty()) {
						if (ordstatus.equals(Open)
								|| ordstatus.equals(Rejected)) {
							replyOrderOrRejected(sessionid, message, fodata,
									sendingtime, transacttime, ordstatus);
						} else if (ordstatus.equals(Withdraw)) {
							replyWithdrawOK(sessionid, message, fodata,
									sendingtime);
						} else if (message.getString(OrdStatus.FIELD).equals(
								Amend)) {
							replyAmendOK(sessionid, message, fodata,
									sendingtime);
						}

					} else if (handlinst != null && handlinst.equals("2")) {
						// log.info("process advertising subscribe");

						String sclordid = message.getString(ClOrdID.FIELD);
						String sorderdate = message
								.getString(TransactTime.FIELD);
						// if (sorderdate == null)
						// sorderdate = message.getHeader().getString(
						// SendingTime.FIELD);
						if (sorderdate == null) {
							sorderdate = getStringTime1(new Date());
						}
						if (sorderdate != null && sorderdate.isEmpty())
							sorderdate = getStringTime1(new Date());
						String stradedate = message
								.getString(EffectiveTime.FIELD);
						if (stradedate == null) {
							stradedate = getStringTime1(new Date());
						}
						if (stradedate != null && stradedate.isEmpty())
							stradedate = getStringTime1(new Date());

						// String symbol = message.getString(Symbol.FIELD);
						// String symbolsfx =
						// message.getString(SymbolSfx.FIELD);
						// InfoSymbolData isd = is.get_isd(symbolsfx,
						// symbol);
						// Double lotsize = oqty / isd.get_lotsize();
						// String sclient = hentryby.get(sclordid);

						Object[] objs = new Object[23];
						objs[0] = message.getString(OrderID.FIELD);
						objs[1] = sclordid;
						objs[2] = message.getString(HandlInst.FIELD)
								.equals("2") ? "1" : "0";
						objs[3] = message.getString(OrigClOrdID.FIELD);
						objs[4] = message.getString(ClientID.FIELD);
						objs[5] = message.getString(ExecBroker.FIELD);
						objs[6] = getDateTime1(sorderdate);

						if (ordstatus.equals("4")) {
							ordstatus = "W";
						} else if (ordstatus.equals("0")) {
							ordstatus = "O";
						} else if (ordstatus.equals("5")) {
							ordstatus = "A";
						}

						objs[7] = ordstatus;
						objs[8] = message.getString(Account.FIELD);
						objs[9] = message.getString(Symbol.FIELD);
						objs[10] = message.getString(SymbolSfx.FIELD);
						objs[11] = message.getString(SecurityID.FIELD);
						objs[12] = message.getString(Side.FIELD);
						objs[13] = message.getDouble(OrderQty.FIELD);
						objs[14] = message.getDouble(Price.FIELD);
						objs[15] = message.getDouble(LeavesQty.FIELD);
						objs[16] = message.getString(OrdType.FIELD);
						objs[17] = message.getString(TimeInForce.FIELD);
						objs[18] = getDateTime1(stradedate);
						objs[19] = message.getString(Text.FIELD);
						objs[20] = message.getString(ContraBroker.FIELD);
						objs[21] = message.getString(ContraTrader.FIELD);
						objs[22] = message.getString(ComplianceID.FIELD);
						fodata.set_objects(objs);
						Vector<Object> v = queryData.getOrderData().set_insert(
								SQUERY.entrynegdeallist.val, fodata);
						String status = (String) v.elementAt(0);

						if (status == null)
							status = "";

						Message broadcast = (Message) message.clone();
						broadcast.getHeader().setString(MsgType.FIELD,
								MsgType.hedBroadcastMessage);
						broadcast.setString(OrdStatus.FIELD, status);
						broadcast.setString(MessageSchedullerType.FIELD,
								MsgType.hedExecutionReport);

						log.info("broadcast advertising message:"
								+ broadcast.toString());
						comHandling.send_appmsg(sessionid, broadcast);

					} else {
						String symbolsfx = message.getString(SymbolSfx.FIELD);

						if (secondaryorderid != null
								&& !secondaryorderid.isEmpty()) {

							if (ordstatus.equals(Partial)
									|| ordstatus.equals(Fully)) {

								// log.info("trade list match:" +
								// sclordid_accid);

								process_fullorpartial(message, fodata,
										sessionid);
							}

						} else if (symbolsfx.equals("NG")) {
							// log.info("neg deal list message process");
							String sclordid = message.getString(ClOrdID.FIELD);
							String orderid = message.getString(OrderID.FIELD);

							// mapclordid_ordid.put(sclordid, orderid);

							// String origmsg = mclord_message.get(sclordid);

							// Message morig = new FixMessage(origmsg)
							// .getQuickfixMessage();

							String sorderdate = message
									.getString(TransactTime.FIELD);
							//
							// if (sorderdate.isEmpty())
							// sorderdate = message.getHeader().getString(
							// SendingTime.FIELD);
							//
							if (sorderdate == null)
								sorderdate = getStringTime1(new Date());

							if (sorderdate != null && sorderdate.isEmpty())
								sorderdate = getStringTime1(new Date());

							String stradedate = message
									.getString(EffectiveTime.FIELD);

							if (stradedate == null) {
								stradedate = getStringTime1(new Date());
							}

							String accid = message.getString(Account.FIELD);

							// String symbol = message.getString(Symbol.FIELD);
							// String symbolsfx =
							// message.getString(SymbolSfx.FIELD);
							// InfoSymbolData isd = is.get_isd(symbolsfx,
							// symbol);
							// Double lotsize = oqty / isd.get_lotsize();

							String sclient = hentryby.get(sclordid);

							Object[] objs = new Object[23];
							objs[0] = message.getString(OrderID.FIELD);
							objs[1] = sclordid;
							objs[2] = message.getString(HandlInst.FIELD)
									.equals("2") ? "1" : "0";
							objs[3] = message.getString(OrigClOrdID.FIELD);
							objs[4] = message.getString(ClientID.FIELD);
							objs[5] = message.getString(ExecBroker.FIELD);
							objs[6] = getDateTime1(sorderdate);

							if (ordstatus.equals("4")) {
								ordstatus = "W";
							} else if (ordstatus.equals("0")
									|| ordstatus.equals("D")) {
								ordstatus = "O";
							} else if (ordstatus.equals("5")) {
								ordstatus = "A";
							}

							objs[7] = ordstatus;
							objs[8] = accid;
							objs[9] = message.getString(Symbol.FIELD);
							objs[10] = message.getString(SymbolSfx.FIELD);
							objs[11] = message.getString(SecurityID.FIELD);
							objs[12] = message.getString(Side.FIELD);
							objs[13] = message.getDouble(OrderQty.FIELD);
							objs[14] = message.getDouble(Price.FIELD);
							objs[15] = message.getDouble(LeavesQty.FIELD);
							objs[16] = "7";
							objs[17] = message.getString(TimeInForce.FIELD);
							objs[18] = getDateTime1(stradedate);
							objs[19] = message.getString(Text.FIELD);
							objs[20] = message.getString(ContraBroker.FIELD);
							objs[21] = message.getString(ContraTrader.FIELD);
							objs[22] = message.getString(ComplianceID.FIELD);

							/*
							 * objs[0] = message.getString(OrderID.FIELD);
							 * objs[1] = message.getString(ExecBroker.FIELD);
							 * objs[2] = "0"; objs[3] =
							 * message.getString(OrderID.FIELD); objs[4] =
							 * message.getString(ClientID.FIELD); objs[5] =
							 * message.getString(ExecBroker.FIELD); objs[6] =
							 * sdfsendingtime.parse(sorderdate); objs[7] =
							 * ordstatus; objs[8] =
							 * message.getString(Account.FIELD); objs[9] =
							 * message.getString(Symbol.FIELD); objs[10] =
							 * message.getString(SymbolSfx.FIELD); objs[11] =
							 * message.getString(SecurityID.FIELD); objs[12] =
							 * message.getString(Side.FIELD); objs[13] =
							 * message.getDouble(CumQty.FIELD); objs[14] =
							 * message.getDouble(Price.FIELD); objs[15] =
							 * message.getDouble(LeavesQty.FIELD); objs[16] =
							 * "7"; objs[17] =
							 * morig.getString(TimeInForce.FIELD); objs[18] =
							 * sdfsendingtime.parse(sorderdate); objs[19] =
							 * message.getString(Text.FIELD); objs[20] =
							 * message.getString(ContraBroker.FIELD); objs[21] =
							 * message.getString(ContraTrader.FIELD); objs[22] =
							 * message.getString(ComplianceID.FIELD);
							 */
							fodata.set_objects(objs);
							Vector<Object> v = queryData.getOrderData()
									.set_insert(SQUERY.entrynegdeallist.val,
											fodata);
							String status = (String) v.elementAt(0);

							// log.info("result negdeal list message "+status);

							if (status == null)
								status = "";

							Message broadcast = (Message) message.clone();
							broadcast.getHeader().setString(MsgType.FIELD,
									MsgType.hedBroadcastMessage);
							broadcast.setString(OrdStatus.FIELD, status);
							broadcast.setString(MessageSchedullerType.FIELD,
									MsgType.hedExecutionReport);

							log.info("broadcast negdeal message:"
									+ broadcast.toString());
							comHandling.send_appmsg(sessionid, broadcast);

							String orderidcontra = hnegdeal.get(sclordid);
							if (orderidcontra != null
									&& !orderidcontra.isEmpty()
									&& ordstatus.equals("O")) {
								String msg = mclord_message.get(orderidcontra);
								if (msg != null && !msg.isEmpty()) {
									Message fxm = new FixMessage(msg)
											.getQuickfixMessage();

									/*
									 * FOData fodatacontra = new FOData();
									 * fodatacontra.set_date(
									 * sdfsendingtime.parse(sendingtime),
									 * sdfsendingtime.parse(transacttime));
									 * fodatacontra.set_string(sclordid,
									 * orderid); // Vector<Object> vresultcontra
									 * = queryData .getOrderData().set_insert(
									 * SQUERY.openorder.val, fodatacontra);
									 */

									log.info("Sending Confirmation Request:from message:orderidcontra:"
											+ orderidcontra
											+ ":sclordid:"
											+ sclordid + ":" + fxm.toString());
									String symbol = message
											.getString(Symbol.FIELD);
									// String orderid =
									// message.getString(OrderID.FIELD);
									Message messageconfirm = new Message();
									messageconfirm.getHeader().setString(
											MsgType.FIELD,
											MsgType.hedNewOrderRequest);

									messageconfirm.setString(ClOrdID.FIELD,
											sclordid);
									messageconfirm.setString(Account.FIELD,
											accid);
									messageconfirm.setString(HandlInst.FIELD,
											"3");
									messageconfirm.setString(Symbol.FIELD,
											fxm.getString(Symbol.FIELD));
									messageconfirm.setString(SymbolSfx.FIELD,
											fxm.getString(SymbolSfx.FIELD));
									messageconfirm.setString(Side.FIELD, "1");
									messageconfirm
											.setString(
													ComplianceID.FIELD,
													fxm.getString(tudCounterpartBuyTradingAccountNumber.FIELD));
									messageconfirm.setString(
											TransactTime.FIELD,
											getStringTime1(new Date()));
									messageconfirm.setString(OrderQty.FIELD,
											fxm.getString(OrderQty.FIELD));
									messageconfirm.setString(OrdType.FIELD,
											fxm.getString(OrdType.FIELD));
									messageconfirm.setDouble(Price.FIELD,
											fxm.getDouble(Price.FIELD));
									messageconfirm.setString(IOIID.FIELD,
											orderid);
									messageconfirm.setString(tudDestUID.FIELD,
											muid.getSjonecuid());
									messageconfirm.setString(
											tudDestIDAtsvr.FIELD,
											muid.getSjonecidatsvr());
									messageconfirm.setString(
											tudDestUType.FIELD,
											muid.getSjonecutype());
									messageconfirm.setString(
											tudBackupField.FIELD, sclordid);

									log.info("Sending Confirmation Request:to jones:"
											+ messageconfirm.toString());
									proxyRouting.processRouting(sessionid,
											messageconfirm);
								}
							}

						}
					}
				}

			} else {
				log.info("process for JONEC");

				if (ordstatus.equals(Open) || ordstatus.equals(Rejected)) {

					replyOrderOrRejected(sessionid, message, fodata,
							sendingtime, transacttime, ordstatus);

				} else if (ordstatus.equals(Withdraw)) {

					replyWithdrawOK(sessionid, message, fodata, sendingtime);

				} else if (message.getString(OrdStatus.FIELD).equals(Amend)) {

					replyAmendOK(sessionid, message, fodata, sendingtime);

				} else if (message.getString(OrdStatus.FIELD).equals(Partial)
						|| message.getString(OrdStatus.FIELD).equals(Fully)) {

					log.info("process partial or fully execution report");

					// process_fullorpartial(message, fodata, sessionid);
					/*
					 * String exectranstype =
					 * message.getString(ExecTransType.FIELD);
					 * 
					 * if (exectranstype != null && !exectranstype.equals("3"))
					 * { String sclordid = message.getString(ClOrdID.FIELD);
					 * String side = message.getString(Side.FIELD);
					 * 
					 * if (sclordid != null && ndl.is_negdealorder(sclordid)) {
					 * 
					 * String sclordidsell = ndl.get_counterclordid(sclordid);
					 * if (sclordidsell != null) {
					 * message.setString(ClOrdID.FIELD, sclordidsell);
					 * message.setString(Side.FIELD, "2");
					 * 
					 * process_fullorpartial(hdata, fodata, sessionid, query); }
					 * 
					 * } }
					 */

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(logger_info_exception(ex));
		}
	}

	private void replyAmendOK(String sessionid, Message message, FOData fodata,
			String sendingtime) throws Exception {

		try {
			String sclordid = message.getString(ClOrdID.FIELD);

			Message morig = new FixMessage(hreqamend.get(sclordid))
					.getQuickfixMessage();

			String ordidorig = morig.getString(OrigClOrdID.FIELD);

			// if(ordidorig == null)
			// ordidorig = mapclordid_origclordid.get(sclordid);

			String sentryby = hentryby.get(sclordid);
			String accid = hcustidby.get(sclordid);

			String sordidnew = message.getString(OrderID.FIELD);
			// log.info("processing amend req:clordid:" + sclordid +
			// ":origclord:"
			// + ordidorig + ":orderid:" + sordidnew);

			if (sordidnew.trim().length() > 0) {
				mapordid_clordid.put(sordidnew, sclordid);
				mapclordid_ordid.put(sclordid, sordidnew);
			}

			// Double price =
			// get_doubletype(message.getString(Price.FIELD),
			// 0);
			// Double lot =
			// get_doubletype(message.getString(OrderQty.FIELD),
			// 0);
			// Double price = message.getDouble(Price.FIELD);
			// Double lot = message.getDouble(OrderQty.FIELD);
			// String secondaryorderid = message
			// .getString(SecondaryOrderID.FIELD);

			InfoSymbolData isd = is.get_isd(morig.getString(SymbolSfx.FIELD),
					morig.getString(Symbol.FIELD));
			// Double volumebei = lot * isd.get_lotsize();

			Double volumebei = message.getDouble(OrderQty.FIELD);
			Double volumeleftbei = message.getDouble(LeavesQty.FIELD);
			Double volumedonebei = message.getDouble(CumQty.FIELD);
			Object[] objs = new Object[7];
			objs[0] = ordidorig;
			objs[1] = sclordid;
			objs[2] = getDateTime1(sendingtime);
			objs[3] = sordidnew;
			objs[4] = volumebei;// vol amend bei
			objs[5] = volumeleftbei;// vol amend left bei
			objs[6] = volumedonebei;// vol amend done bei

			// fodata.set_string(sclordid, ordidorig, sordidnew);
			// fodata.set_date();
			// fodata.set_double(volumebei, volumebei, volumebei);
			fodata.set_objects(objs);

			RMType rmtype = get_basedonclordid(ordidorig);// mclord_rm.get(ordidorig);

			if (rmtype == null) {
				rmtype = get_basedonclordid(sclordid);// mclord_rm.get(sclordid);
			}

			Vector<Object> vresult = queryData.getOrderData().set_insert(
					SQUERY.amendok.val, fodata);
			;

			/*
			 * if (rmtype != null) { synchronized (rmtype) {
			 * 
			 * vresult = queryData.getOrderData().set_insert(
			 * SQUERY.amendok.val, fodata); } } else { vresult =
			 * queryData.getOrderData().set_insert( SQUERY.amendok.val, fodata);
			 * 
			 * }
			 */

			// RMType rmtype = get_basedonclordid(sclordid);

			String isordlist = message.getString(ExecTransType.FIELD);

			if (vresult.size() > 0) {

				OrderOut ord = (OrderOut) vresult.elementAt(0);
				ord.loadMessage(message, maptagamend,
						queryData.getOrderData().mapoutparameter
								.get(SQUERY.amendok.val));

				message.setString(OrdStatus.FIELD, ord.getStatusordernew());

				// log.info("get ord status:" + ord.getStatusorder() +
				// ":"
				// + ord.getStatusordernew());
				// log.info("load message:" + message.toString());

				if (rmtype != null) {

					message.setString(ClientID.FIELD, sentryby);
					message.setString(tudSales.FIELD, sentryby);
					message.setString(tudAccountID.FIELD, accid);

					message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
					message.setString(tudDestIDAtsvr.FIELD, rmtype.getIDAtsvr()); // "1234");
					message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");
					message.setString(tudIsOrdTaker.FIELD, "1");

					message.removeField(OrigClOrdID.FIELD);
					// message.setString(OrdStatus.FIELD, Open);

					// print_pair(" hdata:sreply : " + message);

					Message reply = (Message) message.clone();

					if (message != null) {

						// send Order OK (O)
						Message ortmessage = (Message) message.clone();
						log.info("routing amend message:" + message.toString());
						boolean brouting = proxyRouting.processRouting(
								sessionid, message);
						// print_pair(" brouting exec report: " +
						// brouting);
						// if (ord.getStatusordernew().equals("O")) {

						// String shed =
						// ortmessage.getOverridedHeader().getString(MsgType.FIELD);
						// ortmessage.setString(tudBackupField.FIELD,
						// shed);
						// ortmessage.getOverridedHeader().setString(MsgType.FIELD,
						// MsgType.hedCcMsg);
						// ortmessage.setString(tudAccountOld.FIELD,
						// accid);
						// String accidold = hcustidby.get(ordidorig);
						// ortmessage.setString(tudAccountOld.FIELD,
						// accidold);

						ortmessage.setString(tudIsOrdTaker.FIELD, "1");
						proxyRouting.processTaker(sessionid, ortmessage);
						// } else {
						// log.info("execution report status order for amend:"
						// + ord.getStatusorder());
						// }
					}

					// reply.setString(OrdStatus.FIELD, Amend);

					// send Amend OK (A)
					reply.setString(OrdStatus.FIELD, ord.getStatusorder());
					reply.setString(ClOrdID.FIELD, ordidorig);

					// print_pair(" hdata:sreply : " + reply);

					if (reply != null) {

						log.info(" send Amend OK: " + reply.toString());
						Message ortreply = (Message) reply.clone();
						boolean brouting = proxyRouting.processRouting(
								sessionid, reply);

						// if (ord.getStatusorder().equals("A")) {
						String shed = ortreply.getOverridedHeader().getString(
								MsgType.FIELD);
						ortreply.setString(tudBackupField.FIELD, shed);
						ortreply.getOverridedHeader().setString(MsgType.FIELD,
								MsgType.hedCcMsg);
						String accidold = hcustidby.get(ordidorig);
						ortreply.setString(tudAccountOld.FIELD, accidold);
						ortreply.setString(tudIsOrdTaker.FIELD, "1");
						proxyRouting.processTaker(sessionid, ortreply);

						// }
					}
				} else {
					log.info("can't find rmsource");
				}

			}
		} catch (Exception e) {

			String key = message.getString(OrderID.FIELD);
			boolean isremoved = false;
			isremoved = proxyRouting.getJonecProc().remove_vamend(key);

			log.error("delete key for amend:" + isremoved);

			throw e;
		}
	}

	private void replyWithdrawOK(String sessionid, Message message,
			FOData fodata, String sendingtime) throws Exception {
		try {
			String sentryby = null;
			String accid = null;
			String sordid = message.getString(OrderID.FIELD);
			// String sclordidtmp = mapordid_clordid.get(sordid);

			String sclord = mapordid_clordid.get(sordid);
			String brdid = message.getString(SymbolSfx.FIELD);

			// print_pair("processing execution report withdraw:clordid:"
			// + sclord + ":orderid:" + sordid);
			// log.info("processing execution report withdraw:clordid:" + sclord
			// + ":orderid:" + sordid);

			Date sendtime = getDateTime1(sendingtime);

			if (sclord != null) {
				sentryby = hentryby.get(sclord);
				accid = hcustidby.get(sclord);
				// if(sentryby == null) {
				// sentryby = hentryby.get(sordid);
				// }
			}

			Double voldonbei = message.getDouble(LeavesQty.FIELD);
			if (brdid.contains("NG")) {
				String msg = hreqwithdraw.get(sordid);
				Message mxg = new FixMessage(msg).getQuickfixMessage();

				double ordqty = mxg.getDouble(OrderQty.FIELD);

				voldonbei = ordqty / 500;
				log.info("order qty NG " + ordqty + " " + voldonbei);
			}

			// int price = is.get_price(sclord);

			Object[] objs = new Object[3];

			objs[0] = sclord;
			objs[1] = sendtime;
			objs[2] = voldonbei;

			fodata.set_objects(objs);
			// fodata.set_string(sclord);
			// fodata.set_date(sendtime);
			// fodata.set_double(new Double(price));

			Vector<Object> vresult = queryData.getOrderData().set_insert(
					SQUERY.withdrawok.val, fodata);

			if (vresult.size() > 0) {

				RMType rmtype = get_basedonclordid(sclord);
				OrderOut ord = (OrderOut) vresult.elementAt(0);
				ord.loadMessage(message, maptag,
						queryData.getOrderData().mapoutparameter
								.get(SQUERY.withdrawok.val));
				// message = Function.copyTagMessage(message,
				// dataquery);

				if (rmtype != null) {

					message.setString(ClientID.FIELD, sentryby);
					// message.setString(tudSales.FIELD, sentryby);
					message.setString(tudAccountID.FIELD, accid);
					message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
					message.setString(tudDestIDAtsvr.FIELD, rmtype.getIDAtsvr()); // "1234");
					message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");
					message.setString(tudIsOrdTaker.FIELD, "1");

					message.setString(ClOrdID.FIELD, sclord);
					message.setString(tudSendtime.FIELD, sendingtime);

					// print_pair(" hdata:sreply : " +
					// message.toString());

					if (message != null) {

						log.info("routing execution report withdraw:"
								+ message.toString());
						Message ortmessage = (Message) message.clone();
						boolean brouting = proxyRouting.processRouting(
								sessionid, message);
						// print_pair(" brouting exec report: " +
						// brouting);

						// if (ord.getStatusorder().equals("W")) {
						ortmessage.setString(tudIsOrdTaker.FIELD, "1");
						proxyRouting.processTaker(sessionid, ortmessage);
						// }

					}
				} else {
					log.info("cannot find rmtype");
				}

			} else {
				log.info("failed to inout to database for withdraw execution report");
			}
		} catch (Exception e) {

			String key = message.getString(OrderID.FIELD);
			boolean isremoved = false;

			isremoved = proxyRouting.getJonecProc().remove_vwithdraw(key);

			log.error("delete key for withdraw:" + isremoved);

			throw e;
		}
	}

	private void replyOrderOrRejected(String sessionid, Message message,
			FOData fodata, String sendingtime, String transacttime,
			String ordstatus) throws Exception {
		try {
			String sclordid = message.getString(ClOrdID.FIELD);

			String sdesc = message.getString(Text.FIELD);
			if (sdesc == null)
				sdesc = "no desc";

			String sentryby = hentryby.get(sclordid);
			String accid = hcustidby.get(sclordid);

			String sordid = message.getString(OrderID.FIELD);

			// log.info("processing order open:clordid:" + sclordid
			// + ":orderid:" + sordid);

			Vector<Object> vresult = null;
			// RMType rmtype = mclord_rm.get(sclordid);
			RMType rmtype = get_basedonclordid(sclordid);//
			// mclord_rm.get(sclordid);

			fodata.set_date(getDateTime1(sendingtime),
					getDateTime1(transacttime));


			if (ordstatus.equals(Open)) {

				int price = message.getInt(Price.FIELD);
				if (sordid.trim().length() > 0) {
					mapordid_clordid.put(sordid, sclordid);
					mapclordid_ordid.put(sclordid, sordid);
					// is.set_price(sclordid, price);
				}

				fodata.set_string(sclordid, sordid);

				vresult = queryData.getOrderData().set_insert(
						SQUERY.openorder.val, fodata);
				log.info("result open order:" + sclordid);

			} else {
				fodata.set_string(sclordid, message.getString(Text.FIELD));
				vresult = queryData.getOrderData().set_insert(
						SQUERY.rejectorder.val, fodata);

				log.info("result reject order:" + sclordid);
				String orderidcontra = hnegdeal.get(sclordid);

				if (orderidcontra != null && !orderidcontra.isEmpty()) {
					FOData fodatacontra = new FOData();
					fodatacontra.set_date(getDateTime1(sendingtime),
							getDateTime1(transacttime));
					fodatacontra.set_string(orderidcontra,
							message.getString(Text.FIELD));
					// coba testing
					queryData.getOrderData().set_insert(SQUERY.rejectorder.val,
							fodatacontra);
					log.error("reject order for negdeal crossing: "
							+ message.toString());

				}

			}

			// print_pair("status databases " + vresult.elementAt(0));
			if (vresult.size() > 0 && rmtype != null) {

				if (ordstatus.equals(Rejected)) {
					OrderOut ord = (OrderOut) vresult.elementAt(0);

					ord.loadMessage(message, maptag,
							queryData.getOrderData().mapoutparameter
									.get(SQUERY.rejectorder.val));

				} else if (ordstatus.equals(Open)) {
					message.setString(OrdStatus.FIELD,
							(String) vresult.elementAt(0));
					message.setString(tudSendtime.FIELD, sendingtime);
					message.setString(tudOpentime.FIELD, sendingtime);
				}

				message.setString(ClientID.FIELD, sentryby);
				// message.setString(tudSales.FIELD, sentryby);
				message.setString(tudAccountID.FIELD, accid);

				message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
				message.setString(tudDestIDAtsvr.FIELD, rmtype.getIDAtsvr()); // "1234");
				message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");
				message.setString(tudIsOrdTaker.FIELD, "1");

				// print_pair(" hdata:sreply : " + message);

				if (message != null) {

					Message ortmessage = (Message) message.clone();
					log.info("routing execution report:" + message.toString());
					boolean brouting = proxyRouting.processRouting(sessionid,
							message);
					// print_pair(" brouting exec report: " + brouting);

					proxyRouting.processTaker(sessionid, ortmessage);

				}
			} else {
				log.info("can't find rmsource for ordstatus open or rejected:"
						+ rmtype + ":" + sclordid + ":" + sentryby);
			}
		} catch (Exception e) {
			String key = message.getString(OrderID.FIELD);
			boolean isremoved = false;
			if (message.getString(OrdStatus.FIELD).equals(Rejected)) {
				key = message.getString(ClOrdID.FIELD);
				isremoved = proxyRouting.getJonecProc().remove_vwithdraw(key);

			} else {
				isremoved = proxyRouting.getJonecProc().remove_vopen(key);
			}
			log.error("delete key for open or rejected:" + isremoved);

			throw e;
		}
	}

	protected void process_cancelreject(String sessionid, Message message) {

		// print_pair("receive for replyo cancel sessionid:" + sessionid);
		// print_pair("receive for reply  cancel reject:data:" + message);

		try {

			log.info("receive for reply  cancel reject:data:" + message);
			FOData fodata = new FOData();

			String sclordid = message.getString(ClOrdID.FIELD);
			int scxlrej = message.getInt(CxlRejResponseTo.FIELD);
			String stext = message.getString(Text.FIELD);

			String sendingtime = message.getString(SendingTime.FIELD);

			Date rejecttime = new Date();
			if (sendingtime != null && !sendingtime.isEmpty()) {
				rejecttime = getDateTime1(sendingtime);
			}

			// String sendingtime = message.getString(SendingTime.FIELD) == null
			// || message.getString(SendingTime.FIELD).isEmpty() ?
			// sdfsendingtime
			// .format(new Date())
			// : message.getString(SendingTime.FIELD);
			String origclordid = message.getString(OrigClOrdID.FIELD);
			// String orderid = message.getString(OrderID.FIELD);

			// RMType rmtype = get_basedonclordid(sclordid);
			// RMType rmtype = mclord_rm.get(sclordid);

			RMType rmtype = get_basedonclordid(sclordid);

			// if (rmtype != null) {

			message.setString(tudIsOrdTaker.FIELD, "1");

			if (scxlrej == CancelReqAmend.FIELD) { // amend

				String sentryby = hentryby.get(sclordid);
				String accid = hcustidby.get(sclordid);
				String accidold = hcustidby.get(origclordid);

				message.setString(ClientID.FIELD, sentryby);
				message.setString(tudSales.FIELD, sentryby);
				message.setString(tudAccountID.FIELD, accid);

				String datareq = hreqamend.get(sclordid);
				if (datareq != null) {
					FixMessage fxmsg = new FixMessage(datareq);
					Hashtable<Integer, String> hdatareq = fxmsg.get_dataonly();

					String origclord = hdatareq.get(OrigClOrdID.FIELD);

					Object[] objs = new Object[4];
					objs[0] = origclord;
					objs[1] = sclordid;
					objs[2] = rejecttime;
					objs[3] = stext;

					fodata.set_objects(objs);

					// fodata.set_string(origclord, sclordid, stext);
					// fodata.set_date(sdfsendingtime.parse(sendingtime));

					Vector<Object> vresult = queryData.getOrderData()
							.set_insert(SQUERY.amendreject.val, fodata);

					OrderOut ordout = (OrderOut) vresult.elementAt(0);
					ordout.loadMessage(message, maptagamend, queryData
							.getOrderData().mapoutparameter
							.get(SQUERY.amendreject.val));

					// message.setString(OrdStatus.FIELD, (String) vresult
					// .elementAt(1));
					message.getHeader().setString(MsgType.FIELD,
							MsgType.hedCancelReject);
					message.setString(OrigClOrdID.FIELD, origclord);
					message.setString(tudIsOrdTaker.FIELD, "1");
					message.setString(OrdStatus.FIELD, ordout.getStatusorder());

					// String sheader = hedCancelReject;
					// HeaderAndData handd = new HeaderAndData(sheader,
					// hdata);

					// if (handd != null) {

					if (rmtype != null) {
						message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
						message.setString(tudDestIDAtsvr.FIELD,
								rmtype.getIDAtsvr()); // "1234");
						message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");

						log.info("routing reject amend:" + message.toString());
						Message ortmessage = (Message) message.clone();
						boolean brouting = this.comHandling.send_appmsg(
								sessionid, message);

						/*
						 * String shed = ortmessage.getOverridedHeader()
						 * .getString(MsgType.FIELD);
						 * ortmessage.setString(tudBackupField.FIELD, shed);
						 * ortmessage.getHeader().setString(MsgType.FIELD,
						 * MsgType.hedCcMsg);
						 * ortmessage.setString(tudAccountOld.FIELD, accidold);
						 */

						proxyRouting.processTaker(sessionid, ortmessage);
					} else {
						log.error("Destination can't found");
					}
					// }

				} else {
					log.error(" data reqamend not found at hashtable !! ");
				}
			} else if (scxlrej == CancelReqWithdraw.FIELD) { // withdraw

				String orderid = message.getString(OrderID.FIELD);
				// rmtype = mclord_rm.get(orderid);
				String datareq = hreqwithdraw.get(orderid);

				if (datareq == null) {
					datareq = hreqwithdraw
							.get(message.getString(ClOrdID.FIELD));
				}

				if (datareq != null) {

					FixMessage fxm = new FixMessage(datareq);
					Hashtable<Integer, String> hdatareq = fxm.get_dataonly();

					sclordid = hdatareq.get(ClOrdID.FIELD);
					// log.info("process reqwithdraw cancel:" + scxlrej + ":" +
					// sclordid+":"+orderid);

					String sclientid = hdatareq.get(ClientID.FIELD);
					// String accid = hdatareq.get(tudAccountID.FIELD);
					// String orderid = message.getString(OrderID.FIELD);
					rmtype = get_basedonclordid(sclordid);

					if (orderid.equals("000000000000")) {
						orderid = hdatareq.get(OrderID.FIELD);
					}

					if (rmtype == null)
						rmtype = get_basedonclordid(orderid);

					String text = message.getString(Text.FIELD);
					fodata.set_string(orderid, text, sclordid);
					fodata.set_date(rejecttime);

					Vector<Object> vresult = queryData.getOrderData()
							.set_insert(SQUERY.withdrawreject.val, fodata);

					String statusord = (String) vresult.elementAt(0);
					// print_pair("get status order:" + statusord);
					message.setString(OrdStatus.FIELD, statusord);
					message.setString(tudIsOrdTaker.FIELD, "1");
					message.setString(OrigClOrdID.FIELD,
							hdatareq.get(OrigClOrdID.FIELD));
					message.setString(ClOrdID.FIELD, sclordid);
					message.setString(ClientID.FIELD, sclientid);
					// message.setString(tudAccountID.FIELD, accid);

					if (rmtype != null) {
						message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
						message.setString(tudDestIDAtsvr.FIELD,
								rmtype.getIDAtsvr()); // "1234");
						message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");
						message.getHeader().setString(MsgType.FIELD,
								MsgType.hedCancelReject);

						Message ortmsg = (Message) message.clone();
						ortmsg.setString(tudIsOrdTaker.FIELD, "1");

						// print_pair(" brouting reject withdraw report: "
						// + message.toString());
						log.info(" brouting reject withdraw report: "
								+ message.toString());
						boolean brouting = this.comHandling.send_appmsg(
								sessionid, message);

						proxyRouting.processTaker(sessionid, ortmsg);
					} else {
						log.error("destination cancel reqwithdraw can't found");
					}

				} else {
					log.error(" data reqwithdraw not found at hashtable !! ");
				}

			} else {
				log.error("Type cancel not know");
			}
			// } else {
			// print_pair("CLORDID Can't find");
			// }

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(logger_info_exception(ex));
		}
	}

	protected void process_fullorpartial(Message message, FOData fodata,
			String sessionid) throws Exception {

		try {
			String secordid = message.getString(SecondaryOrderID.FIELD);// markettradeid
			if (secordid == null)
				secordid = "";
			String cbroker = message.getString(ContraBroker.FIELD);
			if (cbroker == null)
				cbroker = "";
			String ctrader = message.getString(ContraTrader.FIELD);
			if (ctrader == null)
				ctrader = "";

			String sclordid = message.getString(ClOrdID.FIELD);
			String sordid = message.getString(OrderID.FIELD);
			// String stransacttime = message.getOverridedHeader().getString(
			// TransactTime.FIELD);

			/*
			 * if (stransacttime != null) { if (stransacttime.isEmpty())
			 * stransacttime = message.getOverridedHeader().getString(
			 * SendingTime.FIELD); }
			 */

			String contrabroker = message.getString(ContraBroker.FIELD);
			// Date transacttime = getDateTime1(stransacttime);
			Date transacttime = new Date();
			fodata.set_string(sclordid, secordid, contrabroker, ctrader, sordid);
			fodata.set_date(transacttime);
			// if (sordid.trim().length() > 0)
			// mapordid_clordid.put(sordid, sclordid);

			// double nvolume = get_doubletype(message.getString(CumQty.FIELD),
			// 0);
			// double nprice = get_doubletype(message.getString(Price.FIELD),
			// 0);
			String symbolsfx = message.getString(SymbolSfx.FIELD);
			String symbol = message.getString(Symbol.FIELD);

			// if (symbolsfx != null && symbolsfx.isEmpty()) {
			// symbolsfx = mapclordid_symbol.get(sclordid);
			// }

			InfoSymbolData isd = is.get_isd(symbolsfx, symbol);

			double nprice = get_doubletype(message.getString(Price.FIELD), 0);

			double lotsize = isd.get_lotsize();

			double lot = get_doubletype(message.getString(CumQty.FIELD), 0);

			// if(symbolsfx.equals("NG"))

			double volume = lot * lotsize;

			log.info("process full or partial:" + sclordid + ":" + sordid + ":"
					+ secordid + ":" + volume + ":" + nprice + ":" + lotsize
					+ ":" + lot);

			fodata.set_double(volume, lot, nprice);

			if (volume > 0 && nprice > 0) {

				RMType rmtype = get_basedonclordid(sclordid);

				Vector<Object> vresult = queryData.getOrderData().set_insert(
						SQUERY.matchorder.val, fodata);

				/*
				 * if (rmtype != null) { RMType rmbefore =
				 * mclord_rm.get(sclordid); if (rmbefore != null) { synchronized
				 * (rmtype) {
				 * 
				 * vresult = queryData.getOrderData().set_insert(
				 * SQUERY.matchorder.val, fodata); } } else { vresult =
				 * queryData.getOrderData().set_insert( SQUERY.matchorder.val,
				 * fodata);
				 * 
				 * } } else {
				 * log.info("can't find rmsource before in match order");
				 * vresult = queryData.getOrderData().set_insert(
				 * SQUERY.matchorder.val, fodata);
				 * 
				 * }
				 */
				OrderOut ord = (OrderOut) vresult.elementAt(0);

				ord.loadMessage(message, maptag,
						queryData.getOrderData().mapoutparameter
								.get(SQUERY.matchorder.val));

				// print_pair("message out from database:" +
				// message.toString());
				// RMType rmtype = mclord_rm.get(sclordid);

				// if (vresult.size() > 0 && rmtype != null) {

				String sentryby = hentryby.get(sclordid);

				if (sentryby != null) {
					message.setString(ClientID.FIELD, sentryby);
					message.setString(tudSales.FIELD, sentryby);
				}

				String accid = hcustidby.get(sclordid);
				if (accid != null) {
					message.setString(tudAccountID.FIELD, accid);
				} else
					log.error("account id can't be NULL");

				message.removeField(ExecBroker.FIELD);
				if (rmtype != null) {
					message.setString(tudDestUID.FIELD, rmtype.getUID()); // "gtw");
					message.setString(tudDestIDAtsvr.FIELD, rmtype.getIDAtsvr()); // "1234");
					message.setString(tudDestUType.FIELD, rmtype.getUType()); // "gtw");
					message.setString(tudIsOrdTaker.FIELD, "1");
					// message.setString(CumQty.FIELD, volume);
					message.setDouble(CumQty.FIELD, volume);

					// String seqno = vresult.elementAt(1);
					// message.setString(tudTRX_SEQNO.FIELD, seqno);

					// String sheader = hedExecutionReport;
					// HeaderAndData handd = new HeaderAndData(sheader, hdata);
					// print_pair(" hdata:sreply : " + hdata);

					if (message != null) {
						Message reply = (Message) message.clone();

						Message ortmessage = (Message) reply.clone();
						log.info(" routing match report: " + reply);
						boolean brouting = this.comHandling.send_appmsg(
								sessionid, reply);

						proxyRouting.processTaker(sessionid, ortmessage);

					}
				} else {
					// print_pair("can't find destination for the message");
					log.error("can't find destination for full or partial");
				}

				// tambahan untuk negdeal confirm
				String contraclordid = hnegdeal.get(sclordid);
				if (contraclordid != null && !contraclordid.isEmpty())
					hnegdeal.remove(sclordid);

				/*
				 * String contraclordid = hnegdeal.get(sclordid);
				 * 
				 * if (contraclordid != null && !contraclordid.isEmpty()) {
				 * 
				 * FOData fodatacontra = new FOData();
				 * fodatacontra.set_string(contraclordid, secordid,
				 * contrabroker, ctrader,sordid);
				 * fodatacontra.set_date(transacttime);
				 * fodatacontra.set_double(volume, lot, nprice);
				 * 
				 * Vector<Object> vresultcontra = queryData.getOrderData()
				 * .set_insert(SQUERY.matchorder.val, fodatacontra);
				 * 
				 * Message execcontra = new Message(); OrderOut ord2 =
				 * (OrderOut) vresultcontra.elementAt(0);
				 * 
				 * ord2.loadMessage(execcontra, maptag,
				 * queryData.getOrderData().mapoutparameter
				 * .get(SQUERY.matchorder.val));
				 * 
				 * print_pair("message contra from database:" +
				 * execcontra.toString());
				 * 
				 * if (sentryby != null) { execcontra.setString(ClientID.FIELD,
				 * sentryby); // execcontra.setString(tudSales.FIELD, sentryby);
				 * }
				 * 
				 * if (accid != null) { execcontra.setString(tudAccountID.FIELD,
				 * accid); } else log.error("account id can't be null");
				 * 
				 * if (rmtype != null) { execcontra.setString(tudDestUID.FIELD,
				 * rmtype.getUID()); // "gtw");
				 * execcontra.setString(tudDestIDAtsvr.FIELD,
				 * rmtype.getIDAtsvr()); // "1234");
				 * execcontra.setString(tudDestUType.FIELD, rmtype.getUType());
				 * // "gtw"); execcontra.setString(tudIsOrdTaker.FIELD, "1"); //
				 * message.setString(CumQty.FIELD, volume);
				 * execcontra.setDouble(CumQty.FIELD, volume);
				 * 
				 * // String seqno = vresult.elementAt(1); //
				 * message.setString(tudTRX_SEQNO.FIELD, seqno);
				 * 
				 * // String sheader = hedExecutionReport; // HeaderAndData
				 * handd = new HeaderAndData(sheader, hdata); //
				 * print_pair(" hdata:sreply : " + hdata);
				 * 
				 * if (execcontra != null) {
				 * 
				 * Message ortmessage = (Message) execcontra.clone(); boolean
				 * brouting = this.comHandling.send_appmsg( sessionid,
				 * execcontra); log.info(" brouting exec report: " + brouting);
				 * proxyRouting.processTaker(sessionid, ortmessage); } } else {
				 * print_pair("can't find destination for the contra message");
				 * log.info("can't find destination"); }
				 * 
				 * }
				 */

			}
		} catch (Exception e) {
			String key = message.getString(SecondaryOrderID.FIELD) + "#"
					+ message.getString(Side.FIELD);
			boolean isremoved = false;
			isremoved = proxyRouting.getJonecProc().remove_vdone(key);

			log.error("delete key for match:" + isremoved);

			throw e;
		}
		// }
	}

	public boolean init_forscheduller(Message message, RMType rm) {
		boolean isscheduler = false;

		String isSched = message.getString(tudBasketOrder.FIELD);

		String ssuid = muid.getSjonecuid(), ssidat = muid.getSjonecidatsvr(), ssutype = muid
				.getSjonecutype();
		String jobkey = message.getString(ClOrdID.FIELD);

		if (isSched != null && isSched.equals("1")) {
			ssuid = muid.getSschuid();
			ssidat = muid.getSschidatsvr();
			ssutype = muid.getSschutype();

			message.setString(tudBasketKey.FIELD, jobkey);
			// mapjobid_rm.put(jobkey, rm);
			// StringBuffer sbf = new StringBuffer();
			// sbf.append(jobkey);
			// sbf.append("###");
			// sbf.append(rm.toString());

			// mclord_rm.put(jobkey, rm);
			// save_datatofile(sbf.toString(), sjobid_rm);
			isscheduler = true;
		}

		message.setString(tudDestUID.FIELD, ssuid);
		message.setString(tudDestIDAtsvr.FIELD, ssidat);
		message.setString(tudDestUType.FIELD, ssutype);
		message.setString(tudSrcUID.FIELD, muid.getSrmuid());
		message.setString(tudSrcIDAtsvr.FIELD, muid.getSrmidatsvr());
		message.setString(tudSrcUType.FIELD, muid.getSrmutype());

		/*
		 * if (isscheduler) { hsched.put(jobkey, message.toString()); }
		 */

		return isscheduler;
	}

	private void load_datareqfromfile(Hashtable<String, String> hreq,
			String sfname) {

		try {
			String sclordid, data;
			String[] split;

			Vector<String> vtemp = new Vector<String>(100, 50);

			load_vectorfromfile(vtemp, sfname);
			for (String s : vtemp) {
				if (s.trim().length() > 0) {
					split = s.split("###", -2);
					sclordid = split[0];
					data = split[1];
					hreq.put(sclordid, data);
				}
			}
		} catch (Exception ex) {
			log.error(logger_info_exception(ex));
			ex.printStackTrace();
		}
	}

	private void load_dataonlinefromfile(Hashtable<String, String> honline,
			String sfname) {
		try {
			String uid, data;
			String[] split;

			Vector<String> vtemp = new Vector<String>(100, 50);

			load_vectorfromfile(vtemp, sfname);
			for (String s : vtemp) {
				if (s.trim().length() > 0) {
					split = s.split("###", -2);
					uid = split[0];
					data = split[1];

					//
					String[] sp = data.split("\\|", -2);
					if (sp[4].equals("1")) {
						StringBuffer sb = new StringBuffer(100);
						sb.append(sp[0]).append("|");
						sb.append(sp[1]).append("|");
						sb.append(sp[2]).append("|");
						sb.append(sp[3]);

						honline.put(uid, sb.toString());
						HashMap params = new HashMap();
						params.put(0, uid);
						params.put(1, sp[0] + "|" + sp[1] + "|" + sp[2] + "|"
								+ sp[3] + "|" + "LOGIN");
						comHandling.getEventDispatcher().pushEvent(
								EventDispatcher.EVENT_SESSION_LOGGEDIN, params);
					}
				}
			}
		} catch (Exception ex) {
			log.error(logger_info_exception(ex));
			ex.printStackTrace();
		}
	}

	private void load_datajobidrm_fromfile(Hashtable<String, RMType> hreq,
			String sfname) {

		try {
			String sclordid, data;
			String[] split;

			Vector<String> vtemp = new Vector<String>(100, 50);

			load_vectorfromfile(vtemp, sfname);
			for (String s : vtemp) {
				if (s.trim().length() > 0) {
					split = s.split("###", -2);
					sclordid = split[0];
					data = split[1];

					String[] spsource = data.split("\\|", -2);
					hreq.put(sclordid, new RMType(spsource[0], spsource[1],
							spsource[2]));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private RMType get_basedonclordid(String sclordid) {
		RMType rmtype = null;

		// String suid = mclord_uid.get(sclordid);

		// if (rmtype == null) {

		String suid = hentryby.get(sclordid);
		if (suid != null && !suid.isEmpty()) {
			rmtype = get_basedonuid(suid);
		} else {
			log.error("suid is null:" + sclordid);
		}

		if (rmtype == null)
			rmtype = mclord_rm.get(sclordid);

		// }

		return rmtype;
	}

	private RMType get_basedonuid(String suid) {
		RMType rm = null;

		suid = suid.trim();
		String stemp = honline.get(suid);
		if (stemp != null) {
			String[] split = stemp.split("\\|", -2);
			rm = new RMType(split[0], split[1], split[2]);
		}

		return rm;
	}

	public String getConverdtedSource(String ssrcuid, String ssrcidatsvr,
			String ssrcutype) {
		return ssrcuid + "#" + ssrcidatsvr + "#" + ssrcutype;
	}

	public void setProxyRouting(ProcessingProxyInterface proxyRouting) {
		this.proxyRouting = proxyRouting;
	}

}
