package com.eqtrade.rm.proxy;

import com.eqtrade.jonec.ABJonecService;
import com.eqtrade.jonec.JonecProcessing;
import com.eqtrade.net.ThirdPartyInterface;
import com.eqtrade.ort.OrtService;
import com.eqtrade.quickfix.resources.Message;

public interface ProcessingProxyInterface {
	boolean processRouting(String sessionid,Message messge);
	public void setThirdPartyInterface(ThirdPartyInterface _tpi);
	public ABJonecService getJonecEngine();
	boolean processTaker(String sessionid,Message messge);
	public OrtService getOrderTaker();
	public JonecProcessing getJonecProc();
}
