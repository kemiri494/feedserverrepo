package com.eqtrade.rm.proxy;

import java.rmi.RemoteException;
import java.util.Hashtable;

import com.db4o.internal.EventDispatcher;
import com.eqtrade.jonec.ABJonecInterface;
import com.eqtrade.jonec.ABJonecService;
import com.eqtrade.jonec.ABJonecServiceSocket;
import com.eqtrade.jonec.InfoSymbol;
import com.eqtrade.jonec.JonecProcessing;
import com.eqtrade.net.CommClientHandling;
import com.eqtrade.net.ThirdPartyInterface;
import com.eqtrade.ort.OrtService;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.quickfix.resources.field.tudIsOrdTaker;
import com.eqtrade.rm.MappingServerID;
import com.eqtrade.rm.proxy.ProxyJonecAndORTEmbedded.JonecMessage;
import com.eqtrade.utils.FixMessageUtils;

import com.eqtrade.database.AccessQueryLocator;

public class ProxyJonecAndORTEmbedded extends ABJonecServiceSocket implements
		ProcessingProxyInterface, ABJonecInterface {

	protected ThirdPartyInterface tpi;
	private JonecProcessing jp;
	private MappingServerID muid;
	private int nportlocalrmi;
	private String sessionid;
	protected OrtService ortService;
	private CommClientHandling comClient;

	public ProxyJonecAndORTEmbedded(MappingServerID _muid,
			Integer nportlocalrmi, String smartins, OrtService ortServ,
			com.eqtrade.dx.EventDispatcher ievent, AccessQueryLocator dbquery,
			CommClientHandling comm,InfoSymbol is) throws Exception {
		super("", "", -1, -1, _muid.getSrmuid(), _muid.getSrmidatsvr(), _muid
				.getSrmutype(), _muid.getSjonecuid(), _muid.getSjonecidatsvr(),
				_muid.getSjonecutype(), -1, -1, -1, nportlocalrmi, smartins,
				ievent, dbquery,is);
	/*	super("", "", -1, -1, _muid.getSrmuid(), _muid.getSrmidatsvr(), _muid
				.getSrmutype(), _muid.getSjonecuid(), _muid.getSjonecidatsvr(),
				_muid.getSjonecutype(), -1, -1, -1, nportlocalrmi, smartins,
				ievent, dbquery,is);
	*/	// this.jonecProcessing = new JonecProcessing(this, nportlocalrmi,
		// srmuid,
		// srmidatsvr, srmtype, is);
		this.comClient = comm;
		this.muid = _muid;
		this.ortService = ortServ;

	}

	@Override
	public boolean processRouting(String sessionid, Message messge) {
		// TODO Auto-generated method stub

		boolean brouting = false;
		String uid = messge.getString(tudDestUID.FIELD);
		String utype = messge.getString(tudDestUType.FIELD);
		String idatsvr = messge.getString(tudDestIDAtsvr.FIELD);

		if (utype != null && utype != null && idatsvr != null) {
			if (uid.equals(muid.getSjonecuid()) && idatsvr.equals(muid.getSjonecidatsvr()) && utype.equals(muid.getSjonecutype())) {
				jonecProcessing.process_dat(sessionid, messge);
				brouting = true;
			} else {
				brouting = comClient.send_appmsg(sessionid, messge);
				brouting = false;
			}
		}

		return brouting;
	}
	
	public JonecProcessing getJonecProc(){
		return jonecProcessing;
	}
	

	@Override
	protected void init_networking(int nheartbt) throws RemoteException {
		jonecProcessing = new JonecProcessing(this, nportlocalrmi, this.srmuid, this.srmidatsvr, this.srmtype, getInfoSymbol());
	}

	@Override
	public boolean send_msgtorm(String sheader, Hashtable<Integer, String> hdata) {
		if (sessionid == null && comClient.getCm() != null) {
			sessionid = comClient.getCm().get_sessionid(muid.getSrmuid(), muid.getSrmidatsvr(), muid.getSrmutype());
		}

		Message message = FixMessageUtils.create_message("", sheader, hdata);

		if (tpi != null) {
			JonecMessage task = new JonecMessage(comClient.getSessionID(), message);
			comClient.executeTask(task);
			/*synchronized (this) {
				try {
					this.wait(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}*/			
		}

		return true;
	}

	@Override
	public boolean send_msgtorm(String sheader, Message message) {
		/*if (sessionid == null && ClientManager.cm != null) {

			sessionid = ClientManager.cm.get_sessionid(muid.getSrmuid(),
					muid.getSrmidatsvr(), muid.getSrmutype());
		}

		String sessionidnew = comClient.getCm().get_sessionid(muid.getSrmuid(),
				muid.getSrmidatsvr(), muid.getSrmutype());
		if (!sessionid.equals(sessionidnew))
			sessionid = sessionidnew;*/

		if (tpi != null) {
			JonecMessage task = new JonecMessage(comClient.getSessionID(), message);
			comClient.executeTask(task);
	//		tpi.process_dat(sessionid, message);
			/*synchronized (this) {
				try {
					this.wait(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}*/
			return true;
		}

		return false;
	}

	public void setThirdPartyInterface(ThirdPartyInterface _tpi) {
		this.tpi = _tpi;
	}

	@Override
	public ABJonecService getJonecEngine() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public boolean processTaker(String sessionid, Message messge) {

		boolean result = false;

		String istaker = messge.getString(tudIsOrdTaker.FIELD);

		if (istaker != null && istaker.equals("1")) {

			ortService.getProcessing().process_dat(sessionid, messge);
			result = true;
		}

		return result;
	}

	@Override
	public OrtService getOrderTaker() {
		// TODO Auto-generated method stub
		return ortService;
	}

	public class JonecMessage extends Thread {
		private String sessionid;
		private Message message;

		public JonecMessage(String sessionid, Message message) {
			this.sessionid = sessionid;
			this.message = message;
		}

		@Override
		public void run() {
			ProxyJonecAndORTEmbedded.this.tpi.process_dat(sessionid, message);
		}

	}

	@Override
	public void stop() {
		//getHandlMsg().setstop();
		jonecProcessing.saving_alldata();
		
	}
	
	

}
