package com.eqtrade.rm;

public class MappingServerID {
	private String sjonecuid, sjonecidatsvr, sjonecutype;
	private String srmuid, srmidatsvr, srmutype;
	private String sschuid, sschidatsvr, sschutype;

	public MappingServerID(String sjonecuid, String sjonecidatsvr,
			String sjonecutype, String srmuid, String srmidatsvr,
			String srmutype, String sschuid, String sschidatsvr,
			String sschutype) {
		super();
		this.sjonecuid = sjonecuid;
		this.sjonecidatsvr = sjonecidatsvr;
		this.sjonecutype = sjonecutype;
		this.srmuid = srmuid;
		this.srmidatsvr = srmidatsvr;
		this.srmutype = srmutype;
		this.sschuid = sschuid;
		this.sschidatsvr = sschidatsvr;
		this.sschutype = sschutype;
	}

	public String getSjonecuid() {
		return sjonecuid;
	}

	public void setSjonecuid(String sjonecuid) {
		this.sjonecuid = sjonecuid;
	}

	public String getSjonecidatsvr() {
		return sjonecidatsvr;
	}

	public void setSjonecidatsvr(String sjonecidatsvr) {
		this.sjonecidatsvr = sjonecidatsvr;
	}

	public String getSjonecutype() {
		return sjonecutype;
	}

	public void setSjonecutype(String sjonecutype) {
		this.sjonecutype = sjonecutype;
	}

	public String getSrmuid() {
		return srmuid;
	}

	public void setSrmuid(String srmuid) {
		this.srmuid = srmuid;
	}

	public String getSrmidatsvr() {
		return srmidatsvr;
	}

	public void setSrmidatsvr(String srmidatsvr) {
		this.srmidatsvr = srmidatsvr;
	}

	public String getSrmutype() {
		return srmutype;
	}

	public void setSrmutype(String srmutype) {
		this.srmutype = srmutype;
	}

	public String getSschuid() {
		return sschuid;
	}

	public void setSschuid(String sschuid) {
		this.sschuid = sschuid;
	}

	public String getSschidatsvr() {
		return sschidatsvr;
	}

	public void setSschidatsvr(String sschidatsvr) {
		this.sschidatsvr = sschidatsvr;
	}

	public String getSschutype() {
		return sschutype;
	}

	public void setSschutype(String sschutype) {
		this.sschutype = sschutype;
	}

}
