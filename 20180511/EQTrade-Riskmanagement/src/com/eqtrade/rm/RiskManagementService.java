package com.eqtrade.rm;

import static com.eqtrade.utils.Function.print_pair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.dx.RMListener;
import com.eqtrade.jonec.InfoSymbol;
import com.eqtrade.net.CommClientHandling;
import com.eqtrade.ort.MediaComm;
import com.eqtrade.ort.OrtService;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.MessageFactory;
import com.eqtrade.rm.proxy.ProcessingProxyInterface;
import com.eqtrade.rm.proxy.ProxyJonecAndORTEmbedded;
import com.eqtrade.timer.HouseKeepingListeners;
import com.eqtrade.timer.Scheduller;
import com.eqtrade.utils.Function;
import com.eqtrade.utils.properties.FEProperties;


public class RiskManagementService extends Object implements RMListener,HouseKeepingListeners {
	private final Logger log = LoggerFactory.getLogger(super.getClass());
	private String service, svr;
	private int nportrmi, nportlocalrmi, nport, socketThread;
	private String surl, sdriver, suid, spwd;
	private FEProperties fep;
	private String srmuid, srmidatsvr, srmutype;
	private int minpoolsize, maxpoolsize, keepalivetime;
	private OrtService ortservice;
	private quickfix.MessageFactory mf = new MessageFactory();
	protected ProcessingProxyInterface proxyInterface;
	protected EventDispatcher ievent;
	private Scheduller scheduller;
	private SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
	private Double lotsize;

	public RiskManagementService(String srmuid, String srmidatsvr,
			String srmutype, String aservice, int aportrmi, int aportlocalrmi,
			String asvr, int aport, int aheartbt, String aurl, String adriver,
			String auid, String apwd, FEProperties afep, EventDispatcher ievent)
			throws Exception {

		ievent.registerListener(this);
		this.service = aservice;
		this.nportrmi = aportrmi;
		this.nportlocalrmi = aportlocalrmi;
		this.nport = aport;
		this.svr = asvr;
		this.surl = aurl;
		this.sdriver = adriver;
		this.suid = auid;
		this.spwd = apwd;
		this.ievent = ievent;
		this.fep = afep;

		this.srmuid = fep.get("rm.uid");
		this.srmidatsvr = fep.get("rm.idatsvr");
		this.srmutype = fep.get("rm.utype");
		this.minpoolsize = Integer.parseInt(fep
				.getProperty("minpoolsize", "20"));
		this.maxpoolsize = Integer.parseInt(fep
				.getProperty("maxpoolsize", "50"));
		this.keepalivetime = Integer.parseInt(fep.getProperty("keepalivetime",
				"20"));
		this.socketThread = Integer.parseInt(fep.getProperty("threadSocket",
				"50"));
		log.info("housekeeping time " + fep.getProperty("houseKeepingTime"));
		Date timeHouseKeeping = sdfTime.parse(fep
				.getProperty("houseKeepingTime"));
		Date time = sdfTime.parse(fep.getProperty("RestartDBTime"));

		String sschuid = fep.get("queue.uid");
		String sschidat = fep.get("queue.idatsvr");
		String sschtype = fep.get("queue.utype");
		String sjonecuid = fep.getProperty("jonec.uid");
		String sjonecidatsvr = fep.getProperty("jonec.idatsvr");
		String sjonectype = fep.getProperty("jonec.type");
		lotsize = Double.parseDouble(fep.getProperty("lotsize"));
		muid = new MappingServerID(sjonecuid, sjonecidatsvr, sjonectype,
				this.srmuid, this.srmidatsvr, this.srmutype, sschuid, sschidat,
				sschtype);

		scheduller = new Scheduller(cm);

		init_allnetworking(aheartbt);
		init_dbconnection();
		init_infosymbol();
		init_proxy();
		initHouseKeeping(timeHouseKeeping);
		initRestartDB(time);
	}

	public void initRestartDB(Date time) {
		try {
			log.info("init restartDB.." + time.toLocaleString());
			JobDetail jd = new JobDetail("RestartDBJob", SchedullerJob.class);
			jd.getJobDataMap().put("listeners", this);
			Trigger trigger = TriggerUtils.makeDailyTrigger("RestartDBJob",
					time.getHours(), time.getMinutes());
			scheduller.addSchedule(trigger, jd);
		} catch (Exception e) {
			log.error(Function.logger_info_exception(e));
		}
	}

	public void initHouseKeeping(Date time) throws SchedulerException {
		log.info("init housekeeping");
		scheduller.scheduleHouseKeeping(time, this);
	}

	public CommClientHandling getCom() {
		return cm;
	}

	private void init_infosymbol() {

		print_pair("Load Symbol....");
		is = new InfoSymbol(queryData);
	}

	private void init_proxy() throws Exception {
		Function.print_pairmust("Init proxy jonec.....");

		FEProperties fep = new FEProperties("jonec.properties");
		String sportlocalrmi = fep.getProperty("-portlocalrmi");
		Integer nportlocalrmi = new Integer(sportlocalrmi.trim());

		String smartins = fep.getProperty("-martins");

		String smaxcounterlock = fep.getProperty("lock.maxcounter", "3");
		int nmaxcounterlock = Function.get_inttype(smaxcounterlock, 3);

		Function.print_pairmust("Init RMProcessing...");

		rmproc = new RiskManagementProcessing(cm, muid, nmaxcounterlock,
				queryData, null, is, lotsize);
		// rmproc.start();

		cm.setThirdPartyInterface(rmproc);
		Function.print_pairmust("Init RMProcessing...done");

		ortservice = new com.eqtrade.ort.OrtService(srmuid, srmidatsvr,
				srmutype, fep, this.ievent, this.queryData, this.cm);

		proxyInterface = new ProxyJonecAndORTEmbedded(this.muid, nportlocalrmi,
				smartins, ortservice, this.ievent, this.queryData, this.cm, is);
		// ((ProxyJonecAndORTEmbedded) proxyInterface);

		rmproc.setProxyRouting(proxyInterface);
		proxyInterface.setThirdPartyInterface(rmproc);
		Function.print_pairmust("Init proxy jonec done");
	}

	// private DBQueryPool dbpool = null;
	private AccessQueryLocator queryData = null;

	private void init_dbconnection() throws Exception {
		// dbpool = new DBQueryPool(surl, sdriver, suid, spwd, 10);
		log.info("init db connection " + fep.get("database"));
		queryData = new AccessQueryLocator(fep.get("database"));
	}

	private CommClientHandling cm = null;
	private RiskManagementProcessing rmproc = null;
	private MappingServerID muid;
	private InfoSymbol is;

	// private Token token;

	private void init_allnetworking(int heartbt) throws Exception {
		log.info("RM net:minpool:" + this.minpoolsize + ":maxpool:"
				+ this.maxpoolsize + ":socketthread:" + this.socketThread);
		Function.print_pairmust("Init networking interface ...");
		cm = new CommClientHandling(heartbt, mf, this.minpoolsize,
				this.maxpoolsize, this.keepalivetime, ievent, socketThread);

	}

	protected void do_fixmsg(String sops, String suid, String sidatsvr,
			String sutype, String svriploc, int nportloc, String aalias,
			String abranch) {

		Function.print_pairmust("mc:logon:" + svriploc + ":" + nportloc);
		Function.print_pairmust("mc:sops:" + sops);

		if (sops.equalsIgnoreCase("logon")) {
			if (cm != null) {
				boolean blogon = cm.connect_tofixsvr(suid, sidatsvr, sutype,
						svriploc, nportloc);
				// Function.print_pairmust("logonConnectToFixsvr:" + blogon);
			}
		}
	}

	protected void do_fixmsg(String sops, String suid, String sidatsvr,
			String sutype) {

		if (sops.equalsIgnoreCase("testreqid")) {
			Function.print_pairmust("mc:testRequestID");
			if (cm.getCm() != null) {

				boolean btestreqid = cm.getCm().test_requestid(suid, sidatsvr,
						sutype);

				Function.print_pairmust("testRequstIDToOtherSide:" + btestreqid);
			}
		} else if (sops.equalsIgnoreCase("logout")) {
			Function.print_pairmust("Logout");
			if (cm != null) {

				boolean blogout = cm.getCm().logout(suid, sidatsvr, sutype,
						"Logout from client");

				Function.print_pairmust("testLogoutFromOtherSide:" + suid);

			}
		}
	}

	public boolean connect_tofixsvr(String psvriploc, int pnportloc) {

		boolean blogon = false;
		this.alias = "aalias";
		this.abranch = "abranch";
		this.svr = psvriploc;
		this.nport = pnportloc;
		if (cm != null) {
			log.info("connecting.....");

			blogon = cm.connect_tofixsvr(srmuid, srmidatsvr, srmutype, svr,
					nport);
			log.info("connected:" + blogon);

		}
		return blogon;
	}

	private String alias, abranch;

	public Object[] getConfigurationClient() {
		Object[] conf = new Object[] { this.srmuid, this.srmidatsvr,
				this.srmutype, this.svr, this.nport };
		return conf;
	}

	public void disconnect() {
		if (cm.disconnected()) {
			log.info("disconnected");
			ievent.pushEvent(EventDispatcher.EVENT_DISCONNECTED, null);
		} else {
			log.info("still connected");

			ievent.pushEvent(EventDispatcher.EVENT_CONNECTED, null);
		}
	}

	public ProxyJonecAndORTEmbedded getJonecService() {
		return (ProxyJonecAndORTEmbedded) proxyInterface;
	}

	@Override
	public void loadConfiguration() {
		HashMap params = new HashMap();
		params.put(0, srmuid);
		params.put(1, srmidatsvr);
		params.put(2, srmutype);
		params.put(3, svr);
		params.put(4, nport);

		ievent.pushEvent(EventDispatcher.EVENT_LOAD_FINISH, params);

	}

	@Override
	public void setConfiguration(HashMap params) {
		// TODO Auto-generated method stub
		this.srmuid = (String) params.get(0);
		this.srmidatsvr = (String) params.get(1);
		this.srmutype = (String) params.get(2);
		this.svr = (String) params.get(3);
		this.nport = (Integer) params.get(4);
		fep.setProperty("rm.uid", srmuid);
		fep.setProperty("rm.idatsvr", srmidatsvr);
		fep.setProperty("rm.utype", srmutype);
		fep.setProperty("-server", svr);
		fep.setProperty("-port", nport + "");
		fep.save();
	}

	public void start() {
		HashMap params = new HashMap();
		params.put(0, srmuid);
		params.put(1, srmidatsvr);
		params.put(2, srmutype);
		params.put(3, svr);
		params.put(4, nport);

		start(params);
	}

	@Override
	public void start(HashMap params) {
		setConfiguration(params);
		log.info("get configuration:" + params.toString());

		if (connect_tofixsvr(this.svr, this.nport)) {
			proxyInterface.getJonecEngine().start_timer();// getJonecEngine().start_timer();
			ievent.pushEvent(EventDispatcher.EVENT_CONNECTED, null);
		} else {
			ievent.pushEvent(EventDispatcher.EVENT_DISCONNECTED, null);
		}
	}

	@Override
	public void stop() {
		// queryData.getOrderData().stop();
		// queryData.getQueryData().stop();

		disconnect();
		rmproc.saveData();
		proxyInterface.getJonecEngine().stop();

	}

	@Override
	public void securitiesRequest(HashMap params) {
		proxyInterface.getJonecEngine().securitiesRequest(params);

	}

	@Override
	public void subscribeUnsubscribe(HashMap params) {
		proxyInterface.getJonecEngine().subscribeUnsubscribe(params);

	}

	@Override
	public void logon_tojones(HashMap params) {
		proxyInterface.getJonecEngine().logon_tojones(params);

	}

	@Override
	public void logout_tojones(HashMap params) {
		proxyInterface.getJonecEngine().logout_tojones(params);

	}

	@Override
	public void startJonec_connection() {
		proxyInterface.getJonecEngine().startJonec_connection();

	}

	@Override
	public void securitiesLoad(HashMap params) {
		proxyInterface.getJonecEngine().securitiesLoad(params);

	}

	@Override
	public void jonecLoad() {
		proxyInterface.getJonecEngine().jonecLoad();
	}

	@Override
	public void changePwd(HashMap params) {
		proxyInterface.getJonecEngine().changePwd(params);

	}

	@Override
	public void boardLoad() {
		proxyInterface.getJonecEngine().boardLoad();
	}

	@Override
	public void connectAll(HashMap arg0) {
		proxyInterface.getJonecEngine().connectAll(arg0);
	}

	@Override
	public void disconnectAll(HashMap arg0) {
		proxyInterface.getJonecEngine().disconnectAll(arg0);
	}

	@Override
	public void subscribeLoad(HashMap arg0) {
		this.proxyInterface.getJonecEngine().subscribeLoad(null);
	}

	@Override
	public void load_setting4taker() {
		MediaComm mc = proxyInterface.getOrderTaker().load_setting4taker();

		HashMap params = new HashMap();
		params.put(0, mc.get_husr());
		ievent.pushEvent(EventDispatcher.EVENT_LOAD_TAKER_FINISHED, params);

	}

	public void shutdown() {
		queryData.getOrderData().stop();
		queryData.getQueryData().stop();
	}

	public AccessQueryLocator getDatabase() {
		return queryData;
	}

	public void restartDB() {
		try {
			log.info("restart DB...");
			queryData.getOrderData().stop();
			queryData.getQueryData().stop();
			init_dbconnection();
			log.info("restart DB finished...");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(Function.logger_info_exception(e));
		}
	}

	@Override
	public void execute() {
		try {
			log.info("execute housekeeping");
			queryData.getOrderData().stop();
			queryData.getQueryData().stop();
			init_dbconnection();
			// queryData.getOrderData().start(fep.get("database"));
			// queryData.getQueryData().start(fep.get("database"));
			// queryData.getQueryData().set_insert(
			// QueryData.SQUERY.houseKeeping.val, null);
			rmproc.loadData();
		} catch (Exception e) {
			log.error(Function.logger_info_exception(e));
		}
	}

	@Override
	public void send_msg(HashMap params) {
		Message msg = (Message) params.get(0);
		getJonecService().getJonecProcessing().process_dat("", msg);
	}

}
