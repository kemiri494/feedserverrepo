package com.eqtrade.rm;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eqtrade.timer.HouseKeepingListeners;

public class SchedullerJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDetail jd = context.getJobDetail();
		RiskManagementService listener = (RiskManagementService) jd.getJobDataMap().get("listeners");
		listener.restartDB();

	}

}
