package com.eqtrade.rm;

import java.io.Serializable;

public final class RMType extends Object {

	private String suid, sidatsvr, sutype;
	public RMType(String auid, String aidatsvr, String atype){
		this.suid = auid;
		this.sidatsvr = aidatsvr;
		this.sutype = atype;
	}

	public String getUID(){ return this.suid;}
	public String getIDAtsvr() { return this.sidatsvr;}
	public String getUType() { return this.sutype;}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getUID()+"|"+getIDAtsvr()+"|"+getUType();
	}
	
	

}