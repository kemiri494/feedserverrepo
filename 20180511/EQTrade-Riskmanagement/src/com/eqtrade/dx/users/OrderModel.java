package com.eqtrade.dx.users;

import com.eqtrade.database.model.OrderListTable;
import com.eqtrade.dx.AbstractListTableModel;

public class OrderModel extends AbstractListTableModel {

	public OrderModel(String[] headers) {
		super(headers);
		// TODO Auto-generated constructor stub
	}

	public String[] getHeaders() {
		return this.headers;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		OrderListTable ord = (OrderListTable) items.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return ord.getBuyorsell();
		case 1:
			return ord.getSecid();
		case 2:
			return ord.getStatusid();
		case 3:
			return ord.getEntryrttime();
		case 4:
			ord.getAmendtime();
		case 5:
			return ord.getWithdrawtime();
		case 6:
			return ord.getMatchtime();
		case 7:
			return ord.getTerminalid();
		case 8:
			return ord.getUserid();
		default:
			break;
		}

		return "";
	}

	@Override
	public void loadList() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object obj) {
		// TODO Auto-generated method stub

	}

}
