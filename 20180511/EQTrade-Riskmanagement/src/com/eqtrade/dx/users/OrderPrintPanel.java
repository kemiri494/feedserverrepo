package com.eqtrade.dx.users;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Vector;

import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryData.SQUERY;
import com.eqtrade.database.model.OrderListTable;
import com.eqtrade.dx.BaseMainPanel;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.dx.MainApp;
import com.eqtrade.rm.RiskManagementService;

public class OrderPrintPanel extends BaseMainPanel {

	private javax.swing.JButton btnPrint;
	private javax.swing.JButton btnView;
	private javax.swing.JTextField fieldTimeEnd;
	private javax.swing.JTextField fieldTimeStart;
	private javax.swing.JSpinner fieldLimit;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JLabel lblTimeEnd;
	private javax.swing.JLabel lblTimeStart;
	private javax.swing.JLabel lblLimit;
	private javax.swing.JLabel lblStatus;
	private javax.swing.JTextField fieldStatus;
	private javax.swing.JTable tabelOrder;
	private OrderModel orderModel;
	private RiskManagementService rmService;
	private OrderModelPrint orderPrint;

	public OrderPrintPanel(String title, MainApp frame, EventDispatcher evt,
			RiskManagementService rmService) {
		super(title, frame, evt);
		this.rmService = rmService;
		init();

	}

	private void init() {
		orderPrint = new OrderModelPrint(this);
		jScrollPane1 = new javax.swing.JScrollPane();
		tabelOrder = new javax.swing.JTable();
		fieldTimeStart = new javax.swing.JTextField();
		lblTimeStart = new javax.swing.JLabel();
		lblTimeEnd = new javax.swing.JLabel();
		fieldTimeEnd = new javax.swing.JTextField();
		btnView = new javax.swing.JButton();
		btnPrint = new javax.swing.JButton();
		lblLimit = new javax.swing.JLabel();
		fieldLimit = new javax.swing.JSpinner();
		fieldStatus = new javax.swing.JTextField();
		lblStatus = new javax.swing.JLabel();
		orderModel = new OrderModel(new String[] { "buysell", "secid",
				"statusid", "entryrttime", "amendtime", "withdrawtime",
				"matchtime", "terminalid", "userid" });

		// tabelOrder.setModel(new javax.swing.table.DefaultTableModel(
		// / new Object[][] {

		// / }, new String[] { "BUYORSELL", "SECID", "STATUSID",
		// / "ENTRY RT TIME", "AMEND TIME", "WITHDRAW TIME",
		// "MATCH TIME", "TERMINALID", "USERID" }));
		tabelOrder.setModel(orderModel);
		tabelOrder.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
		jScrollPane1.setViewportView(tabelOrder);

		lblTimeStart.setText("Entry Time ");

		lblTimeEnd.setText("And");

		btnView.setText("view");
		btnView.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				viewOrder();
			}
		});

		btnPrint.setText("print");
		btnPrint.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				print(e);
			}
		});

		lblLimit.setText("Limit");
		
		lblStatus.setText("Status Order");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														jScrollPane1,
														javax.swing.GroupLayout.Alignment.TRAILING,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														803, Short.MAX_VALUE)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		lblStatus)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		fieldStatus,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		136,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGap(18, 18,
																		18)
																.addComponent(
																		lblLimit,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		42,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		fieldLimit,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		75,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		btnView,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		66,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		btnPrint,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		72,
																		javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														fieldStatus,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(lblStatus)
												.addComponent(lblLimit)
												.addComponent(
														fieldLimit,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(btnView)
												.addComponent(btnPrint))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jScrollPane1,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										293, Short.MAX_VALUE).addContainerGap()));

	}

	protected void print(ActionEvent e) {
		orderPrint.print(orderModel);
	}

	protected void viewOrder() {
		Integer limit = (Integer) fieldLimit.getValue();
		FOData fodata = new FOData();
		fodata.set_int(limit);
		fodata.set_string(fieldStatus.getText().trim());
		try {
			//Vector vobj = rmService.getDatabase().getQueryData()
				//	.set_insert(SQUERY.orderlistParameter.val, fodata);

		//	orderModel.clearData();
			//for (Object object : vobj) {
				//OrderListTable ord = (OrderListTable) object;
		//		orderModel.addData(ord);
			//d}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute(Integer evt, HashMap params) {

	}

}
