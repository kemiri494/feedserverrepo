package com.eqtrade.dx.users;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;

public class OrderModelPrint {
	private Log log = LogFactory.getLog(getClass());

	protected JFileChooser jfc = null;
	protected PdfFileFilter filterFile = null;
	public static SimpleDateFormat formatdate = new SimpleDateFormat(
	"yyyy-MM-dd HH:mm:ss");
	Component parent;

	public OrderModelPrint(Component parent) {
		filterFile = new PdfFileFilter();
		filterFile.addExtension("html");
		filterFile.setDescription(" HTML Format ");
		jfc = new JFileChooser(new File("C:\\"));
		jfc.setFileFilter(filterFile);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setDialogTitle("Save Order to....");
		jfc.setDialogType(JFileChooser.SAVE_DIALOG);
		this.parent = parent;
	}

	public void print(OrderModel model) {
		String filename = "";
		int returnVal = jfc.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File ffile = jfc.getSelectedFile();
			filename = ffile.getAbsolutePath();
			if (!filename.endsWith(".txt")) {
				filename = filename + ".txt";
			}
		} else {
			return;
		}

		try {			
			//int[] columnwidth = (int[]) hsettings.get("width");
			//int[] columnPrint = (int[]) hsettings.get("order");
			String[] columnheader = model.getHeaders();
			FastReportBuilder drb = new FastReportBuilder();

			for (int i = 0; i < columnheader.length; i++) {
				//int idx = (Integer) columnheader[i];
				// if (columnwidth[i] > 0) {
				String header = columnheader[i];

				Style detailStyle = new Style();

				detailStyle.setBorderRight(Border.THIN);
				detailStyle.setBorderLeft(Border.THIN);
				detailStyle.setBorderBottom(Border.THIN);
				detailStyle.setBorderTop(Border.THIN);
				detailStyle.setHorizontalAlign(HorizontalAlign.CENTER);

				Style headerStyle = new Style();
				headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);
				headerStyle.setBorderTop(Border.THIN);
				headerStyle.setBorderBottom(Border.THIN);
				headerStyle.setBorderLeft(Border.THIN);
				detailStyle.setBorderLeft(Border.THIN);
				headerStyle.setBorderRight(Border.THIN);
				detailStyle.setBorderRight(Border.THIN);

				headerStyle.setBackgroundColor(Color.GRAY);
				headerStyle.setTransparency(Transparency.OPAQUE);
				headerStyle.setTextColor(Color.white);
				headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
				headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
				int width = getColumnWidth(i);

				AbstractColumn ac = ColumnBuilder.getNew().setTitle(header)
						.setStyle(detailStyle).setHeaderStyle(headerStyle)
						.setColumnProperty(header, String.class.getName())
						.setWidth(width)
						.setCustomExpression(new CustomOrderExpression(header))
						.build();
				drb.addColumn(ac);
				// }
			}

			JRDataSource ds = new JRTableModelDataSource(model);
			drb.setUseFullPageWidth(true);
			// drb.setPageSizeAndOrientation(new Page(500, 1600, true));
			drb.setPageSizeAndOrientation(Page.Page_A4_Landscape());
			drb.setMargins(20, 20, 5, 5);

			DynamicReport dr = drb.build();
			JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr,
					new ClassicLayoutManager(), ds);

			//JasperExportManager.exportReportToPdfFile(jp, filename);
			JasperExportManager.exportReportToHtmlFile(jp, filename);
			// JasperViewer.viewReport(jp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

	private int getColumnWidth(int i) {
		int defaultWidth = 75;
		switch (i) {
		case 3:case 4:case 5:
			defaultWidth = 100;
			break;
		default:
			break;
		}
		return defaultWidth;
	}



	public class CustomOrderExpression implements CustomExpression {
		private String header;

		public CustomOrderExpression(String column) {
			header = column;
		}

		@Override
		public Object evaluate(Map fields, Map variable, Map parameters) {
			
			log.info("header "+header);
			Object value = fields.get(header);
						
			String strTemp;
			if(value instanceof Timestamp){
				//Timestamp stmp = new Timestamp(((Timestamp) value).getTime());
				Date dt = new Date(((Timestamp) value).getTime());
				value = formatdate.format(dt);
			}
			/*if (value instanceof ImmutableIData) {
				ImmutableIData args = (ImmutableIData) value;
				Object dat = args.getData();
				Order o = (Order) args.getSource();

				if (dat instanceof Double) {
					double d = Math.floor(((Double) dat).doubleValue());
					double s = (((Double) dat).doubleValue()) - d;
					if (s > 0) {
						formatter.setMaximumFractionDigits(2);
						formatter.setMinimumFractionDigits(2);
					} else {
						formatter.setMaximumFractionDigits(0);
						formatter.setMinimumFractionDigits(0);
					}
					return formatter.format(dat);
				} else if (dat instanceof Date) {
					if (strFieldName
							.equals(OrderDef.dataHeader[Order.C_ORDERDATE])) {
						return formatdate.format((Date) dat);
					} else {
						return formattime.format((Date) dat);
					}
				} else if (dat instanceof Timestamp) {
					return formattime.format(new Date(((Timestamp) dat)
							.getTime()));
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_STATUSID])) {
					if (dat != null && o != null) {
						if (dat.equals("o") && o.getIsBasketOrder().equals("1")) {
							strTemp = "Queue";
						} else {
							strTemp = (String) hashStatus.get(dat);
						}
						if (strTemp == null)
							strTemp = dat.toString();
						return strTemp;
					} else {
						strTemp = (String) hashStatus.get(dat);
						if (strTemp == null)
							strTemp = dat + " Unknown";
						return strTemp;
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ORDERTYPE])) {
					strTemp = (String) hashType.get(dat);
					if (strTemp == null)
						strTemp = dat + " Uknown";
					return strTemp;
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ORDTYPE])) {
					if (dat != null) {
						if (dat.equals("7") || dat.equals("E"))
							return "Limited Order";
						else
							return dat + " unknown";
					} else {
						return "";
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ISADVERTISING])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISBASKETORDER])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISOVERLIMIT])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISFLOOR])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISSHORTSELL])) {
					if (dat == null) {
						return "";
					} else {
						return dat.equals("1") ? "Yes" : "No";
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_BUYORSELL])) {
					strTemp = (String) hashSide.get(dat);

					if (strTemp == null)
						strTemp = dat + " Unknown";
					return strTemp;
				} else if (dat == null) {
					return "";
				}

				return " " + dat.toString();

			}*/

			return value;

		}

		@Override
		public String getClassName() {
			return String.class.getName();
		}

	}

	public class PdfFileFilter extends FileFilter {
		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		public PdfFileFilter() {
			this.filters = new Hashtable();
		}

		public PdfFileFilter(String extension) {
			this(extension, null);
		}

		public PdfFileFilter(String extension, String description) {
			this();
			if (extension != null)
				addExtension(extension);
			if (description != null)
				setDescription(description);
		}

		public PdfFileFilter(String[] filters) {
			this(filters, null);
		}

		public PdfFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null)
				setDescription(description);
		}

		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description
							+ " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "."
								+ (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ."
									+ (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				} else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

}
