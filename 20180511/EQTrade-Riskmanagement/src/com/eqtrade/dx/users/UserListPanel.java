package com.eqtrade.dx.users;

import java.util.HashMap;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.eqtrade.dx.BaseMainPanel;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.dx.MainApp;
import com.eqtrade.dx.main.client.ClientTableModel;

public class UserListPanel extends BaseMainPanel implements
		ListSelectionListener {

	private javax.swing.JScrollPane jScrollPane1;
	private JTable jTable1;
	private ClientTableModel model;

	public UserListPanel(String title,MainApp frame, EventDispatcher ievent) {
		super(title,frame,ievent);
	}

	public void init_components() {
		jScrollPane1 = new javax.swing.JScrollPane();
		jTable1 = new javax.swing.JTable();
		model = new ClientTableModel(new String[] { "USER ID", "SESSION ID",
				"GTW ID","STATUS" }, null);

		setName("Form"); // NOI18N

		jScrollPane1.setName("jScrollPane1"); // NOI18N

		jTable1.setModel(model);
		jTable1.setName("jTable1"); // NOI18N
		jScrollPane1.setViewportView(jTable1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
						375, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(15, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
						275, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(14, Short.MAX_VALUE)));
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Integer evt, HashMap params) {
		// TODO Auto-generated method stub
		
	}

}
