package com.eqtrade.dx;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import com.digitprop.tonic.TonicLookAndFeel;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.TestRequest;
import com.eqtrade.quickfix.resources.field.Text;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.rm.RiskManagementService;
import com.eqtrade.test.CommandInterface;
import com.eqtrade.test.Console;
import com.eqtrade.utils.properties.FEProperties;

public class RMGuiMain implements CommandInterface {
	private final Log log = LogFactory.getLog(super.getClass());

	private RiskManagementService rmService;
	private MainWindow clientWindow;
	private String service;
	private EventDispatcher ieventDispatcher;
	private FEProperties fep;
	private RMMenuAction rmMenuAction;

	public RMGuiMain(FEProperties _fep) throws Exception {

		this.fep = _fep;
		this.service = fep.getProperty("-service");

	}

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(new TonicLookAndFeel());
			JFrame.setDefaultLookAndFeelDecorated(true);
			JDialog.setDefaultLookAndFeelDecorated(true);
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
			System.exit(0);
		}

		FEProperties fep = new FEProperties("rm.properties");

		PropertyConfigurator.configure("log.properties");

		try {
			RMGuiMain rmMain = new RMGuiMain(fep);
			rmMain.start_app();
			
			Console con = new Console(rmMain);
			con.start();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void start_app() throws NumberFormatException, Exception {
		this.log.info("Start App");

		final String version = fep.getProperty("-version");
		final String copyright = fep.getProperty("-copyright");

		/*
		 * SwingUtilities.invokeAndWait(new Runnable() {
		 * 
		 * @Override public void run() { // TODO Auto-generated method stub
		 * 
		 * } });
		 */
		RMGuiMain.this.rmMenuAction = new RMMenuAction(this);

		RMGuiMain.this.ieventDispatcher = rmMenuAction;
		String sportrmi = fep.getProperty("-portrmi");
		String sportlocalrmi = fep.getProperty("-portlocalrmi");
		String sport = fep.getProperty("-port");
		String ssvr = fep.getProperty("-server");
		String sheartbt = fep.getProperty("-heartbeat", "45000");

		Integer nportrmi = new Integer(sportrmi);
		Integer nportlocalrmi = new Integer(sportlocalrmi);
		Integer nport = new Integer(sport);
		Integer nheart = new Integer(sheartbt);

		String srmuid = fep.getProperty("rm.uid");
		String srmidatsvr = fep.getProperty("rm.idatsvr");
		String srmtype = fep.getProperty("rm.type");

		String surl = fep.getProperty("-url");
		String sdriver = fep.getProperty("-driver");
		String suid = fep.getProperty("-uid");
		String spwd = fep.getProperty("-pwd");

		rmService = new RiskManagementService(srmuid, srmidatsvr, srmtype,
				service, nportrmi, nportlocalrmi, ssvr, nport,
				Integer.parseInt(sheartbt), surl, sdriver, suid, spwd, fep,
				this.ieventDispatcher);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				MainApp mainGUI = new MainApp(service, version, copyright,
						rmMenuAction);
				rmService.start();
			}
		});
		
		Runtime.getRuntime().addShutdownHook(new Thread(){

			@Override
			public void run() {
				rmService.stop();
				rmService.shutdown();				
			}
			
			
		});
		
	}
	
	public RiskManagementService getService(){
		return rmService;
	}

	@Override
	public boolean runCommand(String scmd) {
		if(scmd.startsWith("send_tosvr")){
			
			String[] sp = scmd.split(" ");
			int countmsg = Integer.parseInt(sp[1]);
			int lengthmsg = Integer.parseInt(sp[2]);
			String[] dest = sp[3].split("#");

			StringBuffer sbf = new StringBuffer();
			for (int i = 0; i < lengthmsg; i++)
				sbf.append("a");

			String msg = sbf.toString();
			Message fmsg = new TestRequest();
			fmsg.setString(Text.FIELD, msg);
			// String sfmsg = fmsg.toString();
			fmsg.setString(tudDestUID.FIELD, dest[0]);
			fmsg.setString(tudDestIDAtsvr.FIELD, dest[1]);
			fmsg.setString(tudDestUType.FIELD, dest[2]);
			
			log.info("sending_data_to:"+dest[0]+"|"+dest[1]+"|"+dest[2]+" "+countmsg+" "+fmsg.toString().getBytes().length);

			for (int i = 0; i < countmsg; i++)
				rmService.getCom().send_appmsg("", (Message) fmsg.clone());
			
			return true;
		}
		return false;
	}

}
