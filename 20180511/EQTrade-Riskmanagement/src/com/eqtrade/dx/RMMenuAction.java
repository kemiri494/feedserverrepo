package com.eqtrade.dx;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import org.jdesktop.swingworker.SwingWorker;
import org.slf4j.LoggerFactory;

import com.eqtrade.dx.jonec.JonecPanelViewerUpdate;
import com.eqtrade.dx.jonec.MassCancelPanel;
import com.eqtrade.dx.main.qoute.ChangePasswordPanel;
import com.eqtrade.dx.main.qoute.SecurityIDPanel;
import com.eqtrade.dx.main.qoute.SubscriberPanel;
import com.eqtrade.dx.taker.OrderTakerViewer;
import com.eqtrade.dx.users.OrderPrintPanel;
import com.eqtrade.jonec.ABJonecService;

public class RMMenuAction extends MonitoringClientMenuAction {

	private org.slf4j.Logger log = LoggerFactory.getLogger(RMMenuAction.class);

	public final String JONEC_PANEL_VIEWER = "jonecViewer";
	public final String SECURITY_PANEL = "securityID";
	public final String CHANGE_PWD_PANEL = "changePwd";
	public final String SUBSCRIBER_PANEL = "Subscriber";
	public final String TAKER_PANEL = "OrderTakerPanel";
	public final String ORDER_PRINT_PANEL = "order.print.panel";
	public final String MASS_CANCEL = "order.masscancel";

	protected JMenuItem menuUser;
	protected JMenuItem menuJonec;
	protected JMenuItem menuSecurityID;
	protected JMenuItem menuChangePassword;
	protected JMenuItem menuSubscriber;
	protected JMenuItem menuTaker;
	protected JMenuItem menuOrderPrint;
	protected JMenuItem menuMassCancel;

	private RMGuiMain main;

	public RMMenuAction(RMGuiMain main) {
		this.main = main;
	}

	@Override
	protected void initComponents() {

		super.initComponents();

		addMainPanel(JONEC_PANEL_VIEWER, new JonecPanelViewerUpdate(
				"Jonec Connection", parent, this));
		addMainPanel(SECURITY_PANEL, new SecurityIDPanel("Security", parent,
				this));
		addMainPanel(CHANGE_PWD_PANEL, new ChangePasswordPanel(
				"Change Password", parent, this));
		addMainPanel(SUBSCRIBER_PANEL, new SubscriberPanel("Subscriber",
				parent, this));
		addMainPanel(TAKER_PANEL, new OrderTakerViewer("Order Taker Viewer",
				parent, this));
		addMainPanel(ORDER_PRINT_PANEL, new OrderPrintPanel(
				"Order Print Panel", parent, this, main.getService()));
		//addMainPanel(MASS_CANCEL, new MassCancelPanel(parent,
			//	(ABJonecService) main.getService().getJonecService()));
		addMainPanel(MASS_CANCEL, new MassCancelPanel(parent, this));

		addEvent(EVENT_SEC_LOAD_FINISHED, (Event) getMainPanel(SECURITY_PANEL));
		addEvent(EVENT_SEC_RECEIVED, (Event) getMainPanel(SECURITY_PANEL));
		addEvent(EVENT_BOARD_RECEIVE, (Event) getMainPanel(SECURITY_PANEL));
		addEvent(EVENT_JONEC_LOAD_FINISHED,
				(Event) getMainPanel(JONEC_PANEL_VIEWER));
		addEvent(EVENT_JONEC_LOAD_FINISHED2,
				(Event) getMainPanel(CHANGE_PWD_PANEL));
		addEvent(EVENT_JONEC_CONNECTED,
				(Event) getMainPanel(JONEC_PANEL_VIEWER));
		addEvent(EVENT_JONEC_DISCONNECTED,
				(Event) getMainPanel(JONEC_PANEL_VIEWER));
		addEvent(EVENT_JONEC_PWD_REPLY,
				(Event) getMainPanel(JONEC_PANEL_VIEWER));
		addEvent(EVENT_JONEC_LOADING, (Event) getMainPanel(JONEC_PANEL_VIEWER));
		addEvent(EVENT_SUBSCRIBE_LOAD, (Event) getMainPanel(SUBSCRIBER_PANEL));

		// addEvent(EVENT_SUBSCRIBE);

		addEvent(EVENT_JONEC_LOAD, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT JONEC LOAD");
				((RMListener) listener).jonecLoad();
			}
		});

		addEvent(EVENT_JONEC_CONNECTALL, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT JONEC CONNECTALL");

				final HashMap pr = params;
				SwingWorker swk = new SwingWorker() {

					@Override
					protected Object doInBackground() throws Exception {
						// TODO Auto-generated method stub

						((RMListener) listener).connectAll(pr);

						return null;
					}

				};
				swk.execute();

			}
		});

		addEvent(EVENT_LOAD_TAKER, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT LOAD TAKER");

				final HashMap pr = params;
				SwingWorker swk = new SwingWorker() {

					@Override
					protected Object doInBackground() throws Exception {
						// TODO Auto-generated method stub

						((RMListener) listener).load_setting4taker();

						return null;
					}

				};
				swk.execute();

			}
		});

		addEvent(EVENT_LOAD_TAKER_FINISHED, (Event) getMainPanel(TAKER_PANEL));

		addEvent(EVENT_JONEC_DISCONNECTALL, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT DISCONNECTALL");

				final HashMap pr = params;
				SwingWorker swk = new SwingWorker() {

					@Override
					protected Object doInBackground() throws Exception {
						// TODO Auto-generated method stub
						((RMListener) listener).disconnectAll(pr);

						return null;
					}

				};
				swk.execute();
			}
		});

		addEvent(EVENT_JONEC_CONNECT, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub

				log.info("EXECUTE EVENT JONEC CONNECT");

				final HashMap pr = params;
				SwingWorker swk = new SwingWorker() {

					@Override
					protected Object doInBackground() throws Exception {
						// TODO Auto-generated method stub

						((RMListener) listener).logon_tojones(pr);

						return null;
					}

				};
				swk.execute();

			}
		});
		addEvent(EVENT_JONEC_DISCONNECT, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub

				log.info("EXECUTE EVENT JONEC DISCONNECT");

				final HashMap pr = params;
				SwingWorker swk = new SwingWorker() {

					@Override
					protected Object doInBackground() throws Exception {
						// TODO Auto-generated method stub

						((RMListener) listener).logout_tojones(pr);

						return null;
					}

				};
				swk.execute();

			}
		});

		addEvent(EVENT_SEC_LOAD, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT SEC LOAD");

				((RMListener) listener).securitiesLoad(params);
			}
		});

		addEvent(EVENT_SEC_REQUEST, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT SEC REQUEST");

				((RMListener) listener).securitiesRequest(params);
			}
		});

		addEvent(EVENT_JONEC_CHANGE_PWD, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT CHANGE PWD");

				((RMListener) listener).changePwd(params);
			}
		});

		addEvent(EVENT_BOARD_REQUEST, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				log.info("EXECUTE EVENT BOARD REQUEST");

				((RMListener) listener).boardLoad();

			}
		});

		/*
		 * addEvent(EventDispatcher.EVENT_SUBSCRIBE_LOAD, new Event() {
		 * 
		 * @Override public void execute(Integer evt, HashMap params) { // TODO
		 * Auto-generated method stub log.info("EXECUTE EVENT SUBSCRIBE LOAD");
		 * 
		 * ((JonecListener) listener).subscribeLoad(null); } });
		 */

		addEvent(EventDispatcher.EVENT_SUBSCRIBE, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				// log.info("EXECUTE EVENT JONEC LOAD");
				log.info("EXECUTE EVENT SUBSCRIBE");

				((JonecListener) listener).subscribeUnsubscribe(params);
			}
		});

		addEvent(EventDispatcher.EVENT_SEND_CHANGE_PASSWORD, new Event() {

			@Override
			public void execute(Integer evt, HashMap params) {
				// TODO Auto-generated method stub
				((JonecListener) listener).changePwd(params);
			}
		});

	}

	@Override
	public void build() {
		// TODO Auto-generated method stub

		super.build();
		menuUser = new JMenuItem();
		menuSubscriber = new JMenuItem();
		menuUser.setText("User List Panel");
		menuUser.setName("userPanel");
		menuUser.addActionListener(this);

		menuOrderPrint = new JMenuItem();
		menuOrderPrint.setText("Order Print");
		menuOrderPrint.setName(ORDER_PRINT_PANEL);
		menuOrderPrint.addActionListener(this);

		menuJonec = new JMenuItem();
		menuJonec.setText("Jonec Connection Viewer");
		menuJonec.setName("jonecViewer");
		menuJonec.addActionListener(this);

		menuSecurityID = new JMenuItem("Security ID");
		menuSecurityID.setName("securityID");
		menuSecurityID.addActionListener(this);

		menuChangePassword = new JMenuItem("Change Password");
		menuChangePassword.setName(CHANGE_PWD_PANEL);
		menuChangePassword.addActionListener(this);

		menuTaker = new JMenuItem("Taker Viewer");
		menuTaker.setName(TAKER_PANEL);
		menuTaker.addActionListener(this);

		menuSubscriber.setText("Subscriber");
		menuSubscriber.setName("Subscriber");
		menuSubscriber.addActionListener(this);

		menuMassCancel = new JMenuItem("Mass Cancel Request");
		menuMassCancel.setText("Masscancel");
		menuMassCancel.setName(MASS_CANCEL);
		menuMassCancel.addActionListener(this);

		menuFile.add(menuUser);
		menuFile.add(menuChangePassword);
		menuView.add(menuJonec);
		menuView.add(menuSecurityID);
		menuView.add(menuSubscriber);
		menuView.add(menuTaker);
		menuView.add(menuOrderPrint);
		menuView.add(menuMassCancel);
	}

}
