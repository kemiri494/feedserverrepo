package com.eqtrade.dx.taker;

public class TakerData {
	public String userSource;
	public String userDestination;

	public TakerData(String userSource, String userDestination) {
		super();
		this.userSource = userSource;
		this.userDestination = userDestination;
	}

	public String getUserSource() {
		return userSource;
	}

	public void setUserSource(String userSource) {
		this.userSource = userSource;
	}

	public String getUserDestination() {
		return userDestination;
	}

	public void setUserDestination(String userDestination) {
		this.userDestination = userDestination;
	}

}
