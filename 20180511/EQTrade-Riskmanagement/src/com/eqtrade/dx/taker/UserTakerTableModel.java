package com.eqtrade.dx.taker;

import com.eqtrade.dx.AbstractListTableModel;

public class UserTakerTableModel extends AbstractListTableModel {

	public UserTakerTableModel(String[] headers) {
		super(headers);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		TakerData td = (TakerData) items.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return td.getUserSource();
		case 1:
			return td.getUserDestination();
		default:
			break;
		}

		return "";
	}

	@Override
	public void loadList() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object obj) {
		// TODO Auto-generated method stub

	}

}
