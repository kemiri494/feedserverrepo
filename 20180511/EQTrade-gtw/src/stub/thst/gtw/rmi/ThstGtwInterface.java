package thst.gtw.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

public interface ThstGtwInterface extends Remote{
    public abstract byte[] getExchange(long l) throws RemoteException;
    public abstract byte[] getBoard(long l) throws RemoteException;
    public abstract byte[] getInvType(long l) throws RemoteException;
    public abstract byte[] getCustType(long l) throws RemoteException;
    public abstract byte[] getAccType(long l) throws RemoteException;
    public abstract byte[] getStatusAcc(long l) throws RemoteException;
    public abstract byte[] getUserType(long l) throws RemoteException;
    public abstract byte[] getSecurities(long l) throws RemoteException;
    public abstract byte[] getAccTypeSec(long l) throws RemoteException;
    public abstract byte[] getStatusOrder(long l) throws RemoteException;
    public abstract byte[] getUserProfile(long l, String s) throws RemoteException;
    public abstract byte[] getOrder(long l, String s, String s1) throws RemoteException;
    public abstract byte[] getAts(long l,String s,String s1) throws RemoteException;// ats
    public abstract byte[] getAtsBrowse(long l) throws RemoteException;// browse order
    public abstract byte[] getSecexboard(long l) throws RemoteException;
    public abstract byte[] getMatchOrder(long l, String s) throws RemoteException;
    public abstract byte[] getPortfolio(long l, String s, String s1) throws RemoteException;
    public abstract byte[] getAccount(long l, String s) throws RemoteException;
    public abstract boolean createOrder(long l, byte abyte0[]) throws RemoteException;   
    public abstract boolean sendTemp(long l, byte abyte0[]) throws RemoteException;
    public abstract boolean entryWithdraw(long l, byte[] bmsg) throws RemoteException;
    public abstract boolean entryAmend(long l, byte abyte0[]) throws RemoteException;
    public abstract boolean notification(long l, byte byte0[]) throws RemoteException;
    public abstract boolean createAts(long l, byte abyte0[]) throws RemoteException; // create ats
    public abstract boolean atsWithdraw(long l,byte[] bmsg) throws RemoteException;// ats withdraw
    public abstract byte[] getNegDeal(long l) throws RemoteException;
    public abstract long login(String s, String s1) throws RemoteException;
    public abstract long loginmobile(String s, String s1,String imei, String platform) throws RemoteException;
    public abstract long getServerTime(long l) throws RemoteException;
    public abstract boolean chgPwd(long l, String s, String s1) throws RemoteException;
    public abstract byte[] getCurrentMsg(long l) throws RemoteException;
    public abstract boolean logout(long l) throws RemoteException;
    boolean send_fixmsg(long session, byte[] mbyte) throws RemoteException;
    public abstract byte[] getCashColl(long session,String tradingid) throws RemoteException;
    public abstract byte[] getBuySellDetail(long session,String tradingid, String accountid) throws RemoteException;//buysell..
    public abstract byte[] getDueDate(long session,String tradingid) throws RemoteException;//DueDate
    public abstract byte[] getBackNotif(long session) throws RemoteException;
    public abstract byte[] getAnnouncement(long l) throws RemoteException;
    public abstract byte[] getBroker(long session) throws RemoteException;
    public abstract byte[] getSecExchBoard(long sessionid) throws RemoteException;
    public abstract byte[] entrySplitDone(long sessionid,byte[] requestsplit) throws RemoteException;      
    public abstract boolean checkpin(long sessionid,String userid,String pin) throws RemoteException;
    public abstract boolean changepin(long sessionid, String userid, String newPIN) throws RemoteException;
    public abstract Vector chgpwd(long l, String s, String s1) throws RemoteException;
}