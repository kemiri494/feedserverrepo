package thst.gtw.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIGatewayInterface extends Remote {

	long register(String suid) throws RemoteException;
	boolean send_fixmsg(long session, String msg) throws RemoteException;
	byte[]	get_fixmsg(long session) throws RemoteException;
	
}