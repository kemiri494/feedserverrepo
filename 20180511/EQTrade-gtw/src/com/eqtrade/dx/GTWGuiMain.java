package com.eqtrade.dx;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.PropertyConfigurator;

import com.digitprop.tonic.TonicLookAndFeel;
import com.eqtrade.gtw.MessageGtwService;
import com.eqtrade.utils.properties.FEProperties;

public class GTWGuiMain {

	private MessageGtwService gtwService;
	private MainWindow clientWindow;
	private String service;
	private FEProperties fep;

	public GTWGuiMain(FEProperties fep) throws Exception {

		this.service = service;
		this.fep = fep;

	}

	public void start_app() throws Exception {

		String srmuid = fep.getProperty("rm.uid");
		String srmidatsvr = fep.getProperty("rm.idatsvr");
		String srmtype = fep.getProperty("rm.type");

		String sgtwuid = fep.getProperty("gtw.uid");
		String sgtwidatsvr = fep.getProperty("gtw.idatsvr");
		String sgtwtype = fep.getProperty("gtw.type");

		String atsidatsvr = fep.getProperty("ats.idatsvr");
		String atstype = fep.getProperty("ats.type");
		String atsuid = fep.getProperty("ats.uid");
		
		String ssessionsleep = fep.getProperty("session.sleep");
		String ssessionmaxidle = fep.getProperty("session.nmaxidle");
		String smaxsession = fep.getProperty("session.nmaxuser");

		int nsleep = new Integer(ssessionsleep);
		int nmaxidle = new Integer(ssessionmaxidle);
		int nmaxsession = new Integer(smaxsession);

		String service = fep.getProperty("-service");
		String sportrmi = fep.getProperty("-portrmi");
		String sportlocalrmi = fep.getProperty("-portlocalrmi");
		String sport = fep.getProperty("-port");
		String ssvr = fep.getProperty("-server");
		String sheartbt = fep.getProperty("-heartbeat", "10");

		Integer nportrmi = new Integer(sportrmi);
		Integer nportlocalrmi = new Integer(sportlocalrmi);
		Integer nport = new Integer(sport);
		Integer nheart = new Integer(sheartbt);

		// for db - properties
		String surl = fep.getProperty("-url");
		String sdriver = fep.getProperty("-driver");
		String suid = fep.getProperty("-uid");
		String spwd = fep.getProperty("-pwd");
		String title = fep.getProperty("-title", "Gateway");
		String version = fep.getProperty("-version", "1.0.0");
		String copyright = fep.getProperty("-copyright",
				"Innovasi Andalan Mandiri");

		MonitoringClientMenuAction menuAction = new MonitoringClientMenuAction();
		gtwService = new MessageGtwService(service, ssvr, nport, Integer
				.parseInt(fep.get("heartbeat")), nportrmi, nportlocalrmi,
				srmuid, srmidatsvr, srmtype, sgtwuid, sgtwidatsvr, sgtwtype,
				nsleep, nmaxidle, nmaxsession, surl, sdriver, suid, spwd, fep,
				menuAction, atsidatsvr, atstype, atsuid);

		MainApp mainGUI = new MainApp(title, version, copyright, menuAction);
		gtwService.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(){

			@Override
			public void run() {
				gtwService.stop();
			}
			
		});
	}

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(new TonicLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		FEProperties fep = new FEProperties("gtw.properties");
		PropertyConfigurator.configure("log.properties");

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {

			GTWGuiMain gtwMain = new GTWGuiMain(fep);

			gtwMain.start_app();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
