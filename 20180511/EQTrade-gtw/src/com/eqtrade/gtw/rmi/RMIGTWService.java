package com.eqtrade.gtw.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import thst.gtw.rmi.RMIGatewayInterface;

import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.gtw.ThreadRMSession;
import com.eqtrade.gtw.msg.MsgManagerInterface;
import com.eqtrade.utils.Function;

public class RMIGTWService extends UnicastRemoteObject implements
		RMIGatewayInterface {

	// private static Logger logger = LoggerFactory.getLogger("RMIService");
	private Logger logger = LoggerFactory.getLogger(getClass());

	private static final long serialVersionUID = 421205440812741225L;

	private Vector<String> vreject;
	protected MsgManagerInterface mmi;
	private RMICleaning rcln;
	private Vector<Long> vrmsession;
	private ThreadRMSession thsession;
	protected MessageGtwServiceInterface mgtw;

	public RMIGTWService(int port, int nsleep, int nmaxidle,
			Vector<String> vreject, MsgManagerInterface mmi,
			Vector<Long> vrmsession, ThreadRMSession thsession,
			MessageGtwServiceInterface mgtw) throws RemoteException {
		super(port);
		this.vreject = vreject;
		this.mmi = mmi;
		this.vrmsession = vrmsession;
		this.thsession = thsession;
		this.mgtw = mgtw;
		mmi.set_session(hsession);

		rcln = new RMICleaning(hsession, nsleep, vreject, nmaxidle, this.mgtw);
		rcln.start();
		Function.print_pair("RmiCleaning thread ...started");
	}

	protected Hashtable<Long, RMISessionCounter> hsession = new Hashtable<Long, RMISessionCounter>(
			10);

	private Hashtable<Long, RMISessionCounter> htmp = new Hashtable<Long, RMISessionCounter>(
			10);

	@Override
	public byte[] get_fixmsg(long session) throws RemoteException {
		byte[] bresult = null;
		Vector<String> vresult;

		RMISessionCounter rsc = hsession.get(session);
		if (rsc == null) {
			rsc = htmp.remove(session);
		} else {
			rsc.reset_counter();
		}

		if (rsc == null) {
			logger.warn("Sessionid not found for : " + session);
			throw new NullPointerException("Sessionid not found");
		}

		Vector<String> vmsgses = this.mmi.get_msg(session);

		String suid = rsc.get_uid();
		Vector<String> vmsg = this.mmi.get_msg(suid);

		if (vmsgses != null) {
			if (vmsg == null)
				vresult = vmsgses;
			else {
				Vector<String> vtemp = new Vector<String>(100, 50);
				vtemp.addAll(vmsgses);
				vtemp.addAll(vmsg);
				vresult = vtemp;
			}
		} else {
			vresult = vmsg;
		}

		if (vresult != null) {

			JSONArray json = new JSONArray(vresult);
			// logger.info("get message from client:" + vresult);

			bresult = Function.compress(json.toString().getBytes());

			logger.info("getMSG:" + suid + ":" + vresult.size());

			// logger.info("Sessionid : " + session + "suid, vresult: " + suid
			// + "," + vresult.toString());
		} else {
			// logger.error("vresult is null for "+suid+" "+session);
		}

		return bresult;
	}

	public Hashtable<Long, RMISessionCounter> getHtmp() {
		return htmp;
	}

	public Hashtable<Long, RMISessionCounter> getHsession() {
		return hsession;
	}

	@Override
	public long register(String suid) throws RemoteException {
		long nresult = 0;
		String stemp;

		if (vrmsession.size() > 0) {
			nresult = vrmsession.remove(0);

			if (!vreject.contains(suid)) {
				vreject.addElement(suid);
			}

			stemp = nresult + "";
			if (stemp.length() > 6) {
				stemp = stemp.substring(stemp.length() - 6, stemp.length());
				nresult = new Long(stemp);
			}

			if (vrmsession.size() <= 10) {
				thsession.ask_sessionlist();
			}
		} else {
			thsession.ask_sessionlist();
		}

		logger.warn("suid:" + suid + ":" + nresult);System.out.println("suid:" + suid + ":" + nresult);
		
		if (nresult <= 0) {
			throw new RemoteException(" session exception ");
		} else {
			hsession.put(nresult, new RMISessionCounter(suid, nresult));
		}
		return nresult;
	}

	@Override
	public boolean send_fixmsg(long session, String msg) throws RemoteException {
		boolean bresult = false;

		if (hsession.containsKey(session)) {
			// logger.warn("session : " + session + " msg : " + msg);
			this.mmi.add_incmsgfromclient(session, msg);
			bresult = true;
			RMISessionCounter rsc = hsession.get(session);
			if (rsc != null)
				rsc.reset_counter();
		}

		return bresult;
	}

	/*
	 * public void clear_vrejectandsession() {
	 * 
	 * logger.error(""); vreject.clear(); hsession.clear();
	 * 
	 * }
	 */

	public boolean delete_session(String suid, boolean issendtorm) {
		Vector<Long> vkeys = new Vector<Long>(10, 5);
		Long lng;
		boolean bresult = false;

		Enumeration<Long> e = hsession.keys();
		while (e.hasMoreElements()) {
			lng = e.nextElement();
			if (hsession.get(lng).get_uid().equals(suid)) {
				vkeys.addElement(lng);
			}
		}

		RMISessionCounter rsc;
		for (Long l : vkeys) {
			rsc = hsession.remove(l);
			bresult = true;
			htmp.put(l, rsc);

			if (bresult) {
				HashMap params = new HashMap();
				params.put(0, l);
				mgtw.eventDispather().pushEvent(
						EventDispatcher.EVENT_SESSION_LOGOUT, params);
			}
		}

		vreject.remove(suid);
		logger.info("clear session : " + suid);

		// send message to rm
		if (thsession != null && issendtorm)
			thsession.cleaning_sessionto_rm(suid);

		return bresult;
	}

	public int get_numberofsession() {

		return hsession.size();

	}

	public boolean delete_session(String suid, String sessiongtw) {
		boolean bresult = false;

		long sessionid = new Long(sessiongtw);
		RMISessionCounter rsc = hsession.remove(sessionid);
		htmp.put(sessionid, rsc);

		return bresult;
	}

	public long get_validsessionid(String suid) {
		long nresult = -1;

		try {
			long ses;
			RMISessionCounter rsc;

			Enumeration<Long> e = hsession.keys();
			while (e.hasMoreElements()) {
				ses = e.nextElement();
				rsc = hsession.get(ses);
				if (rsc.get_uid().equalsIgnoreCase(suid)) {
					nresult = ses;
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return nresult;
	}

}
