package com.eqtrade.gtw.rmi;

public final class RMISessionCounter extends Object {

	private String suid;
	private int counter = 0;
	private long sessionid;
	
	public RMISessionCounter(String auid,long sessionid) {

		this.sessionid = sessionid;
		this.suid = auid;
	}

	public String get_uid() {

		return this.suid;
	}
	
	protected synchronized int get_counter() {

		return counter;
	}

	protected synchronized void set_counter() {

		this.counter++;
	}

	public synchronized void reset_counter() {

		counter = 0;
	}
}