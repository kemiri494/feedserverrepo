package com.eqtrade.gtw.rmi;

import java.rmi.RemoteException;
import java.sql.SQLData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONString;
import org.slf4j.LoggerFactory;

import thst.gtw.rmi.ThstGtwInterface;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryData.SQUERY;
import com.eqtrade.database.model.SplitDone;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.gtw.ThreadRMSession;
import com.eqtrade.gtw.msg.MappingUserID;
import com.eqtrade.gtw.msg.MsgManagerInterface;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.MsgType;
import com.eqtrade.quickfix.resources.field.Text;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.quickfix.resources.field.tudSrcIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudSrcUID;
import com.eqtrade.quickfix.resources.field.tudSrcUType;
import com.eqtrade.quickfix.resources.field.tudStartSession;
import com.eqtrade.quickfix.resources.field.tudUserID;
import com.eqtrade.utils.Function;

import eqtrade.trading.core.Utils;

public class RMIGtwServiceUpdate extends RMIGTWService implements
		ThstGtwInterface {

	private org.slf4j.Logger log = LoggerFactory.getLogger(super.getClass());
	private AccessQueryLocator accessData;
	private MappingUserID muid;
	private SimpleDateFormat sdfformattime = new SimpleDateFormat(
			"yyyyMMdd-HH:mm:ss");

	public RMIGtwServiceUpdate(int port, int nsleep, int nmaxidle,
			Vector<String> vreject, MsgManagerInterface mmi,
			Vector<Long> vrmsession, ThreadRMSession thsession,
			AccessQueryLocator acc, MessageGtwServiceInterface mgtw,
			MappingUserID _muid) throws RemoteException {
		super(port, nsleep, nmaxidle, vreject, mmi, vrmsession, thsession, mgtw);
		this.muid = _muid;
		// System.out.println("set muid:" + muid.get_gtwidatsvr());
		// TODO Auto-generated constructor stub
		this.accessData = acc;
	}

	@Override
	public boolean chgPwd(long l, String s, String s1) throws RemoteException {
		// TODO Auto-generated method stub

		if (!hsession.containsKey(l)) {
			throw new NullPointerException("session id not found");
		}

		FOData fodata = new FOData();
		fodata.set_string(s, s1);

		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.changepwd.val,
					fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			e.printStackTrace();
		}

		if (v == null)
			return false;

		return true;
	}
	
	@Override
	public Vector chgpwd(long l, String s, String s1) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException("session id not found");
		}

		FOData fodata = new FOData();
		fodata.set_string(s, s1);
		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.chgpwd.val,
					fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			e.printStackTrace();
		}

		return v;
	}
	
	@Override
	public boolean createOrder(long l, byte[] abyte0) throws RemoteException {
		// TODO Auto-generated method stub

		return send_fixmsg(l, abyte0);
	}

	@Override
	public boolean send_fixmsg(long l, byte[] abyte) throws RemoteException {

		String dec = new StringBuffer(new String(Utils.decompress(abyte)))
				.toString();

		if (dec != null) {
			return send_fixmsg(l, dec);

		}

		return false;
	}

	public boolean send_fixmsg(long session, String msg) throws RemoteException {
		return super.send_fixmsg(session, msg);
	}

	@Override
	public boolean createAts(long l, byte[] abyte0) throws RemoteException {
		// TODO Auto-generated method stub
		return send_fixmsg(l, abyte0);
	}

	@Override
	public boolean entryAmend(long l, byte[] abyte0) throws RemoteException {
		// TODO Auto-generated method stub

		return send_fixmsg(l, abyte0);
	}

	@Override
	public boolean entryWithdraw(long l, byte[] bmsg) throws RemoteException {
		// TODO Auto-generated method stub
		return send_fixmsg(l, bmsg);
	}

	@Override
	public boolean atsWithdraw(long l, byte[] bmsg) throws RemoteException {
		// TODO Auto-generated method stub
		return send_fixmsg(l, bmsg);
	}

	@Override
	public byte[] getAccType(long l) throws RemoteException {
		// TODO Auto-generated method stub

		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		byte[] bresult = queryData(SQUERY.acctype.val, null);
		return bresult;

	}

	@Override
	public byte[] getAccTypeSec(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		return queryData(SQUERY.accTypeSec.val, null);
	}

	@Override
	public byte[] getAccount(long l, String tradingid) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		RMISessionCounter ses = hsession.get(l);

		String userid = ses.get_uid();

		FOData fodata = new FOData();
		fodata.set_string(userid, tradingid);

		return queryData(SQUERY.accountlist.val, fodata);
	}

	@Override
	public byte[] getBoard(long l) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.board.val, null);
	}

	@Override
	public byte[] getCurrentMsg(long l) throws RemoteException {
		// TODO Auto-generated method stub
		return get_fixmsg(l);
	}

	@Override
	public byte[] getCustType(long l) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.custtype.val, null);
	}

	@Override
	public byte[] getExchange(long l) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.exchange.val, null);
	}

	@Override
	public byte[] getInvType(long l) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.invtype.val, null);
	}

	@Override
	public byte[] getMatchOrder(long l, String orderid) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(l)) {
			log.info("session id not found");
			throw new NullPointerException("session id not found");
		}

		RMISessionCounter ses = hsession.get(l);
		String userid = ses.get_uid();
		// log.info("get userid:" + ses.get_uid());

		FOData fodata = new FOData();
		fodata.set_string(userid, orderid);

		return queryData(SQUERY.matchlist.val, fodata);
	}

	@Override
	public byte[] getNegDeal(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.negdeallist.val, null);

	}

	@Override
	public byte[] getOrder(long l, String tradingid, String orderid)
			throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		RMISessionCounter ses = hsession.get(l);
		String userid = ses.get_uid();
		FOData fodata = new FOData();
		fodata.set_string(userid, tradingid, orderid);

		return queryData(SQUERY.orderlist.val, fodata);
	}

	@Override
	public byte[] getAts(long l, String tradingid, String orderid)
			throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		log.info("get ats");
		RMISessionCounter ses = hsession.get(l);
		String userid = ses.get_uid();
		FOData fodata = new FOData();
		fodata.set_string(userid, tradingid, orderid);
		return queryData(SQUERY.atslist.val, fodata);
	}

	@Override
	public byte[] getAtsBrowse(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		RMISessionCounter ses = hsession.get(l);
		String userid = ses.get_uid();
		FOData fodata = new FOData();
		fodata.set_string(userid);
		return queryData(SQUERY.browseorder.val, fodata);
	}

	@Override
	public byte[] getPortfolio(long l, String tradingid, String securutiesid)
			throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		log.info("get portfolio");
		RMISessionCounter ses = hsession.get(l);
		String userid = ses.get_uid();
		FOData fodata = new FOData();
		fodata.set_string(userid, tradingid);
		return queryData(SQUERY.portfolio.val, fodata);
	}

	@Override
	public byte[] getSecurities(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.securities.val, null);
	}

	@Override
	public long getServerTime(long l) throws RemoteException {
		// TODO Auto-generated method stub
		return new Date().getTime();
	}

	@Override
	public byte[] getStatusAcc(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.statusacc.val, null);
	}

	@Override
	public byte[] getStatusOrder(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.orderStatus.val, null);
	}

	@Override
	public byte[] getUserProfile(long l, String userid) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		RMISessionCounter rm = hsession.get(l);

		if (!rm.get_uid().equals(userid))
			throw new RemoteException("user id is not login");

		FOData fodata = new FOData();
		fodata.set_string(userid);

		return queryData(SQUERY.userProfile.val, fodata);
	}

	@Override
	public byte[] getUserType(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}

		return queryData(SQUERY.userttype.val, null);
	}

	private String ssessionid = null;

	@Override
	public long login(String suid, String spwd) throws RemoteException {
		// TODO Auto-generated method stub

		long sessionid = mgtw.get_validsessionid(suid);

		FOData fodata = new FOData();

		fodata.set_string(suid, spwd);
		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.logonfeed.val,
					fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			e.printStackTrace();
		}

		String stime, scclogon = null;
		String stext = "reply";
		String smsg = null;

		boolean bnumofuser = mgtw.is_under_maxsession_allowed();

		int psukses = (Integer) v.elementAt(0);

		Message message = new Message();
		if ((psukses == 1 && bnumofuser)) {
			// String sutype = "sales";
			sessionid = register(suid);
			message = new Message();
			message.getHeader()
					.setString(MsgType.FIELD, MsgType.hedLogonClient);
			message.setString(tudStartSession.FIELD, sessionid + "");

		} else {
			stext = !bnumofuser ? "server busy " : "invalid userid";


			sessionid = -1;
			message = new Message();
			message.getHeader().setString(MsgType.FIELD, MsgType.hedInvalidPwd);
		}
		message.setString(tudUserID.FIELD, suid);

		message.setString(tudSrcUID.FIELD, muid.get_gtwuid());
		message.setString(tudSrcIDAtsvr.FIELD, muid.get_gtwidatsvr());
		message.setString(tudSrcUType.FIELD, muid.get_gtwtype());
		message.setString(tudDestIDAtsvr.FIELD, muid.get_rmidatsvr());
		message.setString(tudDestUID.FIELD, muid.get_rmuid());
		message.setString(tudDestUType.FIELD, muid.get_rmtype());

		if (ssessionid == null) {

			ssessionid = mgtw
					.getEngine()
					.getCm()
					.get_sessionid(muid.get_gtwuid(), muid.get_gtwidatsvr(),
							muid.get_gtwtype());
		}

		if (ssessionid != null && sessionid > 0) {
			mgtw.getEngine().send_appmsg(ssessionid, message);
			System.out.println("send login to ats server ");
			// send login to ats server yosep 22032017
			Message mess = (Message) message.clone();
			mess.setString(tudDestIDAtsvr.FIELD, muid.get_Atsidatsvr());
			mess.setString(tudDestUID.FIELD, muid.get_Atsuid());
			mess.setString(tudDestUType.FIELD, muid.get_Atstype());
			mgtw.getEngine().send_appmsg(ssessionid, mess);
			
		}
		return sessionid;
	}
	
	@Override
	public long loginmobile(String suid, String spwd,String imei, String platform) throws RemoteException {
		// TODO Auto-generated method stub

		long sessionid = mgtw.get_validsessionid(suid);

		FOData fodata = new FOData();

		fodata.set_string(suid, spwd,imei,platform);
		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.logonfeedmobile.val,
					fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			log.info(e.getMessage());
			e.printStackTrace();
		}

		String stime, scclogon = null;
		String stext = "reply";
		String smsg = null;

		boolean bnumofuser = mgtw.is_under_maxsession_allowed();

		int psukses = (Integer) v.elementAt(0);

		Message message = new Message();
		if ((psukses == 1 && bnumofuser)) {
			// String sutype = "sales";
			sessionid = register(suid);
			message = new Message();
			System.out.println(sessionid+" result loginmobile");
			message.getHeader()
					.setString(MsgType.FIELD, MsgType.hedLogonClient);
			message.setString(tudStartSession.FIELD, sessionid + "");

		} else {
			stext = !bnumofuser ? "server busy " : "invalid userid";


			sessionid = -1;
			message = new Message();
			message.getHeader().setString(MsgType.FIELD, MsgType.hedInvalidPwd);
		}
		message.setString(tudUserID.FIELD, suid);

		message.setString(tudSrcUID.FIELD, muid.get_gtwuid());
		message.setString(tudSrcIDAtsvr.FIELD, muid.get_gtwidatsvr());
		message.setString(tudSrcUType.FIELD, muid.get_gtwtype());
		message.setString(tudDestIDAtsvr.FIELD, muid.get_rmidatsvr());
		message.setString(tudDestUID.FIELD, muid.get_rmuid());
		message.setString(tudDestUType.FIELD, muid.get_rmtype());

		if (ssessionid == null) {

			ssessionid = mgtw
					.getEngine()
					.getCm()
					.get_sessionid(muid.get_gtwuid(), muid.get_gtwidatsvr(),
							muid.get_gtwtype());
		}

		if (ssessionid != null && sessionid > 0) {
			mgtw.getEngine().send_appmsg(ssessionid, message);
		}
		return sessionid;
	}

	//#Valdhy 20141021
	public Vector<Object> Vlogin(String suid, String spwd) throws RemoteException {

		Vector<Object> Vlogin = new Vector<Object>();
		long sessionid = mgtw.get_validsessionid(suid);

		FOData fodata = new FOData();

		fodata.set_string(suid, spwd);
		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.logonfeed.val,
					fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			e.printStackTrace();
		}

		String stime, scclogon = null;
		String stext = "reply";
		String smsg = null;

		boolean bnumofuser = mgtw.is_under_maxsession_allowed();

		int psukses = (Integer) v.elementAt(0);
		String pmsg = (String) v.elementAt(1);
		String pnext_action = (String) v.elementAt(2);

		Message message = new Message();
		if ((psukses == 1 && bnumofuser)) {
			// String sutype = "sales";
			sessionid = register(suid);
			message = new Message();

			message.getHeader()
					.setString(MsgType.FIELD, MsgType.hedLogonClient);
			message.setString(tudStartSession.FIELD, sessionid + "");

		} else {
			stext = !bnumofuser ? "server busy " : pmsg;
			
			sessionid = -1;
			message = new Message();
			message.getHeader().setString(MsgType.FIELD, MsgType.hedInvalidPwd);
		}
		message.setString(tudUserID.FIELD, suid);

		message.setString(tudSrcUID.FIELD, muid.get_gtwuid());
		message.setString(tudSrcIDAtsvr.FIELD, muid.get_gtwidatsvr());
		message.setString(tudSrcUType.FIELD, muid.get_gtwtype());
		message.setString(tudDestIDAtsvr.FIELD, muid.get_rmidatsvr());
		message.setString(tudDestUID.FIELD, muid.get_rmuid());
		message.setString(tudDestUType.FIELD, muid.get_rmtype());

		if (ssessionid == null) {

			ssessionid = mgtw
					.getEngine()
					.getCm()
					.get_sessionid(muid.get_gtwuid(), muid.get_gtwidatsvr(),
							muid.get_gtwtype());
		}

		if (ssessionid != null && sessionid > 0) {
			mgtw.getEngine().send_appmsg(ssessionid, message);
		}
		
		Vlogin.add(sessionid);
		Vlogin.add(pmsg);
		Vlogin.add(pnext_action);

		return Vlogin;
	}

	@Override
	public boolean logout(long l) throws RemoteException {
		// TODO Auto-generated method stub
		boolean bresult = false;
		RMISessionCounter cl = hsession.get(l);

		if (cl != null) {

			String suid = cl.get_uid();

			bresult = delete_session(suid, true);
			HashMap params = new HashMap();
			params.put(0, l);

			mmi.getGtwService().eventDispather()
					.pushEvent(EventDispatcher.EVENT_SESSION_LOGOUT, params);

		}

		return bresult;
	}

	public byte[] queryData(int queryData, FOData fodata)
			throws RemoteException {

		Vector<Object> v;
		try {
			v = accessData.getQueryData().set_insert(queryData, fodata);

			Vector vs = new Vector();
			for (Object obj : v) {
				JSONString js = (JSONString) obj;
				if (js != null) {
					JSONObject jobj = new JSONObject(js.toJSONString());
					vs.add(jobj);
				}
			}

			JSONArray jsonarray = new JSONArray(vs);
			String json = jsonarray.toString();
			// System.out.println(json);

			return Function.compress(json.getBytes());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RemoteException(e.getMessage());
		}

	}

	public String queryDataNotCompress(int queryData, FOData fodata)
			throws RemoteException {

		Vector<Object> v;
		try {
			v = accessData.getQueryData().set_insert(queryData, fodata);
			
//			log.info("querydata "+v.size());

			Vector vs = new Vector();
			for (Object obj : v) {
				JSONString js = (JSONString) obj;
				if (js != null) {
					JSONObject jobj = new JSONObject(js.toJSONString());
					vs.add(jobj);
				}
			}

			JSONArray jsonarray = new JSONArray(vs);
			String json = jsonarray.toString();
//			 System.out.println(json);

			return json;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RemoteException(e.getMessage());
		}

	}
	
	public RMISessionCounter isValidClient(long l){
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		
		return hsession.get(l);
	}

	private byte[] orderData(int queryData, FOData fodata)
			throws RemoteException {
		Vector<Object> v;
		try {
			v = accessData.getOrderData().set_insert(queryData, fodata);

			Vector vs = new Vector();
			for (Object obj : v) {
				JSONString js = (JSONString) obj;
				if (js != null) {
					JSONObject jobj = new JSONObject(js.toJSONString());
					vs.add(jobj);
				}
			}

			JSONArray jsonarray = new JSONArray(vs);
			String json = jsonarray.toString();
			// System.out.println(json);

			return Function.compress(json.getBytes());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RemoteException();
		}
	}

	@Override
	public boolean sendTemp(long l, byte[] abyte0) throws RemoteException {
		// TODO Auto-generated method stub
		return send_fixmsg(l, abyte0);
	}

	public static void main(String[] args) {

		try {
			AccessQueryLocator ac = new AccessQueryLocator(
					"oracle.dbpool.propeties");
			Vector<Object> vacc;
			vacc = ac.getQueryData().set_insert(SQUERY.acctype.val,
					new FOData());
			JSONArray json = new JSONArray(vacc);

			System.out.println(json.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public byte[] getBroker(long session) throws RemoteException {
		// TODO Auto-generated method stub
		if (!hsession.containsKey(session)) {
			throw new NullPointerException("Session Id Not Found");
		}
		return queryData(SQUERY.broker.val, null);
	}

	@Override
	public byte[] getCashColl(long session, String tradingid)
			throws RemoteException {
		if (!hsession.containsKey(session)) {
			throw new NullPointerException("Session Id Not Found");
		}

		RMISessionCounter cl = hsession.get(session);

		if (cl == null) {
			throw new NullPointerException("User Name Not Found");

		}

		String suid = cl.get_uid();

		FOData fodata = new FOData();
		fodata.set_string(suid, tradingid);
		return queryData(SQUERY.cashcoll.val, fodata);
	}

	@Override
	public byte[] getBuySellDetail(long session, String accid, String buyorsell)
			throws RemoteException {
		if (!hsession.containsKey(session)) {
			throw new NullPointerException("Session Id Not Found");
		}

		RMISessionCounter cl = hsession.get(session);

		if (cl == null) {
			throw new NullPointerException("User Name Not Found");

		}

		String suid = cl.get_uid();

		FOData fodata = new FOData();
		fodata.set_string(suid, accid, buyorsell);

		return queryData(SQUERY.buySellDetail.val, fodata);
	}

	// DueDate
	@Override
	public byte[] getDueDate(long session, String tradingid)
			throws RemoteException {
		if (!hsession.containsKey(session)) {
			throw new NullPointerException("Session Id Not Found");
		}

		RMISessionCounter cl = hsession.get(session);

		if (cl == null) {
			throw new NullPointerException("User Name Not Found");

		}

		String suid = cl.get_uid();

		FOData fodata = new FOData();
		fodata.set_string(suid, tradingid);

		return queryData(SQUERY.duedate.val, fodata);
	}

	@Override
	public byte[] getSecExchBoard(long sessionid) throws RemoteException {
		if (!hsession.containsKey(sessionid)) {
			throw new NullPointerException("Session Id Not Found");
		}
		return queryData(SQUERY.secexchboard.val, null);
	}

	@Override
	public byte[] entrySplitDone(long sessionid, byte[] requestsplit)
			throws RemoteException {
		if (!hsession.containsKey(sessionid)) {
			throw new NullPointerException("Session Id Not Found");
		}

		String sreqsplit = new String(Function.decompress(requestsplit));
		try {
			JSONObject jobj = new JSONObject(sreqsplit);
			String orderidfrom = (String) jobj.get("porderidfrom");
			JSONArray orderidto = jobj.getJSONArray("porderidto");
			String accidto = jobj.getString("paccidto");
			JSONArray volumeto = jobj.getJSONArray("pvolumeto");
			JSONArray plotto = jobj.getJSONArray("plotto");
			String splitby = (String) jobj.get("psplitby");
			Date splitdate = sdfformattime.parse(jobj.getString("psplitdate"));
			String splitterminal = (String) jobj.getString("psplitterminal");
			Double splitcounter = jobj.getDouble("psplitcounter");

			FOData fodata = new FOData();
			Object[] objs = new Object[9];
			objs[0] = orderidfrom;
			objs[1] = orderidto.toString();
			objs[2] = accidto;
			objs[3] = volumeto.toString();
			objs[4] = plotto.toString();
			objs[5] = splitby;
			objs[6] = splitdate;
			objs[7] = splitterminal;
			objs[8] = splitcounter;
			fodata.set_objects(objs);

			Vector<Object> v = accessData.getOrderData().set_insert(
					com.eqtrade.database.OrderData.SQUERY.entrysplitdone.val,
					fodata);

			SplitDone sp = (SplitDone) v.elementAt(0);

			Vector vs = new Vector();
			for (Object obj : v) {
				JSONString js = (JSONString) obj;
				if (js != null) {
					JSONObject jresult = new JSONObject(js.toJSONString());
					vs.add(jresult);
				}
			}

			JSONArray jsonarray = new JSONArray(vs);
			String jres = jsonarray.toString();

			// return orderData(
			// com.eqtrade.database.OrderData.SQUERY.entrysplitdone.val,
			// fodata);

			if (sp.getPsukses() == 1) {
				Message message = new Message();
				message.getHeader().setString(MsgType.FIELD,
						MsgType.hedSplitOrder);
				JSONObject jreturn = new JSONObject();
				// jreturn.put("request", jobj.toString());
				jreturn.put("reply", sp.toJSONObject());
				String uid = hsession.get(sessionid).get_uid();
				message.setString(Text.FIELD, jreturn.toString());
				message.setString(ClientID.FIELD, uid);

				if (ssessionid == null) {

					ssessionid = mgtw
							.getEngine()
							.getCm()
							.get_sessionid(muid.get_gtwuid(),
									muid.get_gtwidatsvr(), muid.get_gtwtype());
				}

				if (ssessionid != null && sessionid > 0) {
					log.info("send split order msg:" + message.toString());
					mgtw.getEngine().send_appmsg(ssessionid, message);
				}

			}

			return Function.compress(jres.getBytes());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public boolean changepin(long sessionid, String userid, String newPIN) throws RemoteException {
		if (!hsession.containsKey(sessionid)) {
			throw new NullPointerException("Session Id Not Found");
		}

		FOData fodata = new FOData();
		fodata.set_string(userid, newPIN);
		Vector<Object> v = null;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.changepin.val, fodata);
		} catch (Exception e) {
			Function.logger_info_exception(e);
			e.printStackTrace();
		}

		if (v == null)
			return false;

		return true;
	}

	@Override
	public boolean checkpin(long sessionid, String userid, String pin)
			throws RemoteException {

		FOData fodata = new FOData();

		fodata.set_string(userid, pin);
		Vector<Object> v;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.logon.val, fodata);
			String stime, scclogon = null;
			String stext = "reply";
			String smsg = null;

			int psukses = (Integer) v.elementAt(0);

			return psukses == 0 ? false : true;

		} catch (Exception e) {
			e.printStackTrace();
			Function.logger_info_exception(e);
		}
		return false;

	}

	@Override
	public byte[] getAnnouncement(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		return queryData(SQUERY.announcement.val, null);
	}

	@Override
	public byte[] getSecexboard(long l) throws RemoteException {
		if (!hsession.containsKey(l)) {
			throw new NullPointerException();
		}
		return queryData(SQUERY.secexchboard.val, null);
	}

	@Override
	public boolean notification(long l, byte[] byte0) throws RemoteException {
		return send_fixmsg(l, byte0);
	}

	@Override
	public byte[] getBackNotif(long session) throws RemoteException {
		if (!hsession.containsKey(session)) {
			throw new NullPointerException("Session Id Not Found");
		}
		RMISessionCounter c1 = hsession.get(session);
		if (c1 == null) {
			throw new NullPointerException("Username Is Not Found");
		}
		String suid = c1.get_uid();
		FOData fodata = new FOData();
		fodata.set_string(suid);
		return queryData(SQUERY.backnotif.val, fodata);
	}

	public boolean checkpinNEGO(long sessionid, String pin) throws RemoteException {
		FOData fodata = new FOData();

		fodata.set_string( pin);
		Vector<Object> v;
		try {
			v = accessData.getQueryData().set_insert(SQUERY.pinnego.val, fodata);
			String stime, scclogon = null;
			String stext = "reply";
			String smsg = null;

			int psukses = (Integer) v.elementAt(0);
			return psukses == 0 ? false : true;
		} catch (Exception e) {
			e.printStackTrace();
			Function.logger_info_exception(e);
		}
			return false;
	
	}

}
