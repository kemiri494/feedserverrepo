package com.eqtrade.gtw.rmi;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.utils.Function;

public class RMICleaning extends Thread {
	private final Logger log = LoggerFactory.getLogger(super.getClass());

	private Hashtable<Long, RMISessionCounter> hssn;
	private int nsleep;
	private Vector<String> vreject;
	private MessageGtwServiceInterface mgtw;

	public RMICleaning(Hashtable<Long, RMISessionCounter> ahtmp, int asleep,
			Vector<String> avreject, int amaxidle,
			MessageGtwServiceInterface _mgtw) {

		this.mgtw = _mgtw;
		this.hssn = ahtmp;
		this.nsleep = asleep;
		this.vreject = avreject;
		this.nmaxidle = amaxidle;

	}

	private boolean bstop = false;

	public void setstop() {

		bstop = true;
		synchronized (hssn) {
			hssn.notify();
		}
	}

	public void run() {

		while (!this.bstop) {
			try {
				this.sleep(nsleep);
				proses_cleaning();
			} catch (Exception ex) {
				ex.printStackTrace();
				log.error(Function.logger_info_exception(ex));
			}
		}
	}

	// private final static int C_MAXIDLE = 30;
	private int nmaxidle = 30;

	private void proses_cleaning() {
		RMISessionCounter rsc;
		Long lng;
		Vector<String> vremove = new Vector<String>(10, 5);

		//synchronized (hssn) {
			Enumeration<Long> e = hssn.keys();

			while (e.hasMoreElements()) {
				lng = e.nextElement();
				rsc = hssn.get(lng);
				if (rsc.get_counter() > nmaxidle) {
					vremove.addElement(rsc.get_uid());
					// vreject.remove(rsc.get_uid());

					log.info(" cleaning session for : " + rsc.get_uid()
							+ " session id : " + lng +" "+rsc.get_counter() +" "+nmaxidle);

				}
			}

			for (String suid : vremove) {				
				mgtw.delete_session(suid, false);
			}

			Enumeration<RMISessionCounter> er = hssn.elements();
			while (er.hasMoreElements()) {
				rsc = er.nextElement();
				rsc.set_counter();
			}
		//}

	}
}