package com.eqtrade.gtw;

import static com.eqtrade.utils.Function.print_pairmust;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.gtw.msg.MappingUserID;
import com.eqtrade.gtw.rmi.RMIGtwServiceUpdate;
import com.eqtrade.net.ClientManager;
import com.eqtrade.net.CommClientHandling;
import com.eqtrade.quickfix.resources.CleanSession;
import com.eqtrade.quickfix.resources.MessageFactory;
import com.eqtrade.quickfix.resources.RequestSession;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.quickfix.resources.field.tudSrcIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudSrcUID;
import com.eqtrade.quickfix.resources.field.tudSrcUType;
import com.eqtrade.quickfix.resources.field.tudUserID;
import com.eqtrade.utils.Function;
import com.eqtrade.utils.properties.FEProperties;

public class MessageGtwService implements MessageGtwServiceInterface,com.eqtrade.dx.CommListener {
	protected static Logger logger = LoggerFactory.getLogger("MessageGtwService");
	protected int minpoolsize, maxpoolsize, keepalivetime, socketThread;

	protected int nportrmi;
	protected int nportlocalrmi;
	protected int nport;
	protected String service, svr;	
	protected ThreadRMSession thsession;
	protected int nmaxsession;
	protected String sgtwuid, sgtwidatsvr, sgtwtype, atsuid, atsidatsvr,atstype;
	protected EventDispatcher ievent;
	protected FEProperties fep;
	protected MappingUserID muid;
	
	protected AccessQueryLocator accessData;
	String surl, sdriver, suid, spwd;
	
	protected MessageRMIManager mm;
	private String srmuid, srmidatsvr, srmtype;

	protected CommClientHandling cm = null;
	protected MessageGtwProcessing mgp = null;

	protected RMIGtwServiceUpdate rgtw = null;
	protected Vector<String> vreject = new Vector<String>(10, 5);
	protected Vector<Long> vrmsession = new Vector<Long>(100, 50);

	public MessageGtwService(String service, String ipsvr, int nport,int nheartbt, int nportrmi, int nportlocalrmi, String srmuid,String srmidatsvr, String srmtype, String sgtwuid,String sgtwidatsvr, String sgtwtype, int nsleep, int nmaxidle,int amaxsession, String surl, String sdriver, String suid,String spwd, FEProperties fep, EventDispatcher ievent,String atsidatsvr, String atstype, String atsuid) throws Exception {
		
		this.fep = fep;
		ievent.registerListener(this);
		this.service = service;
		this.nportrmi = nportrmi;
		this.nportlocalrmi = nportlocalrmi;
		this.ievent = ievent;

		// conf database
		this.surl = surl;
		this.sdriver = sdriver;
		this.suid = suid;
		this.spwd = spwd;

		// configuration for comm
		this.nport = nport;
		this.svr = ipsvr;

		// configuration for rm
		this.srmuid = srmuid;
		this.srmidatsvr = srmidatsvr;
		this.srmtype = srmtype;

		// configuration for gw
		this.sgtwuid = sgtwuid;
		this.sgtwidatsvr = sgtwidatsvr;
		this.sgtwtype = sgtwtype;

		// configurasi for ats
		this.atsidatsvr = atsidatsvr;
		this.atstype = atstype;
		this.atsuid = atsuid;

		muid = new MappingUserID(srmuid, srmidatsvr, srmtype, sgtwuid,sgtwidatsvr, sgtwtype, atsidatsvr, atstype, atsuid);

		this.nmaxsession = amaxsession;
		this.minpoolsize = Integer.parseInt(fep.getProperty("minpoolsize", "20"));
		this.maxpoolsize = Integer.parseInt(fep.getProperty("maxpoolsize", "50"));
		this.keepalivetime = Integer.parseInt(fep.getProperty("keepalivetime","20"));
		this.socketThread = Integer.parseInt(fep.getProperty("threadSocket","50"));

		thsession = new ThreadRMSession(sgtwuid, sgtwidatsvr, sgtwtype, this);

		init_database();
		init_rmigateway();
		init_allnetworking(nheartbt);
		init_rmiserver(nportrmi, nportlocalrmi, service, nsleep, nmaxidle);

	}

	protected void init_rmigateway() throws Exception {

		print_pairmust("Creating MsgManager object...");
		mm = new MessageRMIManager(this, this.muid, this.accessData);
		mm.start();
		print_pairmust("Message Manager started");
	}

	private void init_allnetworking(/* int nmaxmem, */int nheartbeat)throws Exception {
		
		cm = new CommClientHandling(nheartbeat, new MessageFactory(),this.minpoolsize, this.maxpoolsize, this.keepalivetime, ievent,socketThread);

		Function.print_pairmust("Init_allnetworking ....done");
		Function.print_pairmust("Init MessageGtwProcessing...");
		
		mgp = new MessageGtwProcessing(this.mm, cm, vrmsession,this.accessData, this.thsession);
		mgp.start();

		cm.setThirdPartyInterface(mgp);
		
		Function.print_pairmust("Init MessageGtwProcessing...done");
	}

	protected void init_rmiserver(int nportrmi, int nportlocal, String service,int nsleep, int nmaxidle) throws Exception {

		rgtw = new RMIGtwServiceUpdate(nportlocal, nsleep, nmaxidle, vreject,mm, vrmsession, thsession, this.accessData, this, this.muid);
		Registry registry = LocateRegistry.createRegistry(nportrmi);
		registry.rebind(service, rgtw);
		Function.print_pairmust("RMIGateway bind at : " + service);
	}

	public void init_database() throws Exception {		
		accessData = new AccessQueryLocator(fep.get("database"),true);
	}

	@Override
	public boolean delete_session(String suid, boolean issendtorm) {
		boolean bresult = false;
		bresult = rgtw.delete_session(suid, issendtorm);
		return bresult;
	}

	@Override
	public boolean delete_session(String suid, String sessiongtw) {
		boolean bresult = false;
		bresult = rgtw.delete_session(suid, sessiongtw);
		return bresult;
	}

	protected void do_fixmsg(String sops, String suid, String sidatsvr,String sutype, String svriploc, int nportloc, String aalias,String abranch) {

		Function.print_pair("mc:logon:" + svriploc + ":" + nportloc);
		Function.print_pair("mc:sops:" + sops);

		if (sops.equalsIgnoreCase("logon")) {
			if (cm != null) {
				boolean blogon = cm.connect_tofixsvr(suid, sidatsvr, sutype,svriploc, nportloc);
				Function.print_pair("logonConnectToFixsvr:" + blogon);
			}
		}
	}

	@Override
	public void do_fixmsg(String sops, String suid, String sidatsvr,String sutype, String sessionid) {
		if (sops.equalsIgnoreCase("getses")) {
			Function.print_pairmust("GetSession");
			if (cm != null) {
				// do..press
				RequestSession reqsess = new RequestSession(new tudSrcUID(suid), new tudSrcIDAtsvr(sidatsvr),	new tudSrcUType(sutype));
				boolean brouting = cm.send_appmsg(sessionid, reqsess);
				Function.print_pair("get session from rm : " + suid);
				System.out.println("get session from rm : " + suid+" "+reqsess);
			}
		} else if (sops.startsWith("cleanses-")) {
			// "cleanses-" + sclientid
			Function.print_pairmust("CleanSessionToRM");
			if (cm != null) {
				String[] split = sops.split("-", -2);

				CleanSession cleanmsg = new CleanSession(new tudSrcUID(suid),new tudSrcIDAtsvr(sidatsvr), new tudSrcUType(sutype),new tudUserID(split[1]));
				cleanmsg.setString(tudDestUID.FIELD, srmuid);
				cleanmsg.setString(tudDestIDAtsvr.FIELD, srmidatsvr);
				cleanmsg.setString(tudDestUType.FIELD, srmtype);

				boolean brouting = cm.send_appmsg(sessionid, cleanmsg);

				Function.print_pair("clean session from rm : " + split[1]);
			}

		}

	}

	@Override
	public long get_validsessionid(String suid) {
		long nresult = -1;
		try {
			nresult = rgtw.get_validsessionid(suid);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return nresult;
	}

	public void kill_client(String sessionid) {
		cm.kill_client(sessionid);
	}

	@Override
	public boolean is_under_maxsession_allowed() {
		boolean bresult = false;

		try {
			int ncurr = rgtw.get_numberofsession();
			bresult = ncurr <= nmaxsession;
		} catch (Exception ex) {
		}
		return bresult;
	}

	public boolean connect_tofixsvr(String psvriploc, int pnportloc) {		
		if (cm != null) {
			this.svr = psvriploc;
			this.nport = pnportloc;

			boolean bsend = cm.connect_tofixsvr(this.sgtwuid, this.sgtwidatsvr,this.sgtwtype, psvriploc, pnportloc);

			if (bsend) {
				thsession.ask_sessionlist();
			}
			return bsend;
		}
		return false;

	}

	public CommClientHandling getEngine() {
		return cm;
	}

	@Override
	public void setConfiguration(HashMap params) {		
		sgtwuid = (String) params.get(0);
		sgtwidatsvr = (String) params.get(1);
		sgtwtype = (String) params.get(2);
		svr = (String) params.get(3);
		nport = (Integer) params.get(4);
		fep.setProperty("gtw.uid", sgtwuid);
		fep.setProperty("gtw.idatsvr", sgtwidatsvr);
		fep.setProperty("gtw.type", sgtwtype);
		fep.setProperty("-server", svr);
		fep.setProperty("-port", nport + "");
		fep.save();
	}

	public void start() {
		HashMap params = new HashMap();
		params.put(0, sgtwuid);
		params.put(1, sgtwidatsvr);
		params.put(2, sgtwtype);
		params.put(3, svr);
		params.put(4, nport);
		start(params);
	}

	@Override
	public void start(HashMap params) {		
		setConfiguration(params);

		if (connect_tofixsvr(svr, nport)) {
			ievent.pushEvent(EventDispatcher.EVENT_CONNECTED, null);
		} else {
			ievent.pushEvent(EventDispatcher.EVENT_DISCONNECTED, null);
		}
	}

	@Override
	public void stop() {		
		if (cm.disconnected()) {
			ievent.pushEvent(EventDispatcher.EVENT_DISCONNECTED, null);
		}
		accessData.getOrderData().stop();
		accessData.getQueryData().stop();
	}

	@Override
	public void loadConfiguration() {		
		HashMap params = new HashMap();
		params.put(0, sgtwuid);
		params.put(1, sgtwidatsvr);
		params.put(2, sgtwtype);
		params.put(3, svr);
		params.put(4, nport);
		ievent.pushEvent(EventDispatcher.EVENT_LOAD_FINISH, params);
	}

	@Override
	public EventDispatcher eventDispather() {		
		return ievent;
	}

	public MessageRMIManager getMm() {
		return mm;
	}

	public RMIGtwServiceUpdate getRgtw() {
		return rgtw;
	}

	public void broadcastTestRequest(String msg) {

	}

	public AccessQueryLocator getAccessData() {
		return accessData;
	}
}
