package com.eqtrade.gtw;

import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.net.CommClientHandling;

public interface MessageGtwServiceInterface {

	boolean delete_session(String suid,boolean issendtorm);

	void do_fixmsg(String sops, String suid, String sidatsvr, String stype,String sessionid);

	boolean delete_session(String suid, String sessiongtw);

	boolean is_under_maxsession_allowed();

	long get_validsessionid(String suid);

	EventDispatcher eventDispather();
	
	CommClientHandling getEngine();

}