package com.eqtrade.gtw;

import java.util.Comparator;

import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.MsgSeqNum;

import com.eqtrade.utils.FixMessage;

public class StringSeqnumOrder implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		
		FixMessage first = new FixMessage(o1);
		Message mfirst = first.getQuickfixMessage();
		
		FixMessage second = new FixMessage(o2);
		Message msecond = second.getQuickfixMessage();
		
		try {
			long ntemp = new Long(mfirst.getHeader().getString(MsgSeqNum.FIELD)) - new Long(msecond.getHeader().getString(MsgSeqNum.FIELD));
			return ntemp > 0 ? 1 : ntemp != 0 ? -1 : 0;
		} catch (NumberFormatException e) {			
			e.printStackTrace();
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
		return 0;		
	}
}
