package com.eqtrade.gtw;

import static com.eqtrade.utils.Function.print_pair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.quartz.listeners.BroadcastSchedulerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quickfix.FieldNotFound;
import quickfix.field.ClOrdID;
import quickfix.field.ClientID;
import java.io.PrintStream;
import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.dx.main.session.CallbackUserProxy;
import com.eqtrade.gtw.msg.MappingUserID;
import com.eqtrade.gtw.msg.MsgManagerInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.net.CommClientHandling;

import com.eqtrade.net.ThirdPartyInterface;
import com.eqtrade.net.data.HeaderAndData;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.TestRequest;
import com.eqtrade.quickfix.resources.field.MessageSchedullerType;
import com.eqtrade.quickfix.resources.field.MsgType;
import com.eqtrade.quickfix.resources.field.Text;
import com.eqtrade.quickfix.resources.field.tudAlias;
import com.eqtrade.quickfix.resources.field.tudBranch;
import com.eqtrade.quickfix.resources.field.tudCurrentTime;
import com.eqtrade.quickfix.resources.field.tudEndSession;
import com.eqtrade.quickfix.resources.field.tudIdatsvr;
import com.eqtrade.quickfix.resources.field.tudStartSession;
import com.eqtrade.quickfix.resources.field.tudUserID;
import com.eqtrade.quickfix.resources.field.tudUserType;
import com.eqtrade.utils.FixMessage;
import com.eqtrade.utils.Function;

public class MessageGtwProcessing extends Thread implements ThirdPartyInterface {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	private MsgManagerInterface mmi;
	private com.eqtrade.net.CommClientHandling commClient;
	private Vector<Long> vrmsession;
	private AccessQueryLocator accessData;
	private ThreadRMSession thesession;

	public MessageGtwProcessing(MsgManagerInterface mmi,CommClientHandling commClient, Vector<Long> vrmsession,AccessQueryLocator access, ThreadRMSession thesession) {
		super();
		this.mmi = mmi;
		this.commClient = commClient;
		this.vrmsession = vrmsession;
		muid = mmi.get_muid();
		this.accessData = access;
		this.thesession = thesession;
		
	}

	private boolean bstop = false;
	
	private MappingUserID muid = null;

	public void process_dat(String sessionid, Message msg) {
		String sheader = msg.getOverridedHeader().getString(MsgType.FIELD);

		FixMessage fxm = new FixMessage(msg);
		Hashtable<Integer, String> hdata = fxm.get_dataonly();
		
		if (!sheader.equals(MsgType.hedInfoSession)) {
			if (sheader.equals(MsgType.hedRequestSession)) {
				String sstart = hdata.get(tudStartSession.FIELD);
				String send = hdata.get(tudEndSession.FIELD);
				System.out.println(sstart+ " 0 00");
				Long nstart = new Long(sstart);
				Long nend = new Long(send);

				synchronized (vrmsession) {
					for (long i = nstart; i <= nend; i++) {
						vrmsession.addElement(i);
					}
				}
			} else if (sheader.equals(MsgType.hedLogonClient)) {
				process_logonclientok(sessionid, fxm);
			} else if (sheader.equals(MsgType.hedLogoutClient)) {
				process_logoutclient(sessionid, fxm);
			} else if (sheader.equals(MsgType.hedBroadcastMessage)) {
				process_broadcastmessage(sessionid, msg);
			} else if(sheader.equals(TestRequest.MSGTYPE)) {
				mmi.broadcastTestRequest(msg.toString());
			} else {
				if (muid == null) 
					muid = mmi.get_muid();
				
				
				boolean bprocess = false;
				String sclordid = hdata.get(ClOrdID.FIELD);
				String sdest = hdata.get(ClientID.FIELD);

				if (sdest != null && !sdest.isEmpty()) {
					bprocess = true;
					this.mmi.add_incmsgfromsvr(sdest, msg.toString(), msg);
				} else {
					logger.error("debug.080724.gtw.processdata tag 109 not found "+ sclordid);
				}
			}
		}
	}

	private void process_broadcastmessage(String sessionid, Message msg) {		
		String msgtype = msg.getString(MessageSchedullerType.FIELD);
		msg.getOverridedHeader().setString(MsgType.FIELD, msgtype);
		msg.removeField(MessageSchedullerType.FIELD);

		mmi.broadcastmsg(msg.toString());
	}

	private void process_logoutclient(String sessionid, FixMessage fxm) {
		if (sessionid != null && fxm != null) {
			com.eqtrade.quickfix.resources.Message logooutreply = (com.eqtrade.quickfix.resources.Message) fxm.getQuickfixMessage().clone();

			String sdestsession;
			try {
				sdestsession = logooutreply.getString(tudStartSession.FIELD);
				if (sdestsession != null) {
					Function.print_pair(" add new msg for client : ");
					Function.print_pair(" sdest : " + sdestsession);
					Function.print_pair(" msg : " + logooutreply);
					String suid = logooutreply.getString(ClientID.FIELD);

					mmi.getGtwService().delete_session(suid, sdestsession);
					this.mmi.add_incmsgfromsvrbdnsession(sdestsession,logooutreply.toString());

					HashMap params = new HashMap();
					params.put(0, new Long(sdestsession));

					mmi.getGtwService().eventDispather().pushEvent(EventDispatcher.EVENT_SESSION_LOGOUT,params);
				}
			} catch (Exception e) {				
				e.printStackTrace();
				//logger.error(e.getMessage());
			}
		}
	}

	private void process_logonclientok(String sessionid, FixMessage fxm) {
		if (fxm != null) {
			Hashtable<Integer, String> hdata = fxm.get_dataonly();
			String suid = hdata.get(tudUserID.FIELD);			
			String stext = "Logon ok";
			String salias = hdata.get(tudAlias.FIELD);
			String sbranch = hdata.get(tudBranch.FIELD);			
			String stime = Function.getStringTime1(new Date());
			String sdestsession = hdata.get(tudStartSession.FIELD);
			
			com.eqtrade.quickfix.resources.Message logonreply = (com.eqtrade.quickfix.resources.Message) fxm.getQuickfixMessage().clone();
			logonreply.setString(Text.FIELD, stext);
			logonreply.removeField(tudCurrentTime.FIELD);
			logonreply.setString(tudCurrentTime.FIELD, stime);

			if (sdestsession != null) {
				Function.print_pair(" add new msg for client : ");
				Function.print_pair(" sdest : " + sdestsession);
				Function.print_pair(" msg : " + logonreply);
				this.mmi.add_incmsgfromsvrbdnsession(sdestsession,logonreply.toString());
				HashMap params = new HashMap();
				params.put(0, suid);
				params.put(1, "this|this|this|" + new String(sdestsession)+ "|" + "LOGIN");
				mmi.getGtwService().eventDispather().pushEvent(EventDispatcher.EVENT_SESSION_LOGGEDIN,params);
			}			
		}
	}
}