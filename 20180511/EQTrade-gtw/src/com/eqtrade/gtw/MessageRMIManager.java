package com.eqtrade.gtw;

import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quickfix.FieldNotFound;
import quickfix.Log;
import quickfix.field.MsgType;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.msg.MappingUserID;
import com.eqtrade.gtw.msg.MsgAbstract;
import com.eqtrade.gtw.msg.MsgManagerInterface;
import com.eqtrade.gtw.rmi.RMISessionCounter;
import com.eqtrade.net.ClientManager;
import com.eqtrade.net.data.SessionAndMessage;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudUserID;
import com.eqtrade.utils.FixMessage;
import com.eqtrade.utils.Function;
import com.eqtrade.utils.properties.FEProperties;

public class MessageRMIManager extends Thread implements MsgManagerInterface {
	private static Logger logger = LoggerFactory.getLogger(MessageRMIManager.class);

	// protected static ResourceBundle mrsc = null;
	protected FEProperties fep;
	protected MessageGtwServiceInterface mgi;
	protected MappingUserID muid;
	protected AccessQueryLocator accessData;
	protected Hashtable<Long, RMISessionCounter> hsession;

	public MessageRMIManager(MessageGtwServiceInterface amgi,MappingUserID _muid, AccessQueryLocator access) throws Exception {
		super();

		this.accessData = access;
		this.mgi = amgi;
		// this.muid = new MappingUserID(srmuid, srmidatsvr, srmtype, sgtwuid, sgtwidatsvr, sgtwtype);
		this.muid = _muid;

		if (fep == null) {
			fep = new FEProperties(getClass().getSimpleName() + ".properties");
		}
		load_msgclass();
	}

	protected Hashtable<String, MsgAbstract> hclass = new Hashtable<String, MsgAbstract>(21, 5);

	private void load_msgclass() {
		String skey, sclass;

		Enumeration<Object> e = fep.getKeys();
		while (e.hasMoreElements()) {
			try {
				skey = (String) e.nextElement();
				sclass = fep.get(skey);

				hclass.put(skey.toUpperCase(), get_object(sclass));
			} catch (Exception ex) {
				ex.printStackTrace();
				// logger.severe(logger_info_exception(ex));
				//logger.error(ex.getMessage());
			}
		}

	}

	private MsgAbstract get_object(String sname) throws Exception {

		Class<?> cls = Class.forName(sname);
		Class partypes[] = new Class[3];

		partypes[0] = Class.forName("com.eqtrade.gtw.MessageGtwServiceInterface");
		partypes[1] = Class.forName("com.eqtrade.database.AccessQueryLocator");
		partypes[2] = Class.forName("com.eqtrade.gtw.msg.MappingUserID");

		Constructor ct = cls.getConstructor(partypes);

		Object arglist[] = new Object[3];
		arglist[0] = this.mgi;
		arglist[1] = this.accessData;
		arglist[2] = this.muid;

		return (MsgAbstract) ct.newInstance(arglist);
	}

	private boolean bstop = false;
	protected Vector<SessionAndMessage> vinmsgc = new Vector<SessionAndMessage>(100, 50);

	@Override
	public void run() {
		String stemp;
		SessionAndMessage sm;

		while (!this.bstop) {
			try {
				if (vinmsgc.size() > 0) {
					sm = vinmsgc.remove(0);
					process_msg(sm.get_session(), sm.get_msg().toString());

				} else {
					synchronized (vinmsgc) {
						vinmsgc.wait();
					}
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				//logger.error(Function.logger_info_exception(ex));
			}
		}
	}

	private String sessionid = "";

	private void process_msg(String sessiongtw, String smsg) {
		Vector<String> vresult;

		logger.info("receive_msg :" + smsg);
		FixMessage fixmsg = new FixMessage(smsg);

		try {
			String header = fixmsg.getQuickfixMessage().getHeader().getString(MsgType.FIELD);
			// Function.print_pair(" msgtype : " + header);
			System.out.println(header+" header...");
			MsgAbstract ma = hclass.get(header);
			// Function.print_pair("msg abstract " + ma+":"+sessionid);
			System.out.println(ma+" -- ");
			if (ma != null && sessionid != null) {
				if (sessionid.equals("")) {
					sessionid = mgi.getEngine().getCm().get_sessionid(muid.get_gtwuid(), muid.get_gtwidatsvr(),muid.get_gtwtype()); // "gtw",
					// "1234",
					// "gtw");
					// Function.print_pair(" Your sessionid : " + sessionid);
				}
				Vector<String> vbypass = new Vector<String>(2, 1);
				vresult = ma.do_action(fixmsg.getQuickfixMessage(), sessionid,sessiongtw, vbypass);
				//logger.info(vresult+"-- vresult "+ sessionid + " session id");
				if (vresult != null && vresult.size() > 0) {
					if (vbypass.size() > 0) {
						add_incmsgfromsvrbdnsession(vbypass.elementAt(0),
								vresult.elementAt(0));

					} else {
						String sendersubid;

						sendersubid = fixmsg.getQuickfixMessage().getString(tudUserID.FIELD);

						Vector<String> vmsg = hmsg.get(sendersubid);
						if (vmsg == null) {
							vmsg = new Vector<String>(100, 50);
							hmsg.put(sendersubid, vmsg); 
						}
						vmsg.addAll(vresult);
					}
				}
			} else {

				//logger.debug("you must first connect to message routing");
			}

		} catch (FieldNotFound e) {			
			// Function.logger_info_exception(e);
			logger.error(Function.logger_info_exception(e));
		}

		// }

	}

	@Override
	public void add_incmsgfromclient(long session, String msg) {
		vinmsgc.addElement(new SessionAndMessage(session + "", msg));
		synchronized (vinmsgc) {
			vinmsgc.notify();
		}
	}

	@Override
	public void add_incmsgfromsvr(String suid, String msg) {
		// suid = suid.toLowerCase();

		logger.info("add income msg from svr:" + suid + ":msg:" + msg);

		// synchronized (hmsg) {
		Vector<String> vtmp = hmsg.get(suid);
		if (vtmp == null) {
			vtmp = new Vector<String>(100, 50);
			hmsg.put(suid, vtmp);
		}

		if (vtmp != null) {

			synchronized (vtmp) {
				 Function.print_pair("mm:add_incmsgfromsvr:" + msg);
				vtmp.addElement(msg);
				//Collections.sort(vtmp, new StringSeqnumOrder());
			}
			//logger.info("vmsg for user "+suid+" "+vtmp.size());

			// Function.print_pair_log("MsgManager.debug.080724:MsgManager:"
			// + suid + " msg : " + msg + ":" + vtmp.size());
			//logger.info("add income msg from svr:" + suid + ":msg:" + msg);

		}
		// }
	}

	@Override
	public void broadcastmsg(String msg) {
		//synchronized (hsession) {
			logger.info("added message to all client");
			Enumeration<Long> en = hsession.keys();
			while (en.hasMoreElements()) {
				long sessid = en.nextElement();
				//logger.info("suid:" + sessid);
				add_incmsgfromsvrbdnsession(sessid + "", msg);
			}
		//}
	}

	@Override
	public void add_incmsgfromsvrbdnsession(String sessionid, String msg) {
		// /Function.print_pair("debug:mm:add_incmsgfromsvrbasedsession:" + msg
		// + ":with session_id:" + sessionid);
		// synchronized (hmsgsession) {

		Vector<String> vtmp = hmsgsession.get(sessionid);
		if (vtmp == null) {
			vtmp = new Vector<String>(100, 50);
			hmsgsession.put(sessionid, vtmp);
		}

		if (vtmp != null) {
			synchronized (vtmp) {
				 Function.print_pair("mm:add_incmsgfromsvrbasedsession:" +
				 msg);
				vtmp.addElement(msg);
			}
		}
		// }
	}

	@Override
	public Vector<String> get_msg(String suid) {
		Vector<String> vresult = null;

		System.out.println("get_msg : " + suid);

		// synchronized (hmsg) {

		Vector<String> vtmp = hmsg.get(suid);
		if (vtmp != null) {
			synchronized (vtmp) {
				if (vtmp.size() > 0) {
					//Collections.sort(vtmp, new StringSeqnumOrder());
					vresult = new Vector<String>(100, 50);
					int i = 0;
					for (String s : vtmp) {
						vresult.addElement(s);
						if (i > 10)
							break;
						i++;
					}

					if (vresult.size() > 0)
						vtmp.removeAll(vresult);
				}
			}
		}
		return vresult;
		// }

	}

	@Override
	public Vector<String> get_msg(long sessionid) {
		Vector<String> vresult = null;

		// System.out.println("get_msg : " + suid);
		// synchronized (hmsgsession) {

		Vector<String> vtmp = hmsgsession.get(sessionid + "");
		if (vtmp != null) {
			synchronized (vtmp) {
				if (vtmp.size() > 0) {
					vresult = new Vector<String>(100, 50);
					int i = 0;
					for (String s : vtmp) {
						vresult.addElement(s);
						if (i > 10)
							break;
						i++;
					}

					if (vresult.size() > 0)
						vtmp.removeAll(vresult);
				}
			}
		}

		return vresult;
		// }
	}

	@Override
	public MappingUserID get_muid() {
		return muid;
	}

	protected Hashtable<String, Vector<String>> hmsg = new Hashtable<String, Vector<String>>(
			100);

	protected Hashtable<String, Vector<String>> hmsgsession = new Hashtable<String, Vector<String>>(
			100);

	@Override
	public void set_session(Hashtable<Long, RMISessionCounter> hsession) {
		this.hsession = hsession;
	}

	@Override
	public MessageGtwServiceInterface getGtwService() {
		return mgi;
	}

	@Override
	public void add_incmsgfromsvr(String suid, String msg, Message fixmsg) {
		
	}

	@Override
	public void broadcastTestRequest(String msg) {
		
	}

}
