package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.net.msg.svr.mgtw;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.HandlInst;
import com.eqtrade.quickfix.resources.field.TransactTime;
import com.eqtrade.utils.Function;

public final class MsgD extends MsgAbstract {

	public MsgD(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);

		//String client = message.getString(ClientID.FIELD);
		
		String clordid = message.getString(ClOrdID.FIELD);
		String handlinst = message.getString(HandlInst.FIELD);

		if (handlinst == null)
			handlinst = Integer.toString(HandlInst.NORMAL);

		String transacttime = message.getString(TransactTime.FIELD);

		if (transacttime == null || transacttime.isEmpty())
			transacttime = sdf.format(new Date());

		message.setString(TransactTime.FIELD, transacttime);

	//	muid.add_clordiduseridid(clordid, client);

		if (mgi.getEngine().getCm() != null) {

			addSourceAndDest(message);
			//Function.print_pair(" sdata : " + message);
			
			boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);
			//Function.print_pair(" send new ord from client : " + brouting);

		}

		return vresult;

	}
}