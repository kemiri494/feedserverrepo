package com.eqtrade.gtw.msg;

import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.quickfix.resources.field.tudSrcIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudSrcUID;
import com.eqtrade.quickfix.resources.field.tudSrcUType;

public abstract class MsgAbstract extends Object {
	protected static Logger logger = Logger.getLogger("global");

	protected MessageGtwServiceInterface mgi;
	protected AccessQueryLocator accessData;
	protected MappingUserID muid;

	public MsgAbstract(MessageGtwServiceInterface amgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		this.mgi = amgi;
		// this.dbpool = adbpool;
		this.muid = amuid;
		this.accessData = accessData;
	}

	public abstract Vector<String> do_action(Message message,
			String sessionid, String sessiongtw, Vector<String> bypass);
	
	public void addSourceAndDest(Message message) {
		message.setString(tudSrcUID.FIELD, muid.get_gtwuid());
		message.setString(tudSrcIDAtsvr.FIELD, muid.get_gtwidatsvr());
		message.setString(tudSrcUType.FIELD, muid.get_gtwtype());
		message.setString(tudDestIDAtsvr.FIELD, muid.get_rmidatsvr());
		message.setString(tudDestUID.FIELD, muid.get_rmuid());
		message.setString(tudDestUType.FIELD, muid.get_rmtype());
	}
	
	public void addSourceAndDestAts(Message mess){
		//isi buat ats.idatsvr, ats.type,
		mess.setString(tudSrcUID.FIELD, muid.get_gtwuid());
		mess.setString(tudSrcIDAtsvr.FIELD, muid.get_gtwidatsvr());
		mess.setString(tudSrcUType.FIELD, muid.get_gtwtype());
		mess.setString(tudDestIDAtsvr.FIELD, muid.get_Atsidatsvr());
		mess.setString(tudDestUID.FIELD, muid.get_Atsuid());
		mess.setString(tudDestUType.FIELD, muid.get_Atstype());
		
	}

}