package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.OrderID;
import com.eqtrade.quickfix.resources.field.TransactTime;
import com.eqtrade.utils.Function;

public final class MsgF extends MsgAbstract {

	public MsgF(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	@Override
	public Vector<String> do_action(Message message,
			String sessionid, String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);

		if (mgi.getEngine().getCm() != null) {
			
			String sclordid;

			String sordid = message.getString(OrderID.FIELD);
			sclordid = message.getString(ClOrdID.FIELD);

			String salesid = message.getString(ClientID.FIELD); // "USR";
			
			//muid.add_clordiduseridid(sclordid, salesid);
			
			String transacttime = message.getString(TransactTime.FIELD);
			
			if(transacttime == null || transacttime.isEmpty())
				transacttime = sdf.format(new Date());
			
			message.setString(TransactTime.FIELD, transacttime);
			addSourceAndDest(message);
	
			
			Function.print_pair("msgf:sdata:" + message);
			boolean brouting = mgi.getEngine().send_appmsg(sessionid,
					message);
			Function.print_pair("send req withdraw : " + brouting);

		}

		return vresult;
	}
}