package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.OrigClOrdID;
import com.eqtrade.quickfix.resources.field.TransactTime;
import com.eqtrade.utils.Function;

public final class MsgG extends MsgAbstract {

	public MsgG(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);

		if (mgi.getEngine().getCm() != null) {
			String transacttime = message.getString(TransactTime.FIELD);

			if (transacttime == null || transacttime.isEmpty())
				transacttime = sdf.format(new Date());

			message.setString(TransactTime.FIELD, transacttime);

			String client = message.getString(ClientID.FIELD);
			String sclordid = message.getString(ClOrdID.FIELD);
			String sorigclordid = message.getString(OrigClOrdID.FIELD);

		//	muid.add_clordiduseridid(sclordid, client);
	//		muid.add_clordiduseridid(sorigclordid, client);
			
			addSourceAndDest(message);

			boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);
			Function
					.print_pair(" send req amend : " + brouting + ":" + message);

			// String client = muid.get_userid(sorigclordid);

		}

		return vresult;
	}
}