package com.eqtrade.gtw.msg;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import com.eqtrade.utils.Function;

public final class MappingUserID extends Object {

	//private Hashtable<String, Vector<String>> hdata = new Hashtable<String, Vector<String>>(
		//	20, 10);

	private String srmuid;
	private String srmidatsvr;
	private String srmtype;

	private String sgtwuid;
	private String sgtwidatsvr;
	private String sgtwtype;
	
	private String atsidatsvr;
	private String atstype;
	private String atsuid;

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
	private String sfname = null;

	public MappingUserID(String armuid, String armid, String armtype,
			String agtwuid, String agtwidatsvr, String agtwtype,
			String atsidatsvr, String atstype, String atsuid) {

		this.srmuid = armuid;
		this.srmidatsvr = armid;
		this.srmtype = armtype;

		this.sgtwuid = agtwuid;
		this.sgtwidatsvr = agtwidatsvr;
		this.sgtwtype = agtwtype;
		
		this.atsidatsvr = atsidatsvr;
		this.atstype = atstype;
		this.atsuid = atsuid;

		//Date date = new Date();
	//	this.sfname = "log/"+sdf.format(date) + "muid.txt";
	//	load_datareqfromfile(hdata, sfname);
		
	}

	public String getLog(String name) {
		URL url = getClass().getResource("log/");
		String path = url.getFile() + name;
		return path;
	}

	private void load_datareqfromfile(Hashtable<String, Vector<String>> hreq,
			String sfname) {

		try {
			String sclordid, usrid;
			String[] split;

			Vector<String> vtemp = new Vector<String>(100, 50);
			Vector<String> vusrid;

			Function.load_vectorfromfile(vtemp, sfname);
			for (String s : vtemp) {
				if (s.trim().length() > 0) {
					split = s.split("###", -2);
					sclordid = split[0];
					usrid = split[1];
					vusrid = hreq.get(sclordid);
					if (vusrid == null) {
						vusrid = new Vector<String>(10, 5);
						hreq.put(sclordid, vusrid);
					}
					vusrid.addElement(usrid);
					// hreq.put(sclordid, usrid);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public String get_Atsidatsvr() {
		return this.atsidatsvr;
	}

	public String get_Atstype() {
		return this.atstype;
	}

	public String get_Atsuid() {
		return this.atsuid;
	}

	private void save_datatofile(String data, String sfname) {

		Function.save_datatofile(data, sfname);
	}

	/*protected void add_clordiduseridid(String sclordid, String susrid) {

		Vector<String> vusrid = hdata.get(sclordid);
		if (vusrid == null) {
			vusrid = new Vector<String>(10, 5);
			hdata.put(sclordid, vusrid);
		}
		if (!vusrid.contains(susrid))
			vusrid.addElement(susrid);

		StringBuffer sbfl = new StringBuffer(100);
		sbfl.append(sclordid);
		sbfl.append("###");
		sbfl.append(susrid);
		save_datatofile(sbfl.toString(), sfname);

	}*/

/*	public Vector<String> get_userid(String sclordid) {
		Vector<String> vresult = null;

		
		 * String sresult = hdata.get(sclordid); if (sresult == null){
		 * print_pair("there is no destination user for : " + sclordid); }
		 
		vresult = hdata.get(sclordid);
		if (vresult == null) {
			Function.print_pair("there is no destination user for : "
					+ sclordid);
		}

		return vresult;
	}*/

	public String get_rmuid() {

		return this.srmuid;
	}

	public String get_rmidatsvr() {

		return this.srmidatsvr;
	}

	public String get_rmtype() {

		return this.srmtype;
	}

	public String get_gtwuid() {

		return this.sgtwuid;
	}

	public String get_gtwidatsvr() {

		return this.sgtwidatsvr;
	}

	public String get_gtwtype() {

		return this.sgtwtype;
	}
}