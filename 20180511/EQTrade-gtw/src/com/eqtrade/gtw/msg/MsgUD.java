package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.TransactTime;
import com.eqtrade.utils.Function;

public final class MsgUD extends MsgAbstract {

	public MsgUD(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	public Vector<String> do_action(Message message,
			String sessionid, String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);

		if (mgi.getEngine().getCm() != null) {
			String reqtime = sdf.format(new Date());

			String sclordid = message.getString(ClOrdID.FIELD);
			String salesid = message.getString(ClientID.FIELD); // "USR";

			//muid.add_clordiduseridid(sclordid, salesid);
			
			message.setString(TransactTime.FIELD, reqtime);
			addSourceAndDest(message);
			boolean brouting = mgi.getEngine().send_appmsg(sessionid,
					message);
			Function.print_pair("send req delete temp : " + brouting+":"+message);

		}

		return vresult;
	}
}