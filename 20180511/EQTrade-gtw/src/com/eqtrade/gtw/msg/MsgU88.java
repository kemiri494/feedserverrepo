package com.eqtrade.gtw.msg;

import static com.eqtrade.utils.Function.build_data;
import static com.eqtrade.utils.Function.build_msg;
import static com.eqtrade.utils.Function.get_doubletype;
import static com.eqtrade.utils.Function.logger_info_exception;
import static com.eqtrade.utils.Function.print_pair;

import java.util.Hashtable;
import java.util.Vector;

import quickfix.field.Account;
import quickfix.field.ClOrdID;
import quickfix.field.HandlInst;
import quickfix.field.OrdType;
import quickfix.field.OrderID;
import quickfix.field.Price;
import quickfix.field.SecondaryOrderID;
import quickfix.field.SecurityID;
import quickfix.field.SecurityTradingStatus;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.field.SymbolSfx;
import quickfix.field.TransactTime;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.data.Fieldmsg;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.*;

public final class MsgU88 extends MsgAbstract {

	public MsgU88(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	@Override
	public Vector<String> do_action(Message message,
			String sessionid, String sessiongtw, Vector<String> bypass) {
		Vector<String> v = new Vector<String>(100, 10);
/*		FOData fodata = new FOData();
		SQLAdapter sql = null;

		try {
			String suid = message.getString(tudUserID.FIELD);
			String streqid = message.getString(tudSTReqId.FIELD);
			String sreqtype = message.getString(tudReqType.FIELD);

			print_pair(" sreqtype : [" + sreqtype + "]");
			if (sreqtype.equals("ID")) {
				v.addAll(get_sec(sql, message.getString(tudSecEndSeqNo.FIELD), suid,
						streqid));
				v.addAll(get_cdat(sql, message.getString(tudCDatEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_board(sql, message.getString(tudBoardEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_exchange(sql, message.getString(tudExchangeEndSeqNo
						.FIELD), suid, streqid));
				v.addAll(get_funrep(sql, message.getString(tudFunRepEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_application(sql, message.getString(tudApplicationEndSeqNo
						.FIELD), suid, streqid));
				v.addAll(get_ordstatus(sql, message.getString(tudOrdStatusEndSeqNo
						.FIELD), suid, streqid));
				v.addAll(get_invtype(sql, message.getString(tudInvTypeEndSeqNo.FIELD), suid, streqid));
				v.addAll(get_ordtype(sql, message.getString(tudOrdTypeEndSeqNo.FIELD), suid, streqid));
				v.addAll(get_acctype(sql, message.getString(tudAccTypeEndSeqNo.FIELD), suid, streqid));
				v.addAll(get_branch(sql, message.getString(tudBranchEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_utype(sql, message.getString(tudUTypeEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_group(sql, message.getString(tudGroupEndSeqNo.FIELD),
						suid, streqid));
				v.addAll(get_approver(sql, message.getString(tudApproverEndSeqNo
						.FIELD), suid, streqid));
				v.addAll(get_uprof(sql, message.getString(tudUProfEndSeqNo.FIELD),
						suid, streqid));

			} else if (sreqtype.equals("A")) {
				v.addAll(get_account(sql, message.getString(tudAccInfoEndSeqNo.FIELD), suid, streqid, message.getString(tudCAC_ID.FIELD), message.getString(tudCLS_INITIALCODE
						.FIELD)));

			} else if (sreqtype.equals("P")) {
				v.addAll(get_portfolio(sql, message.getString(tudPortfolioSeqNo
						.FIELD), suid, streqid, message.getString(tudCAC_ID.FIELD), message.getString(Symbol.FIELD)));

			} else if (sreqtype.equals("O")) {
				v.addAll(get_order(sql, message.getString(tudOrderSeqNo.FIELD),
						suid, streqid, message.getString(tudCAC_ID.FIELD)));

			} else if (sreqtype.equals("T")) {
				v.addAll(get_done(sql, message.getString(tudTradeEndSeqNo.FIELD),
						suid, streqid, message.getString(tudCAC_ID.FIELD),message.getString(ClOrdID.FIELD)));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.severe(logger_info_exception(ex));
		} */
		
		return v;
	}

/*	private Vector<String> get_done(SQLAdapter sql, String seqno, String suid,
			String sreqid, String scacid, String sclordid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid, scacid, sclordid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(done.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_done:" + seqno + " vtmpsize : " + vtmp.size());

		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "T"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "16"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			Vector<Fieldmsg> vdata, vdet;
			String smaxseqno = "0";
			String[] sparse;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				smaxseqno = sparse[0];

				vdet = new Vector<Fieldmsg>(50, 20);
				vdet.addElement(new Fieldmsg(ClOrdID.FIELD, sparse[2]));
				vdet.addElement(new Fieldmsg(tudTRX_SEQNO.FIELD, sparse[3]));
				vdet.addElement(new Fieldmsg(SecondaryOrderID.FIELD,
						sparse[4]));
				vdet.addElement(new Fieldmsg(TransactTime.FIELD, sparse[5]));
				vdet
						.addElement(new Fieldmsg(tudTRX_VOLUME.FIELD,
								sparse[6]));
				vdet.addElement(new Fieldmsg(tudTRX_PRICE.FIELD, sparse[7]));
				vdet.addElement(new Fieldmsg(tudTRX_CONTRABROKER.FIELD,
						sparse[8]));
				vdet.addElement(new Fieldmsg(tudTRX_CONTRATRADER.FIELD,
						sparse[9]));

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[3]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[3]));
				vdata.addElement(new Fieldmsg(tudUserID.FIELD, suid));
				vdata.addElement(new Fieldmsg(tudNoTrade.FIELD, "1"));
				vdata.addAll(vdet);

				v.addElement(build_msg(vheader, build_data(vdata)));

			}

		}

		return v;
	}

	private Vector<String> get_order(SQLAdapter sql, String seqno, String suid,
			String sreqid, String scacid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid, scacid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<Object> vout = new Vector<Object>(2, 1);
		Vector<String> vord = sql.get_query(order.val, fodata, vout);

		print_pair("MsgU88:get_order:" + seqno + " vtmpsize : " + vord.size());
		if (vord != null && vord.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "O"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "16"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata, vdet;

			try {

				// Vector vord = (Vector)
				// get_objectfromdecodedstring(vtmp.elementAt(0));
				// Hashtable hordstatus = (Hashtable)
				// get_objectfromdecodedstring(vtmp.elementAt(1));
				Hashtable hordstatus = (Hashtable) vout.elementAt(0);

				String[] sdet;
				String smaxseqno = "0", sclordid;

				for (String o : vord) {
					sparse = o.split("\\|", -2);

					smaxseqno = sparse[0];

					vdet = new Vector<Fieldmsg>(10, 5);
					vdet
							.addElement(new Fieldmsg(tudCAC_ID.FIELD,
									sparse[2]));
					vdet.addElement(new Fieldmsg(tudCLS_INITIALCODE.FIELD,
							sparse[3]));
					vdet
							.addElement(new Fieldmsg(tudCTT_ID.FIELD,
									sparse[4]));
					vdet
							.addElement(new Fieldmsg(SecurityID.FIELD,
									sparse[5]));
					vdet.addElement(new Fieldmsg(Account.FIELD, sparse[6]));
					vdet.addElement(new Fieldmsg(OrdType.FIELD, sparse[7]));
					vdet
							.addElement(new Fieldmsg(tudORS_ID.FIELD,
									sparse[8]));
					vdet
							.addElement(new Fieldmsg(tudXCG_ID.FIELD,
									sparse[9]));
					vdet
							.addElement(new Fieldmsg(SymbolSfx.FIELD,
									sparse[10]));
					vdet.addElement(new Fieldmsg(ClOrdID.FIELD, sparse[11]));
					vdet.addElement(new Fieldmsg(OrderID.FIELD, sparse[12]));
					vdet.addElement(new Fieldmsg(Side.FIELD, sparse[13]));
					vdet.addElement(new Fieldmsg(tudORD_VOLUME.FIELD,
							sparse[14]));
					vdet.addElement(new Fieldmsg(tudORD_LOT.FIELD,
							sparse[15]));
					vdet.addElement(new Fieldmsg(tudORD_BALANCEVOLUME.FIELD,
							sparse[16]));
					vdet.addElement(new Fieldmsg(tudORD_ORIGINALID.FIELD,
							sparse[17]));
					vdet.addElement(new Fieldmsg(Price.FIELD, sparse[18]));
					vdet
							.addElement(new Fieldmsg(HandlInst.FIELD,
									sparse[19]));

					sclordid = sparse[11];
					Vector vhos = (Vector) hordstatus.get(sclordid);
					if (vhos != null) {
						vdet.addElement(new Fieldmsg(tudNoHistOrderStatus
								.FIELD, vhos.size() + ""));
						for (Object o1 : vhos) {
							sdet = o1.toString().split("\\|", -2);

							vdet.addElement(new Fieldmsg(tudORS_ID.FIELD,
									sdet[0]));
							vdet.addElement(new Fieldmsg(tudHOS_EXECTIME
									.FIELD, sdet[1]));
							vdet.addElement(new Fieldmsg(tudUserID.FIELD,
									sdet[2]));
							vdet
									.addElement(new Fieldmsg(Text.FIELD,
											sdet[3]));
						}
					}

					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[1]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata.addElement(new Fieldmsg(tudUserID.FIELD, suid));
					vdata.addElement(new Fieldmsg(tudNoOrder.FIELD, "1"));
					vdata.addAll(vdet);

					v.addElement(build_msg(vheader, build_data(vdata)));

					muid.add_clordiduseridid(sclordid, suid);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				logger.severe(logger_info_exception(ex));
			}

		}

		return v;
	}

	private Vector<String> get_portfolio(SQLAdapter sql, String seqno,
			String suid, String sreqid, String scacid, String symbol) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid, scacid, symbol);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(portfolio.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:getportoflio:" + seqno + " vtmpsize : "
				+ vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "P"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "17"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata, vdet;
			String smaxseqno = "0";

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				smaxseqno = sparse[0];

				vdet = new Vector<Fieldmsg>(10, 5);
				vdet.addElement(new Fieldmsg(tudCAC_ID.FIELD, sparse[2]));
				vdet.addElement(new Fieldmsg(tudCTT_ID.FIELD, sparse[3]));
				vdet.addElement(new Fieldmsg(SecurityID.FIELD, sparse[4]));
				vdet.addElement(new Fieldmsg(tudCLS_INITIALCODE.FIELD,
						sparse[5]));
				vdet.addElement(new Fieldmsg(tudPFO_BEGVOLUME.FIELD,
						sparse[6]));
				vdet.addElement(new Fieldmsg(tudPFO_CURRENTVOLUME.FIELD,
						sparse[7]));
				vdet.addElement(new Fieldmsg(tudPFO_BEGVALUE.FIELD,
						sparse[8]));
				vdet.addElement(new Fieldmsg(tudPFO_CURRENTVALUE.FIELD,
						sparse[9]));
				vdet.addElement(new Fieldmsg(tudPFO_OPENBUYVOLUME.FIELD,
						sparse[10]));
				vdet.addElement(new Fieldmsg(tudPFO_OPENSELLVOLUME.FIELD,
						sparse[11]));
				vdet.addElement(new Fieldmsg(tudPFO_OPENBUYVALUE.FIELD,
						sparse[12]));
				vdet.addElement(new Fieldmsg(tudPFO_OPENSELLVALUE.FIELD,
						sparse[13]));
				vdet.addElement(new Fieldmsg(tudPFO_TRADEBUYVOLUME.FIELD,
						sparse[14]));
				vdet.addElement(new Fieldmsg(tudPFO_TRADESELLVOLUME.FIELD,
						sparse[15]));
				vdet.addElement(new Fieldmsg(tudPFO_TRADEBUYVALUE.FIELD,
						sparse[16]));
				vdet.addElement(new Fieldmsg(tudPFO_TRADESELLVALUE.FIELD,
						sparse[17]));
				vdet.addElement(new Fieldmsg(tudPFO_LASTPRICE.FIELD,
						sparse[18]));
				vdet
						.addElement(new Fieldmsg(tudPFO_STATUS.FIELD,
								sparse[19]));

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudUserID.FIELD, suid));
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudNoAccPFO.FIELD, "1"));
				vdata.addAll(vdet);

				v.addElement(build_msg(vheader, build_data(vdata)));

			}

		}

		return v;
	}

	private Vector<String> get_account(SQLAdapter sql, String seqno,
			String suid, String sreqid, String scacid, String sclsinit) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid, scacid, sclsinit);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(account.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_account:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "A"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "16"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata, vdet;
			String smaxseqno = "0";

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				smaxseqno = sparse[0];

				vdet = new Vector<Fieldmsg>(10, 5);
				vdet.addElement(new Fieldmsg(tudCAC_ID.FIELD, sparse[2]));
				vdet.addElement(new Fieldmsg(tudCLS_INITIALCODE.FIELD,
						sparse[3]));
				vdet.addElement(new Fieldmsg(tudCTT_ID.FIELD, sparse[4]));
				vdet.addElement(new Fieldmsg(tudCAC_NAME.FIELD, sparse[5]));
				vdet.addElement(new Fieldmsg(tudCAC_BEGTRADINGLIMIT.FIELD,
						sparse[6]));
				vdet.addElement(new Fieldmsg(tudCAC_CURRENT_TRADINGLIMIT
						.FIELD, sparse[7]));
				vdet.addElement(new Fieldmsg(tudCAC_BEGRATIO.FIELD,
						sparse[8]));
				vdet.addElement(new Fieldmsg(tudCAC_CURRENTRATIO.FIELD,
						sparse[9]));
				vdet.addElement(new Fieldmsg(tudCAC_BEGPORTFOLIOVALUE.FIELD,
						sparse[10]));
				vdet.addElement(new Fieldmsg(tudCAC_CURRENTPORTFOLIOVALUE
						.FIELD, sparse[11]));
				vdet.addElement(new Fieldmsg(tudCAC_BEGOUTSTANDING.FIELD,
						sparse[12]));
				vdet.addElement(new Fieldmsg(
						tudCAC_CURRENTOUTSTANDING.FIELD, sparse[13]));
				vdet.addElement(new Fieldmsg(tudCAC_RATIOLIMIT.FIELD,
						sparse[14]));
				vdet.addElement(new Fieldmsg(tudCAC_PLAFONDCREDIT.FIELD,
						sparse[15]));
				vdet.addElement(new Fieldmsg(tudCAC_BEGCASHONHAND.FIELD,
						sparse[16]));
				vdet.addElement(new Fieldmsg(tudCAC_CURRENTCASHONHAND.FIELD,
						sparse[17]));
				vdet.addElement(new Fieldmsg(tudCAC_PAYABLE_T1.FIELD,
						sparse[18]));
				vdet.addElement(new Fieldmsg(tudCAC_PAYABLE_T2.FIELD,
						sparse[19]));
				vdet.addElement(new Fieldmsg(tudCAC_PAYABLE_T3.FIELD,
						sparse[20]));
				vdet.addElement(new Fieldmsg(tudCAC_RECEIVABLE_T1.FIELD,
						sparse[21]));
				vdet.addElement(new Fieldmsg(tudCAC_RECEIVABLE_T2.FIELD,
						sparse[22]));
				vdet.addElement(new Fieldmsg(tudCAC_RECEIVABLE_T3.FIELD,
						sparse[23]));
				vdet.addElement(new Fieldmsg(tudCAC_TRADEBUYVALUE.FIELD,
						sparse[24]));
				vdet.addElement(new Fieldmsg(tudCAC_TRADESELLVALUE.FIELD,
						sparse[25]));
				vdet.addElement(new Fieldmsg(tudCAC_TRADINGSTATUS.FIELD,
						sparse[26]));
				vdet.addElement(new Fieldmsg(tudCAC_ACCUMOVERLIMIT.FIELD,
						sparse[27]));
				vdet
						.addElement(new Fieldmsg(tudCAC_STATUS.FIELD,
								sparse[28]));
				vdet
						.addElement(new Fieldmsg(tudCAC_FEEBUY.FIELD,
								sparse[29]));
				vdet.addElement(new Fieldmsg(tudCAC_FEESELL.FIELD,
						sparse[30]));
				vdet
						.addElement(new Fieldmsg(tudCAC_ALIAS.FIELD,
								sparse[31]));
				vdet.addElement(new Fieldmsg(tudCAC_OVERRIDING.FIELD,
						sparse[32]));

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudUserID.FIELD, suid));
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudNoAccInfo.FIELD, "1"));
				vdata.addAll(vdet);

				v.addElement(build_msg(vheader, build_data(vdata)));

			}

		}

		return v;
	}

	private Vector<String> get_uprof(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<Object> vout = new Vector<Object>(3, 1);
		Vector<String> vusr = sql.get_query(usrprofile.val, fodata, vout);
		print_pair("MsgU88:get_uprof:" + seqno + " vtmpsize : " + vusr.size());

		if (vusr != null && vusr.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "13"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			try {
				
				 * Vector vusr = (Vector)
				 * get_objectfromdecodedstring(vtmp.elementAt(0)); Vector vs =
				 * (Vector) get_objectfromdecodedstring(vtmp.elementAt(1));
				 * Vector vp = (Vector)
				 * get_objectfromdecodedstring(vtmp.elementAt(2));
				 
				Vector vs = (Vector) vout.elementAt(0);
				Vector vp = (Vector) vout.elementAt(1);

				String[] sdet;
				String sid;

				for (String o : vusr) {
					sparse = o.split("\\|", -2);

					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[0]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata
							.addElement(new Fieldmsg(tudUserID.FIELD,
									sparse[2]));
					vdata
							.addElement(new Fieldmsg(tudUST_ID.FIELD,
									sparse[3]));
					vdata
							.addElement(new Fieldmsg(tudUGR_ID.FIELD,
									sparse[4]));
					vdata
							.addElement(new Fieldmsg(tudCMP_ID.FIELD,
									sparse[5]));
					vdata.addElement(new Fieldmsg(tudUSR_ALIAS.FIELD,
							sparse[6]));
					vdata.addElement(new Fieldmsg(tudUSR_MAXVALUEPERTRX
							.FIELD, sparse[7]));
					vdata.addElement(new Fieldmsg(tudUSR_SECLIMIT.FIELD,
							sparse[8]));
					vdata.addElement(new Fieldmsg(tudUSR_STATUS.FIELD,
							sparse[9]));

					if (vs != null) {
						vdata.addElement(new Fieldmsg(tudNoUsrSec.FIELD, vs
								.size()
								+ ""));
						for (Object o1 : vs) {
							sdet = o1.toString().split("\\|", -2);
							vdata.addElement(new Fieldmsg(SecurityID.FIELD,
									sdet[0]));
							vdata.addElement(new Fieldmsg(tudSCU_STATUS
									.FIELD, sdet[1]));
						}
					}

					if (vp != null) {
						vdata.addElement(new Fieldmsg(tudNoUsrApplication
								.FIELD, vp.size() + ""));
						for (Object o2 : vp) {
							sdet = o2.toString().split("\\|", -2);
							vdata.addElement(new Fieldmsg(tudAPP_ID.FIELD,
									sdet[0]));
							vdata.addElement(new Fieldmsg(tudUPR_STATUS
									.FIELD, sdet[1]));
						}
					}

					vdata.addElement(new Fieldmsg(tudNoUsrGroupTaker.FIELD,
							"1"));
					vdata
							.addElement(new Fieldmsg(tudUGR_ID.FIELD,
									sparse[4]));
					vdata.addElement(new Fieldmsg(tudGRT_STATUS.FIELD,
							sparse[10]));

					v.addElement(build_msg(vheader, build_data(vdata)));
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.severe(logger_info_exception(ex));
			}

		}

		return v;
	}

	private Vector<String> get_cdat(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(customer.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_cdat:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "15"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			try {
				String maxseqno = "1";

				for (String s : vtmp) {
					sparse = s.split("\\|", -2);

					maxseqno = sparse[0];

					Vector<Fieldmsg> vc = new Vector<Fieldmsg>(10, 5);
					vc.addElement(new Fieldmsg(tudCLS_INITIALCODE.FIELD,
							sparse[2]));
					vc
							.addElement(new Fieldmsg(tudCSR_NAME.FIELD,
									sparse[3]));
					vc.addElement(new Fieldmsg(tudCSR_ADDRESS1.FIELD,
							sparse[4]));
					vc.addElement(new Fieldmsg(tudCSR_ADDRESS2.FIELD,
							sparse[5]));
					vc.addElement(new Fieldmsg(tudCSR_ZIPCODE.FIELD,
							sparse[6]));
					vc
							.addElement(new Fieldmsg(tudCSR_EMAIL.FIELD,
									sparse[7]));
					vc
							.addElement(new Fieldmsg(tudCSR_PHONE.FIELD,
									sparse[8]));
					vc.addElement(new Fieldmsg(tudCSR_FAX.FIELD, sparse[9]));
					vc.addElement(new Fieldmsg(tudCSR_MOBILEPHONE.FIELD,
							sparse[10]));
					vc.addElement(new Fieldmsg(Account.FIELD, sparse[11]));
					vc
							.addElement(new Fieldmsg(tudCST_CODE.FIELD,
									sparse[12]));
					vc.addElement(new Fieldmsg(tudCSR_STATUS.FIELD,
							sparse[13]));

					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[1]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata.addElement(new Fieldmsg(tudUserID.FIELD, suid));
					vdata.addElement(new Fieldmsg(tudUSC_STATUS.FIELD, "1"));
					vdata.addElement(new Fieldmsg(tudNoUsrCust.FIELD, "1"));
					vdata.addAll(vc);

					v.addElement(build_msg(vheader, build_data(vdata)));

				}

			} catch (Exception ex) {
				ex.printStackTrace();
				logger.severe(logger_info_exception(ex));
			}

		}

		return v;
	}

	private Vector<String> get_board(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(board.val, fodata,
				new Vector<Object>(1));
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "2"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			String sboard;
			Vector<Fieldmsg> vdata, vboard;
			Vector<Vector<Fieldmsg>> vall = new Vector<Vector<Fieldmsg>>(10, 4);
			Hashtable<String, Vector<Fieldmsg>> hboard = new Hashtable<String, Vector<Fieldmsg>>(
					10);
			Vector<String> vcheck = new Vector<String>(10, 5);

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				sboard = sparse[1];
				if (!vcheck.contains(sboard)) {
					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[0]));
					vdata.addElement(new Fieldmsg(SymbolSfx.FIELD, sboard));
					vdata.addElement(new Fieldmsg(tudBRD_DESC.FIELD,
							sparse[2]));
					vdata.addElement(new Fieldmsg(tudBRD_STATUS.FIELD,
							sparse[3]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[5]));

					// repeated group
					vcheck.addElement(sboard);
					vall.addElement(vdata);
				}

				vboard = hboard.get(sboard);
				if (vboard == null) {
					vboard = new Vector<Fieldmsg>(10, 5);
					hboard.put(sboard, vboard);
				}
				vboard.addElement(new Fieldmsg(tudXCG_ID.FIELD, sparse[4]));
				vboard.addElement(new Fieldmsg(tudXBR_STATUS.FIELD,
						sparse[6]));
			}

			int norep;
			for (int i = 0; i < vcheck.size(); i++) {
				sboard = vcheck.elementAt(i);
				vdata = vall.elementAt(i);
				vboard = hboard.get(sboard);
				norep = vboard.size() / 2;
				vdata
						.addElement(new Fieldmsg(tudNoExchId.FIELD, norep
								+ ""));
				vdata.addAll(vboard);

				v.addElement(build_msg(vheader, build_data(vdata)));

			}

		}

		return v;
	}

	private Vector<String> get_exchange(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(exchange.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_exchange:" + seqno + " vtmpsize : "
				+ vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "1"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudXCG_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudXCG_NAME.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudXCG_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_funrep(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(report.val, fodata,
				new Vector<Object>(1));
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "4"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;
			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudFNR_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudFNR_TYPE.FIELD, sparse[3]));
				vdata.addElement(new Fieldmsg(tudFNR_DESC.FIELD, sparse[4]));
				vdata
						.addElement(new Fieldmsg(tudFNR_STATUS.FIELD,
								sparse[5]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;

	}

	private Vector<String> get_application(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(app.val, fodata,
				new Vector<Object>(1));
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "5"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			String sapp;
			Vector<Fieldmsg> vdata, vapp;
			Vector<Vector<Fieldmsg>> vall = new Vector<Vector<Fieldmsg>>(10, 4);
			Hashtable<String, Vector<Fieldmsg>> happ = new Hashtable<String, Vector<Fieldmsg>>(
					10);
			Vector<String> vcheck = new Vector<String>(10, 5);

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				sapp = sparse[2];
				if (!vcheck.contains(sapp)) {
					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[0]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata.addElement(new Fieldmsg(tudAPP_ID.FIELD, sapp));
					vdata.addElement(new Fieldmsg(tudAPP_DESC.FIELD,
							sparse[3]));
					vdata.addElement(new Fieldmsg(tudAPP_STATUS.FIELD,
							sparse[4]));

					// repeated group
					vcheck.addElement(sapp);
					vall.addElement(vdata);
				}

				vapp = happ.get(sapp);
				if (vapp == null) {
					vapp = new Vector<Fieldmsg>(10, 5);
					happ.put(sapp, vapp);
				}
				vapp.addElement(new Fieldmsg(tudFNR_ID.FIELD, sparse[5]));
				vapp
						.addElement(new Fieldmsg(tudAFR_STATUS.FIELD,
								sparse[6]));
			}

			int norep;
			for (int i = 0; i < vcheck.size(); i++) {
				sapp = vcheck.elementAt(i);
				vdata = vall.elementAt(i);
				vapp = happ.get(sapp);
				norep = vapp.size() / 2;
				vdata
						.addElement(new Fieldmsg(tudNoAppFNR.FIELD, norep
								+ ""));
				vdata.addAll(vapp);

				v.addElement(build_msg(vheader, build_data(vdata)));

			}

		}

		return v;
	}

	private Vector<String> get_ordstatus(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(ordstatus.val, fodata,
				new Vector<Object>(1));
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "6"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;
			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudORS_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudORS_DESC.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudORS_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_invtype(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(invtype.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:invtype:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "7"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudINT_ACCOUNT.FIELD,
						sparse[2]));
				vdata.addElement(new Fieldmsg(tudINT_DESC.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudINT_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_ordtype(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(ordtype.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:ordtype:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "8"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudORT_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudORT_DESC.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudORT_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_acctype(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(acctype.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_acctype:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "9"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudCTT_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudCTT_NAME.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudCTT_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_branch(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);
		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(branch.val, fodata,
				new Vector<Object>(1));
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "3"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;
			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudCMP_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudCMP_NAME.FIELD, sparse[3]));
				vdata.addElement(new Fieldmsg(tudCMP_ADDRESS1.FIELD,
						sparse[4]));
				vdata.addElement(new Fieldmsg(tudCMP_ADDRESS2.FIELD,
						sparse[5]));
				vdata
						.addElement(new Fieldmsg(tudCMP_PHONE.FIELD,
								sparse[6]));
				vdata.addElement(new Fieldmsg(tudCMP_FAX.FIELD, sparse[7]));
				vdata.addElement(new Fieldmsg(tudCMP_ZIPCODE.FIELD,
						sparse[8]));
				vdata
						.addElement(new Fieldmsg(tudCMP_STATUS.FIELD,
								sparse[9]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_utype(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(usrtype.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_utype:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "10"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudUST_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudUST_DESC.FIELD, sparse[3]));
				vdata
						.addElement(new Fieldmsg(tudUST_STATUS.FIELD,
								sparse[4]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_group(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<String> vtmp = sql.get_query(usrgroup.val, fodata,
				new Vector<Object>(1));
		print_pair("MsgU88:get_group:" + seqno + " vtmpsize : " + vtmp.size());
		if (vtmp != null && vtmp.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "12"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			for (String s : vtmp) {
				sparse = s.split("\\|", -2);

				vdata = new Vector<Fieldmsg>(10, 5);
				vdata.addAll(vinit);
				vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD, sparse[0]));
				vdata.addElement(new Fieldmsg(tudSeqNo.FIELD, sparse[1]));
				vdata.addElement(new Fieldmsg(tudUGR_ID.FIELD, sparse[2]));
				vdata.addElement(new Fieldmsg(tudCMP_ID.FIELD, sparse[3]));
				vdata.addElement(new Fieldmsg(tudUGR_DESC.FIELD, sparse[4]));
				vdata
						.addElement(new Fieldmsg(tudUGR_STATUS.FIELD,
								sparse[5]));

				v.addElement(build_msg(vheader, build_data(vdata)));
			}
		}

		return v;
	}

	private Vector<String> get_approver(SQLAdapter sql, String seqno,
			String suid, String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<Object> vout = new Vector<Object>(3, 1);
		Vector<String> vapr = sql.get_query(approver.val, fodata, vout);

		print_pair("MsgU88:get_approver  :" + seqno + " vtmpsize : "
				+ vapr.size());
		if (vapr != null && vapr.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "14"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD,
					MsgType.hedReply));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			try {

				// Vector vapr = (Vector)
				// get_objectfromdecodedstring(vtmp.elementAt(0));
				// Hashtable hg= (Hashtable)
				// get_objectfromdecodedstring(vtmp.elementAt(1));
				Hashtable hg = (Hashtable) vout.elementAt(0);

				String[] sdet;
				String sid;

				for (String o : vapr) {
					sparse = o.split("\\|", -2);

					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[0]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata
							.addElement(new Fieldmsg(tudAPR_ID.FIELD,
									sparse[2]));
					vdata
							.addElement(new Fieldmsg(tudUserID.FIELD,
									sparse[3]));
					vdata.addElement(new Fieldmsg(tudAPR_MIN.FIELD,
							sparse[4]));
					vdata.addElement(new Fieldmsg(tudAPR_MAX.FIELD,
							sparse[5]));
					vdata.addElement(new Fieldmsg(tudAPR_STATUS.FIELD,
							sparse[6]));

					sid = sparse[2];
					Vector vg = (Vector) hg.get(sid);
					if (vg != null) {
						vdata.addElement(new Fieldmsg(tudNoApprGroup.FIELD,
								vg.size() + ""));
						for (Object o1 : vg) {
							sdet = o1.toString().split("\\|", -2);

							vdata.addElement(new Fieldmsg(tudUGR_ID.FIELD,
									sdet[0]));
							vdata.addElement(new Fieldmsg(tudCMP_ID.FIELD,
									sdet[1]));
							vdata.addElement(new Fieldmsg(tudAPG_STATUS
									.FIELD, sdet[2]));
						}
					}

					v.addElement(build_msg(vheader, build_data(vdata)));
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.severe(logger_info_exception(ex));
			}

		}

		return v;
	}

	private Vector<String> get_sec(SQLAdapter sql, String seqno, String suid,
			String sreqid) {
		Vector<String> v = new Vector<String>(10, 2);

		FOData fodata = new FOData();
		fodata.set_string(suid);
		fodata.set_double(get_doubletype(seqno, 0));

		Vector<Object> vout = new Vector<Object>(3, 1);
		Vector<String> vsec = sql.get_query(securities.val, fodata, vout);
		print_pair("MsgU88:get_sec:" + seqno + " vtmpsize : " + vsec.size());

		if (vsec != null && vsec.size() > 0) {
			Vector<Fieldmsg> vinit = new Vector<Fieldmsg>(10, 5);
			vinit.addElement(new Fieldmsg(tudSTReqId.FIELD, sreqid));
			vinit.addElement(new Fieldmsg(tudReqType.FIELD, "ID"));
			vinit.addElement(new Fieldmsg(tudRepType.FIELD, "11"));
			vinit.addElement(new Fieldmsg(tudRepStatus.FIELD, "1"));

			Vector<Fieldmsg> vheader = new Vector<Fieldmsg>(2);
			vheader
					.addElement(new Fieldmsg(quickfix.field.MsgType.FIELD, MsgType.hedReply
							));

			String[] sparse;
			Vector<Fieldmsg> vdata;

			try {

				// Vector vsec = (Vector)
				// get_objectfromdecodedstring(vtmp.elementAt(0));
				// Hashtable hx= (Hashtable)
				// get_objectfromdecodedstring(vtmp.elementAt(1));
				// Hashtable ha = (Hashtable)
				// get_objectfromdecodedstring(vtmp.elementAt(2));
				Hashtable hx = (Hashtable) vout.elementAt(0);
				Hashtable ha = (Hashtable) vout.elementAt(1);

				String[] sdet;
				String sid;

				for (String o : vsec) {
					sparse = o.split("\\|", -2);
					vdata = new Vector<Fieldmsg>(10, 5);
					vdata.addAll(vinit);
					vdata.addElement(new Fieldmsg(tudMaxSeqNo.FIELD,
							sparse[0]));
					vdata
							.addElement(new Fieldmsg(tudSeqNo.FIELD,
									sparse[1]));
					vdata.addElement(new Fieldmsg(SecurityID.FIELD,
							sparse[2]));
					vdata.addElement(new Fieldmsg(tudSEC_NAME.FIELD,
							sparse[3]));
					vdata.addElement(new Fieldmsg(tudSEC_LOTSIZE.FIELD,
							sparse[4]));
					vdata.addElement(new Fieldmsg(tudSEC_MINSTEP.FIELD,
							sparse[5]));
					vdata.addElement(new Fieldmsg(SecurityTradingStatus
							.FIELD, sparse[6]));

					sid = sparse[2];
					Vector vx = (Vector) hx.get(sid);
					if (vx != null) {
						vdata.addElement(new Fieldmsg(tudNoBoard.FIELD, vx
								.size()
								+ ""));
						for (Object o1 : vx) {
							sdet = o1.toString().split("\\|", -2);
							vdata.addElement(new Fieldmsg(tudXCG_ID.FIELD,
									sdet[1]));
							vdata.addElement(new Fieldmsg(SymbolSfx.FIELD,
									sdet[0]));
							vdata.addElement(new Fieldmsg(tudSBX_STATUS
									.FIELD, sdet[2]));
						}
					}

					Vector va = (Vector) ha.get(sid);
					if (va != null) {
						vdata.addElement(new Fieldmsg(tudNoAccType.FIELD, va
								.size()
								+ ""));
						for (Object o2 : va) {
							sdet = o2.toString().split("\\|", -2);
							vdata.addElement(new Fieldmsg(tudCTT_ID.FIELD,
									sdet[0]));
							vdata.addElement(new Fieldmsg(tudCTS_STATUS
									.FIELD, sdet[1]));
						}

					}

					v.addElement(build_msg(vheader, build_data(vdata)));
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.severe(logger_info_exception(ex));
			}

		}

		return v;
	}
*/
}