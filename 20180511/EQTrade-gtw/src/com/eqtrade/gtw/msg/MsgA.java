package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;

public final class MsgA extends MsgAbstract {

	public MsgA(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {

		FOData fodata = new FOData();

		Vector<String> vresult = new Vector<String>();
/*
		print_pair(" receive 4 msgA : " + message);
		try {

			String suid = message.getString(tudUserID.FIELD);

			String spwd = message.getString(tudPassword.FIELD);

			fodata.set_string(suid, spwd);
			Vector<Object> v = accessData.getQueryData().set_insert(
					SQUERY.logon.val, fodata);

			String stime, scclogon = null;
			String stext = "reply";
			String smsg = null;
			boolean bdel = false;
			boolean bvalid = false;

			boolean bnumofuser = mgi.is_under_maxsession_allowed();

			int psukses = (Integer) v.elementAt(0);

			if ((psukses == 1 && bnumofuser)) {
								
				message.getHeader().setString(MsgType.FIELD,MsgType.hedLogonClient );
				message.setString(tudStartSession.FIELD, sessiongtw);

				addSourceAndDest(message);
				ClientManager.cm.send_appmsg(sessionid,(Message) message.clone());
				Function.print_pair("send logon infomation to rm " + message);
				
								
				HashMap params = new HashMap();
				params.put(0, suid);
				params.put(1, new String("-|-|-|"+sessiongtw));
				mgi.eventDispather().pushEvent(EventDispatcher.EVENT_SESSION_LOGGEDIN,params );

			} else {
				stext = !bnumofuser ? "server busy " : "invalid userid";

				LogoutClient lgc = new LogoutClient(new tudStartSession(Long
						.parseLong(sessiongtw)));
				lgc.setString(Text.FIELD, stext);
				smsg = lgc.toString();

				bypass.addElement(sessiongtw);

				Function.print_pair(" suid  : " + suid);

				Message invalidpwd = new Message();
				invalidpwd.getHeader().setString(MsgType.FIELD,
						MsgType.hedInvalidPwd);
				invalidpwd.setString(tudUserID.FIELD, suid);
				
				addSourceAndDest(invalidpwd);

				ClientManager.cm.send_appmsg(sessionid, invalidpwd);

				Function.print_pair("send invalid pwd msg to rm");
			}

			if (smsg != null)
				vresult.addElement(smsg);

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.severe(logger_info_exception(ex));
		}
*/
		return vresult;

	}
}
