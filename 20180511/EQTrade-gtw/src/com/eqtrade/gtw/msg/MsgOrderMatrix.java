package com.eqtrade.gtw.msg;

import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;

public class MsgOrderMatrix extends MsgAbstract{

	public MsgOrderMatrix(MessageGtwServiceInterface amgi,AccessQueryLocator accessData, MappingUserID amuid) {
		super(amgi, accessData, amuid);		
	}

	@Override
	public Vector<String> do_action(Message message, String sessionid,String sessiongtw, Vector<String> bypass) {		
		Vector<String> vresult = new Vector<String>(10,5);
		/*String client = message.getString(ClientID.FIELD);
		String clordid = message.getString(ClOrdID.FIELD);
		String handlinst = message.getString(HandlInst.FIELD);
		
		if(handlinst == null)
			handlinst = Integer.toString(HandlInst.NORMAL);
		
		String transacttime = message.getString(TransactTime.FIELD);
		if(transacttime == null)
			transacttime = sdf.format(new Date());
		message.setString(TransactTime.FIELD, transacttime);*/
		
		if (mgi.getEngine().getCm() !=null) {
			addSourceAndDest(message);
			boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);			
		}		
		return vresult;
	}

}
