package com.eqtrade.gtw.msg;

import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;

public class MsgDOMBL extends MsgAbstract{

	public MsgDOMBL(MessageGtwServiceInterface amgi, AccessQueryLocator accessData, MappingUserID amuid) {
		super(amgi, accessData, amuid);
	}

	@Override
	public Vector<String> do_action(Message message, String sessionid, String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10,5);
		if (mgi.getEngine().getCm() !=null) {
			addSourceAndDest(message);
			boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);
		}
		
		return vresult;
	}

}
