package com.eqtrade.gtw.msg;

import static com.eqtrade.utils.Function.print_pair;

import java.util.Vector;

import quickfix.field.Text;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudStartSession;
import com.eqtrade.quickfix.resources.field.tudUserID;

public final class Msg5 extends MsgAbstract {

	public Msg5(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {

		Vector<String> vr = new Vector<String>(10, 5);

		try {
			String stext = "confirming logout";
			message.setString(tudStartSession.FIELD, sessiongtw);
			String suid = message.getString(tudUserID.FIELD);
			message.setString(tudUserID.FIELD, suid);
			message.setString(Text.FIELD, stext);

			vr.add(message.toString());

			//boolean bdel = mgi.delete_session(suid);
			//print_pair(" delete session : " + suid + " : " + bdel);

			/*
			 * hdata_logout.clear();
			 * 
			 * hdata_logout.put(Tag.tudSrcUID.getTag(), muid.get_gtwuid());
			 * hdata_logout.put(Tag.tudSrcIDAtsvr.getTag(),
			 * muid.get_gtwidatsvr());
			 * hdata_logout.put(Tag.tudSrcUType.getTag(), muid.get_gtwtype());
			 */
			addSourceAndDest(message);

			mgi.getEngine().send_appmsg(sessionid, message);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		return vr;
	}
}