package com.eqtrade.gtw.msg;

import static com.eqtrade.utils.Function.get_doubletype;
import static com.eqtrade.utils.Function.print_pair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.database.FOData;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.Text;
import com.eqtrade.quickfix.resources.field.tudCurrentTime;
import com.eqtrade.quickfix.resources.field.tudNewPassword;
import com.eqtrade.quickfix.resources.field.tudPassword;
import com.eqtrade.quickfix.resources.field.tudUserID;
import com.eqtrade.utils.Function;

public final class MsgU1 extends MsgAbstract {

	public MsgU1(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyMMddHHmmss.SSS");

	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {
		Vector<String> v = new Vector<String>(10, 5);
		FOData fodata = new FOData();

		try {
			String suid = message.getString(tudUserID.FIELD);
			String spwd = message.getString(tudPassword.FIELD);
			String snpwd = message.getString(tudNewPassword.FIELD);

			Function.print_pair(" PARAM : " + suid + " : " + spwd + " : "
					+ snpwd);
			fodata.set_string(suid, spwd, snpwd);
			Vector<String> vi = new Vector<String>();

			boolean bvalid = false;
			if (vi != null && vi.size() > 0) {
				String sout = vi.elementAt(0);
				print_pair("MSGUI:sout:" + sout);

				if (get_doubletype(sout, 0) > 0) {
					String sdate = sdf.format(new Date());

					message.setString(tudCurrentTime.FIELD, sdate);
					bvalid = true;
				}

			}

			if (!bvalid) {
				message.setString(Text.FIELD, "unable to changing pwd");

			}

			v.addElement(message.toString());

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.severe(Function.logger_info_exception(ex));
		}

		return v;
	}

}