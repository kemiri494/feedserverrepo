package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.net.ClientManager;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.HandlInst;
import com.eqtrade.quickfix.resources.field.TransactTime;
import com.eqtrade.utils.Function;



public final class MsgUT extends MsgAbstract {

	public MsgUT(MessageGtwServiceInterface mgi, AccessQueryLocator accessData,
			MappingUserID amuid) {

		super(mgi, accessData, amuid);
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	@Override
	public Vector<String> do_action(Message message,
			String sessionid, String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);
		
		String reqtime = sdf.format(new Date());

		String client = message.getString(ClientID.FIELD); // "usr"

		String clordid = message.getString(ClOrdID.FIELD);
		
		String handlinst = message.getString(HandlInst.FIELD);
		if (handlinst == null)
			handlinst = "1";

	//	muid.add_clordiduseridid(clordid, client);

		if (mgi.getEngine().getCm() != null) {

			message.setString(TransactTime.FIELD, reqtime);
			addSourceAndDest(message);

			boolean brouting = mgi.getEngine().send_appmsg(sessionid,
					message);
			Function.print_pair(" send new ord from client : " + brouting+":"+message);

		}

		return vresult;
	}

}