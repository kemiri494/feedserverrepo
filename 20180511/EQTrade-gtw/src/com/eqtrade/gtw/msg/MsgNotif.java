package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;


import quickfix.field.TransactTime;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.ClOrdID;
import com.eqtrade.quickfix.resources.field.ClientID;
import com.eqtrade.quickfix.resources.field.HandlInst;

public final class MsgNotif extends MsgAbstract{

	public MsgNotif(MessageGtwServiceInterface amgi,
			AccessQueryLocator accessData, MappingUserID amuid) {
		super(amgi, accessData, amuid);
		// TODO Auto-generated constructor stub
	}
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {
		// TODO Auto-generated method stub
		Vector<String> vresult = new Vector<String>(10,5);
		/*String client = message.getString(ClientID.FIELD);
		String clordid = message.getString(ClOrdID.FIELD);
		String handlinst = message.getString(HandlInst.FIELD);
		
		if(handlinst == null)
			handlinst = Integer.toString(HandlInst.NORMAL);
		
		String transacttime = message.getString(TransactTime.FIELD);
		if(transacttime == null)
			transacttime = sdf.format(new Date());
		message.setString(TransactTime.FIELD, transacttime);*/
		
		if (mgi.getEngine().getCm() !=null) {
			addSourceAndDest(message);
			boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);
		}
		
		return vresult;
	}

}
