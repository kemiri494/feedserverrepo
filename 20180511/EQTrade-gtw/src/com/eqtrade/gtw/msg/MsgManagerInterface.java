package com.eqtrade.gtw.msg;

import java.util.Hashtable;
import java.util.Vector;

import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.gtw.rmi.RMISessionCounter;
import com.eqtrade.quickfix.resources.Message;

public interface MsgManagerInterface {

	void add_incmsgfromclient(long session, String msg);

	Vector<String> get_msg(String suid);

	void add_incmsgfromsvr(String suid, String msg);

	MappingUserID get_muid();

	Vector<String> get_msg(long sessionid);

	void add_incmsgfromsvrbdnsession(String sessinid, String msg);

	void broadcastmsg(String msg);

	void set_session(Hashtable<Long, RMISessionCounter> hsession);

	MessageGtwServiceInterface getGtwService();

	void add_incmsgfromsvr(String suid, String msg, Message fixmsg);
	
	void broadcastTestRequest(String msg);

}