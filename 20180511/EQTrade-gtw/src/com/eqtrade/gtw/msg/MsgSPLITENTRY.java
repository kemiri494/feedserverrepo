package com.eqtrade.gtw.msg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.HandlInst;
import com.eqtrade.quickfix.resources.field.TransactTime;

public class MsgSPLITENTRY extends MsgAbstract {

	public MsgSPLITENTRY(MessageGtwServiceInterface amgi,
			AccessQueryLocator accessData, MappingUserID amuid) {
		super(amgi, accessData, amuid);
	}
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");


	@Override
	public Vector<String> do_action(Message message, String sessionid,
			String sessiongtw, Vector<String> bypass) {
		Vector<String> vresult = new Vector<String>(10, 5);

		String handlinst = message.getString(HandlInst.FIELD);

		if (handlinst == null)
			handlinst = Integer.toString(HandlInst.NORMAL);

		String transacttime = message.getString(TransactTime.FIELD);

		if (transacttime == null || transacttime.isEmpty())
			transacttime = sdf.format(new Date());

		
		addSourceAndDest(message);
		//Function.print_pair(" sdata : " + message);
		
		boolean brouting = mgi.getEngine().send_appmsg(sessionid, message);
		
		return vresult;
	}

}
