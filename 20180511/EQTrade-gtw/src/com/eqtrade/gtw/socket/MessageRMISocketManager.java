package com.eqtrade.gtw.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.database.AccessQueryLocator;
import com.eqtrade.gtw.MessageGtwServiceInterface;
import com.eqtrade.gtw.MessageRMIManager;
import com.eqtrade.gtw.msg.MappingUserID;
import com.eqtrade.netty.protocol.CompressionEncoder;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudAccountID;
import com.eqtrade.utils.Function;

public class MessageRMISocketManager extends MessageRMIManager {
	private Logger log = LoggerFactory.getLogger(getClass());

	public MessageRMISocketManager(MessageGtwServiceInterface amgi,
			MappingUserID _muid, AccessQueryLocator access) throws Exception {
		super(amgi, _muid, access);
	}

	@Override
	public void add_incmsgfromsvr(String suid, String msg) {
		log.info("deprecated method.");
	}

	@Override
	public void add_incmsgfromsvrbdnsession(String sessionid, String msg) {
		MessageGtwServiceSocket mgtw = (MessageGtwServiceSocket) this.mgi;
		ClientGtw sock = mgtw.getSocketClientReceiver().getDataClientBySesid()
				.get(new Integer(sessionid));

		 log.info("add income msg from bdnsvr:" + sessionid + ":msg:" + msg	+ ":" + sock);

		if (sock != null)
			sock.getClientSocket().sendMessage(
					Function.compress(msg.getBytes()));

	}

	@Override
	public void add_incmsgfromsvr(String suid, String msg, Message fixmsg) {
		MessageGtwServiceSocket mgtw = (MessageGtwServiceSocket) this.mgi;
		ClientGtw sock = mgtw.getSocketClientReceiver().getDataClientByUser()
				.get(suid);
		log.info("add income msg from svr:" + suid + ":msg:" + msg + ":" + sock + " " + sock.getClientSocket());
//		System.out.println("add income msg from svr:" + suid + ":msg:" + msg + ":" + sock + " " + sock.getClientSocket());

		if (sock != null) {
			sock.sendWithFilterAccount(msg, fixmsg);
		}
	}

	@Override
	public void broadcastTestRequest(String msg) {
		((MessageGtwServiceSocket) this.mgi).getSocketConnector().broadcast(
				CompressionEncoder.compress(msg.getBytes()));
	}

}
