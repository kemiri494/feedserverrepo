package com.eqtrade.gtw.socket;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.database.FOData;
import com.eqtrade.database.QueryData.SQUERY;
import com.eqtrade.gtw.rmi.RMISessionCounter;
import com.eqtrade.netty.protocol.CompressionDecoder;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.field.tudAccountID;
import com.eqtrade.utils.Function;

public class SocketClientReceiverImpl implements Receiver {
	// private RMIGtwServiceUpdate rgtw;
	// private MessageRMIManager mmi;
	private MessageGtwServiceSocket msgGtwService;
	private Logger log = LoggerFactory.getLogger(getClass());
	private Hashtable<String, ClientGtw> dataClientByUser = new Hashtable<String, ClientGtw>();
	private Hashtable<Integer, ClientGtw> dataClientBySesid = new Hashtable<Integer, ClientGtw>();

	// private Hashtable<Integer, String> dataFilterClientBySessid = new
	// Hashtable<Integer, String>();

	public SocketClientReceiverImpl(MessageGtwServiceSocket msgGtwService) {
		super();
		// this.rgtw = rgtw;
		// this.mmi = mmi;
		this.msgGtwService = msgGtwService;
	}

	@Override
	public void connected(ClientSocket socket) {
	}

	@Override
	public void receive(ClientSocket clientSocket, byte[] bt) {
		byte[] reply = null;
		String response = "";
		String msg = new String(CompressionDecoder.decompress(bt));
		String[] data = msg.split("\\|");

		//
//		log.info("receive msg " + msg);
		System.out.println("receive msg " + msg);
		try {

			Long sesid = (long) -25;
			if (data[0].equals("login")) {
				String username = data[1], password = data[2];
				
				//#Valdhy 20141021
				sesid = msgGtwService.getRgtw().login(username, password);
//				Vector<Object> Vlogin = null;
//				Vlogin = msgGtwService.getRgtw().Vlogin(username, password);
//				sesid = (Long) Vlogin.elementAt(0);
				
				//#Valdhy 20141021
				String status = "failed, invalid username or password";
//				String status = "invalid";
//				String status = (String) Vlogin.elementAt(1);
//				String next_action = (String) Vlogin.elementAt(2);
				
				// log.info("sessionid " + sesid);
				if (sesid != -25) {
					status = "success";
					clientSocket.setID(sesid.toString());
					clientSocket.validated(true);
					ClientGtw client = new ClientGtw(clientSocket);
					getDataClientByUser().put(username, client);
					getDataClientBySesid().put(sesid.intValue(), client);

				} else {
					clientSocket.validated(false);
				}

				log.info("login.user:" + username + ":" + status + ":" + sesid);

				//#Valdhy 20141021
				response = "login|" + sesid + "|" + status;
//				System.out.println("login.user:" + username + ":" + status + ":" + sesid+" "+response.getBytes().length);
//				response = "login|" + sesid + "|" + status +  "|" + next_action;
//				System.out.println("login|" + sesid + "|" + status +  "|" + next_action);
				reply = Function.compress(response.getBytes());

			} else if (data[0].equals("loginmobile")) {
				String username = data[1], password = data[2], imei =data[3], platform = data[4];
				log.info("login mobile "+data[1]+" "+data[2]+" "+data[3]+" "+data[4]);
				//#Valdhy 20141021
				sesid = msgGtwService.getRgtw().loginmobile(username, password, imei, platform);
//				Vector<Object> Vlogin = null;
//				Vlogin = msgGtwService.getRgtw().Vlogin(username, password);
//				sesid = (Long) Vlogin.elementAt(0);
				
				//#Valdhy 20141021
				String status = "failed, invalid username or password";
//				String status = "invalid";
//				String status = (String) Vlogin.elementAt(1);
//				String next_action = (String) Vlogin.elementAt(2);
				
				// log.info("sessionid " + sesid);
				if (sesid != -25) {
					status = "success";
					clientSocket.setID(sesid.toString());
					clientSocket.validated(true);
					ClientGtw client = new ClientGtw(clientSocket);
					getDataClientByUser().put(username, client);
					getDataClientBySesid().put(sesid.intValue(), client);

				} else {
					clientSocket.validated(false);
				}

				log.info("login.usermobile:" + username + ":" + status + ":" + sesid);

				//#Valdhy 20141021
				response = "loginmobile|" + sesid + "|" + status;
//				System.out.println("login.user:" + username + ":" + status + ":" + sesid+" "+response.getBytes().length);
//				response = "login|" + sesid + "|" + status +  "|" + next_action;
//				System.out.println("login|" + sesid + "|" + status +  "|" + next_action);
				reply = Function.compress(response.getBytes());

			} else if (data[0].equals("trading.test")) {
				clientSocket.validated(true);
				Boolean isBypassToServer = Boolean.parseBoolean(data[1]);
				// log.info("receive test msg:"+msg.getBytes().length+":isSentToRM:"+isBypassToServer);
				if (isBypassToServer) {
					msgGtwService.getMm().add_incmsgfromclient(0L, data[2]);
				}
			} else {

				sesid = Long.parseLong(data[0]);

				if (isFIXProtocol(msg)) {
					msg = msg.substring(msg.indexOf("|") + 1, msg.length());
					msg = msg.replace("|", "\001");
					log.info("send fixmsg " + msg);
					msgGtwService.getRgtw().send_fixmsg(sesid, msg);
				} else {

					RMISessionCounter rmises = msgGtwService.getRgtw()
							.isValidClient(sesid);
					if (data[1].equals("chgpwd")) {
						String userid = data[2];
						String newPIN = data[3];
						boolean ischange = msgGtwService.getRgtw().chgPwd(
								sesid, userid, newPIN);
						response = data[1] + "|" + (ischange ? "ok" : "gagal");
//yosep 24022015		Vector v = msgGtwService.getRgtw().chgpwd(sesid, userid, newPIN);
//						response = data[1] + "|" +v.get(0)+"#"+v.get(1);
						reply = Function.compress(response.getBytes());
					} else if (data[1].equals("acctype")) {

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.acctype.val, null);

						// reply = msgGtwService.getRgtw().getAccType(
						// sesid.longValue());
						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());
						// response = new String(Function.decompress(bdata));

					} else if (data[1].equals("acctypesec")) {
						reply = msgGtwService.getRgtw().getAccTypeSec(
								sesid.longValue());

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.accTypeSec.val,
										null);

						// reply = msgGtwService.getRgtw().getAccType(
						// sesid.longValue());
						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

						// response = new String(Function.decompress(bdata));

					} else if (data[1].equals("account")) {

						// reply = msgGtwService.getRgtw().getAccount(
						// sesid.longValue(), data[2]);

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2]);

						log.info("load account " + rmises.get_uid() + " " + data[2]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.accountlist.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

						// response = new String(Function.decompress(bdata));
						// log.info("account list " + response.getBytes().length
						// + " compressed bytes " + bdata.length);

					} else if (data[1].equals("board")) {
						// reply =
						// msgGtwService.getRgtw().getBoard(sesid.longValue());
						String sreply = msgGtwService.getRgtw().queryDataNotCompress(SQUERY.board.val, null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

						// response = new String(Function.decompress(bdata));

					} else if (data[1].equals("custtype")) {
						// reply = msgGtwService.getRgtw().getCustType(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));

						String sreply = msgGtwService
								.getRgtw()
								.queryDataNotCompress(SQUERY.custtype.val, null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("exchange")) {
						// reply = msgGtwService.getRgtw().getExchange(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService
								.getRgtw()
								.queryDataNotCompress(SQUERY.exchange.val, null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("invtype")) {
						// reply = msgGtwService.getRgtw().getInvType(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.invtype.val, null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equalsIgnoreCase("getAts")) {

						String userid = rmises.get_uid();
						FOData fodata = new FOData();
						fodata.set_string(userid, data[2], data[3]);

						String sreply = msgGtwService.getRgtw().queryDataNotCompress(SQUERY.atslist.val,fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equalsIgnoreCase("getAtsBrowse")) {

						String userid = rmises.get_uid();
						FOData fodata = new FOData();
						fodata.set_string(userid);

						String sreply = msgGtwService.getRgtw().queryDataNotCompress(SQUERY.browseorder.val,fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equalsIgnoreCase("ordermatrix")) {
						log.info("execute ordermatrix");
						String userid = rmises.get_uid();
						FOData fodata = new FOData();
						fodata.set_string(data[2]);

						String sreply = msgGtwService.getRgtw().queryDataNotCompress(SQUERY.ordermatrix.val,fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					}  else if (data[1].equalsIgnoreCase("ordermatrixlist")) {
						log.info("execute ordermatrixlist");
						String userid = rmises.get_uid();
						FOData fodata = new FOData();
						fodata.set_string(data[2],data[3]);

						String sreply = msgGtwService.getRgtw().queryDataNotCompress(SQUERY.ordermatrixlist.val,fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equalsIgnoreCase("announcement")) {

						String userid = rmises.get_uid();
						FOData fodata = new FOData();
						fodata.set_string(userid);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.announcement.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("matchorder")) {
						// reply = msgGtwService.getRgtw().getMatchOrder(
						// sesid.longValue(), data[2]);
						// response = new String(Function.decompress(bdata));

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.matchlist.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("negdeal")) {
						// reply = msgGtwService.getRgtw().getNegDeal(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.negdeallist.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("order")) {
						// reply = msgGtwService.getRgtw().getOrder(
						// sesid.longValue(), data[2], data[3]);
						// response = new String(Function.decompress(bdata));
						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2], data[3], data[4]);
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.orderlist.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());
						
					} else if (data[1].equalsIgnoreCase("GTC")) {
						// reply = msgGtwService.getRgtw().getOrder(
						// sesid.longValue(), data[2], data[3]);
						// response = new String(Function.decompress(bdata));
						FOData fodata = new FOData();
						String userid = rmises.get_uid();
						fodata.set_string(userid);
						fodata.set_string(rmises.get_uid());
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.gtclist.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("portfolio")) {
						// reply = msgGtwService.getRgtw().getPortfolio(
						// sesid.longValue(), data[2], data[3]);
						// response = new String(Function.decompress(bdata));

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.portfolio.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("securities")) {
						// reply = msgGtwService.getRgtw().getSecurities(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.securities.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("servertime")) {
						long time = msgGtwService.getRgtw().getServerTime(
								sesid.longValue());
						response = data[1] + "|" + time + "";

						reply = Function.compress(response.getBytes());

						log.info("response for time: " + response + ":"
								+ reply.length);

					} else if (data[1].equals("statusacc")) {
						// reply = msgGtwService.getRgtw().getStatusAcc(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.statusacc.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("statusorder")) {
						// reply = msgGtwService.getRgtw().getStatusOrder(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.orderStatus.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("userprofile")) {
						// reply = msgGtwService.getRgtw().getUserProfile(
						// sesid.longValue(), data[2]);
						// response = new String(Function.decompress(bdata));

						if (!rmises.get_uid().toUpperCase()
								.equals(data[2].toUpperCase()))
							throw new RemoteException("user id is not login");

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid());

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.userProfile.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("usertype")) {
						// reply = msgGtwService.getRgtw().getUserType(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.userttype.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("broker")) {
						// reply = msgGtwService.getRgtw().getBroker(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.broker.val, null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("cashcoll")) {
						// reply = msgGtwService.getRgtw().getCashColl(
						// sesid.longValue(), data[2]);
						// response = new String(Function.decompress(bdata));

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.cashcoll.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("secexchboard")) {
						// reply = msgGtwService.getRgtw().getSecExchBoard(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.secexchboard.val,
										null);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equalsIgnoreCase("overlimit")) {// yosep overlimit
						FOData fodata = new FOData();
						String userid = rmises.get_uid();
						fodata.set_string(rmises.get_uid());
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.overlimit.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("changepin")) {
						boolean ischange = msgGtwService.getRgtw().changepin(
								sesid.longValue(), data[2], data[3]);
						response = data[1] + "|" + (ischange ? "ok" : "gagal");
						reply = Function.compress(response.getBytes());
					} else if (data[1].equals("checkpin")) {
						boolean ischeck = msgGtwService.getRgtw().checkpin(
								sesid.longValue(), data[2], data[3]);
						response = data[1] + "|" + (ischeck ? "ok" : "gagal");
						reply = Function.compress(response.getBytes());
					} else if (data[1].equals("checkpinnego")) {
						boolean ischeck = msgGtwService.getRgtw().checkpinNEGO(
								sesid.longValue(), data[2]);
						response = data[1] + "|" + (ischeck ? "ok" : "gagal");
						reply = Function.compress(response.getBytes());
					} else if (data[1].equals("entrysplitdone")) {
						log.info("entry splitdone " + data[2]);
						byte[] breply = msgGtwService.getRgtw().entrySplitDone(
								sesid.longValue(),
								Function.compress(data[2].getBytes()));

						response = new String(Function.decompress(breply));
						reply = Function.compress(response.getBytes());
					} else if (data[1].equals("notification")) {
						boolean result = msgGtwService.getRgtw().send_fixmsg(
								sesid.longValue(), data[2]);
						response = new String(data[1] + "|" + result);
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("duedate")) {

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.duedate.val,
										fodata);
						response = new String(data[1] + "|" + sreply);
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("backnotif")) {

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid());

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.backnotif.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

						// reply = msgGtwService.getRgtw().getBackNotif(
						// sesid.longValue());

					} else if (data[1].equals("logout")) {
						// Connector conn = ((MessageSocketManager) mmi)
						// .getConnector(sesid.intValue());
						// if (conn != null) {
						// conn.sendMsgAndWait("logout|ok");
						// }

						boolean islogout = msgGtwService.getRgtw().logout(
								sesid.longValue());
						// response = islogout ? "ok" : "gagal";
						response = "logout|ok";

						reply = Function.compress(response.getBytes());

						ClientGtw gt = getDataClientBySesid().remove(sesid);
						log.info("logout from client " + sesid + " "
								+ (gt != null));

					} else if (data[1].equals("heartbeat")) {
						RMISessionCounter rsc = msgGtwService.getRgtw()
								.getHsession().get(sesid);
						// .info("heartbeat "+sesid+" "+rsc);
						if (rsc == null) {
							rsc = msgGtwService.getRgtw().getHtmp()
									.remove(sesid);
						} else {
							rsc.reset_counter();
						}
					} else if (data[1].equals("ipaddress")) {
						/*
						 * Connector conn = ((MessageSocketManager)
						 * mmi).getConnector(sesid.intValue()); if(conn!= null){
						 * response = conn.getIPAddress(); }
						 */

						response = data[1] + "|" + clientSocket.getIPAddress();

						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("buyselldetail")) {

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid(), data[2], data[3]);

						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.buySellDetail.val,
										fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("securitiespreop")) {
						// reply = msgGtwService.getRgtw().getSecurities(
						// sesid.longValue());
						// response = new String(Function.decompress(bdata));
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.securitiespreop.val,
										null);

						response = data[1] + "|" + sreply;
						log.info( data[1] + "|" + sreply);
						reply = Function.compress(response.getBytes());

					}  else if (data[1].equals("filter.client")) {
						log.info("filter.client:" + msg);
						String type = data[2].trim();
						ClientGtw cl = getDataClientBySesid().get(
								sesid.intValue());

						if (type.equals("add")) {
							String saccid = data[3].trim();
							cl.appendFilter(saccid);
						} else if (type.equals("delete")) {
							String saccid = data[3].trim();
							cl.deleteFilter(saccid);
						} else if (type.equals("setFilter")) {
							String saccid = data[3].trim();
							Boolean bol = Boolean.parseBoolean(saccid);
							cl.setFilter(bol);
						} 
					} else if (data[1].equals("filter.client.load")) {

						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid().toUpperCase());
						String sreply = msgGtwService.getRgtw()
								.queryDataNotCompress(SQUERY.userlist.val,
										fodata);
						response = data[1] + "|" + sreply;
						//log.info("response filter client load "+response);
						reply = Function.compress(response.getBytes());

					} else if (data[1].equals("filter.client.save")) {
						
						log.info("save filter "+msg);
						String sfilter = "";
						
						if(data.length>=3){
							sfilter = data[2].trim();
						}
						
						
						FOData fodata = new FOData();
						fodata.set_string(rmises.get_uid().toUpperCase(),
								sfilter);
						
						Vector<Object> v = msgGtwService.getAccessData().getQueryData().set_insert(SQUERY.saveuserfilter.val, fodata);

						String sreply = "0";
						if(v.size()>0)
							sreply =(String) v.elementAt(0);
						
						//String sreply = msgGtwService.getRgtw()
							//	.queryDataNotCompress(
								//		SQUERY.saveuserfilter.val, fodata);

						response = data[1] + "|" + sreply;
						reply = Function.compress(response.getBytes());
						
					} else if (data[1].equals("filter.order")) {
						String orderid = data[3].trim();
						String type = data[2].trim();
						ClientGtw cl = getDataClientBySesid().get(
								sesid.intValue());
						log.info("filter.order:" + msg);
						if (type.equals("add")) {
							cl.appendFilterOrder(orderid);
						} else if (type.equals("delete")) {
							cl.deleteFilterOrder(orderid);
						}

					} else {

						log.info("msg receive is not found " + msg);
						reply = Function.compress((data[1] + "|error")
								.getBytes());
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			Function.logger_info_exception(e);

			if (data[0].equals("login")) {
				response = "login|-1|error : " + e.getMessage();
			} else {
				response = data[1] + "|error|" + e.getMessage();
			}
			reply = Function.compress(response.getBytes());

		}

		if (reply != null) {
			// log.info("reply size for " + data[0] + " " + reply.length);
			clientSocket.sendMessage(reply);
		}

	}

	public boolean filterByAccount(ClientSocket sock, String sfilter,
			String suid, Message fixmsg) {

		String accid = fixmsg.getString(tudAccountID.FIELD);
		if (sfilter.contains(accid)) {
			return true;
		}

		return false;
	}

	/*
	 * private void deleteFilter(String saccid, Long sesid) { String sfilter =
	 * getDataFilterClientBySessid().get(sesid.intValue()); if (sfilter != null
	 * && !sfilter.isEmpty()) { sfilter = sfilter.replace(saccid, ""); sfilter =
	 * sfilter.replace("||", "|"); }
	 * 
	 * getDataFilterClientBySessid().put(sesid.intValue(), sfilter); }
	 * 
	 * private void appendFilter(String saccid, Long sesid) { String sfilter =
	 * getDataFilterClientBySessid().get(sesid.intValue()); if (sfilter == null
	 * || sfilter.isEmpty()) { sfilter = saccid; } else { // StringBuffer sbf =
	 * new StringBuffer(); // sbf.append(sfilter).append("|").append(saccid);
	 * sfilter = sfilter + "|" + saccid;// sbf.toString(); // sfilter =
	 * sbf.toString(); } // sfilter.replace("||", "|");
	 * getDataFilterClientBySessid().put(sesid.intValue(), sfilter);
	 * 
	 * }
	 */

	private boolean isFIXProtocol(String data) {
		if (data.contains("35=") && data.contains("8="))
			return true;

		return false;
	}

	@Override
	public void disconnect(ClientSocket sock) {
		RMISessionCounter rmises = msgGtwService.getRgtw().getHsession()
				.get(new Long(sock.getID()));
		if (rmises != null) {
			log.info("client closed " + rmises.get_uid());
			// delete_session(rmises.get_uid(), true);

			msgGtwService.delete_session(rmises.get_uid(), true);
		}
	}

	public void deleteDataClientByUser(String user) {
		ClientGtw sock = dataClientByUser.remove(user);
		if (sock != null && sock.getClientSocket().isConnected()) {
			dataClientBySesid.remove(sock.getClientSocket().getID());
			sock.getClientSocket().close();
		}
	}

	public Hashtable<String, ClientGtw> getDataClientByUser() {
		return dataClientByUser;
	}

	public Hashtable<Integer, ClientGtw> getDataClientBySesid() {
		return dataClientBySesid;
	}

	/*
	 * public Hashtable<Integer, String> getDataFilterClientBySessid() { return
	 * dataFilterClientBySessid; }
	 */
	public static void main(String[] args) {
		/*
		 * SocketClientReceiverImpl sc = new SocketClientReceiverImpl(null); int
		 * saccid = 1000000; long start = System.nanoTime(); for (int i = 0; i <
		 * 10000; i++) { saccid++; sc.appendFilter(saccid + "", 1L); } long end
		 * = System.nanoTime(); System.out.println((end - start)); String
		 * sfilter = sc.getDataFilterClientBySessid().get( new
		 * Long(1).intValue()); System.out.println("apended data " +
		 * sfilter.getBytes().length);
		 * 
		 * start = System.nanoTime();
		 * 
		 * sc.deleteFilter("1002582", 1L);
		 * 
		 * end = System.nanoTime(); System.out.println("del time " + (end -
		 * start));
		 * 
		 * sfilter = sc.getDataFilterClientBySessid().get(new
		 * Long(1).intValue()); System.out.println("delete " +
		 * sfilter.getBytes().length);
		 */
		Boolean bol = Boolean.parseBoolean("true");
		System.out.println(bol);
	}

}
