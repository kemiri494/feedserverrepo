package com.eqtrade.gtw.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.gtw.MessageGtwService;
import com.eqtrade.gtw.MessageRMIManager;
import com.eqtrade.gtw.rmi.RMIGtwServiceUpdate;
import com.eqtrade.utils.properties.FEProperties;

public class MessageGtwServiceSocket extends MessageGtwService {
	private SocketInterface socketConnector;
	private SocketClientReceiverImpl socketClientReceiver;
	private Logger log = LoggerFactory.getLogger(getClass());

	public MessageGtwServiceSocket(String service, String ipsvr, int nport,
			int nheartbt, int nportrmi, int nportlocalrmi, String srmuid,
			String srmidatsvr, String srmtype, String sgtwuid,
			String sgtwidatsvr, String sgtwtype, int nsleep, int nmaxidle,
			int amaxsession, String surl, String sdriver, String suid,
			String spwd, FEProperties fep, EventDispatcher ievent,
			String atsidatsvr, String atstype, String atsuid) throws Exception {
		super(service, ipsvr, nport, nheartbt, nportrmi, nportlocalrmi, srmuid,
				srmidatsvr, srmtype, sgtwuid, sgtwidatsvr, sgtwtype, nsleep,
				nmaxidle, amaxsession, surl, sdriver, suid, spwd, fep, ievent,
				atsidatsvr, atstype, atsuid);
		initSocket();
	}

	@Override
	protected void init_rmiserver(int nportrmi, int nportlocal, String service,
			int nsleep, int nmaxidle) throws Exception {
		
		System.out.println("init rmiserver socket");

		rgtw = new RMIGtwServiceUpdate(nportlocal, nsleep, nmaxidle, vreject,
				mm, vrmsession, thsession, this.accessData, this, this.muid);
	}

	public void initSocket() throws Exception {
		log.info("init socket client receiver");
		socketClientReceiver = new SocketClientReceiverImpl(this);
		String sfileconf = fep.get("socketconf");
		socketConnector = SocketFactory.createSocket(sfileconf,
				socketClientReceiver);
		boolean result = socketConnector.start();
		log.info("socket receiver is started "+result);
//		console = new Console(this);
//		console.start();
	}
	Console console ;

	public SocketClientReceiverImpl getSocketClientReceiver() {
		return socketClientReceiver;
	}

	public SocketInterface getSocketConnector() {
		return socketConnector;
	}

	@Override
	public boolean delete_session(String suid, boolean issendtorm) {
		boolean result = super.delete_session(suid, issendtorm);

		socketClientReceiver.deleteDataClientByUser(suid);

		return result;
	}
	
	

	@Override
	protected void init_rmigateway() throws Exception {
		mm = new MessageRMISocketManager(this, this.muid, this.accessData);
		mm.start();
	}

	@Override
	public boolean delete_session(String suid, String sessiongtw) {
		
		boolean isresult = super.delete_session(suid, sessiongtw);

		
		//Long sesid = new Long(sessiongtw);
		//socketClientReceiver.deleteDataClientByUser(suid);
		//socketClientReceiver.getDataClientBySesid().remove(sesid);
		
		return isresult;		
		
	}
	
	

}
