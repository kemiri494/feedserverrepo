package com.eqtrade.gtw.socket;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.PropertyConfigurator;

import com.digitprop.tonic.TonicLookAndFeel;
import com.eqtrade.CommandInterface;
import com.eqtrade.Console;
import com.eqtrade.dx.MainApp;
import com.eqtrade.dx.MainWindow;
import com.eqtrade.dx.MonitoringClientMenuAction;
import com.eqtrade.quickfix.resources.Message;
import com.eqtrade.quickfix.resources.TestRequest;
import com.eqtrade.quickfix.resources.field.Text;
import com.eqtrade.quickfix.resources.field.tudDestIDAtsvr;
import com.eqtrade.quickfix.resources.field.tudDestUID;
import com.eqtrade.quickfix.resources.field.tudDestUType;
import com.eqtrade.utils.properties.FEProperties;

public class GtwGuiMain implements CommandInterface {
	private MessageGtwServiceSocket gtwService;
	private MainWindow clientWindow;
	private String service;
	private FEProperties fep;

	public GtwGuiMain(FEProperties fep) throws Exception {

		this.service = service;
		this.fep = fep;

	}

	public void start_app() throws Exception {
		
		String srmuid = fep.getProperty("rm.uid");
		String srmidatsvr = fep.getProperty("rm.idatsvr");
		String srmtype = fep.getProperty("rm.type");

		String sgtwuid = fep.getProperty("gtw.uid");
		String sgtwidatsvr = fep.getProperty("gtw.idatsvr");
		String sgtwtype = fep.getProperty("gtw.type");

		String ssessionsleep = fep.getProperty("session.sleep");
		String ssessionmaxidle = fep.getProperty("session.nmaxidle");
		String smaxsession = fep.getProperty("session.nmaxuser");

		int nsleep = new Integer(ssessionsleep);
		int nmaxidle = new Integer(ssessionmaxidle);
		int nmaxsession = new Integer(smaxsession);

		String service = fep.getProperty("-service");
		String sportrmi = fep.getProperty("-portrmi");
		String sportlocalrmi = fep.getProperty("-portlocalrmi");
		String sport = fep.getProperty("-port");
		String ssvr = fep.getProperty("-server");
		String sheartbt = fep.getProperty("-heartbeat", "10");

		Integer nportrmi = new Integer(sportrmi);
		Integer nportlocalrmi = new Integer(sportlocalrmi);
		Integer nport = new Integer(sport);
		Integer nheart = new Integer(sheartbt);

		// for db - properties
		String surl = fep.getProperty("-url");
		String sdriver = fep.getProperty("-driver");
		String suid = fep.getProperty("-uid");
		String spwd = fep.getProperty("-pwd");
		String title = fep.getProperty("-title", "Gateway");
		String version = fep.getProperty("-version", "1.0.0");
		String copyright = fep.getProperty("-copyright","Innovasi Andalan Mandiri");

		String atsidatsvr = fep.getProperty("ats.idatsvr");
		String atstype = fep.getProperty("ats.type");
		String atsuid = fep.getProperty("ats.uid");

		MonitoringClientMenuAction menuAction = new MonitoringClientMenuAction();
		gtwService = new MessageGtwServiceSocket(service, ssvr, nport,
				Integer.parseInt(fep.get("heartbeat")), nportrmi,
				nportlocalrmi, srmuid, srmidatsvr, srmtype, sgtwuid,
				sgtwidatsvr, sgtwtype, nsleep, nmaxidle, nmaxsession, surl,
				sdriver, suid, spwd, fep, menuAction, atsuid, atsidatsvr,
				atstype);

		try {
			MainApp mainGUI = new MainApp(title, version, copyright, menuAction);
		} catch (Exception e) {
			e.printStackTrace();
		}
		gtwService.start();

		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				gtwService.stop();
			}

		});

		Console console = new Console(this);
		console.start();
		System.out.println("console started");

	}

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(new TonicLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		FEProperties fep = new FEProperties("gtw.properties");
		PropertyConfigurator.configure("log.properties");

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {

			GtwGuiMain gtwMain = new GtwGuiMain(fep);

			gtwMain.start_app();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean runCommand(String scmd) {
		if (scmd.startsWith("status")) {
			System.out.println("is connected to server routing:"
					+ gtwService.getEngine().isConnected());
			System.out.println("is connected to server client connected:"
					+ gtwService.getSocketConnector().isConnected());
		} else if (scmd.startsWith("send_tosvr")) {
			String[] sp = scmd.split(" ");
			int countmsg = Integer.parseInt(sp[1]);
			int lengthmsg = Integer.parseInt(sp[2]);
			String[] dest = sp[3].split("#");

			StringBuffer sbf = new StringBuffer();
			for (int i = 0; i < lengthmsg; i++)
				sbf.append("a");

			String msg = sbf.toString();
			Message fmsg = new TestRequest();
			fmsg.setString(Text.FIELD, msg);
			// String sfmsg = fmsg.toString();
			fmsg.setString(tudDestUID.FIELD, dest[0]);
			fmsg.setString(tudDestIDAtsvr.FIELD, dest[1]);
			fmsg.setString(tudDestUType.FIELD, dest[2]);

			for (int i = 0; i < countmsg; i++)
				gtwService.getEngine().send_appmsg("",(Message) fmsg.clone());
			
			return true;
		}

		return false;
	}
}
