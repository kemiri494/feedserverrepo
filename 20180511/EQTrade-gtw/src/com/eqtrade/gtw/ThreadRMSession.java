package com.eqtrade.gtw;

import static com.eqtrade.utils.Function.print_pair;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.net.ClientManager;

public final class ThreadRMSession extends Object {
	private Logger log = LoggerFactory.getLogger(super.getClass());
	private String suid, sidatsvr, stype;
	private MessageGtwServiceInterface mgi;

	public ThreadRMSession(String asuid, String asidatsvr, String astype,
			MessageGtwServiceInterface amgi) {

		this.suid = asuid;
		this.sidatsvr = asidatsvr;
		this.stype = astype;
		this.mgi = amgi;
	}

	public void ask_sessionlist() {

		String sessionid =mgi.getEngine().getCm()
				.get_sessionid(suid, sidatsvr, stype);
		
		log.info("ask_sessionlist:" + sessionid+":"+suid+":"+sidatsvr+":"+stype);
		if (sessionid != null) {

			this.mgi.do_fixmsg("getses", suid, sidatsvr, stype, sessionid);

		}
	}

	public boolean cleaning_sessionto_rm(String sclientid) {
		boolean bresult = false;

		try {

			//String sessionid = ClientManager.cm.get_sessionid(suid, sidatsvr,
				//	stype);
			String sessionid = mgi.getEngine().getSessionID();
			if (sessionid != null) {

				this.mgi.do_fixmsg("cleanses-" + sclientid, suid, sidatsvr,
						stype, sessionid);
				log.info("clean session:"+sclientid);
			}

			bresult = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return bresult;
	}
}