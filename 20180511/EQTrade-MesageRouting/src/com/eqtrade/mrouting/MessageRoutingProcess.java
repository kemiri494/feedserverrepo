package com.eqtrade.mrouting;

import static com.eqtrade.utils.Function.get_data;
import static com.eqtrade.utils.Function.logger_info_exception;
import static com.eqtrade.utils.Function.print_pair;

import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import org.slf4j.LoggerFactory;

import quickfix.FieldNotFound;
import com.eqtrade.quickfix.resources.Message;

import com.eqtrade.net.ClientManager;
import com.eqtrade.net.CommServerHandling;
import com.eqtrade.net.ThirdPartyInterface;
import com.eqtrade.net.data.HeaderAndData;
import com.eqtrade.quickfix.resources.InfoSession;
import com.eqtrade.quickfix.resources.Logon;
import com.eqtrade.quickfix.resources.Logout;
import com.eqtrade.quickfix.resources.field.*;
import com.eqtrade.utils.Function;
import com.eqtrade.utils.FixMessageUtils.MyState;
import com.eqtrade.utils.properties.FEProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageRoutingProcess.
 */
public class MessageRoutingProcess implements ThirdPartyInterface {

	/** The logger. */
	private org.slf4j.Logger logger = LoggerFactory
			.getLogger(MessageRoutingProcess.class);

	/** The cm. */
	private CommServerHandling cm;
	private Hashtable<String, Vector<Message>> hpendingmsg = new Hashtable<String, Vector<Message>>();

	/**
	 * Instantiates a new message routing process.
	 * 
	 * @param acm
	 *            the acm
	 * @param fileProps
	 *            the file props
	 */
	public MessageRoutingProcess(CommServerHandling acm, FEProperties fileProps) {

		super();
		this.cm = acm;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.eqtrade.net.ThirdPartyInterface#process_dat(java.lang.String,
	 * com.eqtrade.quickfix.resources.Message)
	 */
	@Override
	public void process_dat(String sessionid, Message msg) {

		String sheader = msg.getOverridedHeader().getString(MsgType.FIELD);
		String smsg = msg.toString();

		try {

			logger.info("mrp:process_dat:" + smsg);
			if (sheader.equals(Logon.MSGTYPE)) {

				process_logon(sessionid, (Logon) msg);
			} else if (sheader.equals(Logout.MSGTYPE)) {
				process_logout(sessionid, msg);
			} else if (sheader.equals(MsgType.hedBroadcastMessage)) {
				process_broadcasttogw(sessionid, msg);
			} else {
				process_others(sessionid, msg);
			}
		} catch (FieldNotFound e) {
			// TODO Auto-generated catch block
			// print_pair("get_error:" + sessionid + ":" + msg);
			e.printStackTrace();
		}
	}

	/**
	 * process broadcast message ke gateway. semua gateway akan dikirim message
	 * ini.
	 * 
	 * @param sessionid
	 *            the sessionid
	 * @param msg
	 *            the msg
	 */
	private void process_broadcasttogw(String sessionid, Message msg) {
		// TODO Auto-generated method stub
		logger.info("broadcast message:"+msg.toString());
		cm.send_appmsg(null, null, "gtw", msg, sessionid);

	}

	/**
	 * process meroutingkan message berdasarkan suid,sidatsvr,dan sutype. jika
	 * identity tersebut null maka akan dikirim ke semua client.
	 * 
	 * @param sessionid
	 *            the sessionid
	 * @param msg
	 *            the msg
	 */
	private void process_others(String sessionid, Message msg) {
		//logger.info("process others");
		String suid = null;
		String sidatsvr = null;
		String sutype = null;

		if (msg.isSetField(tudDestUID.FIELD))
			suid = msg.getString(tudDestUID.FIELD);

		if (msg.isSetField(tudDestIDAtsvr.FIELD))
			sidatsvr = msg.getString(tudDestIDAtsvr.FIELD);

		if (msg.isSetField(tudDestUType.FIELD))
			sutype = msg.getString(tudDestUType.FIELD);


		boolean brouting = cm.send_appmsg(suid, sidatsvr, sutype,
				msg, sessionid);
		
		logger.info("routing message:" + suid + ":" + sidatsvr + ":" + sutype+":sessionid:"+sessionid+":is send:"+brouting);
	/*	
		if(!brouting) {
			String key = suid+"#"+sidatsvr+"#"+sutype;
			Vector<Message> vpen = hpendingmsg.get(key);
			
			if(vpen == null) {
				vpen = new Vector<Message>();
				hpendingmsg.put(key, vpen);
			}
			vpen.add(msg);			
		}*/


	}

	/** The bstop. */
	private boolean bstop = false;

	/**
	 * Process_logon.
	 * 
	 * @param sessionid
	 *            the sessionid
	 * @param logon
	 *            the logon
	 * @throws FieldNotFound
	 *             the field not found
	 */
	private void process_logon(String sessionid, Logon logon)
			throws FieldNotFound {
		//logger.info("process logon");


		//print_pair("process MR:" + sessionid);

		InfoSession mesInfosess = new InfoSession(new tudStateSession(
				tudStateSession.LOGON));

		boolean bbroadcast = this.cm.send_appmsg(null, null, null, mesInfosess,
				sessionid);

		//print_pair("mrp:broadcast:logon:" + mesInfosess);
		
/*		String uid = logon.getString(tudUserID.FIELD);
		String utype = logon.getString(tudUserType.FIELD);
		String idatsvr = logon.getString(tudIdatsvr.FIELD);
		String key = uid+"#"+idatsvr+"#"+utype;
		
		Vector<Message> vm = hpendingmsg.get(key);
		if(vm != null) {
			Vector<Message> vremove = new Vector<Message>();
			for(Message mm : vm) {
				boolean brouting = ClientManager.cm.send_appmsg(uid, idatsvr, utype,
						mm, "");
				
				if(brouting) {
					vremove.add(mm);
				}
			}
			vm.removeAll(vremove);
			hpendingmsg.put(key, vm);
			//logger.info(hpendingmsg.toString());
		}*/

	}

	/**
	 * Process_logout.
	 * 
	 * @param sessionid
	 *            the sessionid
	 * @param msg
	 *            the msg
	 * @throws FieldNotFound
	 *             the field not found
	 */
	private void process_logout(String sessionid, Message msg)
			throws FieldNotFound {
		logger.info("process logout");

		

	}

}
