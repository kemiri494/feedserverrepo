package com.eqtrade.mrouting;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.PropertyConfigurator;

import com.digitprop.tonic.TonicLookAndFeel;
import com.eqtrade.dx.CommListener;
import com.eqtrade.dx.DefaultServerMenuAction;
import com.eqtrade.dx.EventDispatcher;
import com.eqtrade.dx.MainApp;
import com.eqtrade.net.CommServerHandling;
import com.eqtrade.quickfix.resources.MessageFactory;
import com.eqtrade.utils.properties.FEProperties;

public class MessageRoutingService implements CommListener {

	// private Tokn token;
	private CommServerHandling comServerHandling;
	private int minpoolsize, maxpoolsize, keepalivetime, socketThread;
	

	// public static Logger logger = LoggerFactory
	// .getLogger(MessageRoutingService.class);

	// String aortuid, aortidatsvr, aorttype, aspcuid, aspcidatsvr, aspctype;
	int port, nheartbt;
	private FEProperties fep;

	public MessageRoutingService(int port, int nheartbt, FEProperties _fep,
			String service) {
		this.port = port;
		this.nheartbt = nheartbt;
		this.fep = _fep;

		init_networking(port, nheartbt, service, fep);

	}

	private MessageRoutingProcess MR;
	private EventDispatcher eventDispatcher;

	private void init_networking(int port, int nheartbt, String service,
			FEProperties fep) {

		final String title = fep.getProperty("-title", "Message Routing");
		final String version = fep.getProperty("-version", "1.0.0");
		final String copyright = fep.getProperty("-copyright",
				"Innovasi Andalan Mandiri");
		
		this.minpoolsize = Integer.parseInt(fep
				.getProperty("minpoolsize", "20"));
		this.maxpoolsize = Integer.parseInt(fep
				.getProperty("maxpoolsize", "50"));
		this.socketThread = Integer.parseInt(fep.getProperty("threadSocket",
				"50"));
		this.keepalivetime = Integer.parseInt(fep.getProperty("keepalivetime",
				"20"));
		
		DefaultServerMenuAction serverMenu = new DefaultServerMenuAction();

		serverMenu.registerListener(MessageRoutingService.this);

		MessageRoutingService.this.eventDispatcher = serverMenu;
		
		boolean isrunning = Boolean.parseBoolean(fep.getProperty("isprerunningthread","false"));


		comServerHandling = new CommServerHandling(
				MessageRoutingService.this.nheartbt, new MessageFactory(),
				this.minpoolsize, this.maxpoolsize, this.keepalivetime,
				eventDispatcher, socketThread,isrunning);

		MR = new MessageRoutingProcess(comServerHandling,
				MessageRoutingService.this.fep);
		comServerHandling.setThirdPartyInterface(MR);

		MainApp mainWindow = new MainApp(title, version, copyright,
				serverMenu);

	}

	private static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyMMdd HH:mm:ss");

	public static void main(String[] args) throws IOException {

		PropertyConfigurator.configure("log.properties");
		try {
			UIManager.setLookAndFeel(new TonicLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);

		FEProperties fep = new FEProperties("server.properties");

		final String service = fep.getProperty("service");
		String sportrmi = fep.getProperty("portrmi");
		String sportlocalrmi = fep.getProperty("-portlocalrmi");
		String sport = fep.getProperty("-port");
		String smaxmem = fep.getProperty("-maxmem", "100");
		String sheartbt = fep.getProperty("-heartbeat");

		// Integer nportrmi = new Integer(sportrmi);
		// Integer nportlocalrmi = new Integer(sportlocalrmi);
		Integer nport = new Integer(sport);
		// Integer nmaxmem = new Integer(smaxmem);
		Integer nheartbt = new Integer(sheartbt);

		sdf.setTimeZone(TimeZone.getDefault());
		// Function.print_pair("Local date : " + sdf.format(new Date()));

		MessageRoutingService main = new MessageRoutingService(nport, nheartbt,
				fep, service);

	}

	@Override
	public void setConfiguration(HashMap params) {
		// TODO Auto-generated method stub
		port = (Integer) params.get(0);
		comServerHandling.setPort(port);
		fep.setProperty("-port", port + "");
		fep.save();
	}

	@Override
	public void start(HashMap params) {
		// TODO Auto-generated method stub
		setConfiguration(params);
		if (comServerHandling.start(this.port)) {
			eventDispatcher.pushEvent(EventDispatcher.EVENT_CONNECTED, null);
		} else {
			eventDispatcher.pushEvent(EventDispatcher.EVENT_DISCONNECTED, null);

		}

	}

	@Override
	public void loadConfiguration() {
		// TODO Auto-generated method stub
		HashMap params = new HashMap();
		params.put(0, port);
		eventDispatcher.pushEvent(EventDispatcher.EVENT_LOAD_FINISH, params);
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		comServerHandling.stop();
	}
}
